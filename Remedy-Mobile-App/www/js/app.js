var app = angular.module('remedy', ['ionic','ionic.service.analytics', 'ionic-datepicker', 'ngStorage', 'ngCordova', 'ngSanitize', 'firebase', 'remedy.constants']);

function showpanel() {
    $('.splash').fadeOut();
}

$(document).on({
    'DOMNodeInserted': function() {
        $('.pac-item, .pac-item span', this).addClass('needsclick');
    }
}, '.pac-container');

app.run(function($ionicPlatform, $ionicAnalytics, $localStorage, $ionicPopup, promoCodeService, zipCodeService, $rootScope, PreviousState) {
    $rootScope.PreviousState = PreviousState;
    $ionicPlatform.ready(function() {
        setTimeout(showpanel, 0);
        // Added Google Analytics
        if (window.analytics !== undefined) {
            analytics.startTrackerWithId("UA-71024299-1");
        } else {
            console.log("Google Analytics Unavailable");
        }
        // Allow Keyboard scroll on DOM | Default settings given by cordova
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
    zipCodeService.getZipCode();
});
app.directive('input', function($parse) {
    return {
        restrict: 'E',
        require: '?ngModel',
        link: function(scope, element, attrs) {
            if (attrs.ngModel && attrs.value) {
                $parse(attrs.ngModel).assign(scope, attrs.value);
            }
        }
    };
});
