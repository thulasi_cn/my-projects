app
    .service('makeArraService', function() {
        function makeArray(obj) {
            var resultArray = [];
            for (var key in obj) {
                var a = obj[key];
                a.uid = key;
                resultArray.push(a);
            }
            return resultArray;
        }

        function findPromoUid(array, promoCode) {
            var result = [];
            for (var i = 0; i < array.length; i++) {
                var object = array[i];
                if (object.promo_codes == promoCode) {
                    result.push(object);
                }
            }
            return result;
        }

        function findReferralUid(array, referralCode) {
            var result = [];
            for (var i = 0; i < array.length; i++) {
                var object = array[i];
                if (object.referral_codes == referralCode) {
                    result.push(object);
                }
            }
            if (result.length > 0) {
                return result;
            } else {
                return false;
            }
        }

        function userMessageList(array, userUid) {
            var result = [];
            for (var i = 0; i < array.length; i++) {
                var object = array[i];
                if (object.usermsessage_to == userUid) {
                    result.push(object);
                }
            }
            return result;
        }
        return {
            makeArray: makeArray,
            findPromoUid: findPromoUid,
            findReferralUid: findReferralUid,
            userMessageList: userMessageList
        };
    })
    .service('timeValues', function() {
        var todayV = new Date();
        var monthNamesArray = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
        var daysNamesArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        // var tdayV = todayV.getDate();
        if (todayV.getDate() < 9) {
            tdayV = '0' + todayV.getDate();
        } else {
            tdayV = todayV.getDate();
        }
        if ((todayV.getMonth() + 1) < 9) {
            tmnthNum = '0' + (todayV.getMonth() + 1);
        } else {
            tmnthNum = todayV.getMonth() + 1;
        }
        tMondthV = monthNamesArray[todayV.getMonth()];
        tYearV = todayV.getFullYear();
        tFormattedDateV = tYearV + '/' + tmnthNum + '/' + tdayV;

        tomorrowV = new Date(new Date().setDate(new Date().getDate() + 1));
        if (tomorrowV.getDate() < 9) {
            tmdayV = '0' + tomorrowV.getDate();
        } else {
            tmdayV = tomorrowV.getDate();
        }
        if ((tomorrowV.getMonth() + 1) < 9) {
            tmrnthNum = '0' + (tomorrowV.getMonth() + 1);
        } else {
            tmrnthNum = tomorrowV.getMonth() + 1;
        }
        tmMondthV = monthNamesArray[tomorrowV.getMonth()];
        tmYearV = tomorrowV.getFullYear();
        tmFormattedDateV = tmYearV + '/' + tmrnthNum + '/' + tmdayV;

        function today() {
            return todayV;
        }

        function monthNames(mnth) {
            return monthNamesArray[mnth];
        }

        function daysNames(day) {
            return daysNamesArray[day];
        }

        function tday() {
            return tdayV;
        }

        function tMondth() {
            return tMondthV;
        }

        function tYear() {
            return tYearV;
        }

        function tFormattedDate() {
            return tFormattedDateV;
        }

        function tomorrow() {
            return tomorrowV;
        }

        function tmday() {
            return tmdayV;
        }

        function tmMondth() {
            return tmMondthV;
        }

        function tmYear() {
            return tmYearV;
        }

        function tmFormattedDate() {
            return tmFormattedDateV;
        }

        function formatAMPMfromTime(hour) {
            var apm = hour >= 12 ? 'p.m.' : 'a.m.';
            var hours = hour % 12;
            hours = hours ? hours : 12;
            var st = hours + ' ' + apm;
            return st;
        }

        function timeTo12HrFormat(time) {
            var time_part_array = time.split(":");
            var ampm = 'a.m.';
            if (time_part_array[0] >= 12) { ampm = 'p.m.'; }
            if (time_part_array[0] > 12) { time_part_array[0] = time_part_array[0] - 12; }
            formatted_time = time_part_array[0] + ':' + time_part_array[1] + ' ' + ampm;
            return formatted_time;
        }

        function getDuplicateValueTime(arr) {
            var a = [],
                b = [],
                prev;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] !== prev) {
                    a.push(arr[i]);
                    b.push(1);
                } else { b[b.length - 1]++; }
                prev = arr[i];
            }
            return [a, b];
        }

        function getGetOrdinal(n) {
            var s = ["th", "st", "nd", "rd"],
                v = n % 100;
            return n + (s[(v - 20) % 10] || s[v] || s[0]);
        }

        function getVisitTime(dateAndTime, visitType) {
            var fullTime = dateAndTime.split('|')[0],
                fullDate;

            if (dateAndTime.split('|')[1] == "today") {
                fullDate = new Date(tFormattedDateV);

            } else if (dateAndTime.split('|')[1] == "tomorrow") {
                fullDate = new Date(tmFormattedDateV);
            }
            if (visitType == "video_visit") {
                startTime = timeTo12HrFormat(fullTime.split('-')[0]);
                endTime = timeTo12HrFormat(fullTime.split('-')[1]);
            } else {
                startTime = formatAMPMfromTime(fullTime.split('-')[0]);
                endTime = formatAMPMfromTime(fullTime.split('-')[1]);
            }
            appoTime = startTime + ' - ' + endTime + ' ' + daysNamesArray[fullDate.getDay()] + ', ' + monthNamesArray[fullDate.getMonth()] + ' ' + fullDate.getDate() + ', ' + fullDate.getFullYear();

            return appoTime;
        }

        function convertVisitTime(dateAndTime, appointment_type) {
            var fullTime = dateAndTime.split('|')[0],
                fullDate = new Date(dateAndTime.split('|')[1]);
            if (appointment_type == "video_visit") {
                startTime = timeTo12HrFormat(fullTime.split('-')[0]);
                endTime = timeTo12HrFormat(fullTime.split('-')[1]);
            } else {
                startTime = formatAMPMfromTime(fullTime.split('-')[0]);
                endTime = formatAMPMfromTime(fullTime.split('-')[1]);
            }
            appoTime = startTime + ' - ' + endTime + ' ' + daysNamesArray[fullDate.getDay()] + ', ' + monthNamesArray[fullDate.getMonth()] + ' ' + fullDate.getDate() + ', ' + fullDate.getFullYear();

            return appoTime;
        }
        return {
            today: today,
            monthNames: monthNames,
            daysNames: daysNames,
            tday: tday,
            tMondth: tMondth,
            tYear: tYear,
            tFormattedDate: tFormattedDate,
            tomorrow: tomorrow,
            tmday: tmday,
            tmMondth: tmMondth,
            tmYear: tmYear,
            tmFormattedDate: tmFormattedDate,
            formatAMPMfromTime: formatAMPMfromTime,
            timeTo12HrFormat: timeTo12HrFormat,
            getDuplicateValueTime: getDuplicateValueTime,
            getGetOrdinal: getGetOrdinal,
            getVisitTime: getVisitTime,
            convertVisitTime: convertVisitTime
        };
    })
    .service('localNotificationService', function(makeArraService, $firebaseAuth) {
        var auth = $firebaseAuth();

        function activateLocalNotification() {
            if ('device' in window) {
                var loggedInUser = auth.$getAuth();
                cordova.plugins.notification.local.registerPermission(function(granted) {
                    console.log('notification registerPermission', granted);
                });
                cordova.plugins.notification.local.hasPermission(function(granted) {
                    console.log('notification hasPermission', granted);
                });
                firebase.database().ref('user_message').orderByChild("usermsessage_to").equalTo(loggedInUser.uid).on('value', function(messages) {
                    var msgCount = 0;
                    var userMessageList = makeArraService.makeArray(messages.val());
                    if (userMessageList.length > 0) {
                        for (var m = 0; m < userMessageList.length; m++) {
                            if (userMessageList[m].usermsessage_read_status == "false") {
                                msgCount++;
                            }
                        }
                        if (userMessageList[userMessageList.length - 1].usermsessage_read_status == "false") {
                            cordova.plugins.notification.local.schedule({
                                id: 1,
                                title: "Messages reminder",
                                message: 'You have' + msgCount + 'messages unread',
                                at: new Date(),
                                icon: "../img/logo.png"
                            });
                        }
                    }
                });
            }
        }
        return {
            activateLocalNotification: activateLocalNotification
        };
    });
