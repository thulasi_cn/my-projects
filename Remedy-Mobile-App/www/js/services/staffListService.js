app.service('staffListService', function(makeArraService) {
    function getStaffPh_nos(staffs) {
        var pStaffsList = makeArraService.makeArray(staffs);
        var phoneNumbers = [];
        for (var p = 0; p < pStaffsList.length; p++) {
            if (pStaffsList[p].staff_status == "true") {
                phoneNumbers.push(pStaffsList[p].staff_ph_no);
            }
        }
        return phoneNumbers;
    }

    function getStaffEmails(staffs) {
        var eStaffsList = makeArraService.makeArray(staffs);
        var emails = [];
        for (var p = 0; p < eStaffsList.length; p++) {
            if (eStaffsList[p].staff_status == "true") {
                emails.push(eStaffsList[p].staff_email);
            }
        }
        return emails;
    }
    return {
        getStaffPh_nos: getStaffPh_nos,
        getStaffEmails: getStaffEmails

    };

});
