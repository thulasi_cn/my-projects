app.service('appointmentService', function($firebaseArray, $localStorage) {
    function getAppointments() {
        var firebaseAppointment = firebase.database().ref('/appointments');
        var appointments = $firebaseArray(firebaseAppointment);
        return appointments;
    }

    function getSlots() {
        var firebaseSlots = firebase.database().ref('/slots');
        var slots = $firebaseArray(firebaseSlots);
        return slots;
    }


    return {
        getAppointments: getAppointments,
        getSlots: getSlots,
    };

});
