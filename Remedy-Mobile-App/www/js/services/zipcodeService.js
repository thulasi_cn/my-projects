app.service('zipCodeService', function($firebaseArray,$localStorage) {
    function getZipCode() {
	 var fireZipCode = firebase.database().ref('/zip_codes');
	 	var zipCode = $firebaseArray(fireZipCode);
		$localStorage.zipAPI = zipCode;
    }
  
    return {
        getZipCode: getZipCode,
    };
});
