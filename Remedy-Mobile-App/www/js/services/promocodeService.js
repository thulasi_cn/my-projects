app.service('promoCodeService', function($firebaseArray, $localStorage) {
    function getpromoCode() {
        var firePromoCode = firebase.database().ref('/promocodes');
        var promoCode = $firebaseArray(firePromoCode);
        $localStorage.promocodeAPI = promoCode;
         return promoCode;    	
    }


    return {
        getpromoCode: getpromoCode,
    };

});

