var tabIndex;
function userAppointmentFunction($scope, $ionicLoading, $firebaseAuth, makeArraService, timeValues, $ionicTabsDelegate, $timeout) {
    $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
    });
    if (tabIndex == 1) {
        $timeout(function() {
            tabIndex = '';
            angular.element('#return').triggerHandler('click');
        }, 0);
    }    
    $scope.returnTab = function() {
        $ionicTabsDelegate.select(1);
    };
    var auth = $firebaseAuth();
    var loggedInUser = auth.$getAuth();
    firebase.database().ref('/appointments').orderByChild("appointment_user").equalTo(loggedInUser.uid).on("value", function(snapshot) {$scope.userAppointmentsList = []; var myappointments = makeArraService.makeArray(snapshot.val()); for (var t = 0; t < myappointments.length; t++) {var appData = {}; appData.vicit_time = timeValues.convertVisitTime(myappointments[t].visit_time, myappointments[t].appointment_type); appData.uid = myappointments[t].uid; appData.app_type = myappointments[t].appointment_type; $scope.userAppointmentsList.push(appData);
        }
        $ionicLoading.hide();
    });
}

function userAppointmentDetailFunction($scope, $stateParams, $ionicLoading, $firebaseAuth, makeArraService, timeValues) {
    $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
    });
    var appointmentId = $stateParams.appointmentId;
    firebase.database().ref('/appointments/' + appointmentId).once('value', function(appointmentList) {
        $scope.appointment = appointmentList.val();
        $scope.vicit_timeD = timeValues.convertVisitTime($scope.appointment.visit_time, $scope.appointment.appointment_type);
        if ($scope.appointment.appointment_type == "video_visit") {tabIndex = 1; } else if ($scope.appointment.appointment_type == "appointment_visit") {tabIndex = 0; }
        $ionicLoading.hide();
    });

}
app.controller('userAppointmentCtrl', userAppointmentFunction);
app.controller('userAppointmentDetailCtrl', userAppointmentDetailFunction);
