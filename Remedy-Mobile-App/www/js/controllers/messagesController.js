function messageFunction($scope, $sce, $stateParams, $ionicLoading, $firebaseAuth, makeArraService, $state) {
    var auth = $firebaseAuth();
    var loggedInUser = auth.$getAuth();
    $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
    });
    $state.reload();
    $scope.userMessageList = [];
    firebase.database().ref('user_message').orderByChild("usermsessage_to").equalTo(loggedInUser.uid).on('value', function(messages) {
        $ionicLoading.hide();
        $scope.userMessageList = makeArraService.makeArray(messages.val());       
    });
}
function messageDetailFunction($scope, $sce, $ionicLoading, $stateParams, makeArraService) {
    $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
    });
    var messageId = $stateParams.messageId;
    firebase.database().ref('user_message/' + messageId).update({
        usermsessage_read_status: "true",
        updatedAt: Date.now()
    });
    firebase.database().ref('/user_message/' + messageId).on('value', function(messages) {
        $scope.message = messages.val();
        $scope.thisCanBeusedInsideNgBindHtml = $sce.trustAsHtml($scope.message.usermsessage_message_body);
        $ionicLoading.hide();
    });
}
app.controller('messageCtrl', messageFunction);
app.controller('messageDetailCtrl', messageDetailFunction);
