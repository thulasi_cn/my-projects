function locationFunction($scope, $ionicLoading, $ionicPopup, $localStorage, $state, $http, zipCodeService, $location, stateListService) {
    $scope.stateList = stateListService.getStateList();

    var geocoder = new google.maps.Geocoder();
    // Initialize Google Map
    function mapInitialize() {
        var lat = "40.703493";
        var long = "-74.260558";
        var myLatlng = new google.maps.LatLng(lat, long);

        navigator.geolocation.getCurrentPosition(onSuccess, onError, {
            enableHighAccuracy: false
        });

        function onSuccess(position) {
            myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var mapOptions = {
                center: myLatlng,
                zoom: 13,
                mapTypeControl: false,
                zoomControl: false,
                streetViewControl: false
            };
            var map = new google.maps.Map(document.getElementById("map"),
                mapOptions);
            var defaultICON = new google.maps.MarkerImage("img/tppin.png");
            var marker = new google.maps.Marker({
                position: myLatlng,
                title: 'Location',
                map: map,
                icon: defaultICON,
                draggable: false
            });
            updateMarkerPosition(myLatlng);
            geocodePosition(myLatlng);

            google.maps.event.addListener(marker, 'dragend', function() {
                updateMarkerPosition(marker.getPosition());
                geocodePosition(marker.getPosition());
            });

            google.maps.event.addListener(map, "dragend", function() {
                marker.setPosition(map.getCenter());
                updateMarkerPosition(marker.getPosition());
                geocodePosition(marker.getPosition());

            });

            google.maps.event.addListenerOnce(map, 'idle', function() {
                $ionicLoading.hide();
            });

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(input);

            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });
            var markers = [];
            // [START region_getplaces]
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }
                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
                var cords = map.getBounds().getCenter();
                marker.setPosition(map.getCenter());
                updateMarkerPosition(marker.getPosition());
                geocodePosition(marker.getPosition());

            });
            // [END region_getplaces]
            $scope.map = map;
        }

        function onError(data) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error',
                template: "Please try again later."
            });
        }

    }

    // Update marker status param @str
    function updateMarkerStatus(str) {
        document.getElementById('markerStatus').innerHTML = str;
    }

    // Update local markar position param @LatLng
    function updateMarkerPosition(latLng) {
        $localStorage.latitude = latLng.lat();
        $localStorage.longitude = latLng.lng();
    }

    // Update Marker Address text field param @str
    function updateMarkerAddress(str) {
        document.getElementById("pac-input").value = str;
    }

    // Update stored zip code in Localstorage
    function updateStoredZipCode(pos) {
        var zipURL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + pos.lat() + "," + pos.lng() + "&sensor=false";
        $http.get(zipURL).then(successCallback, errorCallback);

        function successCallback(data) {
            var postalSuccess = false;

            angular.forEach(data.data.results[0].address_components, function(value, index) {
                if (value.types == "postal_code") {
                    angular.forEach($localStorage.zipAPI, function(zip, index) {
                        if (zip.zip_code == value.long_name) {
                            postalSuccess = true;
                            $localStorage.zipGetFromAPI = value.long_name;
                        }
                    });
                    if (postalSuccess == false) {
                        angular.element('#confirmAddressMap').prop('disabled', true);
                        $ionicPopup.alert({
                            title: 'Coming Soon',
                            template: "The Remedy app is not yet available in " + value.long_name + ".<br/>Call us to see if we can accommodate you today.<br/><a href='tel:512-900-5844'>Click to Call Remedy</a>"
                        });
                    } else {
                        angular.element('#confirmAddressMap').prop('disabled', false);
                    }
                }
            });

            $ionicLoading.hide();
        }

        function errorCallback(error) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error',
                template: "Sorry we are unable to locate your postal code."
            });
        }
    }

    function geocodePosition(pos) {
        updateStoredZipCode(pos);
        geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
                updateMarkerAddress(responses[0].formatted_address);
            } else {
                updateMarkerAddress('Cannot determine address at this location.');
            }
        });
    }

    if ($location.path() == '/map') {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        ionic.Platform.ready(mapInitialize);
    }
    zipCodeService.getZipCode();
    $scope.showAlert = function(zipcodepass) {
        var alertPopup = $ionicPopup.alert({
            title: 'Coming Soon!',
            template: "The Remedy app is not yet available in " + zipcodepass + ".<br/>Call us to see if we can accommodate you today.<br/><a href='tel:512-900-5844'>Click to Call Remedy</a>"
        });
        alertPopup.then(function(res) {});
    };
    $scope.patientAddress = function(address, extra, checkType) {
        if(checkType != undefined && checkType == 'fromFrom'){
            $localStorage.addressType = 'addressFromForm';
        }
        var extraZipCode = (extra !== undefined) && extra.zip || $localStorage.zipGetFromAPI;
        var allZips = $localStorage.zipAPI;
        var foundZip = false;
        for (var i = 0; i < allZips.length; i++) {
            if (extraZipCode === allZips[i].zip_code) {
                foundZip = true;
                if (extra) {
                    $localStorage.extra = extra;
                }
                if (address) {
                    $localStorage.patientAddress = ((address !== undefined) && address + ', ' || '') + ((extra.address !== undefined) && extra.address + ', ' || '') + extra.city + ', ' + extra.state + ' ' + extra.zip;
                } else {
                    $localStorage.patientAddress = $('#pac-input').val();
                }
                if (window.analytics !== undefined) {
                    analytics.trackView("Patient Details");
                    analytics.trackEvent('Patient Details', 'Patient and contact information.');
                }
                $state.go('details');
            }
            if (i === (allZips.length - 1) && false === foundZip) {
                $scope.showAlert(extraZipCode);
            }
        }
    };

    $scope.verifyLocation = function(goWhere) {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        if (window.analytics !== undefined) {
            analytics.trackView("Verify Location auto Zip");
            analytics.trackEvent('VerifyLocation', 'Location Verification Auto Zip Generate');
        }

        navigator.geolocation.getCurrentPosition(onSuccess, onError, {
            enableHighAccuracy: false
        });

        function onSuccess(position) {
            var zipURL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + "," + position.coords.longitude + "&sensor=false";

            $http.get(zipURL).then(successCallback, errorCallback);

            function successCallback(data) {
                var postalSuccess = false;
                angular.forEach(data.data.results[0].address_components, function(value, index) {
                    if (value.types == "postal_code") {
                        postalSuccess = false;
                        $ionicLoading.hide();
                        $localStorage.errorZip = value.long_name;
                        $localStorage.zipGetFromAPI = value.long_name;
                        angular.forEach($localStorage.zipAPI, function(zip, index) {
                            if (zip.zip_code == value.long_name) {
                                postalSuccess = true;                                
                                $scope.openMap();                                        
                            }
                        });
                    }
                });

                $ionicLoading.hide();

                if (postalSuccess == false) {
                    $ionicPopup.alert({
                        title: 'Coming Soon',
                        template: "The Remedy app is not yet available in " + $localStorage.errorZip + ".<br/>Call us to see if we can accommodate you today.<br/><a href='tel:512-900-5844'>Click to Call Remedy</a>"
                    });
                } else if (goWhere === 'rootStage') {
                    $ionicPopup.alert({
                        title: 'Great News!',
                        template: "Remedy is currently<br/>available in your area!"
                    });
                }
            }

            function errorCallback(error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Error',
                    template: "Sorry we are unable to locate your postal code."
                });
            }
        }

        function onError(error) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error',
                template: "Sorry we are unable to locate your postal code."
            });
        }
    };
    $scope.openMap = function() {
        $localStorage.addressType = "addressFromMap";
        location.href = "#/map";
    };

};
app.controller('locationCtrl', locationFunction);
