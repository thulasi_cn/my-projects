function detailFunction($scope, $localStorage, $state, $ionicPopup, $firebaseAuth, $ionicLoading) {
    var auth = $firebaseAuth();
    var firebaseUser = firebase.database();
    var loggedInUser = auth.$getAuth();
    if ($localStorage.patientDetails !== undefined) {
        $scope.contact_email = $localStorage.patientDetails.contact_email;
        $scope.contact_phone = $localStorage.patientDetails.contact_phone;
    }
    $scope.responsiblePerson = false;
    $scope.resPeson = function(relation) {
        if (relation == "non-patient") {
            $scope.responsiblePerson = true;
        }
    };

    function validatedate(dob) {
        var pdate = dob.split('/');
        var mm = parseInt(pdate[0]);
        var dd = parseInt(pdate[1]);
        var yy = parseInt(pdate[2]);
        var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (mm == 1 || mm > 2) {
            if (dd > ListofDays[mm - 1]) {
                return false;
            }
        }
        if (mm == 2) {
            var lyear = false;
            if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                lyear = true;
            }
            if ((lyear === false) && (dd >= 29)) {
                return false;
            }
            if ((lyear === true) && (dd > 29)) {
                return false;
            }
        }

    }

    function patientDetails(details) {
        var d = details.dob.split('/');
        var date_of_birth;
        var curDate = Date.now();
        var oneYearAge = new Date().setDate(new Date().getDate() - 365);
        if ((!validatedate(details.dob)) && (validatedate(details.dob) !== false)) {
            date_of_birth = new Date(details.dob).getTime();
            if (date_of_birth < oneYearAge) {
                $localStorage.patientDetails = details;
                if (window.analytics !== undefined) {
                    analytics.trackView("Appointment Summary");
                    analytics.trackEvent('Appointment Summary', 'Appointment summary');
                }
                 $state.go('appointment-summary');
            } else {
                 $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Error',
                    template: "Patients must be at least one year old."
                });
            }
        } else {
             $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error',
                template: "Date of birth must be a valid date."
            });
        }

    }
    $scope.patientDetails = function(details) {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        patientDetails(details);
    };
}
app.controller('detailCtrl', detailFunction);
