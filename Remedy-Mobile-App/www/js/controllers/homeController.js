function homeFunction($scope, $ionicLoading, $ionicPopup, $localStorage, $state, $http, $ionicModal, $firebaseAuth, $window, zipCodeService) {
    zipCodeService.getZipCode();
    var auth = $firebaseAuth();
    if ($localStorage.visitTime || $localStorage.extra || $localStorage.patientAddress || $localStorage.patientDetails) {
        delete $localStorage.visitTime;
        delete $localStorage.extra;
        delete $localStorage.patientAddress;
        delete $localStorage.patientDetails;
        delete $localStorage.anonymous;
    }
    var chkUserLogin = function() {
        return $window.sessionStorage.getItem("isLoggedIn") || $window.sessionStorage.getItem("isRegistered");
    };
    $scope.showAlert = function(zipcodepass) {
        var alertPopup = $ionicPopup.alert({
            title: 'Coming Soon!',
            template: "The Remedy app is not yet available in " + zipcodepass + ".<br/>Call us to see if we can accommodate you today.<br/><a href='tel:512-900-5844'>Click to Call Remedy</a>"
        });
        alertPopup.then(function(res) {});
    };
    $scope.openVerification = function(zipcode) {
        var postalSuccess = false;
        angular.forEach($localStorage.zipAPI, function(zip, index) {
            if (zip.zip_code == zipcode) {
                postalSuccess = true;
                if (window.analytics !== undefined) {
                    analytics.trackView("verification");
                    analytics.trackEvent('Verification', 'Check Zip Code');
                }

            }
        });
        if (postalSuccess == true) {
            $ionicPopup.alert({
                title: 'Great News!',
                template: "Remedy is currently<br/>available your area!"
            }).then(function(res) {
                $state.go('verification');
            });
        } else {
            $scope.showAlert(zipcode);
        }

    };
    $scope.openAvailibity = function(user) {
            if (chkUserLogin() == "false") {
                auth.$signInAnonymously().then(function(user) {
                    if (user) {
                        var isAnonymous = user.isAnonymous;
                        var uid = user.uid;
                        $localStorage.visitType = "appointment_visit";
                        $state.go('intro');
                    }
                }, function() {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: "some problem to create anonymus user."
                    });
                });

            } else {
                $localStorage.visitType = "appointment_visit";
                if (window.analytics !== undefined) {
                    analytics.trackView("Check availibity.");
                }
                $state.go('intro');
            }
        }
        /*
        Get current location 
         */
    $scope.verifyLocation = function(goWhere) {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        if (window.analytics !== undefined) {
            analytics.trackView("Verify Location auto Zip");
            analytics.trackEvent('VerifyLocation', 'Location Verification Auto Zip Generate');
        }

        navigator.geolocation.getCurrentPosition(onSuccess, onError, {
            enableHighAccuracy: false
        });

        function onSuccess(position) {
            var zipURL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + "," + position.coords.longitude + "&sensor=false";

            $http.get(zipURL).then(successCallback, errorCallback);

            function successCallback(data) {
                var postalSuccess = false;
                angular.forEach(data.data.results[0].address_components, function(value, index) {
                    if (value.types == "postal_code") {
                        postalSuccess = false;
                        $ionicLoading.hide();
                        $localStorage.errorZip = value.long_name;
                        $localStorage.zipGetFromAPI = value.long_name;
                        angular.forEach($localStorage.zipAPI, function(zip, index) {
                            if (zip.zip_code == value.long_name) {
                                postalSuccess = true;
                                $ionicPopup.alert({
                                    title: 'Great News!',
                                    template: "Remedy is currently<br/>available in your area!"
                                }).then(function(res) {
                                    $state.go('verification');
                                });
                            }

                        });
                    }
                });

                $ionicLoading.hide();

                if (postalSuccess == false) {
                    $ionicPopup.alert({
                        title: 'Coming Soon',
                        template: "The Remedy app is not yet available in " + $localStorage.errorZip + ".<br/>Call us to see if we can accommodate you today.<br/><a href='tel:512-900-5844'>Click to Call Remedy</a>"
                    });
                }
            }

            function errorCallback(error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Error',
                    template: "Sorry we are unable to locate your postal code."
                });
            }
        }

        function onError(error) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error',
                template: "Sorry we are unable to locate your postal code."
            });
        }
    };
    $ionicModal.fromTemplateUrl('templates/how-it-works.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.$on('modal.hidden', function() {
        $('#last').remove();
        var $vw = $("#videowrap");
        $vw.append($('<video style="width: 100%; height: 100%" id="last" class="video-js vjs-default-skin" controls preload="metadata" poster="http://remedyurgentcare.com/REMEDY_ANIMPASS_V4.jpg" data-setup="{ "fluid": true }"> <source src="http://remedyurgentcare.com/Remedy.mp4" type="video/mp4"> <source src="http://remedyurgentcare.com/Remedy.webm" type="video/webm"> </video> </div>'));
    });

    $scope.openModalFit = function() {
        if (window.analytics !== undefined) {
            analytics.trackView("How Remedy Works");
        }
        $scope.modal.show();
    };
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    $scope.callPhone = function() {
        window.location.href = "tel:512.900.5844";
    };


    /* Video visit */
    $scope.openVideoVisit = function() {
        if (chkUserLogin() == "false") {
            auth.$signInAnonymously().then(function(user) {
                if (user) {
                    var isAnonymous = user.isAnonymous;
                    var uid = user.uid;
                    $localStorage.visitType = "video_visit";
                    $state.go('verification');
                }
            }, function() {
                console.log("Anonymous user error");
            });
        } else {
            $localStorage.visitType = "video_visit";
            if (window.analytics !== undefined) {
                analytics.trackView("Check availibity.");
                analytics.trackEvent('Verification', 'Video Visit');
            }
            $state.go('verification');
        }
    };

}
app.controller('homeCtrl', homeFunction);
