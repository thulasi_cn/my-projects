function paymentFunction($scope, $ionicLoading, $ionicModal, $localStorage, $state, $ionicPopup, makeArraService, $firebaseAuth, MailSendeService, stateListService, $http, staffListService, timeValues, ENV, $window, $location) {
    $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
    });
    var auth = $firebaseAuth();
    var loggedInUser = auth.$getAuth();
    var chkUserLogin = function() {
        return $window.sessionStorage.getItem("isLoggedIn") || $window.sessionStorage.getItem("isRegistered");
    };
    if (chkUserLogin() == 'true') {
        $scope.savecardAnonymous = true;
        if (($localStorage.cardData !== undefined) && ($localStorage.cardData !== '')) {
            $scope.savedCard = true;
            $scope.cardData = $localStorage.cardData;
            $ionicLoading.hide();
        } else {
            $scope.savedCard = false;
            $ionicLoading.hide();
        }
    } else {
        $scope.savecardAnonymous = false;
        $ionicLoading.hide();
    }
    $scope.deleteCard = function() {
        $ionicPopup.confirm({
            title: 'Delete',
            template: "Are you sure ?"
        }).then(function(res) {
            if (res) {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                $.ajax({
                    type: "POST",
                    url: ENV.delCardAPI,
                    crossDomain: true,
                    data: { "customer_id": $localStorage.loggedInUserData.stripe_id },
                    success: function(result) {
                        $scope.savedCard = false;
                        firebase.database().ref('/users/' + loggedInUser.uid).update({
                            stripe_id: '',
                            updatedAt: Date.now()
                        });
                        $ionicLoading.hide();
                    },
                    error: function(error) {
                        $scope.savedCard = false;
                        delete $localStorage.stripCID;
                        delete $localStorage.cardData;
                        firebase.database().ref('/users/' + loggedInUser.uid).update({
                            stripe_id: '',
                            updatedAt: Date.now()
                        });
                        $ionicLoading.hide();
                        console.log(error);
                    },
                    dataType: "json"
                });
            }
        });


    };
    $scope.stateList = stateListService.getStateList();
    if ($localStorage.productID) {
        switch ($localStorage.productID) {
            case 1:
                $scope.serviceName = "In-Person Visit";
                $scope.priceCC = "49";
                break;
            case 2:
                $scope.serviceName = "Tele-Medicine Visit";
                $scope.priceCC = "49";
                break;
            case 3:
                $scope.serviceName = "Routine Physical";
                $scope.priceCC = "49";
                break;
        }
    }
    $ionicModal.fromTemplateUrl('templates/otherfee.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal2) {
        $scope.modal2 = modal2;
    });
    $ionicModal.fromTemplateUrl('templates/terms.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal3) {
        $scope.modal3 = modal3;
    });
    $scope.otherFees = function() {
        $scope.modal2.show();
    };
    $scope.termsConditions = function() {
        $scope.modal3.show();
    };
    // Promo code verification
    $scope.applyPromo = function(promocode) {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        var $am = $scope.priceCC;
        var cleancode;
        if (promocode) {
            cleancode = promocode.toUpperCase();
        } else {
            cleancode = "";
        }
        if (cleancode.length == 8 && (cleancode.substring(0, 3) == 'new' || cleancode.substring(0, 3) == 'NEW')) {
            $am = 1;
            $scope.priceCC = $am.toString();
            firebase.database().ref('referralcodes/').once('value').then(function(referral) {
                var referralCodes = makeArraService.makeArray(referral.val());
                if (referralCodes.length < 0) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Ops!',
                        template: "This is not a valid promo code.<br>Please try again."
                    });
                } else {
                    var referralCodeResult = makeArraService.findReferralUid(referralCodes, cleancode);
                    if (referralCodeResult !== false) {
                        if (referralCodeResult[0].referral_codes == cleancode) {
                            var rusage = parseInt(referralCodeResult[0].referral_usage);
                            rusage++;
                            var referralUid = referralCodeResult[0].uid;
                            var referralUsage = firebase.database().ref('referralcodes/' + referralUid);
                            referralUsage.update({ "referral_usage": rusage, "updatedAt": new Date() });
                            $scope.discountedPrice = '48';
                            $scope.discountedFinalPrice = '1';
                            $('.promocodeSection, .activatedPromocode').toggle();
                            $localStorage.promocode = cleancode;
                            $ionicLoading.hide();
                        }
                    } else {
                        firebase.database().ref('referralcodes/').push({
                            referral_codes: cleancode,
                            referral_usage: "1",
                            createdAt: Date.now(),
                            updatedAt: Date.now()
                        });
                        $scope.discountedPrice = '48';
                        $scope.discountedFinalPrice = '1';
                        $('.promocodeSection, .activatedPromocode').toggle();
                        $ionicLoading.hide();
                    }

                }
            });

        } else {
            firebase.database().ref('promocodes/').once('value').then(function(promocode) {
                var results = makeArraService.makeArray(promocode.val());
                if (results.length < 0) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Ops!',
                        template: "This is not a valid promo code.<br>Please try again."
                    });
                } else {
                    var promocodeResult = makeArraService.findPromoUid(results, cleancode);
                    if (promocodeResult !== undefined && promocodeResult.length > 0) {
                        var promoUid = promocodeResult[0].uid;
                        $am = $am - promocodeResult[0].promo_amount;
                        $scope.priceCC = $am.toString();
                        $scope.discountedPrice = promocodeResult[0].promo_amount;
                        $scope.discountedFinalPrice = $am;
                        var usage = parseInt(promocodeResult[0].promo_usage);
                        usage++;
                        var promoUsage = firebase.database().ref('promocodes/' + promoUid);
                        promoUsage.update({ promo_usage: usage, updatedAt: new Date() });
                        $localStorage.promocode = cleancode;
                        $('.promocodeSection, .activatedPromocode').toggle();
                        $ionicLoading.hide();
                    } else {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error',
                            template: "This is not a valid promo code.<br>Please try again."
                        });
                    }
                }


            });

        }
    };

    // Final payment to the Stripe
    $scope.finalPayment = function(validateData, paymentData) {
        if (validateData === true) {
            var paymentDetails = paymentData;
            Stripe.setPublishableKey('pk_test_1vt5FULoYAFlVqXCtWPGq5sq');
            var $form = $('#payment-form');
            var $am = $scope.priceCC;
            var stripeResponseHandler = function(status, response) {
                Stripe.setPublishableKey('pk_test_1vt5FULoYAFlVqXCtWPGq5sq');
                if (response.error) {
                    $form.find('.payment-errors').text(response.error.message).addClass('visible');
                    $form.find('button').prop('disabled', false);
                    $ionicPopup.alert({
                        title: 'Error',
                        template: response.error.message
                    });
                    $ionicLoading.hide();
                } else {
                    var token = response.id;
                    var paymentData = {
                        'stripeToken': token,
                        'receipt_email': $localStorage.patientDetails.contact_email,
                        'am': $am,
                        'create': ''
                    };
                    if (paymentDetails.saveCardChk === true) {
                        paymentData.create = 'save';
                    } else {
                        paymentData.create = 'charge';
                    }
                    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                    $.ajax({
                        type: "POST",
                        url: ENV.paymentApi,
                        crossDomain: true,
                        data: paymentData,
                        success: function(result) {
                            if (result.status == "succeeded") {
                                if (paymentDetails.saveCardChk === true) {
                                    firebase.database().ref('/users/' + loggedInUser.uid).update({
                                        stripe_id: result.customer_id,
                                        updatedAt: Date.now()
                                    });
                                    $localStorage.loggedInUserData.stripe_id = result.customer_id;
                                    $localStorage.stripCID = result.customer_id;
                                    paymentSave(paymentDetails, true);
                                } else {
                                    paymentSave(paymentDetails, true);
                                }
                            } else {
                                $form.find('button').prop('disabled', false);
                                paymentSave(paymentDetails, false);
                                $ionicLoading.hide();
                                $ionicPopup.alert({
                                    title: 'Error',
                                    template: "There was a problem charging your card reason: " + result.status
                                });
                            }
                        },
                        error: function(error) {
                            paymentSave(paymentDetails, false);
                            $form.find('button').prop('disabled', false);
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error',
                                template: "There was a problem charging your card reason: stripe api is not responding"
                            });
                        },
                        dataType: "json"
                    });
                }
            };
            $form.find('.btn-remedy-blue-light').prop('disabled', true);
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            if ($scope.savedCard === true) {
                var paymentCardData = {
                    'customer_id': $localStorage.stripCID,
                    'receipt_email': $localStorage.patientDetails.contact_email,
                    'am': $am,
                    'create': 'charge_save'
                };
                var bdata = $localStorage.cardData[0];
                var savedCardADdetails = {
                    "cardholdername": bdata.name,
                    "stripeaddress": bdata.address_line1,
                    "stripeaddress2": bdata.address_line2,
                    "stripecity": bdata.address_city,
                    "stripestate": bdata.address_state,
                    "stripezip": bdata.address_zip,
                };
                console.log(paymentCardData);
                $.ajax({
                    type: "POST",
                    url: ENV.paymentApi,
                    crossDomain: true,
                    data: paymentCardData,
                    success: function(result) {
                        console.log(result);
                        if (result.status == "succeeded") {
                            paymentSave(savedCardADdetails, true);
                        } else {
                            $form.find('button').prop('disabled', false);
                            paymentSave(savedCardADdetails, false);
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error',
                                template: "There was a problem charging your card reason: " + result.status
                            });
                        }
                    },
                    error: function(error) {
                        console.log(error);
                        $form.find('button').prop('disabled', false);
                        paymentSave(savedCardADdetails, false);
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error',
                            template: "There was a problem charging your card reason: " + result.status
                        });
                    },
                    dataType: "json"
                });
            } else {
                $valuesForToken = {
                    number: paymentDetails.cardnumber,
                    cvc: paymentDetails.cvv,
                    exp_month: paymentDetails.expMonth,
                    exp_year: paymentDetails.expYear,
                    name: paymentDetails.cardholdername,
                    address_line1: paymentDetails.stripeaddress,
                    address_city: paymentDetails.stripecity,
                    address_zip: paymentDetails.stripezip,
                    address_state: paymentDetails.stripestate,
                    address_country: $('.country').val()
                };
                Stripe.card.createToken($valuesForToken, stripeResponseHandler);
            }
        } else {
            $ionicPopup.alert({
                title: 'Error',
                template: "You must accept terms and conditions"
            });
        }
    };

    function paymentSave(paymentDetails, paymentStatus) {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        var insuranceNumber = $scope.insuranceNumber;
        var dateAndTime = $localStorage.visitTime;
        if ($localStorage.visitTime.split('|')[1] == "today") {
            fullTime = $localStorage.visitTime.split('|')[0] + '|' + timeValues.tFormattedDate();
        } else if ($localStorage.visitTime.split('|')[1] == "tomorrow") {
            fullTime = $localStorage.visitTime.split('|')[0] + '|' + timeValues.tmFormattedDate();
        }
        var paymentDetailsData = paymentDetails;
        var patientDetails = $localStorage.patientDetails;
        var dobFromLocalStorage = patientDetails.dob;
        var vistAddress = $localStorage.patientAddress === undefined ? "" : $localStorage.patientAddress;
        var VicitType = $localStorage.visitType;
        var dob = new Date(dobFromLocalStorage).getDate() + '-' + (new Date(dobFromLocalStorage).getMonth() + 1) + '-' + new Date(dobFromLocalStorage).getFullYear();
        var userAppointmentData = {};
        var extraAddress;
        var add1;
        var zipfrmMap;
        if (typeof $localStorage.addressType !== "undefined") {
            extraAddress = '';
            zipfrmMap = $localStorage.zipGetFromAPI;
            add1 = vistAddress;
        } else {
            extraAddress = ($localStorage.extra === undefined ? "" : $localStorage.extra);
            add1 = vistAddress.split(',')[0] === undefined ? "" : vistAddress.split(',')[0];
            zipfrmMap = extraAddress.zip === undefined ? "" : extraAddress.zip;
        }

        userAppointmentData.appointmentAddress = $localStorage.patientAddress;

        /* if ((VicitType == 'appointment_visit') && ($localStorage.addressType !=
                 'addressFromMap')) { 
             userAppointmentData.appointmentAddress = $localStorage.patientAddress;
             // userAppointmentData.appointmentAddress = vistAddress.split(',')[0] + ',' + (extraAddress.address === undefined ? "" : extraAddress.address + ',') + '' + extraAddress.city + ',' + extraAddress.state + ' ' + extraAddress.zip;
         } else if ($localStorage.addressType !=
             'addressFromMap') {
             userAppointmentData.appointmentAddress = $localStorage.patientAddress;
             // userAppointmentData.appointmentAddress = vistAddress;
         }*/

        userAppointmentData.patientName = patientDetails.first_name + ' ' + patientDetails.last_name;
        userAppointmentData.appointmentTime = timeValues.convertVisitTime(fullTime, VicitType);
        userAppointmentData.appointmentType = VicitType;

        var messagePostData = {};
        messagePostData.phone_number = patientDetails.contact_phone;
        messagePostData.app_time = timeValues.convertVisitTime(fullTime, VicitType);
        messagePostData.patient_name = userAppointmentData.patientName;
        messagePostData.patient_email = patientDetails.contact_email;
        messagePostData.app_address = $localStorage.patientAddress; /*vistAddress.split(',')[0];*/
        messagePostData.dob = dob;
        firebase.database().ref('appointments/').push({
            appointment_type: VicitType,
            appointment_user: loggedInUser.uid,
            contact_phone_number: patientDetails.contact_phone,
            contact_email: patientDetails.contact_email,
            visit_time: fullTime,
            visit_address_1: add1,
            // visit_address_2: extraAddress.address === undefined ? "" : extraAddress.address,
            // visit_city: extraAddress.city === undefined ? "" : extraAddress.city,
            // visit_state: extraAddress.state === undefined ? "" : extraAddress.state,
            // visit_zip_code: zipfrmMap,
            patient_name: patientDetails.first_name + ' ' + patientDetails.last_name,
            patient_dob: dob,
            patient_gender: patientDetails.gender,
            patient_relation: patientDetails.relation,
            responsible_party_name: (patientDetails.responsible === undefined ? "" : patientDetails.responsible) + ' ' + (patientDetails.responsible_last === undefined ? "" : patientDetails.responsible_last),
            billing_name: paymentDetailsData.cardholdername,
            billing_address_1: paymentDetailsData.stripeaddress,
            billing_address_2: paymentDetailsData.stripeaddress2 === undefined ? "" : paymentDetailsData.stripeaddress2,
            billing_city: paymentDetailsData.stripecity,
            billing_state: paymentDetailsData.stripestate,
            billing_zip: paymentDetailsData.stripezip,
            billing_promo: $localStorage.promocode === undefined ? "" : $localStorage.promocode,
            payment_amount: $scope.priceCC,
            payment_date: Date.now(),
            payment_status: paymentStatus,
            coordinates: ($localStorage.latitude === undefined ? "" : $localStorage.latitude) + ' ' + ($localStorage.longitude === undefined ? "" : $localStorage.longitude),
            createdAt: Date.now(),
            updatedAt: Date.now()
        }).then(function(snapshot) {
            if (snapshot) {
                if (window.analytics !== undefined) {
                    analytics.trackView("Payment Success");
                    analytics.trackEvent('Confirmed ', 'Landed');
                }
                firebase.database().ref('/staffs').once('value', function(staff) {
                    /*-------MAil----------*/
                    var StaffEmailsList = staffListService.getStaffEmails(staff.val());
                    StaffEmailsList.push(patientDetails.contact_email);
                    for (var e = 0; e < StaffEmailsList.length; e++) {
                        userAppointmentData.patientEmail = StaffEmailsList[e];
                        var mail = MailSendeService.sendMail(userAppointmentData);
                        if (mail.status = "200") {
                            console.log("Mail send success....!!");
                        } else {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error',
                                template: "Patient not a valid email !!!"
                            });
                        }
                    }
                    /*-------MAil----------*/
                    /*-------Message----------*/
                    var StaffPhoneNumbersList = staffListService.getStaffPh_nos(staff.val());
                    $http.get(ENV.smsAPI + '?phone_number=' + StaffPhoneNumbersList.join() + '&app_time=' + messagePostData.app_time + '&patient_name=' + messagePostData.patient_name + '&patient_phone=' + messagePostData.phone_number + '&patient_email=' + messagePostData.patient_email + '&app_address=' + messagePostData.app_address + '&dob=' + messagePostData.dob).success(function(res) {
                        document.getElementById('payment-form').reset();
                        delete $localStorage.promocode;
                        if ($localStorage.visitType == "video_visit") {
                            $ionicLoading.hide();
                            $state.go('video_confirmed');
                        } else if ($localStorage.visitType == "appointment_visit") {
                            $ionicLoading.hide();
                            $state.go('confirmed');
                            $scope.confirmedPatientAddress = $localStorage.patientAddress;
                        }
                    });
                });

            } else {
                $ionicPopup.alert({
                    title: 'Error',
                    template: "There was a problem saving your appointment data.  Please call: 512.900.5844"
                });
                $ionicLoading.hide();
            }
        });
    }
}
app.controller('paymentCtrl', paymentFunction);
