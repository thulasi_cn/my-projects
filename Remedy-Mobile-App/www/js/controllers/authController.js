function authCntrlFunction($scope, $ionicLoading, $ionicPopup, $ionicModal, $localStorage, $state, $ionicPopover, $firebaseAuth, promoCodeService, zipCodeService, $window, localNotificationService) {
    var auth = $firebaseAuth();
    var firebaseUser = firebase.database();
    $scope.isAnonymous = false;
    $scope.$watch(function() {
        return $window.sessionStorage.getItem("isLoggedIn");
    }, function(newVal, oldVal) {
        if (newVal === false || newVal === null) {
            console.log('Trying to logged Out.');
            $scope.logOut();
        }
        console.log("isLoggedIn", newVal, oldVal);
    });
    $scope.$watch(function() {
        return $window.sessionStorage.getItem("isRegistered");
    }, function(newVal, oldVal) {
        if (newVal === false || newVal === null) {
            console.log('Trying to logged Out.');
            $scope.logOut();
        }
        console.log("isRegistered", newVal, oldVal);
    });
    $scope.confirmPassword = (function() {
        return {
            test: function(value) {
                var val = angular.element('#password').val();
                var temp = '^' + val + '$';
                var reg = new RegExp(temp);
                return reg.test(value);
            }
        };
    })();
    $scope.chkUserLogin = function() {
        return $window.sessionStorage.getItem("isLoggedIn") || $window.sessionStorage.getItem("isRegistered");
    };
    $scope.callPhone = function() {
        window.location.href = "tel:5129005844";
    };

    $ionicPopover.fromTemplateUrl('templates/topMenu.html', {
        scope: $scope,
    }).then(function(popover) {
        $scope.popover = popover;
    });
    $scope.openCreateAccount = function(data) {
        $state.go('register', {}, { reload: true });
    };
    $scope.createAccount = function(data) {
        $localStorage.registrationDetails = data;
        var registerUserData = data;
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        if (!$scope.isAnonymous) {
            auth.$createUserWithEmailAndPassword(data.email, data.password).then(function(user) {
                userIonAuth(registerUserData);
                writeUserData(user.uid, registerUserData.first_name, registerUserData.last_name, registerUserData.email, registerUserData.phone);
                $state.go('home');
            }).catch(function(error) {
                if (error === null) {
                    console.log("User created successfully:", user);
                } else {
                    emailExist(error);
                }
            });
        } else {
            var loggedInUser = auth.$getAuth();
            var credential = firebase.auth.EmailAuthProvider.credential(registerUserData.email, registerUserData.password);
            loggedInUser.link(credential).then(function(user) {
                userIonAuth(registerUserData);
                writeUserData(user.uid, registerUserData.first_name, registerUserData.last_name, registerUserData.email, registerUserData.phone);
                delete $localStorage.anonymous;
                localNotificationService.activateLocalNotification();
                $state.go('home');
            }, function(error) {
                console.log("Error upgrading anonymous account", error);
                emailExist(error);
            });
        }

    };

    function emailExist(error) {
        if (error.code == "auth/email-already-in-use") {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Warning',
                template: "This e-mail has already been registered. Please log in or use a different e-mail address."
            });
        } else {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error',
                template: "There was a problem to create your account. <br>Please try again later."
            });
        }
    }

    function userIonAuth(registerUserData) {
        $localStorage.loginData = { "password": registerUserData.password, "username": registerUserData.email };
        window.sessionStorage.setItem("isLoggedIn", true);
        window.sessionStorage.setItem("isRegistered", true);
        $ionicPopup.alert({
            title: 'Success',
            template: "Your account was successfully created."
        });
        $ionicLoading.hide();
    }
    $scope.doLogin = function(user) {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        $localStorage.loginData = user;
        if ((user.username !== "") && (user.password !== "")) {
            auth.$signInWithEmailAndPassword(user.username, user.password)
                .catch(function(error, user) {
                    if (error.code == "auth/user-not-found") {
                        console.log("Error upon login:", error);
                        $scope.loginAlert('Wrong email or password<br>Please try again.');
                        $ionicLoading.hide();
                    }
                }).then(function(snapshot) {
                    if (snapshot && snapshot.email == $localStorage.loginData.username) {
                        $ionicLoading.hide();
                        window.sessionStorage.setItem("isLoggedIn", true);
                        zipCodeService.getZipCode();
                        setTimeout(function() {
                            localNotificationService.activateLocalNotification();
                            $state.go('home');
                        }, 500);
                    }

                });
        } else {
            $scope.loginAlert('Please enter your email and password correctly');
            $ionicLoading.hide();
        }
    };
    $ionicModal.fromTemplateUrl('templates/terms.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal3) {
        $scope.modal3 = modal3;
    });
    $scope.termsConditions = function() {
        $scope.modal3.show();
    };

    $scope.loginAlert = function(msg) {
        if ((msg === '') || (typeof msg === undefined)) {
            msg = 'Username or Password is invalid';
        }
        var alertPopup = $ionicPopup.alert({
            title: 'Error',
            template: msg
        });
        alertPopup.then(function(res) {});
    };

    function writeUserData(userId, first_name, last_name, email, phone) {
        firebaseUser.ref('users/' + userId).set({
            first_name: first_name,
            last_name: last_name,
            email: email,
            phone: phone,
            role: 'user',
            createdAt: Date.now(),
            updatedAt: Date.now()
        });
    }
    $scope.logOut = function() {
        $window.sessionStorage.setItem("isLoggedIn", false);
        $window.sessionStorage.setItem("isRegistered", false);
        delete $localStorage.loginData;
        delete $localStorage.loggedInUserData;
        if ($localStorage.registrationDetails) {
            delete $localStorage.registrationDetails;
        }
        delete $localStorage.visitTime;
        delete $localStorage.extra;
        delete $localStorage.patientAddress;
        delete $localStorage.patientDetails;
        delete $localStorage.promocodeAPI;
        delete $localStorage.zipAPI;
        delete $localStorage.stripCID;
        delete $localStorage.cardData;
        $state.go('home');
        firebase.auth().signOut();
    };
    $scope.forgotPassword = function(email) {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        var auth = firebase.auth();
        auth.sendPasswordResetEmail(email).then(function() {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Reset Password!',
                template: "A link has been sent to your " + email + ". Please check your mail."
            }).then(function(res) {
                document.getElementById('forgotPassowrdFrm').reset();
                $state.go('app');
            });
        }, function(error) {
            $ionicLoading.hide();
            if (error.code == "auth/user-not-found") {
                $scope.loginAlert('Please enter a correct email address.');
            }
        });

    };
}
app.controller('authCtrl', authCntrlFunction);
