function insuranceFunction($scope, $ionicLoading, $localStorage, $state, $cordovaCamera, Camera, $ionicPopup, $firebaseAuth, $ionicNavBarDelegate) {
    if ($state.current.name == "insurancesProcess") {
        $ionicNavBarDelegate.showBackButton(false);
    } else {
        $ionicNavBarDelegate.showBackButton(true);
    }
     // $state.go('insurance-add', {}, { reload: true });
    var auth = $firebaseAuth();
    var loggedInUser = auth.$getAuth();
    $scope.isDisabled = false;
    $scope.srcImage = [];
    if ('device' in window) {
        $scope.takeImageBtn = false;
        $scope.takeImage = function() {
            $ionicPopup.show({
                title: 'Choose File pick type',
                buttons: [{
                    text: '<b class="ion-camera" style="font-size: larger;">Camera</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        uploadFile(1);
                    }
                }, {
                    text: '<b class="ion-images" style="font-size: larger;">Gallary</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        uploadFile(0);
                    }
                }]
            });

            function uploadFile(filePickType) {
                if (filePickType == 0 || filePickType == 1) {
                    var options = {
                        quality: 20,
                        destinationType: 0,
                        sourceType: filePickType,
                        allowEdit: true,
                        encodingType: 1,
                        targetWidth: 100,
                        targetHeight: 100,
                        popoverOptions: 0,
                        saveToPhotoAlbum: false
                    };
                    $cordovaCamera.getPicture(options).then(imgPushSuccess, imgPushFailure);
                } else {
                    return false;
                }
            }
        };
    } else {
        $scope.takeImageBtn = true;
        $(".images").change(function() {
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            if (typeof(FileReader) != "undefined") {
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png)$/;
                $($(this)[0].files).each(function() {
                    var file = $(this);
                    if (regex.test(file[0].name.toLowerCase())) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            imgPushSuccess(e.target.result);
                        };
                        reader.readAsDataURL(file[0]);
                    } else {
                        alert(file[0].name + " is not a valid image file.");
                        return false;
                    }
                });
            } else {
                alert("This browser does not support HTML5 FileReader.");
            }
        });
    }

    function imgPushSuccess(imageData) {
        if (imageData !== null) {
            if ($scope.srcImage.length < 2) {
                if ('device' in window) {
                    $scope.srcImage.push("data:image/png;base64," + imageData);
                    $ionicLoading.hide();
                } else {
                    $scope.srcImage.push(imageData);
                    $ionicLoading.hide();
                }
            } else {
                $scope.isDisabled = false;
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Error',
                    template: "Only 2 images can upload..!!"
                });
            }
            $scope.isDisabled = true;
        } else {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error',
                template: "There was a problem to saving images. <br>Please try again later."
            });
        }
    }

    function imgPushFailure(err) {
        $ionicLoading.hide();
        $ionicPopup.alert({
            title: 'Error',
            template: "There was a problem to saving images. <br>Please try again later."
        });
    }
    /*
    Insurance information save to firebase
     */
    $scope.saveInsuranceInfo = function(insurance) {

        $('#imgf, #imgb').removeAttr('src');

        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        if ($scope.srcImage.length == 2) {
            $state.go('insurancesProcess');
            insurance.user = loggedInUser.uid;
            firebase.database().ref('insurance/').push({
                user: insurance.user,
                name: insurance.name,
                images: $scope.srcImage,
                createdAt: Date.now(),
                updatedAt: Date.now()
            }).then(function(snapshot, error) {
                if (snapshot) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Success',
                        template: "Your Informations have been saved."
                    }).then(function(res) {
                        $localStorage.isInsured = true;
                        $scope.srcImage = [];
                        //document.getElementById('insuranceForm').reset();
                        $state.go('insurance-self');
                    });
                } else if (error) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Error',
                        template: "There was a problem to saving insurance informations. <br>Please try again later."
                    });
                }
            });
        } else {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error',
                template: "Please Choose the files..!!"
            });
        }
    };
}

function insuranceListFunction($scope, $ionicLoading, $localStorage, $state, $ionicPopup, $firebaseAuth, makeArraService) {
    var auth = $firebaseAuth();
    var loggedInUser = auth.$getAuth();

    /*$scope.clrImg = function(){
        alert($('#imgf,#imgb').length);
        
    };*/

    $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
    });
    firebase.database().ref('/insurance').orderByChild("user").equalTo(loggedInUser.uid).on("value", function(snapshot) {
        $scope.userInsurance = makeArraService.makeArray(snapshot.val());
        $ionicLoading.hide();
    });
    $scope.delInsurance = function(uid) {
        $ionicPopup.confirm({
            title: 'Delete',
            template: "Are you sure?"
        }).then(function(res) {
            if(res){
                firebase.database().ref('insurance/' + uid).remove().then(function(resp) {
                    console.log("insurence deleted");
                });
            }
        });

    };
}

app.controller('inusrnaceCtrl', insuranceFunction);
app.controller('inusrnaceListCtrl', insuranceListFunction);
