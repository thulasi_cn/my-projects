function verificationFunction($scope, $ionicLoading, $localStorage, $state) {
    $scope.vvisit = false;
    if ($localStorage.visitType == "video_visit") {
        $scope.vvisit = true;
    }
    $scope.product = function(id) {
        $localStorage.productID = id;
        if (window.analytics !== undefined) {
            analytics.trackView("Verification Confirm.");
            analytics.trackEvent('Verify Confirm', 'Schedule My visit');
        }
        if ($localStorage.visitType == "video_visit") {
            document.getElementById('verifyFRM').reset();
            $state.go('appointment', {}, { reload: true });
        } else if ($localStorage.visitType == "appointment_visit") {
            document.getElementById('verifyFRM').reset();
            $state.go('appointment', {}, { reload: true });
        }
    };
}
app.controller('verificationCtrl', verificationFunction);
