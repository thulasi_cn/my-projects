function accountFunction($scope, $state) {
    function getUserInfo() {
        $scope.userEmail = '';
        $scope.userFirstName = '';
        $scope.userLastName = '';
        $scope.userPhone = '';
        $('#userFirstName').val($scope.userFirstName);
        $('#userLastName').val($scope.userLastName);
        $('#userEmail').val($scope.userEmail);
        $('#userPhone').val($scope.userPhone);
    }
    getUserInfo();
    $scope.accountSave = function(data) {
        var currentUser = Parse.User.current();
        console.log(currentUser);
        if (data.first_name) {
            currentUser.set('first_name', data.first_name);
            $('#userFirstName').val(data.first_name);
        }
        if (data.last_name) {
            currentUser.set('first_name', data.last_name);
            $('#userLastName').val(data.last_name);
        }
        if (data.email) {
            currentUser.set('email', data.email);
            $('#userEmail').val(data.email);
        }
        if (data.phone) {
            currentUser.set('phone', data.phone);
            $('#userPhone').val(data.phone);
        }
        currentUser.save();
        $state.go('account');
    };
    $scope.logout = function() {
        localStorage.clear();
        $state.go('auth');
    };
}
app.controller('accountCtrl', accountFunction);
