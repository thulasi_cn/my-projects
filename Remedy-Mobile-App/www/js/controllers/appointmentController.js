     var TimeInterval, content = [];

     function appointmentFunction($scope, $ionicLoading, $ionicPopup, $localStorage, $state, makeArraService, timeValues) {
         clearInterval(TimeInterval);
         $ionicLoading.show({
             content: 'Loading',
             animation: 'fade-in',
             showBackdrop: true,
             maxWidth: 200,
             showDelay: 0
         });
         if ($localStorage.visitType == "video_visit") {
             var fetchDateData = [],
                 appTimeData = [],
                 content = [],
                 dupes = {},
                 singles = [],
                 c = 0;
             firebase.database().ref('/slots').orderByChild("slot_type").equalTo("video_visit").on('value', function(slots) {
                 var VideoVisitSlotsResults = [];
                 var fetchSeat = [];
                 for (var key in slots.val()) {
                     var slotData = slots.val()[key];
                     if (slotData.slot_status == "true") {
                         slotData.uid = key;
                         VideoVisitSlotsResults.push(slotData);
                     }
                 }
                 for (var i = 0; i < VideoVisitSlotsResults.length; i++) {
                     var object = VideoVisitSlotsResults[i],
                         slot_time = object.slot_time.split('|'),
                         slot_time_time = slot_time[0],
                         slot_time_day = slot_time[1],
                         seatNumber = object.slot_seat;
                     fetchSeat.push(seatNumber);
                     if (slot_time_day === 'today')
                         fetchDateData.push(slot_time_time + '|' + timeValues.tFormattedDate());
                     if (slot_time_day === 'tomorrow') fetchDateData.push(slot_time_time + '|' + timeValues.tmFormattedDate());
                 }
                 firebase.database().ref('/appointments').orderByChild("appointment_type").equalTo("video_visit").on('value', function(appoints) {
                     var appointResults = makeArraService.makeArray(appoints.val());
                     for (var i = 0; i < appointResults.length; i++) {
                         var object = appointResults[i];
                         app_time = object.visit_time;
                         appTimeData.push(app_time);
                     }
                     var sortedAppoint = timeValues.getDuplicateValueTime(appTimeData);
                     for (var j = 0; j < sortedAppoint[0].length; j++) {
                         var curTimeAndDate = sortedAppoint[0][j];
                         var curTimeAndDateNumber = sortedAppoint[1][j];
                         for (var k = 0; k < fetchDateData.length; k++) {
                             if (fetchDateData[k] == curTimeAndDate && fetchSeat[k] == curTimeAndDateNumber) {
                                 fetchDateData.splice(k, 1);
                             }
                         }
                     }
                     for (var l = 0; l < fetchDateData.length; l++) {
                         var timeDateArr = fetchDateData[l].split('|');
                         var time = timeDateArr[0];
                         var date = timeDateArr[1];
                         var startTime = timeDateArr[0].split('-')[0];
                         var endTime = timeDateArr[0].split('-')[1];
                         var getDateValue = new Date();
                         var getTomorrow = new Date(timeValues.tmFormattedDate());

                         if (!dupes[date]) {
                             dupes[date] = true;
                             singles.push(date);
                             if (c == 0) {
                                 content.push('<optgroup label="Today - ', timeValues.daysNames((timeValues.today()).getDay()), ', ', timeValues.tMondth(), ' ', timeValues.getGetOrdinal(timeValues.tday()), ', ', timeValues.tYear(), '">');
                                 c++;
                             } else if (c == 1) {
                                 content.push('</optgroup><optgroup label="Tomorrow - ', timeValues.daysNames((timeValues.tomorrow()).getDay()), ', ', timeValues.tmMondth(), ' ', timeValues.getGetOrdinal(timeValues.tmday()), ', ', timeValues.tmYear(), '">');
                             }
                         }

                         if (date == timeValues.tFormattedDate()) {
                             content.push('<option value="', time, '|', timeValues.tFormattedDate(), '">', timeValues.timeTo12HrFormat(startTime), ' - ', timeValues.timeTo12HrFormat(endTime), '</option>');
                         }
                         if (date == timeValues.tmFormattedDate()) {
                             content.push('<option value="', time, '|', timeValues.tmFormattedDate(), '">', timeValues.timeTo12HrFormat(startTime), ' - ', timeValues.timeTo12HrFormat(endTime), '</option>');
                         }

                     }
                     var totalData = content.join('');
                     $('#doctorsSlot').append(totalData);
                     var filteredTodayTimeDate = [],
                         filterTodayTime = [];
                     for (var m = 0; m < fetchDateData.length; m++) {
                         var res = fetchDateData[m];
                         tdFromFilter = res.split('|')[1];
                         if (timeValues.tFormattedDate() === tdFromFilter) {
                             filteredTodayTimeDate.push(res);
                             filterTodayTime.push((res.split('|')[0]).split('-')[0]);
                         }
                     }
                     TimeInterval = setInterval(function() {
                         var currentHours = new Date().getHours();
                         if (filterTodayTime.length > 0) {
                             for (var n = 0; n < filterTodayTime.length; n++) {
                                 if (parseInt(filterTodayTime[n].split(':')[0]) <= currentHours) {
                                     var $elm = $('#doctorsSlot option[value="' + filteredTodayTimeDate[n].split('|')[0] + '\\|' + filteredTodayTimeDate[n].split('|')[1] + '"]');
                                     if ($elm.length > 0) {
                                         $elm.remove();
                                     }
                                 }
                             }
                         }
                         if ($('#doctorsSlot option').length <= 1) {
                             $scope.no_time_available = true;
                         }
                     }, 1000);

                     setTimeout(function() {
                         $ionicLoading.hide();
                         fetchDateData.length = 0;
                         fetchSeat.length = 0;
                         appTimeData.length = 0;
                         content.length = 0;
                         totalData = '';
                     }, 3000);

                 });
             });
         } else if ($localStorage.visitType == "appointment_visit") {
             var fetchDateData1 = [],
                 appTimeData1 = [],
                 content1 = [],
                 dupes1 = {},
                 singles1 = [],
                 cc = 0;

             firebase.database().ref('/slots').orderByChild("slot_type").equalTo("appointment_visit").on('value', function(slots) {
                 var AppointmentSlotsResults = [];
                 var fetchSeat = [];
                 for (var key in slots.val()) {
                     var slotData = slots.val()[key];
                     if (slotData.slot_status == "true") {
                         slotData.uid = key;
                         AppointmentSlotsResults.push(slotData);
                     }
                 }
                 for (var i = 0; i < AppointmentSlotsResults.length; i++) {
                     var object = AppointmentSlotsResults[i],
                         slot_time = object.slot_time.split('|'),
                         slot_time_time = slot_time[0],
                         slot_time_day = slot_time[1],
                         seatNumber = object.slot_seat;
                     fetchSeat.push(seatNumber);
                     if (slot_time_day === 'today') fetchDateData1.push(slot_time_time + '|' + timeValues.tFormattedDate());
                     if (slot_time_day === 'tomorrow') fetchDateData1.push(slot_time_time + '|' + timeValues.tmFormattedDate());
                 }

                 firebase.database().ref('/appointments').orderByChild("appointment_type").equalTo("appointment_visit").on('value', function(appoints) {
                     var appointResults = makeArraService.makeArray(appoints.val());
                     for (var i = 0; i < appointResults.length; i++) {
                         var object = appointResults[i];
                         app_time = object.visit_time;
                         appTimeData1.push(app_time);
                     }
                     var sortedAppoint = timeValues.getDuplicateValueTime(appTimeData1);
                     for (var j = 0; j < sortedAppoint[0].length; j++) {
                         var curTimeAndDate = sortedAppoint[0][j];
                         var curTimeAndDateNumber = sortedAppoint[1][j];
                         for (var k = 0; k < fetchDateData1.length; k++) {
                             if (fetchDateData1[k] == curTimeAndDate && fetchSeat[k] == curTimeAndDateNumber) {
                                 fetchDateData1.splice(k, 1);
                             }
                         }
                     }
                     for (var l = 0; l < fetchDateData1.length; l++) {
                         var timeDateArr = fetchDateData1[l].split('|');
                         var time = timeDateArr[0];
                         var date = timeDateArr[1];
                         var startTime = timeDateArr[0].split('-')[0];
                         var endTime = timeDateArr[0].split('-')[1];

                         if (!dupes1[date]) {
                             dupes1[date] = true;
                             singles1.push(date);
                             if (cc == 0) {
                                 content1.push('<optgroup label="Today - ', timeValues.daysNames((timeValues.today()).getDay()), ', ', timeValues.tMondth(), ' ', timeValues.getGetOrdinal(timeValues.tday()), ', ', timeValues.tYear(), '">');
                                 cc++;
                             } else if (cc == 1) {
                                 content1.push('</optgroup><optgroup label="Tomorrow - ', timeValues.daysNames((timeValues.tomorrow()).getDay()), ', ', timeValues.tmMondth(), ' ', timeValues.getGetOrdinal(timeValues.tmday()), ', ', timeValues.tmYear(), '">');
                             }
                         }

                         if (date == timeValues.tFormattedDate()) {
                             content1.push('<option value="', time, '|', timeValues.tFormattedDate(), '">', timeValues.formatAMPMfromTime(time.split('-')[0]), ' - ', timeValues.formatAMPMfromTime(time.split('-')[1]), '</option>');
                         }
                         if (date == timeValues.tmFormattedDate()) {
                             content1.push('<option value="', time, '|', timeValues.tmFormattedDate(), '">', timeValues.formatAMPMfromTime(time.split('-')[0]), ' - ', timeValues.formatAMPMfromTime(time.split('-')[1]), '</option>');
                         }
                     }
                     content1.push('</optgroup>');
                     var totalData = content1.join('');
                     $('#doctorsSlot').append(totalData);
                     var filteredTodayTimeDate = [],
                         filterTodayTime = [];
                     for (var m = 0; m < fetchDateData1.length; m++) {
                         var res = fetchDateData1[m],
                             tdFromFilter = res.split('|')[1];
                         if (timeValues.tFormattedDate() === tdFromFilter) {
                             filteredTodayTimeDate.push(res);
                             filterTodayTime.push((res.split('|')[0]).split('-')[0]);

                         }
                     }
                     TimeInterval = setInterval(function() {
                         var currentHours = new Date().getHours();

                         if (filterTodayTime.length) {
                             for (var n = 0; n < filterTodayTime.length; n++) {
                                 if ((parseInt(filterTodayTime[n]) + 1) <= currentHours) {
                                     var data = filteredTodayTimeDate[n].split('|')[1].split('/');
                                     var udate = data[0] + '\\/' + data[1] + '\\/' + data[2];

                                     var $elm = $('#doctorsSlot option[value=' + filteredTodayTimeDate[n].split('|')[0] + '\\|' + udate + ']');
                                     if ($elm.length > 0) {
                                         $elm.remove();
                                     }
                                 }
                             }
                         }

                         if ($('#doctorsSlot option').length <= 1) {
                             $scope.no_time_available = true;
                         }
                     }, 1000);
                     setTimeout(function() {
                         fetchDateData1.length = 0;
                         fetchSeat.length = 0;
                         appTimeData1.length = 0;
                         content1.length = 0;
                         totalData = '';
                         $ionicLoading.hide();
                     }, 3000);

                 });
             });
         }
         $scope.visitTime = function(time) {

             var timeSplit = time.split('|');
             if (timeSplit[1] == timeValues.tFormattedDate()) {
                 $localStorage.visitTime = timeSplit[0] + '|today';
             } else if (timeSplit[1] == timeValues.tmFormattedDate()) {
                 $localStorage.visitTime = timeSplit[0] + '|tomorrow';
             }
             if (window.analytics !== undefined) {
                 analytics.trackView("Visit Location");
                 analytics.trackEvent('Schedule Visit Location', 'Visit Location');
             }
             if ($localStorage.visitType == "appointment_visit") {
                 $state.go('location');
             } else if ($localStorage.visitType == "video_visit") {
                 $state.go('details');
             }
         };
     }

     function appointmentSummeryFunction($scope, $state, $localStorage, timeValues, $ionicLoading, $firebaseAuth, $window, ENV) {
         $ionicLoading.hide();
         if ($localStorage.visitType == "video_visit") {
             $scope.vVisit = true;
         }
         $scope.myAddress = $localStorage.patientAddress;
         if ($localStorage.productID) {
             switch ($localStorage.productID) {
                 case 1:
                     $scope.serviceName = "In-Person Visit";
                     $scope.priceCC = "49";
                     break;
                 case 2:
                     $scope.serviceName = "Tele-Medicine Visit";
                     $scope.priceCC = "49";
                     break;
                 case 3:
                     $scope.serviceName = "Routine Physical";
                     $scope.priceCC = "49";
                     break;
             }
         }
         $scope.frmMapAddress = false;

         $scope.overViewDetail = $localStorage.patientDetails;

         if (typeof $localStorage.extra !== "undefined" && $localStorage.addressType == 'addressFromForm') {
             $scope.frmMapAddress = false;
             $scope.extra = $localStorage.extra;
             $scope.patientAddress = $localStorage.patientAddress;
         }

         if ($localStorage.addressType == 'addressFromMap') {
             $scope.frmMapAddress = true;
             $scope.extraAddress = $localStorage.patientAddress;
         }
         var chkUserLogin = function() {
             return $window.sessionStorage.getItem("isLoggedIn") || $window.sessionStorage.getItem("isRegistered");
         };
         var auth = $firebaseAuth();
         var loggedInUser = auth.$getAuth();

         $scope.vTime = timeValues.getVisitTime($localStorage.visitTime, $localStorage.visitType);
         $scope.gotoPayment = function() {
             $ionicLoading.show({
                 content: 'Loading',
                 animation: 'fade-in',
                 showBackdrop: true,
                 maxWidth: 200,
                 showDelay: 0
             });
             if (window.analytics !== undefined) {
                 analytics.trackView("Payment Page");
                 analytics.trackEvent('Payment ', 'Landed');
             }
             if (chkUserLogin() == 'true') {
                 firebase.database().ref('/users/' + loggedInUser.uid).once("value", function(snapshot) {
                     var currentUserData = snapshot.val();
                     $localStorage.loggedInUserData = currentUserData;
                     if ((currentUserData.stripe_id !== undefined) && (currentUserData.stripe_id !== '')) {
                          $localStorage.stripCID = currentUserData.stripe_id;
                         $.ajax({
                             type: "POST",
                             url: ENV.getCardsAPI,
                             crossDomain: true,
                             data: { "customer_id": currentUserData.stripe_id },
                             success: function(result) {
                                 console.log(result);
                                 $localStorage.cardData = result.sources.data;
                                 // $localStorage.cardDetails = result.sources.data[0];
                                 $ionicLoading.hide();
                                 $state.go("payment", {}, { reload: true });
                             },
                             error: function(error) {
                                 console.log(error);
                             },
                             dataType: "json"
                         });
                     } else {
                         $state.go("payment", {}, { reload: true });
                     }
                 });
             } else {
                 $state.go("payment", {}, { reload: true });
             }
         };


     }
     app.controller('appointmentCtrl', appointmentFunction);
     app.controller('appointmentSummeryCtrl', appointmentSummeryFunction);
