function settingsFunction($scope, $ionicLoading, $ionicPopup, $localStorage, $state, $firebaseAuth) {
    var auth = $firebaseAuth();
    var loggedInUser = auth.$getAuth();
    $scope.saveSettings = function(profile) {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        if ($localStorage.loginData.password === profile.old_password) {
            firebase.auth().signInWithEmailAndPassword($localStorage.loginData.username, $localStorage.loginData.password).catch(function(error, user) {
                if (error === null) {
                    console.log("User signin successfully:", user);
                } else {
                    console.log("Error signin user:", error);

                }
            }).then(function(user, error) {
                if (user) {
                    auth.$updatePassword(profile.confirm_password).then(function() {
                        $localStorage.loginData.password = profile.confirm_password;
                        firebase.database().ref('settings/').push({
                            // option1: SettingsInformation.option1,
                            // option2: SettingsInformation.option2,
                            // option3: SettingsInformation.option3,
                            settings_user: loggedInUser.uid,
                            createdAt: Date.now(),
                            updatedAt: Date.now()
                        }).then(function(snapshot) {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Success!',
                                template: "Password changed successfully!"
                            });
                            $state.go('home');
                        });

                    }).catch(function(error) {
                        console.log(error);
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error',
                            template: "There was a problem to saving yout data. <br>Please try again later."
                        });
                    });
                } else if (error) {
                    console.log("Error signin user:", error);
                }
            });
        } else {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error',
                template: "Old password did not match"
            });
        }
    };
}
app.controller('settingsCtrl', settingsFunction);
