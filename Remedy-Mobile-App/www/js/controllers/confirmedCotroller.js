function confirmFunction($scope, $ionicLoading, $ionicModal, $localStorage, $state, timeValues,$window) {
    $scope.chkUserLogin = function() {
        return $window.sessionStorage.getItem("isLoggedIn") || $window.sessionStorage.getItem("isRegistered");
    };
    $scope.overViewDetail = $localStorage.patientDetails;
    $scope.vTime = timeValues.getVisitTime($localStorage.visitTime, $localStorage.visitType);
    // if (typeof $localStorage.patientAddress !== "undefined") {
    //      $scope.patientAddress = $localStorage.patientAddress.split(',')[0];
    //  }
    //  if (typeof $localStorage.extra !== "undefined") {
    //      $scope.extra = $localStorage.extra;
    //  }
    $scope.$watch(function(){ 
        return $localStorage.patientAddress;
    }, function(newValue, oldValue){
        $scope.confirmedPatientAddress = $localStorage.patientAddress;
    });
    
    
    

    $ionicModal.fromTemplateUrl('templates/consent-to-treat.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal5) {
        $scope.modal5 = modal5;
        consent_to_treat_load = function() {
            $ionicLoading.hide();
        };
    });
    $scope.iframeModalCheck = true;
    $ionicLoading.hide();

    if ('device' in window) {
        $scope.webConsent = false;
    } else {
        $scope.webConsent = true;
    }

    $scope.consent_to_treat_open = function() {
        $scope.modal5.show();
        if ($scope.iframeModalCheck) {
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
        }
        $scope.iframeModalCheck = false;
    };

    $scope.newAppointment = function() {
        $state.go('home');
    };
}
app.controller('confirmCtrl', confirmFunction);
