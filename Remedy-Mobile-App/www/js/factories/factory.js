app.factory("stripeService", function($http) {
    return {
        payment: function(token, amount, success) {
            $http({
                    url: 'payment.php',
                    method: "POST",
                    dataType: "json",
                    data: {
                        'stripeToken': token
                    }
                })
                .success(function(data) {
                    success(data);
                });
        }
    };
}).factory('Camera', function($q) {

    return {
        getPicture: function(options) {
            var q = $q.defer();

            navigator.camera.getPicture(function(result) {
                q.resolve(result);
            }, function(err) {
                q.reject(err);
            }, options);

            return q.promise;
        }
    };

}).factory('PreviousState', ['$rootScope', '$state',
    function($rootScope, $state) {
        var lastHref = "/home",
            lastStateName = "home",
            lastParams = {};

        $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
            lastStateName = fromState.name;
            lastParams = fromParams;
            lastHref = $state.href(lastStateName, lastParams)
        })

        return {
            getLastHref: function() {
                return lastHref;
            },
            goToLastState: function() {
                return $state.go(lastStateName, lastParams);
            },
        }

    }
]);
