app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $stateProvider
        .state('app', {
            url: '/auth',
            templateUrl: 'templates/login.html',
            controller: 'authCtrl'
        })
        .state('forgot-password', {
            url: '/forgot_password',
            templateUrl: 'templates/forgot_password.html',
            controller: 'authCtrl'
        })
        .state('register', {
            url: '/register',
            templateUrl: 'templates/register.html',
            controller: 'authCtrl'
        })
        .state('service-menu', {
            url: '/service',
            templateUrl: 'templates/service-menu.html'
        })
        .state('home', {
            url: '/home',
            templateUrl: 'templates/intro.html',
            controller: 'homeCtrl'
        })
        .state('intro', {
            url: '/intro',
            templateUrl: 'templates/home.html',
            controller: 'homeCtrl'
        })
        .state('verification', {
            url: '/verification',
            templateUrl: 'templates/verification.html',
            controller: 'verificationCtrl'
        })
        .state('appointment', {
            url: '/appointment',
            templateUrl: 'templates/appointment.html',
            controller: 'appointmentCtrl'
        })
        .state('account', {
            url: '/account',
            templateUrl: 'templates/account.html',
            controller: 'accountCtrl'
        })
        .state('accountEdit', {
            url: '/accountEdit',
            templateUrl: 'templates/accountEdit.html',
            controller: 'accountCtrl'
        })
        .state('location', {
            url: '/location',
            templateUrl: 'templates/location.html',
            controller: 'locationCtrl'
        })
        .state('map', {
            url: '/map',
            templateUrl: 'templates/map.html',
            controller: 'locationCtrl'
        })
        .state('appointment-summary', {
            url: '/appointment-summary',
            templateUrl: 'templates/appointment-summary.html',
            controller: 'appointmentSummeryCtrl'
        })
        .state('payment', {
            cache: false,
            url: '/payment',
            templateUrl: 'templates/payment.html',
            controller: 'paymentCtrl'
        })
        .state('confirmed', {
            url: '/confirmed',
            templateUrl: 'templates/confirmed.html',
            controller: 'confirmCtrl'
        })
        .state('video_confirmed', {
            url: '/video_confirmed',
            templateUrl: 'templates/video_confirmed.html',
            controller: 'confirmCtrl'
        })
        .state('details', {
            url: '/details',
            templateUrl: 'templates/details.html',
            controller: 'detailCtrl'
        })
        .state('appointments', {
            url: '/appointments',
            templateUrl: 'templates/appointments.html',
            controller: 'userAppointmentCtrl'
        })
        .state('appointmentDetails', {
            url: '/appointment/:appointmentId',
            templateUrl: 'templates/appointmentDetail.html',
            controller: 'userAppointmentDetailCtrl'
        })
        .state('insurances', {
            url: '/insurances',
            templateUrl: 'templates/insurance_main_menu.html',
            controller: 'inusrnaceCtrl'
        })
        .state('insurance-add', {
            url: '/insurance_add',
            templateUrl: 'templates/insurance_add.html',
            controller: 'inusrnaceCtrl'
        })
        .state('insurance-self', {
            url: '/insurance_self',
            templateUrl: 'templates/insurance_self.html',
            controller: 'inusrnaceListCtrl'
        })
        .state('insurancesProcess', {
            url: '/insurance_process',
            templateUrl: 'templates/insurancProssessPage.html',
            controller: 'inusrnaceCtrl'
        })
        .state('settings', {
            url: '/settings',
            templateUrl: 'templates/settings.html',
            controller: 'settingsCtrl'
        })
        .state('messages', {
            url: '/messages',
            templateUrl: 'templates/messages.html',
            controller: 'messageCtrl'
        })
        .state('message', {
            url: '/message/:messageId',
            templateUrl: 'templates/message.html',
            controller: 'messageDetailCtrl'
        });

    $urlRouterProvider.otherwise('/auth');
});
