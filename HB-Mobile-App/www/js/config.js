angular.module("hbMobile.constants", [])

.constant("ENV", {
	"name": "development",
	"hostname": "",
	"imagePath": "https://testing.healthybillions.com/hb-images",
	"coreUrl": "https://testing.healthybillions.com/hb-core",
	"crmUrl": "https://testing-crm.healthybillions.com/hb-crm",
	"diagnosticUrl": "https://testing.healthybillions.com/diagnostic",
	"payment": {
		"URL": "https://testing.healthybillions.com/member/#/payment/prepare",
		"successURL": "https://testing-crm.healthybillions.com/member/#/paymentSuccess",
		"failureURL": "https://testing-crm.healthybillions.com/member/#/paymentFail"
	},
	"phoneNumber": "8100888888",
	"chat": {
		"url": "https://tawk.to/3ab51212e2b2314b8beca1b8bbb0b7c4ce2e041f/popout/default/?$_tawk_popout=true"
	},
	"share": {
		"title": "HealthyBillions Mobile App",
		"message": "I am using the HealthyBillions app. Check it out",
		"androidURL": "https://goo.gl/ojuhY5",
		"iosURL": ""
	},
	"rate": {
		"iOSAppId": "",
		"androidURL": "market://details?id=com.ionicframework.hbmobile769640"
	},
	"serviceCategory": {
		"24/7 Medical Concierge": "24-7-medical-concierge",
		"24/7 Doctor Connect": "24-7-doctor-connect",
		"Travel Emergency": "travel-emergency",
		"24/7 Ambulance": "24-7-ambulance",
		"Medical Second Opinion": "medical-second-opinion",
		"Diagnostic": "diagnostic",
		"Pharmacy": "pharmacy",
		"Physiotherapy at home": "physiotherapy-at-home",
		"Doctor at home": "doctor-at-home",
		"Nurse at home": "nurse-at-home",
		"Yoga at Home": "yoga-at-home",
		"Dietician service": "dietician-service",
		"Eye Care": "eye-care",
		"Dental Care": "dental-care",
		"Gynae Care": "gynae-care",
		"Spa Service": "spa-service"
	}
})

;