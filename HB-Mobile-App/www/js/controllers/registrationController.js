'use strict';

angular.module('hbMobile')
    .controller('RegistrationController', ['$scope', '$rootScope', '$state', 'spinner', 'memberService', '$ionicPopup', 'signUpService', 'sessionService','IonicPushNotification', function ($scope, $rootScope, $state, spinner, memberService, $ionicPopup, signUpService, sessionService,IonicPushNotification) {
        $scope.signUpData = {};

        var registrationError = function (response) {
            $scope.errors = {};
            return angular.forEach(response.data, function (error, field) {
                $scope.errors[field] = error[0];
                return $scope.errors;

            });
        };

        var registrationSuccess = function (response) {
            $scope.errors = {};
            if (response.data.member) {
                sessionService.webLogin($scope.signUpData.email, $scope.signUpData.password);
            }
            if (response.data.status == "sendactivationcode") {
                $state.go('account.activate');
            }
            else if (response.data == "Existing active member") {
                $ionicPopup.confirm({
                    title: 'Existing active member',
                    template: "You an Existing active member. Please login to continue."
                }).then(function (login) {
                    if (login) {
                        $state.go('account.login');
                    }
                });
            }
            else if (response.data.phone == true) {
                $ionicPopup.confirm({
                    title: 'Phone Number already taken',
                    template: response.data.status
                })
            }
            else if (response.data.email == true) {
                $ionicPopup.confirm({
                    title: 'Email ID already taken',
                    template: response.data.status
                })
            }
        };

       $scope.registerUser = function (registrationForm) {
            if (registrationForm.$valid) {
                spinner.forPromise(IonicPushNotification.initialize().then(function(){
                    var subscriptionData = angular.copy($scope.signUpData);
                    subscriptionData.device_id=$rootScope.deviceID;
                    subscriptionData.device_token=$rootScope.deviceToken;
                    subscriptionData._token = window.localStorage.corecsrfToken;
                    spinner.forPromise(memberService.user_subscription_check(subscriptionData)).then(registrationSuccess, registrationError);
                }));
            }
        };

    }]);

