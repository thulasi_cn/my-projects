"use strict"
angular.module('hbMobile')
    .controller('DentalController',['spinner','$scope','dentalSummaryData','$http','$ionicSlideBoxDelegate',function (spinner,$scope,dentalSummaryData,$http,$ionicSlideBoxDelegate) {

        if(dentalSummaryData.data.success && window.localStorage.loggedInUser){
             $scope.dentalData=dentalSummaryData.data.result;
        }else{
            $scope.partners = dentalSummaryData.data.result;
        }

        $scope.truncate = function(n) {
            return parseInt(n);
        };

        $scope.lastSlide = function() {
    	   $ionicSlideBoxDelegate.previous();
		};

		$scope.allSlid = function(slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

		$scope.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
        };

        $scope.partnerList = function(){
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
