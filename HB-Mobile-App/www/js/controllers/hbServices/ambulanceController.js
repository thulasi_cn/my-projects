"use strict";

angular.module('hbMobile')
    .controller('AmbulanceController',['spinner','$scope','ambulanceSummaryData','$http','$ionicSlideBoxDelegate',function (spinner,$scope,ambulanceSummaryData,$http,$ionicSlideBoxDelegate) {
        if(ambulanceSummaryData.data.success && window.localStorage.loggedInUser){
            $scope.ambulance_data=ambulanceSummaryData.data.result;
        }else{
            $scope.partners = ambulanceSummaryData.data.result;
        }
        $scope.truncate = function(n) {
            return parseInt(n);
        };
        $scope.lastSlide = function() {
          $ionicSlideBoxDelegate.previous();
        };
        $scope.nextSlide = function() {
            $ionicSlideBoxDelegate.next();
        };
         $scope.allSlid = function(slideNumber) {
             $ionicSlideBoxDelegate.slide(slideNumber);
         };
        $scope.partnerList = function(){
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
