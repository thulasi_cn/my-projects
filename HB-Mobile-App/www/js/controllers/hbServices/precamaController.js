"use strict";

angular.module('hbMobile')
    .controller('PrecamaController', ['spinner', '$scope', 'PrecamaResultsData', '$http', '$ionicSlideBoxDelegate', 'PrecamaOrderData', 'PrecamaPartnerData', 'ENV', function (spinner, $scope, PrecamaResultsData, $http, $ionicSlideBoxDelegate, PrecamaOrderData, PrecamaPartnerData, ENV) {

        $scope.imagePath = ENV.imagePath;
        if (PrecamaResultsData != false) {
            if (PrecamaResultsData.data) {
                $scope.precamaData = PrecamaResultsData.data;
            }
            if (PrecamaOrderData.data) {
                $scope.precamaCurrentOrders = [];
                $scope.precamaLastOrders = [];

                angular.forEach(PrecamaOrderData.data, function (value, key) {
                    value.preferred_date = value.created_at.split(" ")[0];
                    value.preferred_time = value.created_at.split(" ")[1]
                    if(value.products && value.products[0].product_type != 'Membership'){
                        if (value.isActive == 1) {
                            $scope.precamaCurrentOrders.push(value);
                        }
                        else {
                            $scope.precamaLastOrders.push(value);
                        }
                    }
                });
            }
        }
        if (PrecamaPartnerData.data) {
            $scope.precamaPartner = PrecamaPartnerData.data.data;
        }

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };
        $scope.truncate = function (n) {
            return parseInt(n);
        };
        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };
        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };
        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
