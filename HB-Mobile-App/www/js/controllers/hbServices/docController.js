"use strict";

angular.module('hbMobile')
    .controller('DocController', ['spinner', '$scope', 'docSummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, docSummaryData, $http, $ionicSlideBoxDelegate) {
        if (window.localStorage.loggedInUser) {
            if (docSummaryData.data.success) {
                $scope.docData = docSummaryData.data.result;
            }
            $scope.add_feedback = true;
            $scope.add_feedback_form = false;
            $scope.getRating = function (rate) {
            };
            $scope.showFeedbackForm = function () {
                $scope.add_feedback = false;
                $scope.add_feedback_form = true;
            };
            $scope.getDocFeedback = function (feedbackFrom, ticketid) {
                $scope.feedbackdata = feedbackDataRecv;
                $scope.feedbackdata._token = window.localStorage.corecsrfToken;
                $scope.feedbackdata.ticketid = ticketid;

            };
        }
        else {
            $scope.partners = docSummaryData.data.result;
        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
