"use strict";

angular.module('hbMobile')
    .controller('EyeController', ['spinner', '$scope', 'eyeSummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, eyeSummaryData, $http, $ionicSlideBoxDelegate) {
        if (eyeSummaryData.data.success && window.localStorage.loggedInUser) {
            $scope.eyeData = eyeSummaryData.data.result;
        } else {
            $scope.partners = eyeSummaryData.data.result;
        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
