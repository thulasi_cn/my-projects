"use strict";
angular.module('hbMobile')
    .controller('SpaController', ['spinner', '$scope', 'spaSummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, spaSummaryData, $http, $ionicSlideBoxDelegate) {
        if (spaSummaryData != false && spaSummaryData.data.success) {
            $scope.spaData = spaSummaryData.data.result;
        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };
    }]);
