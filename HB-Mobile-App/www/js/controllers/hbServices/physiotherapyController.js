"use strict";

angular.module('hbMobile')
    .controller('PhysioController', ['spinner', '$scope', 'physioSummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, physioSummaryData, $http, $ionicSlideBoxDelegate) {
        if (physioSummaryData != false && physioSummaryData.data.success) {
            $scope.phySummaryData = physioSummaryData.data.result;

        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

    }]);
