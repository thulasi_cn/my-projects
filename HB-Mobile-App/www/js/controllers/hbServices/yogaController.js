"use strict";

angular.module('hbMobile')
    .controller('YogaController', ['spinner', '$scope', 'yogaSummaryData', '$http', '$ionicSlideBoxDelegate', 'yogaService', function (spinner, $scope, yogaSummaryData, $http, $ionicSlideBoxDelegate, yogaService) {
        if (yogaSummaryData != false && yogaSummaryData.data.success) {
            $scope.summaryData = yogaSummaryData.data.result;
            if (yogaSummaryData.data.result.performance != null) {
                $scope.selectPerformanceMember = yogaSummaryData.data.result.performance[0];
                initFetchPerformance($scope.selectPerformanceMember);
            }

        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        function initFetchPerformance(member) {

            spinner.forPromise(yogaService.getPerformanceDetails(member.contactid)).success(function (memberPerformance) {
                $scope.labels = [];
                $scope.data = [[]];
                angular.forEach(memberPerformance.result.performance_details, function (value, key) {
                    $scope.data[0].push(value.ratings);
                });
                for (var i = 0; i < memberPerformance.result.performance_details.length; i++) {
                    $scope.labels.push(i);
                }
            });
        }

        $scope.fetchPerformance = function (member) {
            initFetchPerformance(member);
        }

    }])
    .controller('YogaSessionController', ['spinner', '$scope', '$http', '$ionicSlideBoxDelegate', 'yogaSessionData', '$stateParams', 'yogaService', function (spinner, $scope, $http, $ionicSlideBoxDelegate, yogaSessionData, $stateParams, yogaService) {

        if (yogaSessionData.data.success) {
            $scope.yogaSession = yogaSessionData.data.result.session_details;
            $scope.yogaTasks = yogaSessionData.data.result.task_details;
            $scope.yogaSessionID = $stateParams.sessionId;

        }
        $scope.rescheduleSession = function (sessionId) {
            var reScheduleData = {};
            reScheduleData._token = window.localStorage.corecsrfToken;
            reScheduleData.id = '37x' + sessionId;
            reScheduleData.cf_941 = 'Reschedule Requested';
            reScheduleData.type = 'post';
            spinner.forPromise(yogaService.sendRescheduleRequest(reScheduleData)).success(function (response) {
                if (response.result.cf_941) {
                    $scope.rescheduleMessage = response.result.cf_941;
                }
                else {
                    $scope.rescheduleMessage = "We cannot reschedule this session now. Please contact Healthybiilions for more support.";
                }
            });
        }
    }]);
