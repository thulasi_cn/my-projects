"use strict";

angular.module('hbMobile')
    .controller('PharmacyController', ['spinner', '$scope', 'PharmacySummaryData', '$http', '$ionicSlideBoxDelegate', 'PharmacyService', '$state', '$cordovaCamera', '$ionicPopup', 'FileUploader', 'ENV', function (spinner, $scope, PharmacySummaryData, $http, $ionicSlideBoxDelegate, PharmacyService, $state, $cordovaCamera, $ionicPopup, FileUploader, ENV) {
        if (window.localStorage.loggedInUser && PharmacySummaryData.data.success) {
            $scope.pharmacyData = PharmacyService.getSummaryAllData();
        } else {
            $scope.partners = PharmacySummaryData.data.result;
        }
        $scope.imagePath = ENV.imagePath;

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.createPharmacyTicket = function () {
            $state.go('dashboard.BuyPharmacyService');
        };

        $scope.takePicture = function (ticketid) {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: true
            };

            $cordovaCamera.getPicture(options).then(function (imageData) {
                $scope.imgURI = "data:image/jpeg;base64," + imageData;
                $scope.tktId = ticketid;
                var fileURL = $scope.imgURI;

                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
                options.mimeType = "image/jpeg";
                options.chunkedMode = true;

                var params = {};
                params._token = window.localStorage.corecsrfToken;
                params.ticketid = ticketid;

                options.params = params;

                var ft = new FileTransfer();
                ft.upload(fileURL, encodeURI(ENV.coreUrl + '/api/ticket/uploadprescription'), function (success) {
                }, function (error) {
                }, options);
            })

        };

        $scope.cancelPicture = function () {
            $scope.imgURI = null;
        };
    }])

    .controller('PharmacyTicketController', ['spinner', '$scope', '$http', '$ionicSlideBoxDelegate', '$stateParams', 'PharmacyService', '$state', 'PharmacyDataTkt', '$ionicPopup', function (spinner, $scope, $http, $ionicSlideBoxDelegate, $stateParams, PharmacyService, $state, PharmacyDataTkt, $ionicPopup) {
        $scope.pharmacyTktData = PharmacyService.getSummaryAllData();
        $scope.savePharmacyTicket = function (formData) {
            formData._token = window.localStorage.corecsrfToken;
            var user = JSON.parse(window.localStorage.loggedInUser);
            formData.parent_id = user.uuid;
            return spinner.forPromise(PharmacyService.saveTicket(formData)).then(function (response) {
                if (response.data == 'Ticket created successfully') {
                    var offlinePopup = $ionicPopup.alert({
                        title: 'Success!',
                        template: 'Ticket created successfully!'
                    });
                    $state.go('dashboard.Pharmacy');
                }
                else {
                    alert('Error!');
                }
            });
        }
    }])

    .controller('PharmacyOrderController', ['spinner', '$scope', '$http', '$ionicSlideBoxDelegate', '$stateParams', 'phamOrderSummaryData', 'PharmacyService', '$state', function (spinner, $scope, $http, $ionicSlideBoxDelegate, $stateParams, phamOrderSummaryData, PharmacyService, $state) {
        $scope.phamOrderData = phamOrderSummaryData;
        $scope.getPhamFeedback = function (feedbackDataRecv, ticketid) {
            feedbackDataRecv._token = window.localStorage.corecsrfToken;
            feedbackDataRecv.ticketid = ticketid;
            return spinner.forPromise(PharmacyService.postPhamFeedBack(feedbackDataRecv)).then(function (response) {
                if (response.data == 1) {
                    PharmacyService.setfeedbackData(feedbackDataRecv, ticketid);
                    PharmacyService.getSummaryAllData();
                    $state.go('dashboard.viewPharmacyOrder');
                }
                else {
                    alert('Error!');
                }
            });
        };
    }
    ]);

