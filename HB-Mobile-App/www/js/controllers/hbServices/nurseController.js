"use strict";

angular.module('hbMobile')
    .controller('NurseController', ['spinner', '$scope', 'nurseSummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, nurseSummaryData, $http, $ionicSlideBoxDelegate) {
        if (nurseSummaryData.data.success && window.localStorage.loggedInUser) {
            $scope.nurseData = nurseSummaryData.data.result;
        } else {
            $scope.partners = nurseSummaryData.data.result;
        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
