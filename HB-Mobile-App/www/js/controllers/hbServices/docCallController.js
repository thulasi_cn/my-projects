"use strict";

angular.module('hbMobile')
    .controller('DocCallController', ['spinner', '$scope', 'docCallSummaryData', '$http', '$ionicSlideBoxDelegate', 'docService', '$state', function (spinner, $scope, docCallSummaryData, $http, $ionicSlideBoxDelegate, docService, $state) {
        if (window.localStorage.loggedInUser && docCallSummaryData) {
            $scope.docCallData = docCallSummaryData;
        } else {
            $scope.partners = docCallSummaryData.data.result;
        }

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.getDocFeedback = function (feedbackDataRecv, ticketid) {
            feedbackDataRecv._token = window.localStorage.corecsrfToken;
            feedbackDataRecv.ticketid = ticketid;
            return spinner.forPromise(docService.postDocFeedBack(feedbackDataRecv)).then(function (response) {
                if (response.data == 1) {
                    docService.setfeedbackData(feedbackDataRecv, ticketid);
                    $state.go('dashboard.doctorOnCall');
                }
                else {
                    alert('Error!');
                }
            });
        };
    }]);