"use strict";

angular.module('hbMobile')
    .controller('GynaeController', ['spinner', '$scope', 'gynaeSummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, gynaeSummaryData, $http, $ionicSlideBoxDelegate) {
        if (gynaeSummaryData.data.success && window.localStorage.loggedInUser) {
            $scope.gynaeData = gynaeSummaryData.data.result;
        } else {
            $scope.partners = gynaeSummaryData.data.result;
        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
