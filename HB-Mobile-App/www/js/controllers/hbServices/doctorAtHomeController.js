"use strict";

angular.module('hbMobile')
    .controller('DoctorAtHomeController', ['spinner', '$scope', 'DoctorAtHomeSummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, DoctorAtHomeSummaryData, $http, $ionicSlideBoxDelegate) {
        if (window.localStorage.loggedInUser && DoctorAtHomeSummaryData.data.success) {
            $scope.DoctorAtHomeSummaryData = DoctorAtHomeSummaryData.data.result;
        } else {
            $scope.partners = DoctorAtHomeSummaryData.data.result;
        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
