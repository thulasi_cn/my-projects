"use strict";

angular.module('hbMobile')
    .controller('DieticianController', ['spinner', '$scope', 'DietSummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, DietSummaryData, $http, $ionicSlideBoxDelegate) {
        if (window.localStorage.loggedInUser && DietSummaryData.data.success) {
            $scope.dietData = DietSummaryData.data.result;
        } else {
            $scope.partners = DietSummaryData.data.result;
        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
