"use strict";
angular.module('hbMobile')
    .controller('TravelEmergencyController', ['spinner', '$scope', 'TravelEmergencySummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, TravelEmergencySummaryData, $http, $ionicSlideBoxDelegate) {
        if (window.localStorage.loggedInUser && TravelEmergencySummaryData.data.success) {
            $scope.travelData = TravelEmergencySummaryData.data.result;
        } else {
            $scope.partners = TravelEmergencySummaryData.data.result;
        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };
    }]);
