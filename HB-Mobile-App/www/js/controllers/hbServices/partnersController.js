"use strict";

angular.module('hbMobile')
    .controller('PartnersController', ['spinner', '$scope', 'partners', '$http', '$ionicSlideBoxDelegate', 'consultants', function (spinner, $scope, partners, $http, $ionicSlideBoxDelegate, consultants) {
        if (partners.data) {
            $scope.partners = partners.data.result.partners;
        }
        if (consultants.data) {
            $scope.consultants = consultants.data.result;
        }
    }]);
