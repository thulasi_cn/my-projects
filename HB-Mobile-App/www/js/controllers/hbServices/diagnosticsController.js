"use strict";
angular.module('hbMobile')
    .controller('DiagnosticsController', ['spinner', '$scope', 'yogaSummaryData', '$ionicSlideBoxDelegate', function (spinner, $scope, yogaSummaryData, $ionicSlideBoxDelegate) {
        if (yogaSummaryData != false && yogaSummaryData.data.success) {
            $scope.summaryData = yogaSummaryData.data.result;
        }

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.partnerList = function () {
            $ionicSlideBoxDelegate.slide(1);
        };

    }]);
