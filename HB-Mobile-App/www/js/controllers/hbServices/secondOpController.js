"use strict";

angular.module('hbMobile')
    .controller('SecondOpController', ['spinner', '$scope', 'secondOpSummaryData', '$http', '$ionicSlideBoxDelegate', function (spinner, $scope, secondOpSummaryData, $http, $ionicSlideBoxDelegate) {
        if (secondOpSummaryData != false && secondOpSummaryData.data.success) {
            $scope.secondOpData = secondOpSummaryData.data.result;
        }

        $scope.truncate = function (n) {
            return parseInt(n);
        };

        $scope.allSlid = function (slideNumber) {
            $ionicSlideBoxDelegate.slide(slideNumber);
        };

        $scope.lastSlide = function () {
            $ionicSlideBoxDelegate.previous();
        };

        $scope.nextSlide = function () {
            $ionicSlideBoxDelegate.next();
        };
    }]);
