'use strict';

angular.module('hbMobile')
    .controller('SettingsController', ['$scope', '$rootScope', '$state', 'spinner','$ionicPopup','userService', function ($scope, $rootScope, $state, spinner, $ionicPopup,userService) {
            $scope.changePasswordData={};
            var loggedInUser = angular.isString(window.localStorage.loggedInUser) ? JSON.parse(window.localStorage.loggedInUser) : {};
            $scope.loggedInUser = loggedInUser;
            $scope.sendChangePassword = function(passwordChangeForm){
                if (passwordChangeForm.$valid) {
                    var postData = angular.copy($scope.changePasswordData);
                    postData['_token'] = window.localStorage.corecsrfToken;
                    postData['user_id'] = loggedInUser.id;
                    postData['auth_source'] = loggedInUser.auth_source;
                    spinner.forPromise(userService.changePassword(postData)).then(function(response) {
                        loggedInUser.auth_source='web';
                        window.localStorage.loggedInUser = JSON.stringify(loggedInUser);
                        $rootScope.loggedInUser = JSON.parse(window.localStorage.loggedInUser);
                        if(window.localStorage.loginCredential){
                            var loginCredential=JSON.parse(window.localStorage.loginCredential);
                            loginCredential.password=postData.password;
                            window.localStorage.loginCredential = JSON.stringify(loginCredential);
                        }
                        $scope.changePasswordData={};
                        passwordChangeForm.$setPristine(true);
                        var passwordPopup = $ionicPopup.alert({
                            title: 'Password Change',
                            template: response.data
                        });
                    },function(response){
                        $scope.changePasswordData={};
                        passwordChangeForm.$setPristine(true);
                        var errorMessgae = "";
                        if (response.data.password)
                            errorMessgae = response.data.password[0];

                        if (response.data.old_password)
                            errorMessgae = response.data.old_password[0];

                        var passwordPopup = $ionicPopup.alert({
                            title: 'Error!',
                            template: errorMessgae
                        });      
                    });
                }else{
                    return false;
                }
            };
            $scope.pushNotification = true;
            $scope.pushNotificationChange = function(pushNotification) {
                if (pushNotification == true) {
                    IonicPushNotification.notificationEnable();
                }else{
                    IonicPushNotification.notificationDisable();
                }
            };
    }]);