"use strict";

angular.module('hbMobile')
    .controller('UserController', ['spinner', '$scope', 'userService', '$state', '$ionicPopup', 'memberService', '$cordovaOauth', 'ENV', '$location', '$rootScope','IonicPushNotification', 'chatService','signUpService','membershipData','$ionicLoading','sessionService',function(spinner, $scope, userService, $state, $ionicPopup, memberService, $cordovaOauth, ENV, $location, $rootScope,IonicPushNotification, chatService, signUpService, membershipData,$ionicLoading, sessionService) {

        if(window.localStorage.loggedInUser){
            $rootScope.loggedInUser = JSON.parse(window.localStorage.loggedInUser);
            $scope.imageUrl = ENV.imagePath + '/member/profile/thumb/' +  $rootScope.loggedInUser.member.uuid + '.jpg';
        }
        $scope.loginData = {};
        $scope.phoneNumber = ENV.phoneNumber;

        $scope.toggleChat = function () {
            chatService.startChat();
        };

        $scope.shareNative = function() {
            var appStoreURL = ionic.Platform.isAndroid() ? ENV.share.androidURL : ENV.share.iosURL;
            window.plugins.socialsharing.share(ENV.share.message, ENV.share.title, null, appStoreURL);
        };

        $scope.rateUs = function(){
            AppRate.navigateToAppStore();
        };

        var getZones = function (response) {
            $scope.zones = response.data;
        };

        var getZoneError = function (response) {
        };

        $scope.returnDashBoard = function () {
            $state.go('dashboard.physical');
        };

        $scope.editProfile = function () {
            $scope.errors = {};
            $scope.zones = {};
            spinner.forPromise(userService.getZones()).then(getZones, getZoneError);
            $state.go('dashboard.editProfile');
        };

        $scope.updateAccount = function () {
            $scope.successMsg = '';
            $scope.errors = {};
            if($scope.profileData.member.dateOfBirth)
                $scope.profileData.member.dob=moment($scope.profileData.member.dateOfBirth).format("YYYY-MM-DD");
            var postData = angular.copy($scope.profileData.member);
            postData['_token'] = window.localStorage.corecsrfToken;
            spinner.forPromise(memberService.updateMember(postData)).then(
                updateAccountSuccess,
                updateAccountError
            );
        };

        var updateAccountSuccess = function (response) {
            $scope.successMsg = "Account Updated Successfully.";
            $scope.profileData.member = response.data;
            $scope.viewProfileData.member = response.data;
            $rootScope.loggedInUser.member = response.data;
            var data = JSON.parse(window.localStorage.loggedInUser);
            data.member = response.data;
            window.localStorage.loggedInUser = JSON.stringify(data);
            $state.go('dashboard.viewProfile');

        };

        var updateAccountError = function (response) {
            return angular.forEach(response.data, function (errors, field) {
                return $scope.errors[field] = errors.join(', ');
            });
        };

        $scope.logoutUser = function () {
            sessionService.logout();
        };

        $scope.viewMyProfile = function () {
            $scope.profileData = JSON.parse(window.localStorage.loggedInUser);
            $scope.viewProfileData = angular.copy($scope.profileData);
            $scope.successMsg = '';
            $state.go('dashboard.viewProfile');
        };

        $rootScope.notification_icon = ionic.Platform.isIOS();

        $scope.CallNumber = function () {
            window.plugins.CallNumber.callNumber(function () {}, function () {}, $scope.phoneNumber);
        };

        $scope.membershipDetails = membershipData;
        $scope.upgradeButton=true;
        if ($scope.membershipDetails && $scope.membershipDetails.membership_type.name == "Platinum") {
            $scope.upgradeButton=false;
        };

        $scope.upgradeMembership = function(){
            $state.go('subscription.list');
        }
    }]);
