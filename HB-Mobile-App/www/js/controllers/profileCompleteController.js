'use strict';
angular.module('hbMobile')
    .controller('ProfileCompleteController', ['$scope', 'userService', '$state', '$location', 'spinner', '$ionicPopup', '$rootScope', '$ionicModal', 'memberService', function ($scope, userService, $state, $location, spinner, $ionicPopup, $rootScope, $ionicModal, memberService) {

        $scope.profileCompleteData = angular.isString(window.localStorage.loggedInUser) ? JSON.parse(window.localStorage.loggedInUser) : {};
        if (document.getElementById("passwordChangeForm") != null)
            document.getElementById("passwordChangeForm").reset();
        $scope.message = "";

        var getZones = function (response) {
            $scope.zones = response.data;
        };

        var getZoneError = function (err) {
            
        };

        spinner.forPromise(userService.getZones()).then(getZones, getZoneError);

        $scope.completeProfile = function (profileCompleteForm) {
            if (profileCompleteForm.$valid) {
                $scope.errors = {};
                $scope.loggedInUser.member.phone = $scope.profileCompleteData.phone;
                $scope.loggedInUser.phone = $scope.profileCompleteData.phone;
                $scope.loggedInUser.email = $scope.profileCompleteData.email;
                $scope.loggedInUser.member.city = $scope.profileCompleteData.city;
                $scope.loggedInUser.member.post_code = $scope.profileCompleteData.post_code;
                $scope.loggedInUser.member.gender = $scope.profileCompleteData.gender;
                var postData = angular.copy($scope.loggedInUser);
                postData['_token'] = window.localStorage.corecsrfToken;
                spinner.forPromise(memberService.updateMemberSocial(postData)).then(function (response) {
                        window.localStorage.loggedInUser = JSON.stringify(response.data);
                        $state.go('dashboard.physical');
                    },
                    function (response) {
                        var errorMessage = '';
                        if (response.data.phone) {
                             errorMessage = "This phone number is already taken"
                        }
                        else if (response.data.email) {
                            errorMessage = "This email is already taken"
                        }
                        else {
                            errorMessage = "Login failed"
                        }
                        var errorPopup = $ionicPopup.alert({
                            title: 'Login Failed',
                            template: errorMessage
                        });

                    }
                );
            }
        }
    }]);

