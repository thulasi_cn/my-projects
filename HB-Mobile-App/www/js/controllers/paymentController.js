'use strict';

angular.module('hbMobile')
    .controller('PaymentController', ['$scope', 'paymentService','orderService','sessionService', function ($scope, paymentService, orderService, sessionService) {

        var orderDetails = orderService.getData();
        var loggedInUser = JSON.parse(window.localStorage.loggedInUser);

        $scope.paymentSuccess = false;
        $scope.paymentFailure = false;

        var paymentSuccess = function (response){
            if(response.payment_status == 'success'){
                $scope.paymentSuccess = true;
            }
            else{
                $scope.paymentFailure = true;
            }
            sessionService.loginSavedUser(false);
        };

        var paymentFailure = function(response){
            $scope.paymentFailure = true;
            sessionService.loginSavedUser(false);
        };

        if (!_.isEmpty(orderDetails)) {
            paymentService.payment(loggedInUser.id, orderDetails.id).then(paymentSuccess, paymentFailure);
        }
    }]);
