'use strict';
angular.module('hbMobile')
    .controller('SignUpController', ['$scope', 'userService', '$state', '$location', 'spinner', '$ionicPopup', '$rootScope', '$ionicModal','memberService','IonicPushNotification','signUpService','orderService', function($scope, userService, $state, $location, spinner, $ionicPopup, $rootScope, $ionicModal,memberService,IonicPushNotification,signUpService,orderService) {
        $scope.signUpData = signUpService.getData();

        $scope.profileCompleteData = angular.isString(window.localStorage.loggedInUser) ? JSON.parse(window.localStorage.loggedInUser) : {};
        if (document.getElementById("passwordChangeForm") != null)
            document.getElementById("passwordChangeForm").reset();

        $scope.message = "";
        $scope.loginData = {};


        var loginOrReset = function(message) {
            $scope.errorMsg = message;
            $scope.loginOrResetPopup = $ionicPopup.show({
                templateUrl: 'templates/accounts/forgot_password_popup.html',
                title: 'Account Already Exists',
                scope: $scope,
                buttons: [{
                    text: '<b>Cancel</b>'
                }, {
                    text: '<b>Login</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        $scope.loginOrResetPopup.close();
                        $state.go('account.login');
                    }
                }]
            });
            $scope.loginOrResetPopup.then(function(res) {

            });

        };
        $scope.closeAndRedirect = function() {
            $scope.loginOrResetPopup.close();
            $state.go('account.forgotPassword');
        };
        var errorPopup = function(message) {

            var alertPopup = $ionicPopup.alert({
                title: 'Validation Error',
                template: message
            });
            alertPopup.then(function() {});
        };

        var invalidUser = function(response) {
            if (response.data) {
                if (response.data.error == "validation")
                    errorPopup(response.data.msg);
                else if (response.data.error == 'activated')
                    loginOrReset(response.data.msg);
                else {
                    errorPopup(response.data.login);
                }
            } else {
                $rootScope.alertMsg = "Error in web service";
            }


        };

        var validUser = function(response) {
            if (response.data.status == 'sendactivationcode') {
                signUpService.setData(angular.isObject(response.data['user']) ? response.data['user'] : {});
                $state.go('account.activate');

            } else if (response.data.status == 'newuser') {
                if (response.data.loginField == 'email') {
                    $scope.signUpData.email = $scope.signUpData.login;
                } else if (response.data.loginField == 'phone') {
                    $scope.signUpData.phone = parseInt($scope.signUpData.login);
                }
                signUpService.setData(angular.isObject($scope.signUpData) ? $scope.signUpData : {});

              $state.go('account.register');
            }
        };

        $scope.validatePhoneEmail = function() {
            var postData = $scope.signUpData;
            postData._token = window.localStorage['corecsrfToken'];
            spinner.forPromise(userService.checkUserByPhoneOrEmail(postData)).then(validUser, invalidUser)
        };

        $scope.activationData = {};


        if(!_.isEmpty(signUpService.getData())){
          $scope.activationData.phone = signUpService.getData().phone;
        }

        $scope.activateAccount = function(acForm) {
            if (acForm.$valid) {
                var postData = {};
                postData = angular.copy($scope.activationData);
                postData['_token'] = window.localStorage.corecsrfToken;
                spinner.forPromise(userService.activateuser(postData)).then(
                    activationSuccess,
                    errorActivation
                );
            } else {
                var errorPopup = $ionicPopup.alert({
                    title: 'Invalid Entry',
                    template: 'Please fill all fields.'
                });
                errorPopup.then(function(res) {});
            }
        };

        var activationSuccess = function(response) {
            window.localStorage.loggedInUser = JSON.stringify(response.data);
            $scope.userData = response.data;
            signUpService.setData({});
            if(document.getElementById("acForm")) document.getElementById("acForm").reset();                
            $scope.activationData={};
            window.localStorage.loggedInUser = JSON.stringify(response.data);
            $rootScope.loggedInUser = JSON.parse(window.localStorage.loggedInUser);

            window.localStorage.loginCredential = JSON.stringify({
                login: response.data.phone,
                password: $scope.activationData.password
            });

            $state.go('dashboard.physical');
        };

        var errorActivation = function(response) {
            $scope.errors = {};
            return angular.forEach(response.data, function(errors, field) {
                return $scope.errors[field] = angular.isObject(errors) ? errors.join() : errors;
            });
        };


        //New user Registration Save
        $scope.save = function(signupform) {
            if(signupform.$valid){
                if(document.getElementById("subscriptionForm")) document.getElementById("subscriptionForm").reset();
                $scope.errors = {};
                var postData = angular.copy($scope.signUpData);
                if($scope.signUpData.id=="") delete postData.id;
                spinner.forPromise(IonicPushNotification.initialize().then(function(){
                    if (postData.password == postData.confirmed_password) {
                        postData.device_id = $rootScope.deviceID;
                        postData.device_token = $rootScope.deviceToken;
                        var orderDetails =orderService.getData();
                        postData['_token'] = window.localStorage.corecsrfToken;
                           spinner.forPromise(userService.createUser(postData)).then(
                           newUserSuccess,
                           newUserError
                        );
                    } else {
                        return $scope.errors['confirmed_password'] = 'Password and Confirm Password do not match';
                    }
                })
                );
            }
        };

        var guestLoginSuccess = function(response) {
            signUpService.setData(response.data);
            window.localStorage.guestUser=JSON.stringify(signUpService.getData());
            $state.go('subscription.payment');
        };



        var newUserSuccess = function(response) {
            IonicPushNotification.updateIonicUser(response);
            window.localStorage.loggedInUser = JSON.stringify(response.data);
            window.localStorage.loginCredential = JSON.stringify($scope.loginData);
            $rootScope.loggedInUser = JSON.parse(window.localStorage.loggedInUser);
            if (!_.isEmpty(orderService.getData())) {
                var orderDetails =orderService.getData();
                if (orderDetails.price == 0) {
                    $state.go('dashboard.physical');
                } else {
                    $state.go('subscription.payment');
                }
            } else {
                $state.go('dashboard.physical');
            }
        };


        var newUserError = function(response) {
            return angular.forEach(response.data, function(errors, field) {
                return $scope.errors[field] = errors.join(', ');
            });
        };


        $scope.sendResetCode = function(passwordFormData) {
            $scope.error = false;
            var postData = angular.copy(passwordFormData);
            postData['_token'] = window.localStorage.corecsrfToken;
            spinner.forPromise(userService.forgotPassword(postData)).then(function(response) {
                $state.go('account.forgotConfirmation');
            }, function(response) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Incorrect Email',
                    template: response.data.email
                });
                alertPopup.then(function(res) {
                    return;
                });
            });
        };
    }]);
