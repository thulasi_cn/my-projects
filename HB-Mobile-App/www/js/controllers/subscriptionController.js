  "use strict";

  angular.module('hbMobile')
      .controller('SubscriptionController', ['spinner','$scope', '$rootScope','memberShipListData', '$state', 'orderService', 'memberService',function (spinner,$scope, $rootScope, memberShipListData, $state, orderService,memberService) {

          $scope.membershipType = memberShipListData.data;
          $scope.planPrice = $scope.membershipType[0].price;
          $scope.membershipData = $scope.membershipType[0];
          $scope.userData = JSON.parse(window.localStorage.loggedInUser);
          if($scope.membershipData.id == $scope.userData.membership_type_id){$scope.selectData ="Current Plan";}
          else{$scope.selectData ="Pay Now";}

          $scope.updatePrice = function (slideIndex) {

              $scope.membershipData = $scope.membershipType[slideIndex];
              $scope.planPrice = $scope.membershipType[slideIndex].price;
              $scope.membershipData = $scope.membershipType[slideIndex];
              if($scope.membershipData.id == $scope.userData.membership_type_id){$scope.selectData ="Current Plan";}
              else if($scope.planPrice == 0){$scope.selectData ="Select Plan";}
              else{$scope.selectData ="Pay Now";}
          };

          $scope.selectMemberShip = function () {
              if (($scope.membershipData.length) > 0) {
                  $scope.membershipData = $scope.membershipData[0];
              }
              orderService.setData($scope.membershipData);
              var orderDetails = orderService.getData();
              var membershipData={};
              membershipData.user_id=$scope.userData.id;
              membershipData.membership_type_id=$scope.membershipData.id;
              membershipData._token = window.localStorage.corecsrfToken;
              if (orderDetails.price == 0) {
                spinner.forPromise(memberService.upgradeMember(membershipData)).then(function (response) {
                  $state.go('dashboard.physical')
                  });
                } else {
                  $state.go('subscription.payment');
              }
          }
      }]);
