"use strict";

angular.module('hbMobile')
    .controller('LoginController', ['spinner', '$scope', '$ionicPopup','sessionService', function(spinner, $scope, $ionicPopup, sessionService) {
        $scope.loginData = {};

        $scope.loginUser = function (loginForm) {
            if (loginForm.$valid) {
                sessionService.webLogin($scope.loginData.login, $scope.loginData.password);
            } else {
                $ionicPopup.alert({
                    title: 'Required Field',
                    template: 'Both the fields required.'
                })
            }
        };

    }]);
