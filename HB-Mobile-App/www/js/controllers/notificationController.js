"use strict";

angular.module('hbMobile')
    .controller('notificationListController',['$scope','notificationListData', function ($scope,notificationListData) {
           $scope.notificationList = notificationListData.data;
        }
 ]).controller('notificationController',['$scope','notificationData', function ($scope,notificationData) {
                $scope.notificationDetails = notificationData.data;
        }
 ]);
