'use strict';

angular.module('hbMobile')
    .factory('categoryService', ['$q', '$http', 'ENV', function ($q, $http, ENV) {
        var data = [];

        var getData = function () {
            var deferred = $q.defer();
            if (_.isEmpty(data)) {
                $http.get(ENV.coreUrl + '/servicecategories/all').then(function (response) {
                    var count = 0;
                    angular.forEach(response.data, function (value, key) {
                        count++;
                        data.push({id: value.id, name: value.servicecategoriesname, slug: value.slug});
                        if (count == response.data.length) {
                            deferred.resolve(data);
                        }
                    })
                });
            }
            else {
                deferred.resolve(data);
            }
            return deferred.promise;
        };

        var setData = function (dataToSet) {
            for (var i = 0; i < dataToSet.data.length; i++) {
                data.push({id: dataToSet.data[i].sync.reference_id, name: dataToSet.data[i].servicecategoriesname, slug: dataToSet.data[i].slug});
            }
        };

        return {
            getData: getData,
            setData: setData
        }
    }]);

