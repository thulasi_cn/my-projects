"use strict";

angular.module('hbMobile')
    .factory('sessionService', ['userService', 'spinner', 'IonicPushNotification', '$ionicPopup', '$state', '$rootScope', '$ionicLoading', function(userService, spinner, IonicPushNotification, $ionicPopup, $state, $rootScope, $ionicLoading) {
        var loginData = {
            device_id: '',
            device_token: '',
            login: '',
            password: '',
            auth_source: '',
            email: '',
            socialId: ''
        };

        var isStateChangeAllowed = true;

        var loginSuccess = function(response) {
            $rootScope.loggedInUser = response.data;
            window.localStorage.loggedInUser = JSON.stringify(response.data);
            window.localStorage.loginCredential = JSON.stringify(loginData);
            IonicPushNotification.updateIonicUser(response);
            if((response.data.email.length=='') ||(response.data.phone.length=='')) {
                isStateChangeAllowed = false;
                $state.go('registration.complete');
            }
            if (isStateChangeAllowed)
                $state.go('dashboard.physical');
            else
                isStateChangeAllowed = true;
        };

        var loginError = function(response) {
            $ionicPopup.alert({
                title: 'Error Logging In',
                template: response.data
            }).then(function() {
                $state.go('account.login');
            });
        };

        var webLogin = function(login, password) {
            loginData.login = login;
            loginData.password = password;
            loginData.auth_source = 'web';
            loginData.device_id = $rootScope.deviceID;
            loginData._token = window.localStorage.corecsrfToken;

            spinner.forPromise(IonicPushNotification.initialize().then(
                function(name) {
                 loginData.device_token = $rootScope.deviceToken;
                    spinner.forPromise(userService.loginUser(loginData), 'Logging In..').then(loginSuccess, loginError);
                },
                function(result) {
                    console.log("Failed to get the device Token, result is " + result);
                }))
        };

        //TODO - Remove loginData.id from socialLogin after core changes have gone to production
        var socialLogin = function(socialId, email, firstName, lastName, gender, imageUrl, auth_source) {
            delete window.localStorage.corecsrfToken;
            loginData.device_id = $rootScope.deviceID;
            loginData.device_token = $rootScope.deviceToken;
            loginData.id = socialId;
            loginData.socialId = socialId;
            loginData.email = email;
            loginData.first_name = _.capitalize(firstName);
            loginData.last_name = _.capitalize(lastName);
            loginData.gender = _.capitalize(gender);
            loginData.auth_source = auth_source;
            loginData.imageUrl = imageUrl;
            spinner.forPromise(userService.socialConnect(loginData)).then(loginSuccess, loginError);
        };

        var logout = function() {
            spinner.forPromise(userService.logoutUser($rootScope.deviceID)).then(function() {
                delete window.localStorage.loginCredential;
                delete window.localStorage.loggedInUser;
                delete window.localStorage.corecsrfToken;

                IonicPushNotification.unRegister();

                delete window.localStorage.ionic_io_user_d9e182f6;
                $rootScope.loggedInUser = null;

                if ($rootScope.deviceID) {
                    facebookConnectPlugin.logout(function() {
                            $ionicLoading.hide();
                            $state.go('account.login');
                        },
                        function(fail) {
                            $ionicLoading.hide();
                        });
                    window.plugins.googleplus.disconnect({},
                        function(err) {
                            alert(err);
                        }
                    );
                };

                $state.go('account.login');
            });
        };

        var loginSavedUser = function(allowStateChange) {
            isStateChangeAllowed = typeof allowStateChange !== 'undefined' ? allowStateChange : true;
            if (window.localStorage.loginCredential) {
                delete window.localStorage.corecsrfToken;
                var loginCredentials = JSON.parse(window.localStorage.loginCredential);
                if (loginCredentials.auth_source == 'web')
                    webLogin(loginCredentials.login, loginCredentials.password);
                else
                    socialLogin(loginCredentials.socialId, loginCredentials.email, loginCredentials.first_name, loginCredentials.last_name, loginCredentials.gender, loginCredentials.imageUrl, loginCredentials.auth_source);
            }
        };

        return {
            webLogin: webLogin,
            socialLogin: socialLogin,
            loginSavedUser: loginSavedUser,
            logout: logout
        }
    }]);