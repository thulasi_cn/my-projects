"use strict";

angular.module('hbMobile').factory('orderService', [function () {
    var data = {
        "id": "",
        "name": "",
        "period": "",
        "price": "",
        "description": "",
        "uuid": ""
    };

    var getData = function () {
        return data;
    };

    var setData = function (dataToSet) {
        data.id = dataToSet.id ? dataToSet.id : data.id;
        data.name = dataToSet.name ? dataToSet.name : data.name;
        data.period = dataToSet.period ? dataToSet.period : data.period;
        data.price = dataToSet.price ? dataToSet.price : data.price;
        data.description = dataToSet.description ? dataToSet.description : data.description;
        data.uuid = dataToSet.uuid ? dataToSet.uuid : data.uuid;
    };

    return {
        getData: getData,
        setData: setData
    }
}]);