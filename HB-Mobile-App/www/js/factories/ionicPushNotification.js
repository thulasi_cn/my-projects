'use strict';

angular.module('hbMobile')
    .factory('IonicPushNotification', ['$rootScope','$state','$q','$http','ENV','spinner','userService', function($rootScope,$state,$q,$http,ENV,spinner,userService){
     Ionic.io();
     var push = new Ionic.Push({
         "onNotification": function(notification) {
             if(window.localStorage.loginCredential){
                     $state.go('dashboard.notification');
             }
         },
         "onRegister": function(data) {
             console.log('OnRegister : '+data.token);
         }
     });
     var user = Ionic.User.current();

     var initialize = function()
     {
         var deferred = $q.defer();
         if($rootScope.deviceID) {
             user.id = $rootScope.deviceID;
             user.save().then(
                 function () {
                     var callback = function (data) {
                         push.addTokenToUser(user);
                         $rootScope.deviceToken = data.token;
                         user.save().then(function () {
                         deferred.resolve();
                         });
                     };
                     push.register(callback);
                 },
                 function (response) {
                     if ($rootScope.deviceID)
                         deferred.reject(response);
                     else
                         deferred.resolve();
                 }
             );
         }
         else {
             deferred.resolve("Promise Resolve as there is no device Id");
         }
         return deferred.promise;
     };

     var updateIonicUser = function(ionicUser)
     {
         var deferred = $q.defer();

         user.set('phone', ionicUser.data.phone);
         user.set('email', ionicUser.data.email);
         user.save().then(
             function(responce){
                 deferred.resolve();
             },
             function(){
                 deferred.reject(response);
             }
         );
         return deferred.promise;
     };

     var unRegister = function()
     {
         user.unset('phone');
         user.unset('email');
         user.removePushToken($rootScope.deviceToken);
         user.save();
     };

     var notificationDisable = function(){

        push.unregister().then(function(){
            var deviceData = {};
                     deviceData.device_id = $rootScope.deviceID;
                     deviceData._token = window.localStorage.corecsrfToken;
            spinner.forPromise(userService.unregisterDevice(deviceData));
        });
     };

     var notificationEnable = function(){
            user.removePushToken($rootScope.deviceToken);
            user.save().then(function(){
                var callback = function (pushToken) {
                     user.addPushToken(pushToken);
                     var deviceData = {};
                     deviceData.device_id = $rootScope.deviceID;
                     deviceData.device_token = pushToken.token;
                     deviceData.user_id = $rootScope.loggedInUser.id;
                     deviceData._token = window.localStorage.corecsrfToken;
                     $rootScope.deviceToken = pushToken.token;
                     user.save().then(function(){
                        spinner.forPromise(userService.registerDevice(deviceData));
                     });
                     
                };
                push.register(callback);
        });
        

     };

    return {
        initialize : initialize,
        updateIonicUser : updateIonicUser,
        unRegister : unRegister,
        notificationDisable : notificationDisable,
        notificationEnable : notificationEnable
    };
}]);
