'use strict';

angular.module('hbMobile').factory('signUpService', [function () {
    var data = {
        "id": "",
        "phone": "",
        "email": "",
        "first_name": "",
        "last_name": "",
        "password": "",
        "confirmed_password": "",
        "accept_term_status": false
    };

    var getData = function () {
        return data;
    };

    var setData = function (dataToSet) {
        if (_.isEmpty(dataToSet))
            return data = {
                "id": "",
                "phone": "",
                "email": "",
                "first_name": "",
                "last_name": "",
                "password": "",
                "confirmed_password": "",
                "accept_term_status": false
            };
        data.id = dataToSet.id ? dataToSet.id : data.id;
        data.phone = dataToSet.phone ? dataToSet.phone : data.phone;
        data.email = dataToSet.email ? dataToSet.email : data.email;
        data.first_name = dataToSet.first_name ? dataToSet.first_name : data.first_name;
        data.last_name = dataToSet.last_name ? dataToSet.last_name : data.last_name;
        data.password = dataToSet.password ? dataToSet.password : data.password;
        data.confirmed_password = dataToSet.confirmed_password ? dataToSet.confirmed_password : data.confirmed_password;
        data.accept_term_status = dataToSet.accept_term_status ? dataToSet.accept_term_status : data.accept_term_status;
    };

    return {
        getData: getData,
        setData: setData
    }
}]);

