'use strict';

angular.module('hbMobile', ['ionic','ionic.service.core', 'hbMobile.constants', 'ngCordova', 'tabSlideBox', 'angularMoment', 'chart.js', 'angularFileUpload'])
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'ENV', '$ionicConfigProvider', function ($stateProvider, $urlRouterProvider, $httpProvider, ENV, $ionicConfigProvider) {

        if (!ionic.Platform.isIOS())
            $ionicConfigProvider.scrolling.jsScrolling(false);

/*=================================== Account =====================================================*/

        $stateProvider
            .state('account', {
                url: '',
                abstract: true,
                templateUrl: 'templates/accounts.html'

            })
            .state('account.login', {
                url: '/login',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/accounts/login_page.html',
                        controller: 'LoginController'
                    }
                },
                resolve: {
                    loggedInData: ['sessionService', function (sessionService) {
                        sessionService.loginSavedUser();
                    }]
                }
            })

            .state('account.terms', {
                url: '/signup/terms',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/accounts/terms.html',
                        controller: 'SignUpController'
                    }
                }
            })

            .state('account.activate', {
                url: '/signup/activate',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/accounts/activate_account.html',
                        controller: 'SignUpController'
                    }
                }
            })
            .state('account.forgotPassword', {
                url: '/forgot_password',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/accounts/forgot_password.html',
                        controller: 'SignUpController'
                    }
                }
            })

            .state('account.confirmation', {
                url: '/signup/confirmation',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/accounts/confirmation.html',
                        controller: 'SignUpController'
                    }
                }
            })

            .state('account.forgotConfirmation', {
                url: '/forgot_password/activation_link_sent',

                views: {
                    'mainContent': {
                        templateUrl: 'templates/accounts/password_confirmation.html',
                        controller: 'SignUpController'
                    }
                }
            })

            .state('account.settings', {
                url: '/account/settings',
                cache: false,

                views: {
                    'mainContent': {
                        templateUrl: 'templates/accounts/settings.html',
                        controller: 'SettingsController'
                    }
                }
            })

/*=================================== Registration =====================================================*/

            .state('registration', {
                url: '/registration',
                abstract: true,
                templateUrl: 'templates/accounts.html'
            })
            .state('registration.signup', {
                url: '/signup',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/registration/registration.html',
                        controller: 'RegistrationController'
                    }
                }
            })
            .state('registration.complete', {
                url: '/signup/complete',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/registration/profile_complete.html',
                        controller: 'ProfileCompleteController'
                    }
                }
            })

/*=================================== Subscription =====================================================*/

            .state('subscription', {
                url: '/subscribe',
                abstract: true,
                templateUrl: 'templates/subscription.html'
            })
            .state('subscription.list', {
                url: '/list',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/subscription/list.html',
                        controller: 'SubscriptionController'
                    }
                },
                resolve: {
                    memberShipListData: ['userService', 'spinner', function (userService, spinner) {
                        return spinner.forPromise(userService.getMembershipTypes()).then(function (response) {
                            return response;
                        });
                    }]
                }
            })
            .state('subscription.payment', {
                url: '/payment',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/subscription/payment.html',
                        controller: 'PaymentController'
                    }
                }
            })

/*=================================== Dashboard =====================================================*/

            .state('dashboard', {
                url: '/dashboard',
                abstract: true,
                cache: false,
                templateUrl: 'templates/menu.html',
                controller: 'UserController',
                resolve: {membershipData: [function(){return false}]}
            })
            .state('dashboard.physical', {
                url: '',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/dashboard/dashboard.html',
                        controller: 'UserController'
                    }
                }
            })
            .state('dashboard.notification', {
                url: '/notification',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/dashboard/notification.html',
                         controller: 'notificationListController',
                       resolve: {
                           notificationListData: ['notificationService', 'spinner',function (notificationService, spinner) {
                                return spinner.forPromise(notificationService.getAllNotifications()).then(function (response){
                                    return response;
                                });
                           }]
                       }
                    }
                }
            })
            .state('dashboard.notificationDetails', {
               url: '/notificationDetails/:notificationId',
               views: {
                   'mainContent': {
                       templateUrl: 'templates/dashboard/notificationDetails.html',
                       controller: 'notificationController',
                       resolve: {
                           notificationData: ['notificationService', 'spinner', '$stateParams', function (notificationService, spinner, $stateParams) {
                                return spinner.forPromise(notificationService.getNotification($stateParams.notificationId));
                           }]
                       }
                   }
                }
             })
            .state('dashboard.viewProfile', {
                url: '/viewProfile',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/user/viewProfile.html'
                    }
                }
            })

            .state('dashboard.editProfile', {
                url: '/editProfile',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/user/editProfile.html'
                    }
                }
            })
            .state('dashboard.membership', {
                url: '/membership',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/user/membership.html',
                        controller: 'UserController',
                        resolve: {
                            membershipData: ['spinner', 'userService', function (spinner, userService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(userService.getMembership(user.member.user_id).then(function(response){
                                        return response.data;
                                    }));
                                } else {
                                    return false;
                                }
                            }]
                        }
                    }
                }
            })
            .state('dashboard.yoga', {
                url: '/yoga',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/yoga/yogaDashboard.html',
                        controller: 'YogaController',
                        resolve: {
                            yogaSummaryData: ['spinner', 'yogaService', function (spinner, yogaService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(yogaService.getSessions(user.uuid));
                                } else {
                                    return false;
                                }
                            }]
                        }
                    }
                }
            })
            .state('dashboard.yogaSessionDetails', {
                url: '/yogaSessionDetails/:sessionId',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/yoga/viewSessionDetails.html',
                        controller: 'YogaSessionController',
                        resolve: {
                            yogaSessionData: ['yogaService', 'spinner', '$stateParams', function (yogaService, spinner, $stateParams) {
                                var user = JSON.parse(window.localStorage.loggedInUser);
                                return spinner.forPromise(yogaService.getSessionDetails($stateParams.sessionId, user.uuid));
                            }]
                        }
                    }
                }
            })
            .state('dashboard.physiotherapy', {
                url: '/physiotherapy',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/physiotherapy/physioDashboard.html',
                        controller: 'PhysioController',
                        resolve: {
                            physioSummaryData: ['spinner', 'physioService', function (spinner, physioService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(physioService.getSessions(user.uuid));
                                } else {
                                    return false;
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.spa', {
                url: '/spa',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/spa/spaDashboard.html',
                        controller: 'SpaController',
                        resolve: {
                            spaSummaryData: ['spinner', 'spaService', function (spinner, spaService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(spaService.getSessions(user.uuid));
                                } else {
                                    return false;
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.doctorOnCall', {
                url: '/doctorOnCall',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/doctorOnCall/docDashboard.html',
                        controller: 'DocController',
                        resolve: {
                            docSummaryData: ['spinner', 'docService', function (spinner, docService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(docService.getSessions(user.uuid));
                                } else {
                                    return spinner.forPromise(docService.getPartnersInformation());
                                }
                            }]
                        }
                    }
                }
            })
            .state('dashboard.viewAppointment', {
                url: '/viewAppointment/:id',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/doctorOnCall/viewAppProfile.html',
                        controller: 'DocCallController',
                        resolve: {
                            docCallSummaryData: ['spinner', 'docService', '$stateParams', function (spinner, docService, $stateParams) {
                                return docService.getCallDetailsById($stateParams.id);
                            }]
                        }

                    }
                }
            })

            .state('dashboard.ambulance', {
                url: '/ambulance',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/ambulance/ambulance.html',
                        controller: 'AmbulanceController',
                        resolve: {
                            ambulanceSummaryData: ['spinner', 'AmbulanceService', function (spinner, AmbulanceService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(AmbulanceService.ambulanceSummaryData(user.uuid));
                                } else {
                                    return spinner.forPromise(AmbulanceService.getPartnersInformation());
                                }
                            }]
                        }

                    }
                }
            })

            .state('dashboard.eyecare', {
                url: '/eyecare',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/eyeCare/eyeCareDashboard.html',
                        controller: 'EyeController',
                        resolve: {
                            eyeSummaryData: ['spinner', 'eyeService', function (spinner, eyeService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(eyeService.getSessions(user.uuid));
                                } else {
                                    return spinner.forPromise(eyeService.getPartnersInformation());
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.dentalcare', {
                url: '/dentalcare',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/dentalCare/dentalCareDashboard.html',
                        controller: 'DentalController',
                        resolve: {
                            dentalSummaryData: ['spinner', 'dentalService', function (spinner, dentalService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(dentalService.getSessions(user.uuid));
                                } else {
                                    return spinner.forPromise(dentalService.getPartnersInformation());
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.gynaecare', {
                url: '/gynaecare',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/gynaeCare/gynaeCareDashboard.html',
                        controller: 'GynaeController',
                        resolve: {
                            gynaeSummaryData: ['spinner', 'gynaeService', function (spinner, gynaeService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(gynaeService.getSessions(user.uuid));
                                } else {
                                    return spinner.forPromise(gynaeService.getPartnersInformation());
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.secondOpinion', {
                url: '/secondOpinion',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/secondOpinion/secondOpDashboard.html',
                        controller: 'SecondOpController',
                        resolve: {
                            secondOpSummaryData: ['spinner', 'secondOpService', function (spinner, secondOpService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(secondOpService.getSessions(user.uuid));
                                } else {
                                    return false;
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.nurse', {
                url: '/nurseAtHome',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/nurse/nurseDashboard.html',
                        controller: 'NurseController',
                        resolve: {
                            nurseSummaryData: ['spinner', 'nurseService', function (spinner, nurseService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(nurseService.getSessions(user.uuid));
                                } else {
                                    return spinner.forPromise(nurseService.getPartnersInformation());
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.doctorAtHome', {
                url: '/doctorAtHome',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/doctorAtHome/doctorAtHomeDashboard.html',
                        controller: 'DoctorAtHomeController',
                        resolve: {
                            DoctorAtHomeSummaryData: ['spinner', 'DoctorAtHomeService', function (spinner, DoctorAtHomeService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(DoctorAtHomeService.getDoctorAtHomeServiceData(user.uuid));
                                } else {
                                    return spinner.forPromise(DoctorAtHomeService.getPartnersInformation());
                                }
                            }]
                        }
                    }
                }
            })
            .state('dashboard.Precama', {
                url: '/diagnostic',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/precama/precamaDashboard.html',
                        controller: 'PrecamaController',
                        resolve: {
                            PrecamaOrderData: ['spinner', 'PrecamaService', function (spinner, PrecamaService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(PrecamaService.lastOrders(user.id)).then(function (response) {
                                        return response;
                                    });
                                } else {
                                    return false;
                                }
                            }],

                            PrecamaResultsData: ['spinner', 'PrecamaService', function (spinner, PrecamaService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(PrecamaService.getResults());
                                } else {
                                    return false;
                                }
                            }],

                            PrecamaPartnerData: ['spinner', 'PrecamaService', function (spinner, PrecamaService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(PrecamaService.precamaPartners());
                                } else {
                                    return spinner.forPromise(PrecamaService.precamaPartners());
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.TravelEmergency', {
                url: '/TravelEmergency',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/travelEmergency/travelEmergencyDashboard.html',
                        controller: 'TravelEmergencyController',
                        resolve: {
                            TravelEmergencySummaryData: ['spinner', 'TravelEmergencyService', function (spinner, TravelEmergencyService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(TravelEmergencyService.getTravelEmergencyServiceData(user.uuid));
                                } else {
                                    return spinner.forPromise(TravelEmergencyService.getPartnersInformation());
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.Pharmacy', {
                url: '/Pharmacy',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/pharmacy/pharmacyDashboard.html',
                        controller: 'PharmacyController',
                        resolve: {
                            PharmacySummaryData: ['spinner', 'PharmacyService', function (spinner, PharmacyService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(PharmacyService.getPharmacyServiceData(user.uuid));
                                } else {
                                    return spinner.forPromise(PharmacyService.getPartnersInformation());
                                }
                            }]
                        }
                    }
                }
            })

            .state('dashboard.Dietician', {
                url: '/Dietician',
                cache: false,
                views: {
                    'mainContent': {
                        templateUrl: 'templates/dietician/dieticianDashboard.html',
                        controller: 'DieticianController',
                        resolve: {
                            DietSummaryData: ['spinner', 'DieticianService', function (spinner, DieticianService) {
                                if (window.localStorage.loggedInUser) {
                                    var user = JSON.parse(window.localStorage.loggedInUser);
                                    return spinner.forPromise(DieticianService.getSessions(user.uuid));
                                } else {
                                    return spinner.forPromise(DieticianService.getPartnersInformation());
                                }
                            }]
                        }
                    }
                }
            })
            .state('dashboard.BuyPharmacyService', {
                url: '/BuyPharmacyService',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/pharmacy/pharmacyBuy.html',
                        controller: 'PharmacyTicketController',
                        resolve: {
                            PharmacyDataTkt: ['spinner', 'PharmacyService', function (spinner, PharmacyService) {
                                var user = JSON.parse(window.localStorage.loggedInUser);
                                return spinner.forPromise(PharmacyService.getPharmacyServiceData(user.uuid));
                            }]
                        }
                    }
                }
            })

            .state('dashboard.viewPharmacyOrder', {
                url: '/viewPharmacyOrder/:ticketId',
                views: {
                    'mainContent': {
                        templateUrl: 'templates/pharmacy/viewOrderProfile.html',
                        controller: 'PharmacyOrderController',
                        resolve: {
                            phamOrderSummaryData: ['spinner', 'PharmacyService', '$stateParams', function (spinner, PharmacyService, $stateParams) {
                                return PharmacyService.getOrderDetailsById($stateParams.ticketId);
                            }]
                        }

                    }
                }
            });

        $httpProvider.interceptors.push('HttpInterceptor');
        // if none of the above states are matched, use this as the fallback
        if (window.localStorage.loggedInUser) {
            $urlRouterProvider.otherwise('/dashboard');
        } else {
            $urlRouterProvider.otherwise('/login');
        }
    }])
    .run(['$ionicPlatform', '$ionicPopup', '$rootScope', '$cordovaSplashscreen', 'ENV', 'TokenService', '$state', '$ionicHistory', '$cordovaDevice', '$timeout', function ($ionicPlatform, $ionicPopup, $rootScope, $cordovaSplashscreen, ENV, TokenService, $state, $ionicHistory, $cordovaDevice,$timeout) {

        setTimeout(function () {
            $cordovaSplashscreen.hide();
        }, 500);

        TokenService.refreshToken();

        delete window.localStorage.ionic_io_user_d9e182f6;


        $ionicPlatform.ready(function () {

            $rootScope.deviceID = $cordovaDevice.getUUID();
            $timeout(function () {
                $cordovaSplashscreen.hide();
            }, 500);
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }

            var clickyClasses = ['sound-click', 'button', 'a'];

            nativeclick.watch(clickyClasses);

            document.addEventListener("offline", justOffline, false);
            document.addEventListener("online", justOnline, false);


            function justOffline() {
                $rootScope.networkError = {
                    error: 'internet',
                    msg: 'No Internet connection.'
                };
                var offlinePopup = $ionicPopup.alert({
                    title: 'Offline',
                    template: 'Check your internet connection'
                });
                offlinePopup.then(function(res) {
                 offlinePopup.close();
               });
            }

            function justOnline() {
                $rootScope.networkError = false;
            }

            AppRate.preferences.displayAppName = 'HealthyBillions';
            AppRate.preferences.storeAppURL.ios = ENV.rate.iOSAppId;
            AppRate.preferences.storeAppURL.android = ENV.rate.androidURL;
            AppRate.preferences.usesUntilPrompt = 2;
            AppRate.preferences.promptAgainForEachNewVersion = false;
            AppRate.promptForRating();
        });

        $ionicPlatform.registerBackButtonAction(function () {
            if ($state.current.name == "account.login" || $state.current.name == "dashboard.physical") {

            var myPopup = $ionicPopup.confirm({
                title: 'Confirm Exit',
                template: "Do you want to Exit?"
            });
            myPopup.then(function(close) {
                if (close) {
                    ionic.Platform.exitApp();
                }
            });
        } else {
            $ionicHistory.goBack();
        }
        $timeout(function() {
            myPopup.close();
        }, 6000);
    },100);
        
    }]);
