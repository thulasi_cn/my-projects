'use strict';

angular.module('hbMobile')
    .factory('TokenService', ['$http','ENV', function ($http,ENV) {
        function getToken() {
            return $http.get(ENV.coreUrl + '/auth',{cache:false}).then(
                success,
                fail
            );
        }

        function success(response) {
            if(!response.data.loggedIn){
                delete window.localStorage['loggedInUser'];
            }
            window.localStorage['corecsrfToken'] = response.data.token;
            return response;
        }

        function fail(response) {
            JSON.stringify(response.data);
            return response;
        }

        function getDToken() {
            return $http.get(ENV.diagnosticUrl + '/getToken',{cache:false}).then(
                dsuccess,
                dfail
            );
        }

        function dsuccess(response) {
            window.localStorage['dcsrfToken']=response.data;
            return response;
        }

        function dfail(response) {
            return response;
        }

        function refreshToken(){
            getToken();
            getDToken();
        }

        return {
            get: getToken,
            getDToken: getDToken,
            refreshToken: refreshToken
        };
}]);
