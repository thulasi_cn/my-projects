'use strict';

angular.module('hbMobile')
    .factory('requestService', ['$http', 'ENV', function ($http, ENV) {
        var saveTicket = function(ticketData) {
            return $http.post(ENV.coreUrl + '/api/ticket/save', ticketData);
        };

        return {
            saveTicket: saveTicket
        };
    }]);
