'use strict';

angular.module('hbMobile')
    .service('socialService',['$http','ENV',function($http,ENV){
        this.getGoogleUserData = function(){
            $http.defaults.headers.common.Authorization = "Bearer " + window.localStorage.getItem("access_token");
            return $http.get(ENV.googleAPI.peopleURL);
        };

        this.getFacebookUserData = function(authResponse){
            var data = {
                params: {
                    access_token: authResponse.accessToken,
                    fields: "first_name,last_name,gender,email,picture.width(9999)",
                    format: "json"
                }
            };
            return $http.get(ENV.facebookAPI.graphURL, data);
        };
    }]);
