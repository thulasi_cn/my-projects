'use strict';

angular.module('hbMobile')
    .service('notificationService',['$http','ENV', '$rootScope',function($http,ENV, $rootScope){
        this.getAllNotifications = function() {
            return $http.get(ENV.coreUrl +'/notifications/all/'+$rootScope.loggedInUser.id);
        };

        this.getNotification = function(notification_id){
            return $http.get(ENV.coreUrl +'/notifications/details/'+notification_id);
        };
    }]);
