'use strict';

angular.module('hbMobile')
    .service('PharmacyService', ['$http','ENV', function ($http,ENV) {
    	var getPhamSessionsData;
        function getPharmacyServiceData (accountId){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=GetPharmacydata&accountid='+accountId).success(function(response){
        		 getPhamSessionsData = response.result;
                 return getPhamSessionsData;
        	})
       	}
        function saveTicket (ticketData){
            return $http.post(ENV.coreUrl + '/api/ticket/save',ticketData);

        }
        function getOrderDetailsById(id){
        	var response;
        	var array = getPhamSessionsData.lastOrderData;
        	angular.forEach(array, function(value, key){
			    if(value.ticketid == id)
			      response =  array[key];
			    });
        	return response;
        }

        function postPhamFeedBack(postData){
        	return $http.post(ENV.coreUrl+"/api/feedback/save",postData);
    	}

    	function getSummaryAllData(){
            return getPhamSessionsData;
    	}

    	function setfeedbackData(feedbackDataRecv,ticketid){
    		angular.forEach(getPhamSessionsData.lastOrderData, function(value,key) {
    			if(value['ticketid'] == ticketid){
    				getPhamSessionsData.lastOrderData[key].ticket_feedback = feedbackDataRecv.feedback;
    				getPhamSessionsData.lastOrderData[key].text_feedback = feedbackDataRecv.comment;
    				getPhamSessionsData.lastOrderData[key][5]=feedbackDataRecv.feedback;
    				getPhamSessionsData.lastOrderData[key][6]=feedbackDataRecv.comment;
    			}
			})
			
    	}

        function uploadPrescription(postData){
            return $http.post(ENV.coreUrl+'/api/ticket/uploadprescription',postData);
        }

        function getPartnersInformation(){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=Pharmacy');
        }
        return {
            getPharmacyServiceData: getPharmacyServiceData,
            getOrderDetailsById: getOrderDetailsById,
            postPhamFeedBack: postPhamFeedBack,
            getSummaryAllData:getSummaryAllData,
            setfeedbackData:setfeedbackData,
            uploadPrescription : uploadPrescription,
            saveTicket:saveTicket,
            getPartnersInformation:getPartnersInformation
        };
    }]);
