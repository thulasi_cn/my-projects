'use strict';

angular.module('hbMobile')
    .service('PartnersService', ['$http','ENV', function ($http,ENV) {

        this.getPartnersInformation = function(slug){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category='+slug);
        };

        this.getPartnersConsultants = function(partnersid,servicecategoryid){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersConsultants&partnersid='+partnersid+'&servicecategoryid='+servicecategoryid);
        }
    }]);
