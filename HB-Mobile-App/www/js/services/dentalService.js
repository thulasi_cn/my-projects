'use strict';

angular.module('hbMobile')
    .service('dentalService', ['$http','ENV', function ($http,ENV) {
        this.getSessions = function(accountId){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=getDentalCareData&accountid='+accountId);
        }
        this.getPartnersInformation = function(){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=Dental Care');
        }
    }]);
