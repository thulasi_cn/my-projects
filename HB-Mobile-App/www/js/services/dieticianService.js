'use strict';

angular.module('hbMobile')
    .service('DieticianService', ['$http','ENV', function ($http,ENV) {

        this.getSessions= function(accountId){
        	return $http.get( ENV.crmUrl + '/webservice.php?operation=GetDieticianData&accountid=' + accountId);
        };

        this.getPartnersInformation = function(){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=Dieticians');
        }
    }]);
