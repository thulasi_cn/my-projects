'use strict';

angular.module('hbMobile')
    .factory('yogaService', ['$http','ENV', function ($http,ENV) {
        function getSessionsByAccountId(accountId){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=GetYogadata&accountid='+accountId);
        }

        function getSessionsDetailsBySessionId(sessionId,accountId){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=getSessionDetails&sessionid='+sessionId+'&accountid='+accountId).success(function(response){
            	return response;

            });
        }

        function customerPerformanceDetails(contactid){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=customerPerformanceDetails&contactid='+contactid).success(function(response){
                return response;

            });
        }

        function sendRescheduleRequest(postData){
            return $http.post(ENV.coreUrl+"/api/crm/apidata",postData);
        }



        return {
            getSessions: getSessionsByAccountId,
            getSessionDetails: getSessionsDetailsBySessionId,
            getPerformanceDetails: customerPerformanceDetails,
            sendRescheduleRequest:sendRescheduleRequest
        };
    }]);
