'use strict';

angular.module('hbMobile')
    .service('DoctorAtHomeService', ['$http','ENV', function ($http,ENV) {

        this.getDoctorAtHomeServiceData = function(accountId){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=getDoctorAtHomeServiceData&accountid='+accountId);
        };

        this.getPartnersInformation = function(){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=Doctor at Home');
        }
    }]);
