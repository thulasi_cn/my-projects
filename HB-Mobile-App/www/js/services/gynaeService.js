'use strict';

angular.module('hbMobile')
    .service('gynaeService', ['$http','ENV', function ($http,ENV) {

        this.getSessions = function(accountId){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=getGynaeCareData&accountid='+accountId);
        };

        this.getPartnersInformation = function(){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=Gynae Care');
        }
    }]);
