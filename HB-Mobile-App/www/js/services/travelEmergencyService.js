'use strict';
angular.module('hbMobile')
    .service('TravelEmergencyService', ['$http','ENV', function ($http,ENV) {

        this.getPartnersInformation = function(){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=Travel Emergency');
        };

        this.getTravelEmergencyServiceData = function(accountId){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=getTravelEmergencyServiceData&accountid='+accountId);
        }
   }]);
