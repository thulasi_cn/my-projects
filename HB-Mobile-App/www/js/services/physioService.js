'use strict';

angular.module('hbMobile')
    .service('physioService', ['$http','ENV', function ($http,ENV) {
        this.getSessions = function(accountId){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=getPhysiotherapyData&accountid='+accountId);
        }
    }]);
