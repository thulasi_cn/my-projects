'use strict';

angular.module('hbMobile')
    .service('paymentService', ['$http', 'ENV', '$q', function ($http, ENV, $q) {

        this.payUCredential = function (data) {
            return $http.post(ENV.coreUrl + '/payu/membership/credential', data);
        };

        this.payment = function (userId, packageId) {
            var deferred = $q.defer();
            var browserRef = window.open(ENV.payment.URL + '/' + userId + '/' + packageId, '_blank', 'location=no,clearsessioncache=yes,clearcache=yes');
            browserRef.addEventListener("loadstart", function (event) {
                var successURL = ENV.payment.successURL;
                var failureURL = ENV.payment.failureURL;
                if ((event.url).indexOf(successURL) === 0 || (event.url).indexOf(failureURL) === 0) {
                    browserRef.removeEventListener("exit", function (event) {
                    });
                    browserRef.close();
                    deferred.resolve({payment_status: (event.url).indexOf(successURL) === 0 ? 'success' : 'failure'});
                }
            });
            browserRef.addEventListener('exit', function (event) {
                deferred.reject("Payment Failure. Please Try Again");
            });
            return deferred.promise;
        }
    }]);