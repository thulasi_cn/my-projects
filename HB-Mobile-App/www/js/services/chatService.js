'use strict';

angular.module('hbMobile')
    .service('chatService',['$http','ENV','$q',function($http,ENV,$q){

        this.startChat = function(){
            var deferred = $q.defer();
            var browserRef = window.open(ENV.chat.url, '_blank', 'location=no,clearsessioncache=yes,clearcache=yes');
            browserRef.addEventListener("loadstop", function(event) {
                browserRef.insertCSS({code: "#formCloseChat { display: none} #backButtonContainer {display: none } #prechatButtonContainer button { width: 100%} .formButton { width: 100%}"});
            });
            browserRef.addEventListener('exit', function(event) {
                deferred.resolve("Completed");
            });
            return deferred.promise;
        }
    }]);