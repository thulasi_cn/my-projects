'use strict';

angular.module('hbMobile')
    .service('eyeService', ['$http','ENV', function ($http,ENV) {

        this.getSessions = function(accountId){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=getEyeCareData&accountid='+accountId);
        };

        this.getPartnersInformation = function(){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=Eye Care');
        }
    }]);
