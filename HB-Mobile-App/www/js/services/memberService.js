'use strict'
angular.module('hbMobile')
    .service('memberService',['$http','ENV',function($http,ENV){

        this.createMember=function(data){
            return $http.post(ENV.coreUrl + '/member/create',data);
        };
        this.getMembers=function(user_id){
            return $http.get(ENV.coreUrl + '/user/members/'+user_id, {
                cache: false}).then(function(response){
                return response.data;
            });
        };
        this.getMemberByUserId=function(user_id){
            return $http.get(ENV.coreUrl + '/users/'+user_id, {
                cache: false}).then(function(response){
                return response;
            });
        };
        this.updateMember=function(data){
            return $http.post(ENV.coreUrl + '/member/update',data);
        };
        this.updateMemberSocial=function(data){
            return $http.post(ENV.coreUrl + '/member/complete_profile',data);
        };
        this.searchMember=function(keyword){
            return $http.get(ENV.coreUrl + '/member/search/', {params:keyword,
                cache: true}).then(function(response){
                return response.data;
            });
        };
        this.user_subscription_check=function(data){
            return $http.post(ENV.coreUrl + '/member/user_subscription_check',data);
        };
        this.upgradeMember=function(data){
          return $http.post(ENV.coreUrl + '/membership/upgrade',data);
        };


    }]);
