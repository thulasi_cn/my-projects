'use strict';

angular.module('hbMobile')
    .factory('secondOpService', ['$http','ENV', function ($http,ENV) {
        var getSessionsByAccountId = function(accountId){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=getMedicalSecondOpinionData&accountid='+accountId).success(function(data){
            	 return data;
            });
        };

        return {
            getSessions: getSessionsByAccountId
        };
    }]);
