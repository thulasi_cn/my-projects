angular.module('hbMobile')
.service('HttpInterceptor',['$q','$rootScope','ENV','$location','$injector', function($q,$rootScope,ENV,$location,$injector) {

    this.request = function(config) {
        if(config.url.indexOf(ENV.crmUrl,0) > -1){
            config.withCredentials=false;
        }
        if(config.method === 'POST' && !window.localStorage.corecsrfToken){
            var deferred = $q.defer();
            $injector.invoke(function($http){
                return $http.get(ENV.coreUrl + '/auth',{cache:false}).then(function(res){
                    window.localStorage['corecsrfToken'] = res.data.token;
                    config.data._token = res.data.token;
                    deferred.resolve(config);
                });
            });
            return deferred.promise;
        }
        return config;
    };

    this.responseError = function(response) {
        if(response.status == 0 && !response.config.timeout) {
            if($rootScope.networkError && $rootScope.networkError.error =='internet'){
            }else{
                $rootScope.networkError={error:'webservice',msg:'Webservice unavailable.'};
                return $q.reject(response);
            }
        }
        if (response.status === 500) {
            $rootScope.networkError={error:'webservice',msg:'Webservice Error.'};
        }
        if (response.status === 401) {
            $location.path('/login');
        }
        return $q.reject(response);
    };

    this.response   = function(response) {
        $rootScope.networkError=false;
        return response;
    }
}]);
angular.module('hbMobile')
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.withCredentials = true;
    }]);
