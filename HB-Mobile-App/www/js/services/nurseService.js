'use strict';

angular.module('hbMobile')
    .service('nurseService', ['$http','ENV', function ($http,ENV) {

        this.getSessions = function(accountId){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=NursesAlldata&accountid='+accountId);
        };

        this.getPartnersInformation = function(){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=Nurses at Home');
        }
    }]);
