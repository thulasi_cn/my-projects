'use strict';

angular.module('hbMobile')
    .service('PrecamaService', ['$http','ENV', function ($http,ENV) {
         this.lastOrders = function(accountId) {
         	return $http.get(ENV.coreUrl + '/orders?user_id='+accountId);
        };
        
        this.getResults = function($member_uuid) {
            return $http.get(ENV.diagnosticUrl + '/result/user?member_uuid='+ $member_uuid);
        };

        this.precamaPartners = function() {
            return $http.get(ENV.diagnosticUrl + '/organizations');
        };

    }]);
