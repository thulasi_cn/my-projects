'use strict';

angular.module('hbMobile')
    .factory('spaService', ['$http','ENV', function ($http,ENV) {
        var getSessionsByAccountId = function(accountId){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=GetSpadata&accountid='+accountId);
        };

        return {
            getSessions: getSessionsByAccountId
        };
    }]);
