'use strict';

angular.module('hbMobile')
    .factory('userService', ['$http','ENV', function ($http,ENV) {
        function createUser(userData){
            return $http.post(ENV.coreUrl + '/member/register',  userData);
        }

        function activateUser(activationCode){
            return $http.get(ENV.coreUrl + '/members/activateAccount/'+activationCode);
        }

        function loginUser(data){
            return $http.post(ENV.coreUrl + '/member/login',data);
        }

        function logoutUser(device_id){
            return $http.get(ENV.coreUrl + '/member/logout?device_id='+device_id);
        }

        function forgotPassword(data){
            return $http.post(ENV.coreUrl + '/member/forgot_password',data);
        }

        function changePassword(data){
            return $http.post(ENV.coreUrl + '/member/password/change',data);
        }

        function socialConnect(data){
            return $http.post(ENV.coreUrl + '/member/social_connect',data);
        }

        function activateuser(data){
            return $http.post(ENV.coreUrl + '/members/active_existaccount',data);
        }

        function checkUserByPhoneOrEmail(data){
            return $http.post(ENV.coreUrl + '/member/check_user_exists',data);
        }

        function getMembershipTypes(){
           return $http.get(ENV.coreUrl + '/membership_type/list');
        }

        function registerDevice(device){
            return $http.post(ENV.coreUrl + '/device/register',device);
        }

        function unregisterDevice(device){
            return $http.post(ENV.coreUrl + '/device/unregister',device);
        }

        function getMembership(user_id){
            return $http.get(ENV.coreUrl + '/membership/details/'+user_id);
        }

        function getZones(){
            return $http.get(ENV.coreUrl + '/zones');
        }


        return {
            createUser: createUser,
            activateUser: activateUser,
            loginUser: loginUser,
            logoutUser: logoutUser,
            forgotPassword: forgotPassword,
            changePassword: changePassword,
            socialConnect:socialConnect,
            activateuser: activateuser,
            checkUserByPhoneOrEmail:checkUserByPhoneOrEmail,
            getMembershipTypes:getMembershipTypes,
            registerDevice: registerDevice,
            unregisterDevice : unregisterDevice,
            getMembership:getMembership,
            getZones:getZones
        };
    }]);
