'use strict';

angular.module('hbMobile')
    .factory('docService', ['$http','ENV', function ($http,ENV) {
    	var getSessionsData;
        function getSessionsByAccountId(accountId){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=GetDoctorOnCallData&accountid='+accountId).success(function(data){
            	 getSessionsData = data;
                 return getSessionsData;
            });
            
        }

        function getCallDetailsById(id){
        	var response;
            var array = getSessionsData.result.lastCallData;
        	angular.forEach(array, function(value, key){
			    if(value.ticketid == id)
			      response =  array[key];
			    });
        	return response;
        }
        function postDocFeedBack(postData){
        	return $http.post(ENV.coreUrl+"/api/feedback/save",postData);
    	}
        function setfeedbackData(feedbackDataRecv,ticketid){
            angular.forEach(getSessionsData.result.lastOrderData, function(value,key) {
                if(value['ticketid'] == ticketid){
                    getSessionsData.result.lastCallData[key].ticket_feedback = feedbackDataRecv.feedback;
                    getSessionsData.result.lastCallData[key].text_feedback = feedbackDataRecv.comment;
                    getSessionsData.result.lastCallData[key][5]=feedbackDataRecv.feedback;
                    getSessionsData.result.lastCallData[key][6]=feedbackDataRecv.comment;
                }
            })
            
        }

        function getPartnersInformation(){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=24/7 Doctor Connect');
        }
        return {
            getSessions: getSessionsByAccountId,
            getCallDetailsById: getCallDetailsById,
            setfeedbackData:setfeedbackData,
            postDocFeedBack: postDocFeedBack,
            getPartnersInformation : getPartnersInformation
        };
    }]);
