'use strict';

angular.module('hbMobile')
    .service('AmbulanceService',['$http','ENV',function($http,ENV){
        this.ambulanceSummaryData = function(accountid){
            return $http.get(ENV.crmUrl + '/webservice.php?operation=getAmbulanceServiceData&accountid='+accountid);
        };
        this.getPartnersInformation = function(){
        	return $http.get(ENV.crmUrl + '/webservice.php?operation=partnersInformation&service_category=Ambulance');
        }
    }]);
