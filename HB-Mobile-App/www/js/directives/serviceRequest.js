"use strict";
angular.module('hbMobile')
    .directive('serviceRequest', ['$ionicModal', '$ionicPopup', 'categoryService', 'spinner', 'ENV',function ($ionicModal, $ionicPopup, categoryService, spinner,ENV) {

        var Controller = ['$scope', 'requestService', 'spinner', function($scope, requestService, spinner) {

            $scope.requestData = {};

            $scope.requestData.message = null;

            $scope.model = { name: 'Model Name' };
            $scope.form = {};

            $ionicModal.fromTemplateUrl('templates/serviceRequest/service_request.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.requestModal = modal;
            });

            $scope.closeRequestModal = function () {
                document.getElementById("requestForm").reset();
                $scope.form.requestForm.$setPristine();
                $scope.requestModal.hide();
            };

            var submitSuccess = function (response) {
                var returnData = JSON.parse(response.data);
                var confirmPopup = $ionicPopup.alert({
                    title: '<b>Request Submitted.</b>',
                    template: 'Service Request Id:' + returnData.ticket_no + '<br>'+
              'A HealthyBillions representative will get in touch with you soon.' + '<br>'+'Thank You.'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        document.getElementById("requestForm").reset();
                        $scope.requestModal.hide();
                    } else {
                    }
                });

            };
            var submitError = function (response) {
                var confirmPopup = $ionicPopup.alert({
                    title: 'Request Failed.',
                    template: 'Sorry Request failed.'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        $scope.form.requestForm.$setPristine();
                        $scope.requestModal.hide();
                    } else {
                    }
                });
            };

            $scope.submitQuery = function () {
                if (window.localStorage.loggedInUser)
                    var userData = JSON.parse(window.localStorage.loggedInUser);
                var postData = angular.copy($scope.requestData);
                postData._token = window.localStorage.corecsrfToken;
                if (userData) {
                    postData.member_id = userData.member.id;
                }
                if (userData) {
                    postData.zone = userData.member.city;
                }
                else {
                    postData.zone = "Kolkata";
                }
                if (userData) {
                  postData.phone = userData.phone;
                }
                if(!$scope.requestData.service.id) {
                  for (var i = 0; i < $scope.serviceList.length; i++) {
                    if ($scope.serviceList[i].name == $scope.requestData.service.name) {
                      postData.servicecategory = $scope.serviceList[i].id;
                      }
                    }
                  }
                else{
                  postData.servicecategory=$scope.requestData.service.id;
                }
                postData.servicename = $scope.requestData.service.name;
                postData.ticket_type = 'Enquiry';
                return spinner.forPromise(requestService.saveTicket(postData)).then(
                    submitSuccess,
                    submitError
                );
            }
        }];

        var link = function($scope, element, attr) {
            var openRequestForm = function () {
                if (window.localStorage.loggedInUser) {
                    $scope.requestData.phone = JSON.parse(window.localStorage.loggedInUser).phone;
                } else {
                    $scope.requestData.phone = '';
                }
                spinner.forPromise(categoryService.getData().then(function (data) {
                        $scope.serviceList = data;
                        $scope.requestData.service={slug: ENV.serviceCategory[attr.servicename], name: attr.servicename};
                        $scope.requestData.description = null;
                        $scope.requestModal.show();
                    })
                );

            };
            element.bind('click', openRequestForm);
        };

        return ({
            controller: Controller,
            link: link,
            restrict: "E"
        });
    }]);


