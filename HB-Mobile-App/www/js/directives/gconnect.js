'use strict';

angular.module('hbMobile')
    .directive('gconnectButton', ['spinner','$ionicPopup','IonicPushNotification', 'sessionService', function(spinner,$ionicPopup,IonicPushNotification, sessionService){
    return {
        restrict: 'A',
       
        link: function($scope, element) {

            var googleLoginSuccess = function(response){
                sessionService.socialLogin(response.userId, response.email, response.givenName, response.familyName, response.gender, response.imageUrl, 'google');
            };

            var googleLoginError = function(response) {
                $ionicPopup.alert({
                    title: 'Error Signing In Using Google',
                    template: response
                })
            };

            var google_login = function() {
                spinner.forPromise(IonicPushNotification.initialize().then(
                    function(){
                        window.plugins.googleplus.login({}, googleLoginSuccess, googleLoginError);
                    })
                );
            };

            element.bind('click', google_login);

        }
    }
}]);
