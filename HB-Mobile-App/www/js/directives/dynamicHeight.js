"use strict";

angular.module('hbMobile')
    .directive('dynamicHeight', function() {
        return {
            require: ['^ionSlideBox'],
            link: function(scope, elem, attrs, slider) {
                scope.$watch(function() {
                    return slider[0].__slider.selected();
                }, function(val) {
                    //getting the heigh of the container that has the height of the viewport
                    var newHeight = window.getComputedStyle(elem.parent()[0], null).getPropertyValue("height");

                  if (newHeight && newHeight != '0px') {
                        elem.find('ion-scroll')[0].style.height = newHeight;
                    }
                });
            }
        };
    });
