'use strict';

angular.module('hbMobile')
    .directive('fconnectButton',['spinner','$state','socialService','$ionicPopup','IonicPushNotification','$ionicLoading', 'sessionService', function(spinner,$state,socialService,$ionicPopup,IonicPushNotification,$ionicLoading, sessionService){
        return {
            restrict: 'A',

            link: function(scope,element) {
                var facebook_login = function() {                    
                    spinner.forPromise(IonicPushNotification.initialize().then(function() {
                            facebookConnectPlugin.getLoginStatus(function(success){
                                spinner.forPromise(facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError));
                            });
                        })
                    );
                };

                var fbLoginSuccess = function(response) {
                    if (!response.authResponse){
                        fbLoginError("Cannot find the authResponse");
                        return;
                    }
                    var authResponse = response.authResponse;
                    facebookConnectPlugin.api('/me?fields=email,first_name,last_name,gender,picture&access_token=' + authResponse.accessToken, null,
                      function (response) {
                        if(!response.email)
                        {
                            $ionicPopup.alert({
                                title: 'Message..!',
                                template: 'Error signing with facebook as no email credentials in facebook'
                            });
                            $state.go('account.login');
                        }
                        var userInfo = response;
                        sessionService.socialLogin(userInfo.id, userInfo.email, userInfo.first_name, userInfo.last_name, userInfo.gender, userInfo.picture.data.url, 'facebook');
                          },
                          function (response) {
                                    console.log(response);
                          }
                    )
                };

                var fbLoginError = function(error){
                    $ionicLoading.hide();
                };

                element.bind('click', facebook_login);

            }
        }
    }]);
