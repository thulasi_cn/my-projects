angular.module('hbMobile')
    .directive("sessionList" ,function () {
        return {
	            restrict: "AE",
	            require: "ngModel",
	            scope : {
			            sessions : "=ngModel"
        		},
        		template: '<div class="list" ng-if="sessions">'+
                        '<a class="item" href="#" ng-repeat="session in sessions">'+
                        	'<h2>{{session.start_date+" "+session.start_time | amDateFormat:"Do MMM hh:mm:A"}}</h2>'+
                        	'<p>Trainer: {{session.consultant}}</p>'+
                        '</a>'+
                    '</div>',
            	link: function(scope, element, attrs,ngModel) {

            	}
        	}
        }
   )

   .directive("inVoice" ,function () {
        return {
	            restrict: "AE",
	            require: "ngModel",
	            scope : {
			            inVoices : "=ngModel"
        		},
        		template: '<div class="list" ng-if="inVoices">'+
	                         '<div class="item" href="#" ng-repeat="inVoice in inVoices">'+
	                         '<div class="item item-divider"><b>Invoice No: #{{inVoice.invoice_no}}</b></div>'+
	        					'<div align="left" style="margin-top:5px; font-weight:bold">{{inVoice.accountname}}'+
	        					'</div> '+

	        					'<label>Date : {{inVoice.invoicedate | date:"mediumDate"}}</label><br> '+
	        					'<div><b>Amount : {{inVoice.total}} INR</b></div>'+
	        					'<div ng-show="inVoice.total_payment_amount"><b>Total Payment : {{inVoice.total_payment_amount}} INR</b></div>'+


	        				'</div>'+
        				   	'</div>',
            	link: function(scope, element, attrs,ngModel) {

            	}
        	}
        }
   )
.directive('starRating',
	function() {
		return {
			restrict : 'A',
			template : '<ul class="rating">'
				     + '<li ng-repeat="star in stars" ng-class="star">'
					 + '\u2605'
					 + '</li>'
					 + '</ul>',
			//require:'ngModel',
			scope : {
				ratingValue : '=',
				max : '=',
				onRatingSelected : '&'
			},
			controllerAs: 'DocController',
			link : function(scope, elem, attrs) {
				var updateStars = function() {
					scope.stars = [];
					for ( var i = 0; i < scope.max; i++) {
						scope.stars.push({
							filled : i < scope.ratingValue
						});
					}
				};

				scope.$watch('ratingValue',
					function(oldVal, newVal) {
						if (newVal) {
							updateStars();
						}
					}
				);
			}
		};
	}
)

.directive('banner', function () {
	return {
        restrict: 'A',
		link: function (scope, elem, attrs) {
			//var element = document.getElementsByClassName('scroll');
			//console.log(element[0].style);
			//console.log(element[0].height);
			//var winHeight = $window.innerHeight;
            //elem.css('height', 10 + 'px');
        	}
	    };
	})
. directive('onlyDigits', function () {

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;
            ngModel.$parsers.unshift(function (inputValue) {
                var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
                ngModel.$viewValue = digits;
                ngModel.$render();
                return digits;
            });
        }
    };
})
. directive('partnersInformation', function () {
	return {
	            restrict: "A",
	            require: "ngModel",
	            scope : {
			            partners : "=ngModel"
        		},
        		template: '<div class="list" ng-if="partners">'+
                        '<span class="item" ng-repeat="partner in partners">'+
                        	'<a href="#/partners/consultants/{{partner.partnersid}}/{{partner.servicecategoryid}}"><h2>{{partner.partnersname}}</h2></a>'+
                        '</span>'+
                    '</div>',
            	link: function(scope, element, attrs,ngModel) {

            	}
        	}
});
