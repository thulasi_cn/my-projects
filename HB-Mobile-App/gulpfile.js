var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
var jshint = require('gulp-jshint');
var minifyCss = require('gulp-minify-css');
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require("gulp-uglify");
var imagemin = require('gulp-imagemin');
var htmlreplace = require('gulp-html-replace');
var templateCache = require('gulp-angular-templatecache');
var ngConstant = require('gulp-ng-constant');
var sh = require('shelljs');

var paths = {
    scripts: ['./www/js/**/*.js', '!./www/js/app.bundle.min.js'], // exclude the file we write too
    images: ['./www/img/**/*'],
    templates: ['./www/templates/**/*.html'],
    css: ['./www/css/**/*.css',
        './www/lib/tabbed-slide-box/tabbedSlideBox/tabSlideBox.css',
        './www/lib/angular-chart.js/dist/angular-chart.css'],
    html: ['./www/index.html'],
    ionicbundle: ['./www/lib/ionic/js/ionic.bundle.min.js'],
    ionicfonts: ['./www/lib/ionic/fonts/*'],
    lib: ['./www/lib/**/*.js'],
    fonts: ['./www/fonts/*'],
    dist: ['./dist/']
};
var files = {
    jsbundle: 'app.bundle.min.js',
    appcss: 'app.min.css'
}

gulp.task('default',['config']);

gulp.task('build', ['config','scripts', 'styles', 'imagemin', 'index', 'copy']);

gulp.task('release', ['config-prod']);

gulp.task('clean', function () {
    return gulp.src(paths.dist, {
            read: false
        })
        .pipe(clean());
});

gulp.task('config', function() {
    gulp.src('./config/testing/config.json')
        .pipe(ngConstant())
        .pipe(concat('config.js'))
        .pipe(gulp.dest("./www/js/"))
});
gulp.task('config-prod', function() {
    gulp.src('./config/prod/config.json')
        .pipe(ngConstant())
        .pipe(concat('config.js'))
        .pipe(gulp.dest("./www/js/"));

    gulp.src('./config/prod/google-services.json')
        .pipe(gulp.dest("./"))
});

// Prepare Index.html for dist - ie. using min files
gulp.task('index', ['clean'], function () {
    gulp.src(paths.html)
        .pipe(htmlreplace({
            'css': 'css/' + files.appcss,
            'js': 'js/'+ files.jsbundle
        }))
        .pipe(gulp.dest(paths.dist + '.'));
});

// scripts - clean dist dir then annotate, minify, concat
gulp.task('scripts', ['clean', 'templateCache'], function () {
    gulp.src(paths.scripts)
        //.pipe(jshint())
        //.pipe(jshint.reporter('default'))
        .pipe(ngAnnotate({
            remove: true,
            add: true,
            single_quotes: true
        }))
        .pipe(uglify())
        .pipe(concat(files.jsbundle))
        .pipe(gulp.dest(paths.dist + 'js'));
});

// concat all html templates and load into templateCache
gulp.task('templateCache', ['clean'], function () {
    return gulp.src(paths.templates)
        .pipe(templateCache({
            'filename': 'templates.js',
            'root': 'templates/',
            'module': 'hbMobile'
        }))
        .pipe(gulp.dest('./www/js'));
});

// Copy all other files to dist directly
gulp.task('copy', ['clean'], function () {
    // Copy ionic bundle file
    gulp.src(paths.ionicbundle)
        .pipe(gulp.dest(paths.dist + 'lib/ionic/js/.'));

    // Copy ionic fonts
    gulp.src(paths.ionicfonts)
        .pipe(gulp.dest(paths.dist + 'lib/ionic/fonts'));

    // Copy lib scripts
    gulp.src(paths.lib)
        .pipe(gulp.dest(paths.dist + 'lib'));

    // Copy fonts
    gulp.src(paths.fonts)
        .pipe(gulp.dest(paths.dist + 'fonts'));
});

// styles - min app css then copy min css to dist
gulp.task('minappcss', function () {
    return gulp.src(paths.css)
        .pipe(minifyCss())
        .pipe(concat(files.appcss))
        .pipe(gulp.dest(paths.dist + 'css'));
});

// styles - min app css then copy min css to dist
gulp.task('styles', ['clean', 'minappcss']);

// Imagemin images and ouput them in dist
gulp.task('imagemin', ['clean'], function () {
    gulp.src(paths.images)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist + 'img'));
});

gulp.task('watch', function () {
    gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function () {
    return bower.commands.install()
        .on('log', function (data) {
            gutil.log('bower', gutil.colors.cyan(data.id), data.message);
        });
});

gulp.task('git-check', function (done) {
    if (!sh.which('git')) {
        console.log(
            '  ' + gutil.colors.red('Git is not installed.'),
            '\n  Git, the version control system, is required to download Ionic.',
            '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
            '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
        );
        process.exit(1);
    }
    done();
});