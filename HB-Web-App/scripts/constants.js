"use strict";

angular.module('constants', [])
    .constant('ENV', {
        hostname: '',
        imagePath: 'https://192.168.33.10/hb-images',
        coreUrl: 'https://192.168.33.10/hb-core',
        diagnosticUrl: 'https://192.168.33.10/hb-diagnostic',
        crmUrl : 'https://192.168.33.10/hb-crm',
        facebookAPI: '537469639761322',
        googleAPI:'550320462097-235n0g1i8e04rp2vhpd2oimf8b0eb8kq.apps.googleusercontent.com'
    });