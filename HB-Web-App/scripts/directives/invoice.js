angular.module('hbMainUiApp')
    .directive("invoiceList", function() {
        return {
            restrict: "A",
            require: "ngModel",
            scope: {
                invoices: "=ngModel"
            },
            template: '<table class="table table-striped table-bordered table-hover dataTable no-footer">' +
                '<tbody> <tr> <th>Invoice No</th> <th>Invoice Date</th> <th>Total</th><th>Total Payment</th> ' +
                '<tr ng-repeat="invoice in invoices">' +
                '<td>{{invoice.invoice_no}}</td>' +
                '<td>{{invoice.invoicedate}}</td>' +
                '<td>{{invoice.total}}</td>' +
                '<td>{{invoice.total_payment_amount}}</td>' + 
                '</tr></tbody></table>',
            link: function(scope, element, attrs, ngModel) {

            }
        }
    });
