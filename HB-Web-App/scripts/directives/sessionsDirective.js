angular.module('hbMainUiApp')
    .directive("sessionList", function() {
        return {
            restrict: "A",
            require: "ngModel",
            scope: {
                sessions: "=ngModel"
            },
            template: '<table class="table table-striped table-bordered table-hover dataTable no-footer">' +
                '<tbody> <tr> <th>Consultant</th> <th>Date</th> <th>Time</th> ' +
                '<tr ng-repeat="session in sessions">' +
                '<td>{{session.consultant}}</td> <td><i class="fa fa-calendar"></i> ' +
                '{{session.start_date | date:"mediumDate"}}</td><td ><i class="fa fa-clock-o"></i>' +
                ' {{session.start_time}}</td></tr></tbody></table>',
            link: function(scope, element, attrs, ngModel) {

            }
        }
    }).directive("comingSession", function() {
        return {
            restrict: "A",
             require: "ngModel",
            scope: {
                upComingSessionData: "=ngModel"
            },
            template: '<table class="table table-striped table-bordered table-hover dataTable no-footer">'+
                        '<tbody> <tr> <th>Trainer</th> <th>Date and Time</th> <th>Status</th></tr>'+
                        '<tr ng-repeat="session in upComingSessionData">'+
                        '<td>{{session.consultant_name }}</td><td>{{session.datetime}}</td>'+
                        '<td>{{session.status}}</td></tr></tbody></table>',
            link: function(scope, element, attrs, ngModel) {

            }
        }
    });