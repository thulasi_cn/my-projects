angular.module('hbMainUiApp')
    .directive("packageList", function() {
        return {
            restrict: "A",
            require: "ngModel",
            scope: {
                packages: "=ngModel"
            },
            template: '<table class="table table-striped table-bordered table-hover dataTable no-footer">' +
                '<tbody> <tr> <th>Package Name</th><th>TL</th><th>No of Sessions</th> <th>Total</th></tr> ' +
                '<tr ng-repeat="package in packages">' +
                '<td>{{package.servicename}}</td>' +
                '<td>{{package.consultantsname}}</td>' +
                '<td>{{package.sessions}}</td>' +
                '<td>{{package.total | currency:"Rs "}}</td>' + 
                '</tr></tbody></table>',
            link: function(scope, element, attrs, ngModel) {

            }
        }
    });