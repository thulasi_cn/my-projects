angular.module('hbMainUiApp')
    .directive('checkImage',['$http',function($http) {
        return {
            restrict: 'A',
            link: function($scope, element, attrs) {
                attrs.$observe('ngSrc', function(ngSrc) {
                    $http.get(ngSrc).success(function(){}).error(function(){
                        element.attr('src', 'images/profile-icon.png');
                    });
                });
            }
        };
    }]);