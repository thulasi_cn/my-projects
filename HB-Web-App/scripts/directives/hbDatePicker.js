/*
/ depends upon "datetimepicker":"2.4.3" (Bower package)
 */
angular.module('hbMainUiApp')
    .directive("hbdatepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModel) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "YY-DD-MM",
                    onSelect: function (dateText) {
                        updateModel(dateText);

                    },
                    onClose:function(){
                        elem.blur();
                    },
                    closeOnDateSelect:true,
                    defaultDate:'+1970/01/02',
                    format:'YYYY-MM-DD',
                    minDate : false,
                    minTime:false,
                    formatTime:false,

                    datepicker:true,
                    timepicker:false,
                    momentformat:false

                };
                angular.forEach(options, function (value,key) {
                    if (attrs[key.toLowerCase()]!=undefined) {
                        var v = attrs[key.toLowerCase()];

                        if(v=='true'){
                            v=true;
                        }
                        if(v=='false'){
                            v=false;
                        }
                        options[key]=v;
                    }
                });
                // Outer change
                ngModel.$render = function () {
                    var date = ngModel.$viewValue ? ngModel.$viewValue : null;
                    if(parseInt(ngModel.$viewValue) > 0){

                        if(options.momentformat){
                             elem.val( angular.copy(moment(ngModel.$viewValue)).format(options.momentformat));
                        } else{
                             elem.val(ngModel.$viewValue);
                        }
                    }

                  else
                        elem.val('');

                };
                ngModel.$render();

                elem.datetimepicker(options);
            }
        }
    });