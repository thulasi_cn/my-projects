angular.module('hbMainUiApp')
    .directive("hbdatetimepicker", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModel) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModel.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "YYYY-DD-MM",
                    onSelect: function (dateText) {
                        updateModel(dateText);

                    },
                    onClose:function(){
                        elem.blur();
                    },
                    closeOnDateSelect:true,
                    defaultDate:'+1970/01/02',
                    format:'YYYY-MM-DD h:mm a',
                    formatTime:'HH:MM A',
                    formatDate:'YYYY-MM-DD',
                    minDate:new Date(),
                    datepicker:true,
                    timepicker:true,
                    momentformat:true,
                    allowTimes:[
                        '06:00',
                        '07:00',
                        '08:00',
                        '09:00',
                        '11:00',
                        '12:00',
                        '13:00',
                        '14:00',
                        '15:00',
                        '16:00',
                        '17:00',
                        '18:00',
                        '19:00',
                        '20:00',
                        '21:00',
                        '22:00',
                    ]

                };
                angular.forEach(options, function (value,key) {
                    if (attrs[key.toLowerCase()]!=undefined) {
                        var v = attrs[key.toLowerCase()];

                        if(v=='true'){
                            v=true;
                        }
                        if(v=='false'){
                            v=false;
                        }
                        options[key]=v;
                    }
                });
                // Outer change
                ngModel.$render = function () {
                    var date = ngModel.$viewValue ? ngModel.$viewValue : null;
                    if(parseInt(ngModel.$viewValue) > 0){

                        if(options.momentformat){
                             elem.val( angular.copy(moment(ngModel.$viewValue)).format(options.momentformat));
                        } else{
                             elem.val(ngModel.$viewValue);
                        }
                    }

                  else
                        elem.val('');

                };
                ngModel.$render();

                elem.datetimepicker(options);
            }
        }
    });

Date.parseDate = function( input, format ){
    return moment(input,format).toDate();
};
Date.prototype.dateFormat = function( format ){
    return moment(this).format(format);
};