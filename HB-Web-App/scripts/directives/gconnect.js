angular.module('hbMainUiApp')
    .directive('gconnectButton', function(Facebook,$cookies,spinner,userService,sessionService, $location,GAuth,GApi,$window,$q,$rootScope,$state){
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.bind('click', google_login);

            function google_login() {
                GAuth.login().then(function(){
                    getGoogleUser().then(function() {
                    var info=$scope.guser;
                    var userData = $rootScope.gapi.user;
                    userData['_token']=$cookies.corecsrfToken;
                    userData['first_name']=_.capitalize(info.given_name);
                    userData['last_name']=_.capitalize(info.family_name);
                    userData['gender']= _.capitalize(info.gender);
                    userData['auth_source']= 'google';
                    userData['socialId']= info.id;
                    userData['imageUrl']= info.picture;
                    sessionService.socialConnect(userData);
                });

                }, function() {
                    console.log('login fail');
                });
            }; 

            function getGoogleUser() {
                var deferred = $q.defer();
                $window.gapi.client.oauth2.userinfo.get().execute(function(resp) {
                    if (!resp.code) {
                        $scope.guser=resp;
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                });
                return deferred.promise;
            }

        }
    }
});