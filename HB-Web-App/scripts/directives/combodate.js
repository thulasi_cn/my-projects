/*
 / depends upon "datetimepicker":"2.4.3" (Bower package)
 */
angular.module('hbMainUiApp')
    .directive("combodate", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, elem, attrs, ngModel) {

                var options = {
                    value: new Date(),
                    minYear: moment().format('YYYY'),
                    maxYear: 2025,
                    customClass:null,
                    smartDays:true
                };
                angular.forEach(options, function (value,key) {
                    if (attrs[key.toLowerCase()]!=undefined) {
                        var v = attrs[key.toLowerCase()];

                        if(v=='true'){
                            v=true;
                        }
                        if(v=='false'){
                            v=false;
                        }
                        options[key]=v;
                    }
                });
                // Outer change
                ngModel.$render = function () {
                    var date = ngModel.$viewValue ? ngModel.$viewValue : null;
                    if(!isNaN(parseInt(date)))
                        elem.combodate('setValue',date);
                    else{
                        elem.combodate('setValue','');
                    }

                };
                ngModel.$render();
                elem.combodate(options);


            }
        }
    });