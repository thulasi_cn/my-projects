angular.module('hbMainUiApp')
    .directive('fconnectButton', function(Facebook,$cookies,spinner,sessionService,$location,$state){
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.bind('click', fb_login);

            function fb_login() {
                Facebook.login(function(response) {
                Facebook.api('/me?fields=name,email,first_name,last_name,picture.width(9999)', function(response) {
                    $scope.fbuser = response;
                    $scope.fbuser['_token']=$cookies.corecsrfToken;
                    $scope.fbuser['auth_source']= 'facebook';
                    $scope.fbuser['socialId']= response.id;
                    $scope.fbuser['imageUrl']= response.picture.data.url;
                    sessionService.socialConnect($scope.fbuser);
                    });
                },{ scope: 'email', return_scopes: true });
            }
        }
    }
});