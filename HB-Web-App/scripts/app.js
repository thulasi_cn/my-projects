'use strict';
angular
    .module('hbMainUiApp', ['ngAnimate', 'ngCookies', 'ngResource', 'ui.router', 'ngSanitize', 'ngTouch', 'constants', 'chart.js', 'angularFileUpload', 'angularMoment', 'facebook', 'angular-google-gapi', 'ngDialog'])
    .config(function($stateProvider, $urlRouterProvider, ENV, $httpProvider, FacebookProvider) {
        FacebookProvider.init(ENV.facebookAPI);
        $stateProvider
            .state('user', {
                url: '',
                abstract: true,
                template: '<div ui-view="userUI"></div>'
            })
            .state('user.physical', {
                url: '/login',
                views: {
                    'userUI': {
                        templateUrl: 'views/user/login.html',
                        controller: 'LoginController'
                    }
                }
            })
            .state('user.forgotpassword', {
                url: '/forgotpassword',
                views: {
                    'userUI': {
                        templateUrl: 'views/user/forgotpassword.html',
                        controller: 'LoginController'
                    }
                }
            })
            .state('user.forgot_password', {
                url: '/resetpassword/{activationCode}',
                views: {
                    'userUI': {
                        templateUrl: 'views/user/reset_password.html',
                        controller: 'AccountActivationController'
                    }
                },
                resolve: {
                    activatedUser: ['userService', '$stateParams', function(userService, $stateParams) {
                        return userService.checkActiveToken($stateParams.activationCode).then(function(response) {
                            return response.data;
                        }, function(response) {
                            return response.data;
                        });
                    }]
                }
            })
            .state('user.invalidurl', {
                url: '/invalidurl',
                views: {
                    'userUI': {
                        templateUrl: 'views/user/invalid.html',
                        controller: 'RegistrationController'
                    }
                }
            })
            .state('user.activate', {
                url: '/activateuser',
                views: {
                    'userUI': {
                        templateUrl: 'views/user/activateuser.html',
                        controller: 'RegistrationController'
                    }
                }
            })
            /**************************** Registration **************************/
            .state('registration', {
                url: '',
                abstract: true,
                template: '<div ui-view="userUI"></div>'
            })
            .state('registration.physical', {
                url: '/signup',
                views: {
                    'userUI': {
                        templateUrl: 'views/user/registration/registration.html',
                        controller: 'RegistrationController'
                    }
                }
            })
            .state('registration.complete', {
                url: '/signup/complete',
                views: {
                    'userUI': {
                        templateUrl: 'views/user/registration/profile_complete.html',
                        controller: 'ProfileCompleteController'
                    }
                }
            })
            /**************************** Payment **************************/
            .state('subscription', {
                url: '',
                abstract: true,
                template: '<div ui-view="subscription"></div>'
            })
            .state('subscription.payment', {
                url: '/payment',
                views: {
                    'subscription': {
                        templateUrl: 'views/user/payment.html',
                        controller: 'PaymentController'
                    }
                },
                resolve: {
                    payUDetails: ['paymentService', 'spinner', '$cookies', function(paymentService, spinner, $cookies) {
                        var payUPosted = paymentService.payUPostCredentials(JSON.parse($cookies.get('loggedInUser')));
                        return spinner.forPromise(paymentService.payUCredential(payUPosted)).then(function(data) {
                            return data;
                        });
                    }],
                    payTmDetails: ['paymentService', 'spinner', '$cookies', function(paymentService, spinner, $cookies) {
                        var payTmPosted = paymentService.payTmPostCredentials(JSON.parse($cookies.get('loggedInUser')));
                        return spinner.forPromise(paymentService.payTmCredential(payTmPosted)).then(function(data) {
                            return data;
                        });
                    }]
                }
            })
            .state('subscription.paymentFail', {
                url: '/paymentFail',
                views: {
                    'subscription': {
                        templateUrl: 'views/user/paymentfail.html',
                        controller: 'PaymentController'
                    }
                },
                resolve: {
                    paymentDetails: [function() {
                        return false;
                    }]
                }
            })
            .state('subscription.paymentSuccess', {
                url: '/paymentSuccess',
                views: {
                    'subscription': {
                        templateUrl: 'views/user/paymentsuccess.html',
                        controller: 'PaymentController'
                    }
                },
                resolve: {
                    payUDetails: ['$cookies', function($cookies) {
                        if ($cookies.orderDetails || $cookies.subscriptionType) {
                            delete $cookies.orderDetails;
                            delete $cookies.subscriptionType;
                            return false;
                        } else {
                            return false;
                        }
                    }],
                    payTmDetails: ['$cookies', function($cookies) {
                        if ($cookies.orderDetails || $cookies.subscriptionType) {
                            delete $cookies.orderDetails;
                            delete $cookies.subscriptionType;
                            return false;
                        } else {
                            return false;
                        }
                    }]
                }
            })
            /**************************** DashBoard **************************/
            .state('dashboard', {
                url: '',
                abstract: true,
                template: '<div id="wrapper">' +
                    '<div ui-view="dashboardHeader"></div>' +
                    '<div ui-view="dashboardSidebar"></div>' +
                    '<div id="page-wrapper" ui-view="dashBoard">' +
                    '</div>' +
                    '</div>'
            })
            .state('dashboard.physical', {
                url: '/dashboard',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'dashBoard': {
                        templateUrl: 'views/dashboard/index.html',
                        controller: 'DashboardController'
                    }
                }
            })

        .state('dashboard.notifications', {
            url: '/notifications',
            views: {
                'dashboardHeader': {
                    templateUrl: 'views/dashboard/includes/header.html',
                    controller: 'DashboardHeaderController'
                },
                'dashboardSidebar': {
                    templateUrl: 'views/dashboard/includes/sidebar.html',
                    controller: 'DashboardSidebarController'
                },
                'dashBoard': {
                    templateUrl: 'views/dashboard/notification/notifications.html',
                    controller: 'AllNotificationController'
                }
            },
            resolve: {
                notificationsData: ['NotificationService', 'spinner', '$stateParams', function(NotificationService, spinner, $stateParams) {
                    return spinner.forPromise(NotificationService.getMoreNotifications(2)).then(function(data) {
                        return data;
                    });
                }],
            }
        })

        .state('dashboard.notification', {
            url: '/notification/:id',
            views: {
                'dashboardHeader': {
                    templateUrl: 'views/dashboard/includes/header.html',
                    controller: 'DashboardHeaderController'
                },
                'dashboardSidebar': {
                    templateUrl: 'views/dashboard/includes/sidebar.html',
                    controller: 'DashboardSidebarController'
                },
                'dashBoard': {
                    templateUrl: 'views/dashboard/notification/notificationDetail.html',
                    controller: 'NotificationController'
                }
            },
            resolve: {
                notificationDetailData: ['NotificationService', 'spinner', '$stateParams', function(NotificationService, spinner, $stateParams) {
                    return spinner.forPromise(NotificationService.getNotification($stateParams.id)).then(function(data) {
                        return data;
                    });
                }],
            }
        })

        .state('service', {
                url: '',
                abstract: true,
                template: '<div id="wrapper">' +
                    '<div ui-view="dashboardHeader"></div>' +
                    '<div ui-view="dashboardSidebar"></div>' +
                    '<div id="page-wrapper" ui-view="service">' +
                    '</div>' +
                    '</div>'
            })
            .state('service.yoga', {
                url: '/yoga',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/yoga.html',
                        controller: 'YogaController'
                    }
                },
                resolve: {
                    yogaData: ['YogaService', 'spinner', '$cookies', function(YogaService, spinner, $cookies) {
                        var user_data = JSON.parse($cookies.get('loggedInUser'));
                        return spinner.forPromise(YogaService.getSessionsByAccountId(user_data.uuid)).then(function(data) {
                            return data;
                        });
                    }],
                }
            })
            .state('service.physiotherapy', {
                url: '/physiotherapy',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/physiotherapy.html',
                        controller: 'PhysiotherapyController'
                    }
                },
                resolve: {
                    physiotherapyServiceData: ['PhysiotherapyService', 'spinner', '$cookies', function(PhysiotherapyService, spinner, $cookies) {
                        var user_data = JSON.parse($cookies.get('loggedInUser'));

                        return spinner.forPromise(PhysiotherapyService.getPhysiotherapyServiceData(user_data.uuid)).then(function(data) {
                            return data;
                        });
                    }],
                }
            })
            .state('service.spa', {
                url: '/spa',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/spa.html',
                        controller: 'spaController'
                    }
                },
                resolve: {
                    spaServiceData: ['SpaService', 'spinner', '$cookies', function(SpaService, spinner, $cookies) {
                        var user_data = JSON.parse($cookies.get('loggedInUser'));

                        return spinner.forPromise(SpaService.getSpaServiceData(user_data.uuid)).then(function(data) {
                            return data;
                        });
                    }],
                }
            })
            .state('service.doctor_on_call', {
                url: '/doctorOnCall',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/doctorOnCall.html',
                        controller: 'DoctorOnCallController'
                    }
                },
                resolve: {
                    doctorOnCallServiceData: ['doctorOnCallService', 'spinner', '$cookies', function(doctorOnCallService, spinner, $cookies) {
                        var user_data = JSON.parse($cookies.get('loggedInUser'));

                        return spinner.forPromise(doctorOnCallService.getdoctorOnCallServiceData(user_data.uuid)).then(function(data) {
                            return data;
                        });
                    }],
                }
            })
            .state('service.ambulance', {
                url: '/ambulance',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/ambulance.html',
                        controller: 'AmbulanceServiceController'
                    }
                },
                resolve: {
                    AmbulanceServiceData: ['AmbulanceService', 'spinner', '$cookies', function(AmbulanceService, spinner, $cookies) {
                        var user_data = JSON.parse($cookies.get('loggedInUser'));

                        return spinner.forPromise(AmbulanceService.getAmbulanceServiceData(user_data.uuid)).then(function(data) {
                            return data;
                        });
                    }],
                }
            })
            .state('service.eyeCare', {
                url: '/eyeCare',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/eyeCareService.html',
                        controller: 'EyeCareServiceController'
                    }
                },
                resolve: {
                    EyeCareServiceData: ['EyeCareService', 'spinner', '$cookies',
                        function(EyeCareService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(EyeCareService.getEyeCareData(user_data.uuid)).then(function(data) {
                                return data;
                            });
                        }
                    ]
                }
            })
            .state('service.dentalCare', {
                url: '/dentalCare',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/dentalCareService.html',
                        controller: 'DentalCareServiceController'
                    }
                },
                resolve: {
                    DentalCareServiceData: ['DentalCareService', 'spinner', '$cookies',
                        function(DentalCareService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(DentalCareService.getDentalCareData(user_data.uuid)).then(function(data) {
                                return data;
                            });
                        }
                    ],
                }
            })
            .state('service.gynaeCare', {
                url: '/gynaeCare',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/gynaeCareService.html',
                        controller: 'GynaeCareServiceController'
                    }
                },
                resolve: {
                    GynaeCareServiceData: ['GynaeCareService', 'spinner', '$cookies',
                        function(GynaeCareService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(GynaeCareService.getGynaeCareData(user_data.uuid)).then(function(data) {
                                return data;
                            });
                        }
                    ],
                }
            })
            .state('service.medical_second_opinion', {
                url: '/medicalSecondOpinion',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/medicalSecondOpinion.html',
                        controller: 'MedicalSecondOpinionServiceController'
                    }
                },
                resolve: {
                    MedicalSecondOpinionServiceData: ['MedicalSecondOpinionService', 'spinner', '$cookies',
                        function(MedicalSecondOpinionService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(MedicalSecondOpinionService.getmedicalSecondOpinionData(user_data.uuid)).then(function(data) {
                                return data;
                            });
                        }
                    ],
                }
            })
            .state('service.nurseAtHome', {
                url: '/nurseAtHome',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/nurseAtHome.html',
                        controller: 'NurseAtHomeServiceController'
                    }
                },
                resolve: {
                    NurseAtHomeServiceData: ['NurseAtHomeService', 'spinner', '$cookies',
                        function(NurseAtHomeService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(NurseAtHomeService.getNurseAtHomeServiceData(user_data.uuid)).then(function(data) {
                                return data;
                            });
                        }
                    ],
                }
            })
            .state('service.doctorAtHome', {
                url: '/doctorAtHome',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/doctorAtHome.html',
                        controller: 'DoctorAtHomeServiceController'
                    }
                },
                resolve: {
                    DoctorAtHomeServiceData: ['DoctorAtHomeService', 'spinner', '$cookies',
                        function(DoctorAtHomeService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(DoctorAtHomeService.getDoctorAtHomeServiceData(user_data.uuid)).then(function(data) {
                                return data;
                            });
                        }
                    ],
                }
            })
            .state('service.travelEmergency ', {
                url: '/travelEmergency',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/travelEmergency.html',
                        controller: 'TravelEmergencyServiceController'
                    }
                },
                resolve: {
                    TravelEmergencyServiceData: ['TravelEmergencyService', 'spinner', '$cookies',
                        function(TravelEmergencyService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(TravelEmergencyService.getTravelEmergencyServiceData(user_data.uuid)).then(function(data) {
                                return data;
                            });
                        }
                    ],
                }
            })
            .state('service.pharmacy ', {
                url: '/Pharmacy',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/pharmacy.html',
                        controller: 'PharmacyServiceController'
                    }
                },
                resolve: {
                    PharmacyServiceData: ['PharmacyService', 'spinner', '$cookies',
                        function(PharmacyService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(PharmacyService.getPharmacyServiceData(user_data.uuid)).then(function(data) {
                                return data;
                            });
                        }
                    ]
                }
            })
            .state('service.dietician ', {
                url: '/dietician',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/dieticianService.html',
                        controller: 'DieticianController'
                    }
                },
                resolve: {
                    DieticianServiceData: ['DieticianService', 'spinner', '$cookies',
                        function(DieticianService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(DieticianService.getDieticianData(user_data.uuid)).then(function(data) {
                                return data;
                            });
                        }
                    ]
                }
            })
            .state('service.precama ', {
                url: '/precama',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'service': {
                        templateUrl: 'views/dashboard/services/precama.html',
                        controller: 'PrecamaController'
                    }
                },
                resolve: {
                    LastOrdersData: ['PrecamaService', 'spinner', '$cookies',
                        function(PrecamaService, spinner, $cookies) {
                            var user_data = JSON.parse($cookies.get('loggedInUser'));

                            return spinner.forPromise(PrecamaService.lastOrders(user_data.id)).then(function(data) {
                                return data;
                            });
                        }
                    ],
                    PrecamaPartners: ['PrecamaService', 'spinner',
                        function(PrecamaService, spinner) {
                            return spinner.forPromise(PrecamaService.precamaPartners()).then(function(data) {
                                return data;
                            });
                        }
                    ],
                    ResultData: ['PrecamaService', 'spinner',
                        function(PrecamaService, spinner) {
                            return spinner.forPromise(PrecamaService.getResults()).then(function(data) {
                                return data;
                            });
                        }
                    ]

                }
            })
            .state('account', {
                url: '/account',
                abstract: true,
                template: '<div id="wrapper">' +
                    '<div ui-view="dashboardHeader"></div>' +
                    '<div ui-view="dashboardSidebar"></div>' +
                    '<div id="page-wrapper" ui-view="account">' +
                    '</div>' +
                    '</div>'
            })
            .state('account.physical', {
                url: '',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'account': {
                        templateUrl: 'views/dashboard/account.html',
                        controller: 'AccountController'
                    }
                },
                resolve: {
                    members: ['memberService', 'spinner', '$cookies', function(memberService, spinner, $cookies) {
                        var user_data = JSON.parse($cookies.get('loggedInUser'));
                        return spinner.forPromise(memberService.getMembers(user_data.id)).then(function(data) {
                            return data;
                        });
                    }]
                }
            })
            .state('account.setting', {
                url: '/setting',
                views: {
                    'dashboardHeader': {
                        templateUrl: 'views/dashboard/includes/header.html',
                        controller: 'DashboardHeaderController'
                    },
                    'dashboardSidebar': {
                        templateUrl: 'views/dashboard/includes/sidebar.html',
                        controller: 'DashboardSidebarController'
                    },
                    'account': {
                        templateUrl: 'views/dashboard/accountsetting.html',
                        controller: 'AccountController'
                    }
                },
                resolve: {
                    members: function() {
                        return false;
                    },
                }
            }).state('paymentPortal', {
                url: '',
                abstract: true,
                controller: '',
                template: '<div class="page-payment-portal">' +
                    '<div ui-view="success"></div>'

            })
            .state('paymentPortal.physical', {
                url: '/payment/prepare/{user_id}/{membership_type}',
                views: {
                    'success': {
                        templateUrl: 'views/payment_portal/payuform.html',
                        controller: 'PaymentPortalController',
                        resolve: {
                            payuParams: ['paymentPortalService', 'spinner',  '$stateParams', function(paymentPortalService, spinner,  $stateParams) {
                                return spinner.forPromise(paymentPortalService.getPayuParamsForUser($stateParams.user_id, $stateParams.membership_type)).then(function(response) {
                                    return response.data;
                                });
                            }],
                            paytmParams: ['paymentPortalService', 'spinner',  '$stateParams', function(paymentPortalService, spinner,  $stateParams) {

                                return spinner.forPromise(paymentPortalService.getPaytmParamsForUser($stateParams.user_id, $stateParams.membership_type)).then(function(response) {
                                    return response.data;
                                });
                            }]

                        }
                    }
                }
            })
            .state('paymentPortal.success', {
                url: '/payment/success',
                views: {
                    'success': {
                        templateUrl: 'views/payment_portal/success.html'
                    }
                }
            })
            .state('paymentPortal.fail', {
                url: '/payment/fail',
                views: {
                    'success': {
                        templateUrl: 'views/payment_portal/fail.html'
                    }
                }
            });
           
        $urlRouterProvider.otherwise('/login');
        $httpProvider.interceptors.push('HttpInterceptor');
    }).run(['TokenService', 'spinner', '$rootScope', 'ENV', 'GAuth', 'GApi', 'NotifyService', function(TokenService, spinner, $rootScope, ENV, GAuth, GApi, NotifyService) {
        var CLIENT = ENV.googleAPI;
        GAuth.setClient(CLIENT);
        $rootScope.imagePath = ENV.imagePath;
        $rootScope.hostname = ENV.hostname;
        TokenService.refreshToken();
        $rootScope.notificationData = NotifyService.getData();
        return true;
    }]);