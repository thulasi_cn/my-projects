angular.module('hbMainUiApp')
    .factory('NotifyService', ['spinner', '$state', '$cookies', 'NotificationService','$rootScope',function(spinner, $state, $cookies,NotificationService,$rootScope) {
        var getData = function() {
            if($cookies.get('loggedInUser')){
                spinner.forPromise(NotificationService.getPaginateNotifications()).then(function(response){
                var user_data = JSON.parse($cookies.get('loggedInUser'));
                $rootScope.notificationData = response.data.data;
                return true;
                 })
            }
            else{
                $rootScope.notificationData = {};
                return true;
            }
         };

         var getMoreData = function(page_id) {
            if($cookies.get('loggedInUser')){
                spinner.forPromise(NotificationService.getMoreNotifications(page_id)).then(function(response){
                if(page_id!==1){$rootScope.notificationData = $rootScope.notificationData.concat(response.data.data)} else {$rootScope.notificationData = response.data.data};
                return true;
                 })
            }
            else{
                $rootScope.notificationData = {};
                return true;
            }
         };
         return {

             getData: getData,
             getMoreData:getMoreData
         }
    }]);