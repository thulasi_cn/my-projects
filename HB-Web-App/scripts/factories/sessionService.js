"use strict";

angular.module('hbMainUiApp')
    .factory('sessionService', ['userService', 'spinner', '$state', '$rootScope', '$cookies', 'paymentService', 'NotifyService',function(userService, spinner, $state, $rootScope, $cookies, paymentService,NotifyService) {
        var loginData = {
            login: '',
            password: '',
            auth_source: '',
            email: '',
            socialId: ''
        };
        var isStateChangeAllowed = true;
        var loginSuccess = function(response){
            $cookies.put('loggedInUser', JSON.stringify(response.data), {path: '/'},{expires: 365});

            $rootScope.notificationData = NotifyService.getData();
            if ($cookies.subscriptionType) {
                spinner.forPromise(userService.membershipTypes($cookies.subscriptionType)).then(function(response) {
                    $state.go('subscription.payment');
                })
            } else {
                if ((response.data.email.length == '') || (response.data.phone.length == '')) {
                    isStateChangeAllowed = false;
                    $state.go('registration.complete');
                }
                if (isStateChangeAllowed) {
                    $state.go('dashboard.physical');
                } else
                    isStateChangeAllowed = true;
            }



        };

        var webLogin = function(login, password) {
            loginData.login = login;
            loginData.password = password;
            loginData.auth_source = 'web';
            loginData._token = $cookies.corecsrfToken;
            return spinner.forPromise(userService.loginUser(loginData), 'Logging In..').then(loginSuccess, loginError);
        };

        var socialConnect = function(userData) {
            spinner.forPromise(userService.socialConnect(userData)).then(loginSuccess, loginError);
        }

        var loginError = function(error) {
            return error.data;
        };

        return {
            webLogin: webLogin,
            socialConnect: socialConnect
        }
    }]);