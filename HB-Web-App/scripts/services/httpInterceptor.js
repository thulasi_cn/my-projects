angular.module('hbMainUiApp')
.service('HttpInterceptor',['$q','ENV', function($q,ENV) {
    var service = this;

    service.request = function(config) {
        if(config.url.indexOf(ENV.crmUrl,0) > -1){
            config.withCredentials=false;
        }
        return config;
    };

    service.responseError = function(response) {
        if (response.status === 500) {
            response.data = {'server': ['We are facing some issues with the Server. Please try again after some time']};

        }
        return $q.reject(response);
    };
}]);
angular.module('hbMainUiApp')
    .config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
}]);
