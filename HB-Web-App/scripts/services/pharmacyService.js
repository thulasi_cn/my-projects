'use strict';
angular.module('hbMainUiApp')
	.service('PharmacyService', ['$http', 'ENV', function($http, ENV) {
		this.getPharmacyServiceData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=GetPharmacyData&accountid=' + user_uuid);
		}
	}]);