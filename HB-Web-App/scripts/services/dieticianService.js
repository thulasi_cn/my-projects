'use strict';
angular.module('hbMainUiApp')
    .service('DieticianService', ['$http', 'ENV', function($http, ENV) {

        this.getDieticianData = function(user_uuid) {
            return $http.get(ENV.crmUrl + '/webservice.php?operation=GetDieticianData&accountid=' + user_uuid);
        }
    }]);