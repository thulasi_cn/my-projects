'use strict';

angular.module('hbMainUiApp')
    .service('paymentPortalService',['$http','ENV',function($http,ENV){

        this.getPayuParamsForUser=function(user_id,membership_type){
            return $http.get(ENV.coreUrl + '/user_payment/'+user_id+'/'+membership_type, {
                cache: false}).then(function(response){
                return response;
            });
        }

        this.getPaytmParamsForUser=function(user_id,membership_type){
            return $http.get(ENV.coreUrl + '/paytm/'+user_id+'/'+membership_type, {
                cache: false}).then(function(response){
                return response;
            });
        }


    }]);