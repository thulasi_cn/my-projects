'use strict';
angular.module('hbMainUiApp')
    .service('AmbulanceService', ['$http', 'ENV', function($http, ENV) {
        this.getAmbulanceServiceData = function(user_uuid) {
            return $http.get(ENV.crmUrl + '/webservice.php?operation=getAmbulanceServiceData&accountid=' + user_uuid);
        }
    }]);