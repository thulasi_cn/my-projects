'use strict';
angular.module('hbMainUiApp')
	.service('SpaService', ['$http', 'ENV',  function($http, ENV) {

		this.getSpaServiceData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=GetSpaData&accountid=' + user_uuid);
		}
	}]);