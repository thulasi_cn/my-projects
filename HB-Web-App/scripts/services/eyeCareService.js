'use strict';
angular.module('hbMainUiApp')
	.service('EyeCareService', ['$http', 'ENV', function($http, ENV) {

		this.getEyeCareData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=getEyeCareData&accountid=' + user_uuid);
		}
	}]);