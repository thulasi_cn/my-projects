'use strict';
angular.module('hbMainUiApp')
	.service('GynaeCareService', ['$http', 'ENV', function($http, ENV) {
		this.getGynaeCareData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=getGynaeCareData&accountid=' +user_uuid);
		}
	}]);