'use strict';
angular.module('hbMainUiApp')
	.service('MedicalSecondOpinionService', ['$http', 'ENV', function($http, ENV) {
		this.getmedicalSecondOpinionData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=getMedicalSecondOpinionData&accountid=' + user_uuid);
		}
	}]);