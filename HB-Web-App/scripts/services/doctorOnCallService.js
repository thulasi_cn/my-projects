'use strict';
angular.module('hbMainUiApp')
	.service('doctorOnCallService', ['$http', 'ENV', function($http, ENV) {
		this.getdoctorOnCallServiceData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=GetDoctorOnCallData&accountid=' + user_uuid);
		}
	}]);