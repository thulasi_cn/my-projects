'use strict';
angular.module('hbMainUiApp')
	.service('YogaService', ['$http', 'ENV',function($http, ENV) {
		this.getSessionsByAccountId = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=GetYogadata&accountid=' + user_uuid);
		}
		this.Performance = function(contactId) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=customerPerformanceDetails&contactid=' + contactId);
		}
	}]);