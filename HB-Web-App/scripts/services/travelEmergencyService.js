'use strict';
angular.module('hbMainUiApp')
	.service('TravelEmergencyService', ['$http', 'ENV', function($http, ENV) {
		this.getTravelEmergencyServiceData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=getTravelEmergencyServiceData&accountid=' + user_uuid);
		};
	}]);