'use strict';
angular.module('hbMainUiApp')
	.service('NurseAtHomeService', ['$http', 'ENV',  function($http, ENV) {
		this.getNurseAtHomeServiceData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=NursesAlldata&accountid=' + user_uuid);
		}
	}]);