'use strict';

angular.module('hbMainUiApp')
    .service('userService', ['$http','ENV','$cookies','$q',function ($http,ENV,$cookies,$q) {
        this.loginUser=function(data){
            return $http.post(ENV.coreUrl + '/member/login',data);
        }
        this.logoutUser=function(data){
            return $http.get(ENV.coreUrl + '/member/logout',data);
        }
        this.registerUser = function(data){
            return $http.post(ENV.coreUrl + '/member/register',data);
        }
        this.saveGuestUser = function(data){
            return $http.post(ENV.coreUrl + '/member/guest_register',data);
        }
        this.forgotPassword = function(data){
            return $http.post(ENV.coreUrl + '/member/forgot_password',data);
        }
        this.checkActiveToken = function(reset_token){
            return $http.get(ENV.coreUrl + '/member/checkToken/'+reset_token);
        }
        this.resetPassword = function(data){
            return $http.post(ENV.coreUrl + '/member/update/password',data);
        }
        this.checkIfUserExists = function(data){
            return $http.post(ENV.coreUrl + '/member/check_user_exists',data);
        }
        this.activateuser = function(data){
            return $http.post(ENV.coreUrl + '/members/active_existaccount',data);
        }
        this.changePassword = function(data){
            return $http.post(ENV.coreUrl + '/member/password/change',data);
        }
        this.socialConnect = function(data){
            return $http.post(ENV.coreUrl + '/member/social_connect',data);
        }
        this.getLoggedinUser = function(data){
            return $http.get(ENV.coreUrl + '/me');
        }        
        this.membershipTypes = function(membershipTypeUuid){
           return $http.get(ENV.coreUrl + '/membershiptype/'+membershipTypeUuid).success(function(res){
                  return $cookies.orderDetails = JSON.stringify(res);
           });                    
        }
        this.getZones = function(){
            return $http.get(ENV.coreUrl + '/zones');
        }
    }]);



