'use strict';

angular.module('hbMainUiApp')
    .service('NotificationService',['$http','ENV', '$rootScope','$cookies',function($http,ENV, $rootScope,$cookies){
    	this.getAllNotifications = function() {
        	var user_data = JSON.parse($cookies.get('loggedInUser'));
            return $http.get(ENV.coreUrl +'/notifications/all/'+user_data.id);
        };

        this.getPaginateNotifications = function() {
            var user_data = JSON.parse($cookies.get('loggedInUser'));
            return $http.get(ENV.coreUrl +'/notifications/paginate/'+user_data.id);
        };

        this.getMoreNotifications = function(page_id) {
            if(!page_id){page_id=1};
            var user_data = JSON.parse($cookies.get('loggedInUser'));
            return $http.get(ENV.coreUrl +'/notifications/paginate/'+user_data.id+'/?page='+page_id);
        };

        this.getNotification = function(notification_id){
        	return $http.get(ENV.coreUrl +'/notifications/details/'+notification_id);
        };
    }]);
