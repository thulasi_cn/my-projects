'use strict';
angular.module('hbMainUiApp')
    .service('PrecamaService', ['$http', 'ENV',  function($http, ENV) {
        this.lastOrders = function(user_id) {
            return $http.get(ENV.coreUrl + '/orders?user_id=' + user_id);
        };
        this.getResults = function() {
            return $http.get(ENV.diagnosticUrl + '/result/search');
        };
        this.precamaPartners = function() {
            return $http.get(ENV.diagnosticUrl + '/organizations');
        };


    }]);