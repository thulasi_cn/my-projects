'use strict';
angular.module('hbMainUiApp')
	.service('DoctorAtHomeService', ['$http', 'ENV', function($http, ENV) {

		this.getDoctorAtHomeServiceData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=getDoctorAtHomeServiceData&accountid=' + user_uuid);
		}
	}]);