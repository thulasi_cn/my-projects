'use strict'
angular.module('hbMainUiApp')
    .service('paymentService', ['$http', 'ENV', '$cookies', function($http, ENV, $cookies) {

        this.payUCredential = function(data) {
            return $http.post(ENV.coreUrl + '/payu/credential', data);
        };
        this.payTmCredential = function(data) {

            return $http.post(ENV.coreUrl + '/paytm/credential', data);
        };

        this.payUPostCredentials = function(user_data) {
            var payUPosted = {};
            var orderDetails = $.parseJSON($cookies.orderDetails);
            payUPosted.email = user_data.email;
            payUPosted.surl = ENV.coreUrl + '/membership/payu/success';
            payUPosted.furl = ENV.hostname + '/member/#/paymentFail';
            payUPosted.firstname = user_data.member.first_name;
            payUPosted.lastname = user_data.member.last_name;
            payUPosted.phone = user_data.member.phone;
            payUPosted.udf4 = ENV.hostname + '/member/#/paymentSuccess';
            payUPosted.udf1 = user_data.id;
            payUPosted.amount = orderDetails.price;
            payUPosted.udf5 = 'Membership';
            payUPosted._token = $cookies.corecsrfToken;
            payUPosted.productinfo = JSON.stringify(orderDetails);
            return payUPosted;
        }

        this.payTmPostCredentials = function(user_data) {
            var payTmPosted = {};
            var membarship = 'Membership';
            var orderDetails = $.parseJSON($cookies.orderDetails);
            payTmPosted.CALLBACK_URL=ENV.coreUrl + '/order/paytm/success';
            payTmPosted.ORDER_ID=Math.random().toString(36).substring(10);
            payTmPosted.CUST_ID=user_data.id;
            payTmPosted.MOBILE_NO=user_data.phone;
            payTmPosted.EMAIL=user_data.email;
            payTmPosted.TXN_AMOUNT=orderDetails.price;
            var extra_param = user_data.id+'|'+user_data.member.id+'|'+membarship+'|'+orderDetails.id;
            payTmPosted.MERC_UNQ_REF=extra_param;
            payTmPosted._token = $cookies.corecsrfToken;
            return payTmPosted;
        }

        
    }]);