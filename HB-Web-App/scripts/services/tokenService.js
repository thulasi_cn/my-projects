'use strict';

angular.module('hbMainUiApp')
    .factory('TokenService', ['$http','$cookies','ENV','spinner', function ($http,$cookies,ENV,spinner) {
        function getToken() {
            return spinner.forPromise($http.get(ENV.coreUrl + '/auth',{cache:false})).then(
                success,
                fail
            );
        }

        function success(response) {
            if(!response.data.loggedIn){
                delete $cookies.loggedInUser;
            }
            $cookies.corecsrfToken=response.data.token;
            return response;
        }

        function fail(response) {
            return response;
        }

        function getDToken() {
            return spinner.forPromise($http.get(ENV.diagnosticUrl + '/getToken',{cache:false})).then(
                dsuccess,
                dfail
            );
        }

        function dsuccess(response) {
            $cookies.dcsrfToken=response.data;
            return response;
        }

        function dfail(response) {
            return response;
        }

        function refreshToken(){
            getToken();
            getDToken();
        }

        return {
            get: getToken,
            getDToken: getDToken,
            refreshToken: refreshToken
        };
}]);
