'use strict';
angular.module('hbMainUiApp')
    .service('DentalCareService', ['$http', 'ENV', function($http, ENV) {

        this.getDentalCareData = function(user_uuid) {
            return $http.get(ENV.crmUrl + '/webservice.php?operation=getDentalCareData&accountid=' + user_uuid);
        }
    }]);