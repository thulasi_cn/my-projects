'use strict';
angular.module('hbMainUiApp')
	.service('PhysiotherapyService', ['$http', 'ENV', function($http, ENV) {
		this.getPhysiotherapyServiceData = function(user_uuid) {
			return $http.get(ENV.crmUrl + '/webservice.php?operation=getPhysiotherapyData&accountid=' + user_uuid);
		}
	}]);