'use strict';

angular.module('hbMainUiApp')
    .controller('DashboardHeaderController', ['$scope', '$cookies', '$state', 'userService','spinner','ENV', 'NotifyService',function($scope, $cookies, $state, userService, spinner,ENV,NotifyService) {
        var user_data = JSON.parse($cookies.get('loggedInUser'));
        if (!user_data) {
            $state.go('user.physical');
        }else if (user_data.phone == '') {
              $state.go('registration.complete');
        }
        if (JSON.parse($cookies.get('loggedInUser'))) {
            $scope.userName = JSON.parse($cookies.get('loggedInUser'));
        }

        $scope.logout = function() {
            spinner.forPromise(userService.logoutUser()).then(function(response) {
                delete $cookies.subscriptionType;
                $cookies.remove('loggedInUser', {path: '/'});
                $state.go('user.physical');
            });
        }
       
        $scope.settings = function() {
            $state.go('account.setting');
        }
        $scope.showMoreNotifications = function(page_id) {
            NotifyService.getMoreData(page_id);
            $state.go('dashboard.notifications');
        }

    }]);