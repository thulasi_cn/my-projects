'use strict';

angular.module('hbMainUiApp')
	.controller('PaymentController', ['$scope', '$cookies', 'payUDetails','$state','spinner','userService','payTmDetails', function($scope, $cookies,payUDetails,$state,spinner,userService,payTmDetails) {

		if(payUDetails && payTmDetails){
			$scope.paymentDetail = payUDetails.data.posted;
			$scope.payTmDetail = payTmDetails.data.posted;
			$scope.payuReady = true;
			delete $cookies.subscription;
			$scope.waitLoader = false;
			$scope.orderInfo = $.parseJSON($scope.paymentDetail.productinfo);
			$scope.paymentType= 'payumoney';
			$scope.payment = function(){
				$scope.payuReady = false;
				$scope.waitLoader = true;
				payuForm.action=payUDetails.data.payuUrl.concat('/_payment');
				payuForm.submit();
			};
			$scope.processPaytm = function(){
				$scope.paytmReady=false;
				$scope.waitLoader=true;
				paytmForm.action=payTmDetails.data.paytmUrl;
				paytmForm.submit();
			};
		}

		$scope.selectPaymentGateway=function(paymentType){
			$scope.paymentType = paymentType;
		};


		$scope.goToDashboard = function(){
				spinner.forPromise(userService.getLoggedinUser()).then(function(response){
					$cookies.put('loggedInUser', JSON.stringify(response.data), {path: '/'},{expires: 365});
					$state.go('dashboard.physical');

				});
			};
	}]);