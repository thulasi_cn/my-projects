'use strict';

angular.module('hbMainUiApp').controller('YogaController', ['$scope', '$http', 'ENV', 'yogaData', 'YogaService', 'spinner',
    function($scope, $http, ENV, yogaData, YogaService, spinner) {

        $scope.yogaData = yogaData.data;
        $scope.perfGraph = function(contactId) {
            spinner.forPromise(YogaService.Performance(contactId)).success(function(memberPerformance) {
                $scope.labels = [];
                $scope.data = [
                    []
                ];
                $scope.series = ['Ratings'];
                angular.forEach(memberPerformance.result.performance_details, function(value, key) {
                    $scope.data[0].push(value.ratings);
                });
                for (var i = 0; i < memberPerformance.result.performance_details.length; i++) {
                    $scope.labels.push(i + 1);
                };
            });
        }
        if (yogaData.data.result.performance) {
            $scope.selectMemberPerformance = yogaData.data.result.performance[0];
            $scope.perfGraph(yogaData.data.result.performance[0].contactid);
        }
        $scope.performanceGraph = function(member) {
            $scope.perfGraph(member.contactid);
        };
    }
]);