'use strict';

angular.module('hbMainUiApp')
	.controller('EyeCareServiceController', ['$scope', 'EyeCareServiceData',
		function($scope, EyeCareServiceData) {
			$scope.eyeCareData = EyeCareServiceData.data.result;
		}
	]);