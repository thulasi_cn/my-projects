'use strict';

angular.module('hbMainUiApp')
    .controller('DoctorOnCallController', ['$scope', '$http', 'ENV', 'doctorOnCallServiceData','$state','spinner',
     function($scope, $http, ENV, doctorOnCallServiceData,$state,spinner) {
            $scope.doctorOnCallData = doctorOnCallServiceData.data.result;
            $scope.add_feedback = true;
            $scope.add_feedback_form = false;
            $scope.showFeedbackForm = function() {
                $scope.add_feedback = false;
                $scope.add_feedback_form = true;
            };
            $scope.returnBack = function() {
                $scope.add_feedback = true;
                $scope.add_feedback_form = false;
            };

            $scope.getFeedback = function(feedbackData, ticketid) {
                $scope.data = feedbackData;
                $scope.data._token = $cookies.corecsrfToken;
                $scope.data.ticketid = ticketid;
                spinner.forPromise($http.post(ENV.coreUrl + "/api/feedback/save", $scope.data).success(function(response) {
                    if (response==1){
                        $state.go($state.current, {}, {reload: true});
                    }else if(response == 0){
                      $scope.error = "Please add feedback correctly";  
                    }
                }));
            };
        
    }]);