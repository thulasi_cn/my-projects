'use strict';

angular.module('hbMainUiApp')
	.controller('NurseAtHomeServiceController', ['$scope', 'NurseAtHomeServiceData',
		function($scope, NurseAtHomeServiceData) {
			$scope.nurseAtHome = NurseAtHomeServiceData.data.result;
		}
	]);