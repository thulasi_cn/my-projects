'use strict';

angular.module('hbMainUiApp')
	.controller('DentalCareServiceController', ['$scope', 'DentalCareServiceData',
		function($scope, DentalCareServiceData) {
			$scope.dentalCareData = DentalCareServiceData.data.result;
		}
	]);