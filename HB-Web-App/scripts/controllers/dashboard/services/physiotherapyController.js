'use strict';

angular.module('hbMainUiApp')
    .controller('PhysiotherapyController', ['$scope', '$http', 'ENV', 'physiotherapyServiceData',
        function($scope, $http, ENV, $cookies, physiotherapyServiceData) {
            $scope.selectMemberPerformance = false;
            if (physiotherapyServiceData) {
                $scope.selectMemberPerformance = physiotherapyServiceData.data.result.performance[0];
            $scope.invoices = physiotherapyServiceData.data.result.invoices;
            $scope.lastSessions = physiotherapyServiceData.data.result.lastSessions;
            $scope.packages = physiotherapyServiceData.data.result.packages;
            $scope.trainerInfo = physiotherapyServiceData.data.result.trainerInfo;
            $scope.upcomingSessions = physiotherapyServiceData.data.result.upcomingSessions;
            }

        }
    ]);