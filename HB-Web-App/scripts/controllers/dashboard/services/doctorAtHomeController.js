'use strict';

angular.module('hbMainUiApp')
	.controller('DoctorAtHomeServiceController', ['$scope', 'DoctorAtHomeServiceData',
		function($scope, DoctorAtHomeServiceData) {
			$scope.doctorAtHome = DoctorAtHomeServiceData.data.result;
		}
	]);