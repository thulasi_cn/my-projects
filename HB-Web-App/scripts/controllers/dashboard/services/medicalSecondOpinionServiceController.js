'use strict';

angular.module('hbMainUiApp')
	.controller('MedicalSecondOpinionServiceController', ['$scope', 'MedicalSecondOpinionServiceData',
		function($scope, MedicalSecondOpinionServiceData) {
			$scope.MedicalSecondOpinionData = MedicalSecondOpinionServiceData.data.result;
		}
	]);