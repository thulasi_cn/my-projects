'use strict';

angular.module('hbMainUiApp')
    .controller('PharmacyServiceController', ['$scope', '$cookies', 'PharmacyServiceData', 'spinner', '$http', 'ENV', '$state', 'FileUploader', '$cookies', '$rootScope',
        function($scope, $cookies, PharmacyServiceData, spinner, $http, ENV, $state, FileUploader, $rootScope) {
            $scope.pharmacyData = PharmacyServiceData.data.result;
            $scope.add_feedback = true;
            $scope.add_feedback_form = false;

            $scope.showFeedbackForm = function(ticketid) {
                $scope.testId = ticketid;
            };
            $scope.returnBack = function() {
                $scope.testId = '';
            };
            $scope.getFeedback = function(feedbackData, ticketid) {
                $scope.data = feedbackData;
                $scope.data._token = $cookies.corecsrfToken;
                $scope.data.ticketid = ticketid;
                spinner.forPromise($http.post(ENV.coreUrl + "/api/feedback/save", $scope.data).success(function(response) {
                    if (response == 1) {
                        $state.go($state.current, {}, {
                            reload: true
                        });
                    } else if (response == 0) {
                        $scope.error = "Please add feedback correctly";
                    }
                }));
            };
            $scope.createTicket = function(tkt) {
                $scope.newTkt = tkt;
                $scope.newTkt.parent_id = $rootScope.loggedInUser.sync.reference_id;
                $scope.newTkt.servicecategory = PharmacyServiceData.data.result.serviceCategory[0].servicecategoriesid;
                $scope.newTkt._token = $cookies.corecsrfToken;

                spinner.forPromise($http.post(ENV.coreUrl + "/api/ticket/save", $scope.newTkt).success(function(response) {
                    if (response) {
                        $scope.ticketSuccess = response;
                        newTicketForm.reset();
                    }
                }));

            };

        }
    ]);