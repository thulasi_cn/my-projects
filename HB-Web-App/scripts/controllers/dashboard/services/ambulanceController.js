'use strict';

angular.module('hbMainUiApp')
	.controller('AmbulanceServiceController', ['$scope', 'AmbulanceServiceData', function($scope, AmbulanceServiceData) {
		$scope.ambulanceData = AmbulanceServiceData.data.result;
	}]);