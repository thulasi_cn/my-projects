'use strict';

angular.module('hbMainUiApp')
	.controller('DieticianController', ['$scope', 'DieticianServiceData',
		function($scope, DieticianServiceData) {
			$scope.dieticianData = DieticianServiceData.data.result;

		}
	]);