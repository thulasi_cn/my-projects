'use strict';

angular.module('hbMainUiApp')
	.controller('GynaeCareServiceController', ['$scope', 'GynaeCareServiceData',
		function($scope, GynaeCareServiceData) {
			$scope.gynaeCareData = GynaeCareServiceData.data.result;
		}
	]);