'use strict';

angular.module('hbMainUiApp')
	.controller('spaController', ['$scope', 'spaServiceData',
		function($scope, spaServiceData) {
			$scope.spaData = spaServiceData.data;
		}
	]);