'use strict';

angular.module('hbMainUiApp')
    .controller('PrecamaController', ['$scope', '$http', 'ENV', 'LastOrdersData',
        'PrecamaPartners', 'ResultData',
        function($scope, $http, ENV, LastOrdersData, PrecamaPartners, ResultData) {
            $scope.PrecamaPartnersData = PrecamaPartners.data.data;
            $scope.ResultList = ResultData.data.data;
            $scope.currentActiveOrders = [];
            $scope.LastOrdersData = [];

            angular.forEach(LastOrdersData.data, function(value, key) {
                if (value.isActive == 1) {
                    angular.forEach(value.products, function(value, key) {
                        $scope.currentActiveOrders.push(value);
                    });
                } else {
                    angular.forEach(value.products, function(value, key) {
                        $scope.LastOrdersData.push(value);
                    });

                }

            });


        }
    ]);