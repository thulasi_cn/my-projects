'use strict';

angular.module('hbMainUiApp')
	.controller('TravelEmergencyServiceController', ['$scope', 'TravelEmergencyServiceData',
		function($scope, TravelEmergencyServiceData) {
			$scope.travelEmergencyData = TravelEmergencyServiceData.data.result;
		}
	]);