'use strict'
angular.module('hbMainUiApp')
    .controller('PaymentPortalController',['$scope','payuParams','$sce','paytmParams', function($scope,payuParams,$sce,paytmParams){
        
        if(payuParams && paytmParams){
            $scope.paymentType= 'payumoney';
            $scope.payUPosted=payuParams.posted;
            $scope.paytmPosted=paytmParams.posted;
            $scope.productInfo= JSON.parse(payuParams.posted.productinfo);           
            $scope.selectPaymentGateway=function(paymentType){
                $scope.paymentType = paymentType;
            };
            $scope.payment = function(){
                payuForm.action=$sce.trustAsResourceUrl(payuParams.payuUrl.concat('/_payment'));                
                payuForm.submit();
            };
            $scope.processPaytm = function(){
                paytmForm.action=$sce.trustAsResourceUrl(paytmParams.paytmUrl);  
                paytmForm.submit();
            };
        }
    }]);