'use strict';

angular.module('hbMainUiApp')
	.controller('DashboardSidebarController', ['$scope', '$cookies', 'ENV',function($scope, $cookies,ENV) {

		if(JSON.parse($cookies.get('loggedInUser'))){
			$scope.userData = JSON.parse($cookies.get('loggedInUser'));
			$scope.imageUrl = ENV.imagePath + '/member/profile/thumb/' +  $scope.userData.member.uuid + '.jpg';		
		}

	}]);