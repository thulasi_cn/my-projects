'use strict';

angular.module('hbMainUiApp')
    .controller('AccountController', ['$scope', '$state', 'spinner', 'memberService', 'userService', 'members', '$cookies', function($scope, $state, spinner, memberService, userService, members, $cookies) {
        var user_data = JSON.parse($cookies.get('loggedInUser'));

        if (user_data !== undefined) {
            $scope.userData = JSON.parse($cookies.get('loggedInUser'));
            $scope.members = members;
        };


        if(!parseInt(user_data.member.dob))
            $scope.userData['dobBool']=1;
        else
            $scope.userData['dobBool']=0;

        $scope.updateUser = function() {
            if ($scope.updateUserForm.$valid) {
                $scope.successMsg = '';
                $scope.errors = {};
                var postData = {};
                postData = angular.copy($scope.userData.member);
                postData['_token'] = $cookies.corecsrfToken;
                spinner.forPromise(memberService.updateMember(postData)).then(updateSuccess,showError);
            };

        };
        $scope.changepassword = function(user) {
            if ($scope.changePasswordForm.$valid) {
                $scope.errors = '';
                $scope.success='';
                var New_password = angular.copy(user);
                New_password['user_id'] = user_data.id;
                New_password['_token'] = $cookies.corecsrfToken;
                New_password['auth_source'] = user_data.auth_source;
                spinner.forPromise(userService.changePassword(New_password)).then(
                    function(response) {
                        changePasswordForm.reset();
                        $scope.success = response.data;
                    },
                    showError
                );
            };

        }

        var updateSuccess = function(response) {
            user_data.member = response.data;
            $cookies.put('loggedInUser', JSON.stringify(user_data), {path: '/'},{expires: 365});
            $scope.success = "Account Updated Successfully."
        };

        var showError = function(response) {
            $scope.errors = {};
            return angular.forEach(response.data, function(errors, field) {
                return $scope.errors[field] = errors.join(', ');
            });
        };

    }]);