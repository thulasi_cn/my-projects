"use strict";

angular.module('hbMainUiApp')
    .controller('LoginController', ['spinner', '$scope', '$cookies', '$state', 'sessionService','userService', function(spinner, $scope, $cookies, $state, sessionService, userService) {
        if($cookies.get('loggedInUser')) {
            $state.go('user.physical');
        }
        
        if($cookies.subscriptionType)
        delete $cookies.subscriptionType;

        $scope.user = {};
        $scope.userLogin = function() {
            if ($scope.loginForm.$valid) {
                spinner.forPromise(sessionService.webLogin($scope.user.login, $scope.user.password)).then(function(error) {
                    $scope.errors = error;
                });
            } else{
                return false;
            }
        };
        $scope.forgotPassword = function(forgotPasswordForm) {
            if (forgotPasswordForm.$valid) {
                $scope.user._token = $cookies.corecsrfToken;
                spinner.forPromise(userService.forgotPassword($scope.user)).then(function(success) {
                    $scope.user = '';
                    forgotPasswordForm.$setPristine(true);
                    return $scope.success = success.data;
                }, function(error) {
                    $scope.user = '';
                    forgotPasswordForm.$setPristine(true);
                    return $scope.errors = error.data;
                });
            };

        };
        $scope.backToLogin = function() {
            $state.go('user.physical');
        };


    }]);