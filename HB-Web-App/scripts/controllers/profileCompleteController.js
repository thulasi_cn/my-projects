'use strict';
angular.module('hbMainUiApp')
    .controller('ProfileCompleteController', ['$scope', 'userService', '$state', '$location', 'spinner', '$rootScope', 'memberService','$cookies', 'NotifyService',function ($scope, userService, $state, $location, spinner, $rootScope, memberService,$cookies,NotifyService) {

        var userData = JSON.parse($cookies.get('loggedInUser'));

        if(userData.phone != '') {
            $state.go('dashboard.physical');
        }

        if(userData){
            $scope.profileCompleteData = angular.copy(userData);
            $scope.profileCompleteData.first_name = angular.copy(userData.member.first_name);
            $scope.profileCompleteData.last_name = angular.copy(userData.member.last_name);
        }else{
            $state.go('user.physical');
        }

        spinner.forPromise(userService.getZones()).then(function (response) {
            $scope.zones = response.data;
        }, function (err) {});

        $scope.completeProfile = function (profileCompleteForm) {
            if (profileCompleteForm.$valid) {
                $scope.errors = '';
                userData.member.phone = $scope.profileCompleteData.phone;
                userData.phone = $scope.profileCompleteData.phone;
                userData.email = $scope.profileCompleteData.email;
                userData.member.city = $scope.profileCompleteData.city;
                userData.member.post_code = $scope.profileCompleteData.post_code;
                userData.member.gender = $scope.profileCompleteData.gender;
                userData.member.first_name = $scope.profileCompleteData.first_name;
                userData.member.last_name = $scope.profileCompleteData.last_name;
                userData['_token'] = $cookies.corecsrfToken;

                spinner.forPromise(memberService.updateMemberSocial(userData)).then(function (response) {
                        $cookies.put('loggedInUser', JSON.stringify(response.data), {path: '/'},{expires: 365});
                        $rootScope.notificationData = NotifyService.getData();
                        $state.go('dashboard.physical');
                    },
                    function (response) {
                        var errorMessage = '';
                        if (response.data.phone) {
                            $scope.errors = "This phone number is already taken"
                        }
                        else {
                            $scope.errors = "Login failed"
                        }
                    }
                );
            }else{
                return false;
            }
        };

    }]);

