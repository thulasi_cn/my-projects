'use strict';

angular.module('hbMainUiApp').controller('NotificationController',['$scope', '$http', 'ENV', 'notificationDetailData', 'spinner',
    function($scope, $http, ENV, notificationDetailData, spinner) {
    	$scope.notificationDetailData = notificationDetailData.data;
		}
    ]).controller('AllNotificationController',['$rootScope','$scope', '$http', 'ENV', 'notificationsData', 'spinner','NotifyService',
    function($rootScope, $scope, $http, ENV, notificationsData, spinner, NotifyService) {
        var pagination_id = 2;
    	$scope.loadMoreNotification = function () {
            NotifyService.getMoreData(pagination_id);
            pagination_id++;
    	}
    }
    ]);