'use strict';

angular.module('hbMainUiApp')
    .controller('RegistrationController', ['$scope', '$rootScope', '$state', 'spinner', 'memberService', 'userService', 'sessionService', '$cookies', '$location','ngDialog', function($scope, $rootScope, $state, spinner, memberService, userService, sessionService, $cookies, $location,ngDialog) {

        $scope.signUpData = {};
        $scope.registerData = {};
        var newuserPhone = '';
        $scope.newuser = {};
        var userData = $cookies.get('loggedInUser');
        if ($location.search().membershiptype) {
            if (userData) {
                $state.go('dashboard.physical');
            }else{
                $cookies.subscriptionType = $location.search().membershiptype;
            }
        } 

        newuserPhone = $cookies.newuserPhone;
        if (newuserPhone != '') {
            $scope.newuser.phone = newuserPhone;
        }

        var registrationError = function(response) {
            $scope.errors = {};
            return angular.forEach(response.data, function(error, field) {
                $scope.errors[field] = error[0];
                return $scope.errors;
            });
        };

        var registrationSuccess = function(response) {
            $scope.warning = {};
            if (response.data.member) {
                sessionService.webLogin($scope.registerData.email, $scope.registerData.password);
            }
            if (response.data.status == "sendactivationcode") {
                $state.go('user.activate');
            } else if (response.data == "Existing active member") {
                $scope.warning = 'You an Existing active member. Please login to continue.';
                $state.go('user.physical');
            } else if (response.data.phone == true) {
                $scope.warning = 'Phone Number already taken';
            } else if (response.data.email == true) {
                $scope.warning = 'Email already taken';
            }
        };

        $scope.registerUser = function(registrationForm) {
            $scope.errors = '';
            $scope.warning ='';
            if (registrationForm.$valid) {
                $scope.registerData = angular.copy($scope.signUpData);
                $scope.registerData._token = $cookies.corecsrfToken;
                $cookies.newuserPhone = $scope.registerData.phone;
                spinner.forPromise(memberService.register($scope.registerData)).then(registrationSuccess, registrationError);
            } else
                return true
        };
        $scope.resetRegistrationForm = function(registrationForm) {
            $scope.signUpData = {};
            registrationForm.$setPristine(true);
        }

        $scope.activateUser = function() {
            if ($scope.activationForm.$valid) {
                $scope.newuser._token = $cookies.corecsrfToken;
                spinner.forPromise(userService.activateuser($scope.newuser)).then(activeSuccess, activeError);
            }

        };
        var activeSuccess = function(response) {
            delete $cookies.newuserPhone;
            $cookies.loggedInUser = JSON.stringify(response.data);
            $state.go('dashboard.physical');

        };

        var activeError = function(response) {
            return $scope.errors = response.data;
        };
        $scope.termsConditions = function(){
            ngDialog.open({
                template: 'views/user/termsandconditions.html',
                closeByEscape : true,
                closeByDocument : false
            });
        };

    }]);