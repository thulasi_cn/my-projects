'use strict';

angular.module('hbMainUiApp')
    .controller('AccountActivationController',['$scope','activatedUser','userService','$state','spinner','$cookies', function ($scope, activatedUser, userService,$state,spinner,$cookies) {
        if(activatedUser==null){
            $state.go('user.invalidurl');
        }
        $scope.user = {
            email: activatedUser.email,
            reset_code: activatedUser.reset_code
        };

        $scope.resetPassword = function(){
            if($scope.user.password === $scope.user.confirm_password){
                var postData=angular.copy($scope.user);
                postData._token=$cookies.corecsrfToken;
                spinner.forPromise(userService.resetPassword(postData)).then(function(data){
                    //$state.go('registration.confirmation');
                });
            }
        };

        $scope.resetUserPassword = function(){
            if($scope.user.password === $scope.user.confirm_password){
                var postData=angular.copy($scope.user);
                postData._token=$cookies.corecsrfToken;
                spinner.forPromise(userService.resetPassword(postData)).then(function(data){
                    $scope.successMessage = "Password changed successfully";
                });
            }
        }

    }]);
