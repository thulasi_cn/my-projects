var gulp = require('gulp');

var gulp = require("gulp");
var gutil = require("gulp-util");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var minifyCSS = require("gulp-minify-css");
var ngAnnotate = require('gulp-ng-annotate');
var templateCache = require('gulp-angular-templatecache');
var rename = require("gulp-rename");
//for cleaning out dist/ directory before build:
var del = require('del');
var debug = require('gulp-debug');
var gulpsync = require('gulp-sync')(gulp);
var path = require('path');

//var pathDef = require("./lib/main/package.json");
var depsFilePath = path.resolve(process.cwd(), "config/files/default.dependencies.json");
var pathDef = require(depsFilePath);

var paths = {
    build: {
        main: './public/dist/'
    },
    files: {
        lib: pathDef.lib,
        dev: pathDef.dev
    }

};

var prepareFileNames = function(fileNames){
    var libArray = [];
    fileNames.forEach(function (item) {
        libArray.push('public' + item);
    });
    return libArray;
}

var handleBuild = function (fileNames, toLib) {
    var libArray = prepareFileNames(fileNames);
    return gulp.src(libArray)
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(concat(toLib))
       // .pipe(debug())
        .pipe(gulp.dest('public/dist/'));
};

var handleNonMinifiedBuild = function (fileNames, toLib) {
    var libArray = prepareFileNames(fileNames);
    return gulp.src(libArray).pipe(concat(toLib))
     //   .pipe(debug())
        .pipe(gulp.dest('public/dist/'));
};

var handleBuildCss = function (fileNames, toLib) {
    var libArray = prepareFileNames(fileNames);
    return gulp.src(libArray)
            .pipe(minifyCSS())
            .pipe(concat(toLib))
            .pipe(gulp.dest('public/dist/'));
};

var handleNonMinifiedBuildCss = function (fileNames, toLib) {
    var libArray = prepareFileNames(fileNames);
    return gulp.src(libArray)
            .pipe(concat(toLib))
            .pipe(gulp.dest('public/dist/'));
};

//task to clear out dist/ folder befor building out deployment version of app - runs before every other task in 'gulp build'
gulp.task('empty-dist', function () {
    return del(paths.build.main + '*');
});

gulp.task('build-vendor-js', function () {
    return handleBuild(paths.files.lib.js, 'lib.min.js');
});

gulp.task('build-app-js', function () {
    return handleBuild(paths.files.dev.js, 'app.min.js');
});

gulp.task('build-vendor-css', function () {
    return handleBuildCss(paths.files.lib.css, 'lib.min.css');
});

gulp.task('build-app-css', function () {
    return handleBuildCss(paths.files.dev.css, 'app.min.css');
});

gulp.task('build-non-minified-vendor-js', function () {
    return handleNonMinifiedBuild(paths.files.lib.js, 'lib.js');
});

gulp.task('build-non-minified-app-js', function () {
    return handleNonMinifiedBuild(paths.files.dev.js, 'app.js');
});

gulp.task('build-non-minified-vendor-css', function () {
    return handleNonMinifiedBuildCss(paths.files.lib.css, 'lib.css');
});

gulp.task('build-non-minified-app-css', function () {
    return handleNonMinifiedBuildCss(paths.files.dev.css, 'app.css');
});

gulp.task('build-async', ['build-vendor-js', 'build-app-js', 'build-vendor-css', 'build-app-css']);

gulp.task('build-async-non-minified', ['build-non-minified-vendor-js','build-non-minified-app-js', 'build-non-minified-vendor-css', 'build-non-minified-app-css']);

gulp.task('build-both',['build-async', 'build-async-non-minified']);

gulp.task('build', gulpsync.sync(['empty-dist','build-both']));


gulp.task('default', function () {
    // place code for your default task here
});
