window.console = window.console || {};
window.console.log = window.console.log || function () {};

var app = angular.module('go', ['ui.router', 'ngSanitize', 'ngFacebook', 'ngAnimate', 'ui.bootstrap', 'chieffancypants.loadingBar', 'ng.deviceDetector',
  'ui.bootstrap.modal', 'popoverToggle', 'satellizer', 'angular-growl', 'angular-loading-bar', 'jcs-autoValidate', 'rorymadden.date-dropdowns', 'ngCookies',
  'angular-jqcloud', 'LocalStorageModule', 'akoenig.deckgrid', 'angular-images-loaded', 'ngScrollable',
  'pascalprecht.translate', 'oc.lazyLoad', 'ng-jws', 'angularMoment', 'infinite-scroll', 'checklist-model',
  'mainModule', 'service.config', 'navigationModule', 'authentication', 'usersModule', 'filters','adsModule', 'searchModule', 'directives', 'commonModule', 'taxonomyModule', 'spamModule', 'maintenanceModule', 'toggle-switch', 'ngjsColorPicker','perfect_scrollbar', 'slickCarousel', 'angular-intro','rzModule','tribeModule','policyModule']);
var tokenAppConfig = {
    /* When set to false a query parameter is used to pass on the auth token.
     * This might be desirable if headers don't work correctly in some
     * environments and is still secure when using https. */
    useAuthTokenHeader: false
};

var liveAPIhost = GOWEB_CONF.liveAPIhost ? GOWEB_CONF.liveAPIhost : window.location.hostname;//'li1271-97.members.linode.com';
/*
app.config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
});
*/

app.config(function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.spinnerTemplate = '<div id="custom-loading-bar-spinner" class="pace-activity"></div>';
});

app.config(['ngJwsProvider', function (ngJwsProvider) {
    //var url = 'ws://' + location.hostname + ':8787/jWebSocket/jWebSocket';
    var url = GOWEB_CONF.webSocketHost ? GOWEB_CONF.webSocketHost : 'ws://' + GOWEB_CONF.liveAPIhost + ':8787/jWebSocket/jWebSocket';
    ngJwsProvider.setUrl(url);
    ngJwsProvider.setUsername('root');
    ngJwsProvider.setPassword('root');
}]);

app.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: 'js/app/localization/locale/',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage(GOWEB_CONF.defaultLang);
    $translateProvider.fallbackLanguage(GOWEB_CONF.defaultLang);
    $translateProvider.useSanitizeValueStrategy('sanitize');
}]);

app.constant('_', window._);
app.run(function ($rootScope) {
    $rootScope._ = window._;
});

app.config(['$authProvider',
    function ($authProvider) {
        $authProvider.twitter({
            //url: '//' + window.location.hostname + ':8080/olyfeWS/auth/twitter',
            //url: '//' + liveAPIhost + ':8080/olyfeWS/auth/twitter',
            url: GOWEB_CONF.apiUrl + 'auth/twitter',
            loginRedirect: '/#/user/profile'
        });
}]);

app.config(['$facebookProvider',
 function ($facebookProvider) {

        // var apiKeyConfig = {
        //     'beta.zimgo.com':1595125600735687,
        //     'li1268-67.members.linode.com:3000': 370871136387450,
        //     '45.79.169.67:3000': 1088286371217849,
        //     'li859-165.members.linode.com:3000': 1704217103142780,
        //     'gosocial.devel:3030': 1421940601443356,
        //     'prod1.gobpu.com:3030': 467627706723442,
        //     'xdsocial.com:3030': 1595132507401663,
        //     'gosocial.devel:3000': 1484801981823884
        // };
        var apiKeyConfig = GOWEB_CONF.apiKeyConfig;
        //$facebookProvider.setAppId(apiKeyConfig[window.location.hostname + ':' + window.location.port]);
        $facebookProvider.setAppId(apiKeyConfig[liveAPIhost + (!!window.location.port?':' + window.location.port:'')]);
        $facebookProvider.setVersion("v2.2");
 }]);

app.config(['$httpProvider',
    function ($httpProvider) {
        $httpProvider.interceptors.push(['$q', '$rootScope', '$location',
            function ($q, $rootScope, $location) {
                return {
                    'responseError': function (rejection) {
                        var status = rejection.status;
                        var config = rejection.config;
                        var method = config.method;
                        var url = config.url;

                        if (status == 401) {
                            $rootScope.logout('noModalOpen');
                                //$location.path("/login");
                        } else {
                            $rootScope.error = method + " on " + url + " failed with status " + status;
                            // showMessage('You do not have access to this resource', 'errorMessage', 200000);
                            // alert(status);
                        }

                        return $q.reject(rejection);
                    }
                };
        }]);

        /* Registers auth token interceptor, auth token is either passed by header or by query parameter
         * as soon as there is an authenticated user */
        $httpProvider.interceptors.push(['$q', '$rootScope', '$location',
            function ($q, $rootScope, $location) {
                return {
                    'request': function (config) {
                        // console.log('request: '+JSON.stringify(config));
                        /*if (!$rootScope.authToken){
                        $rootScope.logout();
                        //return;
                    }*/
                        var isRestCall = config.url.indexOf('/olyfeWS') > -1;
                        if (isRestCall && angular.isDefined($rootScope.authToken)) {
                            var authToken = $rootScope.authToken;
                            if (tokenAppConfig.useAuthTokenHeader) {
                                config.headers['X-Auth-Token'] = authToken;
                            } else {
                                if (config.url.indexOf('?') > -1) {
                                    config.url = config.url + "&token=" + authToken;
                                } else {
                                    config.url = config.url + "?token=" + authToken; //+ '&app=' + ($rootScope.app ? $rootScope.app : '');
                                }
                            }
                        }
                        return config || $q.when(config);
                    }
                };
        }]);
}]);

app.config(['$logProvider' ,function($logProvider){
     $logProvider.debugEnabled(false);
}]);

app.run(['$rootScope', '$log', '$location', 'UsersService', 'localStorageService', 'AuthService', '$state', '$stateParams', '$window', '$document', '$timeout', 'commonSearchService',
         'taxonomyService','languageService','tribeService',
    function ($rootScope, $log, $location, UsersService, localStorageService, AuthService, $state, $stateParams, $window, $document, $timeout, commonSearchService, taxonomyService,languageService,tribeService) {
        if(GOWEB_CONF.analyticsId){
            GA('create', GOWEB_CONF.analyticsId, 'auto');
            GA('send', 'pageview', window.location.hash);
            // GA('send', {
            //   hitType: 'pageview',
            //   page: window.location.hash
            // });
        }

        if(GOWEB_CONF.getMode){
            $state.go("main.base.maintenance.main.main");
            console.log("GOWEB_CONF.getMode:",GOWEB_CONF.getMode);
            return;
        }

        var selectedLanguage = languageService.getCurrent();
        var apiPathFB = "//connect.facebook.net/en_US/all.js";
        if(selectedLanguage.id === "ko"){
            apiPathFB = "//connect.facebook.net/ko_KR/all.js";
        }
        if(selectedLanguage.id === "ko"){
            $rootScope.prvtPlcKo = true;
        }else if (selectedLanguage.id === "en") {
            $rootScope.prvtPlcEn = true;
        }
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = apiPathFB;
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $rootScope.$on('fb.load', function () {
            $window.dispatchEvent(new CustomEvent('fb.load'));
        });

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'twitterLink'));

        //  $timeout(function () {

        // It's very handy to add references to $state and $stateParams to the $rootScope
        // so that you can access them from any scope within your applications.For example,
        // <li ui-sref-active="active }"> will set the <li> // to active whenever
        // 'contacts.list' or one of its decendents is active.
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        /* Reset error when a new view is loaded */
        $rootScope.$on('$viewContentLoaded', function () {
            //alert('view content loaded');
            delete $rootScope.error;
            /*if (!$rootScope.authToken){
                $rootScope.logout();
            }*/

        });

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {

              if($rootScope.authToken && toState.name == 'main.base.landingbase.main'){
                event.preventDefault();
                if(fromState.name != 'main.base.content.main.main'){
                    if(GOWEB_CONF.getMode){
                        $state.go("main.base.maintenance.main.main")
                    }else{
                        $state.go('main.base.content.main.main');
                    }
                }
              }

        });



        $document.on('click', function(e) {
            if( $('.popover').is(':visible') && ((!$(e.target).hasClass('popoverParent') && $(e.target).parents('.popoverParent').size() === 0) && (!$(e.target).hasClass('popover') && $(e.target).parents('.popover').size() === 0)) ){
                    $rootScope.$broadcast("modalOpenChk", { "target" : event.target });
                }
        });



        /* debug ui-router */
        var debugRouter = false;
        if (debugRouter) {
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                $log.debug('$stateChangeStart to ' + toState.to + '- fired when the transition begins. toState,toParams : \n', toState, toParams);
            });
            $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
                $log.debug('$stateChangeError - fired when an error occurs during transition.');
                $log.debug(arguments);
            });
            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                // console.log('$stateChangeSuccess to ' + toState.name + '- fired once the state transition is complete.');
            });
            // $rootScope.$on('$viewContentLoading',function(event, viewConfig){
            //   // runs on individual scopes, so putting it in "run" doesn't work.
            //   console.log('$viewContentLoading - view begins loading - dom not rendered',viewConfig);
            // });
            $rootScope.$on('$viewContentLoaded', function (event) {
                $log.debug('$viewContentLoaded - fired after dom rendered', event);
            });
            $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
                $log.debug('$stateNotFound ' + unfoundState.to + '  - fired when a state cannot be found by its name.');
                $log.debug(unfoundState, fromState, fromParams);
            });
        }
        /* end of debug ui-router */

        /*$rootScope.loadSocialAccounts = function () {
            UsersService.getSocialAccounts()
                .success(function (data, status) {
                    if (data){
                        //$rootScope.socialAccounts = data;

                    }
                })
        };*/

        $rootScope.hasRole = function (role) {
            if ($rootScope.user === undefined) {
                return false;
            }
            if ($rootScope.user.roles[role] === undefined) {
                return false;
            }
            return $rootScope.user.roles[role];
        };

        $rootScope.logout = function (type) {
            delete $rootScope.user;
            delete $rootScope.authToken;
            localStorageService.remove('authToken');
            localStorageService.remove('user');
            localStorageService.remove('rememberMe');
            // tribe Modules
            localStorage.removeItem('CurrentCatagory');
            localStorage.removeItem('CurrentTribe');
            localStorage.removeItem('CurrentClan');
            localStorage.removeItem('loggedInTribeman');

            Intercom('shutdown');
            /*if ($location.path() !== '/login')
                $location.path('/login');*/

            commonSearchService.setCurrent(null);
            UsersService.logoutType(type);
            
            $state.go('main.base.landingbase.main');
        };

        $rootScope.getMomentDate = function(dt){
            if(dt){
                
                //var dt1 = moment((new Date((dt.toString().indexOf('.') > -1) ? dt.split('.')[0] : dt +' UTC').toString()).split(' GMT')[0]),//moment(dt),
                var dt1 = moment((new Date((dt.toString().indexOf('.') > -1) ? dt.split('.')[0].replace(/ /, 'T') : dt.replace(/ /, 'T') +' UTC').toString()).split(' GMT')[0]),//moment(dt),
                    //dt2 = moment(new Date((new Date()).valueOf() +(new Date()).getTimezoneOffset() * 60000)),
                 dt2 = moment(new Date()),
                monthDiff = dt2.diff(dt1, 'months'),
                dayDiff = dt2.diff(dt1, 'days');

                if(monthDiff > 0){
                    return dt1.format("MMM Do YYYY");
                }else{
                    if(dayDiff < 25){
                        return dt1.fromNow();
                    }else{
                        return dt1.format("MMM Do YYYY");
                    }
                    
                }
            }
        };

        $rootScope.getHexCode = function(per){
            if(per !== undefined && per >= 0 && per <=100){
               var rVal = 255,
                    gVal = 0,
                    bVal = 0;
                return "rgb( "+parseInt(rVal - rVal*(per/100))+", "+parseInt(gVal + rVal*(per/100))+", "+bVal+" )"; 
            }else{
                return "";
            }
            
        };

        $rootScope.taxonomies = []; 
                   
        var loadTaxonomies = function (fn) {
            taxonomyService.filterTaxonomies({}, 0, 0)
                .success(function (result) {
                
                    if (result && result.list && result.list.length > 0) {
                        $rootScope.taxonomies = result.list;
                    }
                    if (fn) {
                        fn();
                    }
                })
                .error(function (data, status) {});

        };

        var originalPath = $location.path();
        $log.debug('originalPath: ' + originalPath);
        var allowedPaths = ['/registration', '/registration/confirm'];
        var loginPath = '/login';

        var shouldCheckAuthToken = allowedPaths.indexOf(originalPath);

        if (shouldCheckAuthToken === -1) {

            var authToken = localStorageService.get('authToken'); //$cookieStore.get('authToken');
            if (authToken) {
                $rootScope.authToken = authToken;

                UsersService.getUser().
                success(function (data, status) {
                    $rootScope.user = data;
                    Intercom('boot', {  
                        app_id: 'hyxd20av',  
                            name: $rootScope.user.name, // Full name
                            email: $rootScope.user.accountName, // Email address,
                            language_override: selectedLanguage.id
                        });
                    // $state.go('main.base.content.main.main');
                    tribeService.loggedInTribeman().then(function(res) {
                            $rootScope.tribemanId = res.data.tribemanId;
                            localStorage.setItem('loggedInTribeman', JSON.stringify(res.data));
                        }, function(err) {
                            console.log(err);
                        });
                    $rootScope.getSocialAccounts(null, function () {
                        loadTaxonomies(function () {
                            // if ($rootScope.missingSocialAccounts.length > 0) {
                                // $state.go('main.users.base.profile');
                            // } else if ($rootScope.$state.current.name)
                                $state.go($rootScope.$state.current.name);
                        });
                    });
                }).
                error(function (data, status) {
                    localStorageService.remove('authToken');
                    $rootScope.logout('noModalOpen');
                });
            } else {
                // user
                $rootScope.logout('noModalOpen');
                //  $state.go('main.base.landingbase.main');
            }

            /*var user = localStorageService.get('user');
            if (user) {
                $rootScope.user = user;
                $state.go('main.base.content.main.main');
            } else {
                $rootScope.logout();
            }*/

            $rootScope.initialized = true;
        } else {
            if ($location.search().token)
                $rootScope.authToken = $location.search().token;
        }
        // }, 0);

        $rootScope.missingSocialAccounts = [];
        $rootScope.activeSocialAccounts = ['Twitter','Facebook'];

        $rootScope.getSocialAccounts = function (socialAccounts, next) {
            if (socialAccounts) {
                var grouped = _.groupBy(socialAccounts, 'socialType');
                $rootScope.missingSocialAccounts = [];
                if (!grouped.TW) {
                    $rootScope.missingSocialAccounts.push('Twitter');
                }

                if (!grouped.FB) {
                    $rootScope.missingSocialAccounts.push('Facebook');
                }
                $rootScope.activeSocialAccounts = _.difference($rootScope.activeSocialAccounts, $rootScope.missingSocialAccounts);
            } else {
                UsersService.getSocialAccounts()
                    .success(function (data, status) {
                        //$scope.handleSocialAccounts(data);
                        var grouped = _.groupBy(data, 'socialType');
                        $rootScope.missingSocialAccounts = [];
                        if (!grouped.TW) {
                            $rootScope.missingSocialAccounts.push('Twitter');
                        }

                        if (!grouped.FB) {
                            $rootScope.missingSocialAccounts.push('Facebook');
                        }
                        $rootScope.activeSocialAccounts = _.difference($rootScope.activeSocialAccounts, $rootScope.missingSocialAccounts);
                        if (next) {
                            next();
                        }

                    });
            }
        };



}]);

/* some global functions  - refactor into own .js */


function imageExists(file_url) {
    var http = new XMLHttpRequest();
    //Access-Control-Allow-Origin: *
    http.open('HEAD', file_url, false);
    http.send();

    return http.status != 404;
}

var prepareFilterQueryString = function (filter, queryParam) {

    if (filter) {
        var tmpfl = null;
        if (filter instanceof Object) {
            tmpfl = filter;
        } else if (filter instanceof String) {

            var rowSplitted = [];
            if (filter.indexOf(';') > 0) {
                var tmp = filter.split(';');
                for (var i in tmp) {
                    if (tmp[i])
                        rowSplitted.push(tmp[i]);
                }
            } else {
                rowSplitted.push(filter);
            }
            tmpfl = {};
            for (var i in rowSplitted) {
                var tmprow = rowSplitted[i];
                var splRow = tmprow.split(':');
                //var obj = {};
                tmpfl[splRow[0]] = splRow[1];
                //tmpfl.push(obj);
            }
        }

        if (tmpfl) {
            if (!queryParam){
                queryParam = 'filter';
            }
            fl = '&'+queryParam+'=' + encodeURIComponent(JSON.stringify(tmpfl));
            return fl;
        }
    }
    return '';

};
