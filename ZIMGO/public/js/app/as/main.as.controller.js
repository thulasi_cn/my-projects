var app = angular.module('mainModule');

app.controller('MainAdsCtrl', ['$scope', '$rootScope', '$state', 'UsersService',
    function ($scope, $rootScope, $state, UsersService) {
        $scope.imgLoadedEvents = {
            always: function (instance) {},
            done: function (instance) {
              /* if (!$scope.$$phase) {
                    $scope.$apply();
               }*/
                $rootScope.$broadcast('imagesloaded:mainadd',instance);
            },
            fail: function (instance) {}
        };
}]);
