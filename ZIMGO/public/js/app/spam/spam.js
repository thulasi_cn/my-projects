var spamModule = angular.module('spamModule', ['ui.router']);

spamModule.config(
  ['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {


      $stateProvider
        .state('main.base.spam', {
          abstract: true,
          url: 'spam',
          views: {
            'content': {
              templateUrl: '/js/app/main/templates/content/main.base.content.view.html',
              controller: 'SpamContentResultMainCtrl'
            },
            'navbar@main': {
              templateUrl: '/js/app/navigation/templates/spam.navigation.view.html',
              controller: 'MainSpamNavigationCtrl'
            }
          }
        })
        .state('main.base.spam.main', {
          url: '',
          abstract: true,
          views: {
            'main@main.base.spam': {
              templateUrl: '/js/app/spam/templates/main.spam.base.view.html'
            }
          }

        })
        .state('main.base.spam.main.main', {
          url: '',
          views: {
            '@main.base.spam.main': {
                templateUrl: '/js/app/spam/templates/spam.result.main.view.html'
            }
          }
        });
    }
  ]
);
