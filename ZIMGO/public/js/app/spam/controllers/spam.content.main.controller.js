var app = angular.module('spamModule');

app.controller('SpamContentResultMainCtrl', ['$scope', '$rootScope', '$state',
    function ($scope, $rootScope, $state) {
        
        $scope.selectedTab = "spam_list";

        $scope.toggleTab = function(type, $event){
             $scope.selectedTab = type;
        };

}]);
