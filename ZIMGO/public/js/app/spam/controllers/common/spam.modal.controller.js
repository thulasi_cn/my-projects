var spamModule = angular.module('spamModule');

spamModule.controller('ModalSpamNavigationCtrl', ['$scope', '$rootScope', '$modal', '$modalInstance', 'growl', 'spamService', 
    function ($scope, $rootScope, $modal, $modalInstance, growl, spamService) {
    	 $scope.addSpam = function(){
            spamService.addSpam($scope.spamModel)
            .success(function (data, status) {
            	console.log("data1:",data);
                growl.success("Spam filter has been added successfully.", { ttl: 10000 });
                $scope.closeModal();
            }).
            error(function (data, status) {
            	growl.error("Failed to add into spam filter.", { ttl: 10000 });
            });
        };

        $scope.closeModal = function(){
            $modalInstance.close();
        };
    }]);