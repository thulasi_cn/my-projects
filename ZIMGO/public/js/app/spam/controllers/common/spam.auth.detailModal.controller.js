var spamModule = angular.module('spamModule');

spamModule.controller('spamAuthDetailModalController', ['$scope', '$modalInstance', 'allMsg',
    function ($scope, $modalInstance, allMsg) {

        $scope.allMsg = allMsg;

        $scope.closeModal = function(){
            $modalInstance.close();
        };
    }]);