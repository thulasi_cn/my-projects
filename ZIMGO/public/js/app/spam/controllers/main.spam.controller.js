var spamModule = angular.module('spamModule');

spamModule.controller('MainSpamNavigationCtrl', ['$scope', '$rootScope', '$modal', 'socialService', 
    function ($scope, $rootScope, $modal, socialService) {
    	socialService.setCookiesInFilter();

    	$rootScope.shwLoad = true;

	    $scope.activeAllSocialLinks = function(){
	        if(angular.element('.main-search-sns.filter-sns li').length > 0){
	            setTimeout(function(){
	                angular.element('.main-search-sns.filter-sns li').trigger('click');
	            }, 2000);
	        }
	    };
    
    	$scope.socialConnectNetworks = socialService.getSocialNetworks();        
        $scope.filterSocial = function(sn){
            socialService.toggleAddFilter(sn);
        };
        
        $scope.isSnsSelected = function(sn){
            if((sn.name === 'youtube' || sn.name === 'flickr') && $rootScope.disableSocialPart){
                return false;
            }else{
                return socialService.filterExists(sn);
            }
        };

        $scope.checkSelection = function(sn){
            if( $scope.isSnsSelected(sn) && (sn.id === 'YT' || sn.id === 'FL') && $scope.disableSocialPart){
                return true;
            }else{
                return false;
            }
        };

        $scope.opemModalForm = function(){
            var modalInstance = $modal.open({
                templateUrl: 'js/app/spam/templates/common/spam.modal.view.html',
                controller: 'ModalSpamNavigationCtrl',
                windowClass: 'spam-modal-window'
            });
            modalInstance.result.then(function(registerModel) {

            }, function() {});
        };
    }]);