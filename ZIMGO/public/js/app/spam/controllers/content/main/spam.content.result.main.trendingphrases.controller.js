var spamModule = angular.module('spamModule');

spamModule.controller('SpamMainContentResultTrendingphrasesCtrl', [ '$scope', '$rootScope', '$document', '$window', 'growl', '$filter', 'limitService', 'socialService', 'spamService', function( $scope, $rootScope, $document, $window, growl, $filter, limitService, socialService, spamService ){
	$scope.initPagingData = function () {
        $scope.pagingData = {
            from: 0,
            limit: limitService.getRange(),
            total: null,
            isCompleted: false,
            lastRow: null
        };
        $scope.result = [];
    };

    $scope.initPagingData();

    $scope.$watchCollection(function () {
        return socialService.getFilter();
    }, function (newValue, oldValue) {
        if (newValue !== oldValue) {
            $scope.initPagingData();
            $scope.run();
        }
    });

    $scope.getParsedJsonMedia = function (mediaJsonStr) {
        var items = [];
        if (mediaJsonStr) {
            mediaJsonStr.forEach(function (i) {
                var media = JSON.parse(i);
                items.push(media);
            });
        }
        return items;
    };

    $scope.removeDuplicatesFromNew = function (existingList, resultList) {
        var newList = [];
        if (existingList) {
            _.forEach(resultList, function (resItem) {
                var found = _.find(existingList, function (item) {
                    return item['authorId'] === resItem['authorId'];
                });
                if (!found) {
                    newList.push(resItem);
                }
            });
        } else {
            newList = resultList;
        }
        return newList;
    };

    $scope.handleResult = function (result) {

        var imCleanList = _.uniqBy(result.list, 'authorId');

        var newList = $scope.removeDuplicatesFromNew($scope.result, imCleanList);

        var diff = _.differenceBy(newList, $scope.result, 'authorId');
        if (diff && diff.length > 0) {
            $scope.result = $scope.result.concat(newList);
        }
        $scope.pagingData.lastRow = $scope.result[$scope.result.length - 1];
    };

    $scope.updateLimit = function () {
        $scope.pagingData.limit = $scope.pagingData.limit + limitService.getRange();
        $scope.run();
    };

    $scope.run = function(){

    	var filter = {};
        if (socialService.getFilter() && socialService.getFilter().length > 0) {
            filter['SOURCE'] = _.map(socialService.getFilter(), 'id');
        }



        var lastRow = {};
        if ($scope.pagingData.lastRow) {
            lastRow['author_id'] = $scope.pagingData.lastRow['authorId'];         
        }

        if (!$rootScope.shwLoad) {
            $rootScope.shwLoad = true;
        }

        spamService.getSpamResults( $scope.pagingData.limit, filter, lastRow )
            .success(function (result) {
                    if ($rootScope.shwLoad) {
                        $rootScope.shwLoad = false;
                    }
                if (result && result.list && result.list.length > 0) {
                    $scope.handleResult(result);
                    $scope.lastCnt = $scope.result.length;
                }
            })
            .error(function (data, status) {

            });

    };

    $scope.delSpam = function(authorId){
        var status = confirm( $filter('translate')('Are you sure about permanently deleting all items by the author of this content?') );
        if (status === true) {
            spamService.deleteSpam( authorId )
            .success(function (result) {
                for( var i=0;i< $scope.result.length; i++ ){
                    if($scope.result[i]["authorId"] === authorId){
                        $scope.result.splice(i,1);
                    }
                }
                growl.success($filter('translate')("Spam filter has been deleted successfully."), { ttl: 10000 });
            })
            .error(function (data, status) {
                console.log("data2:",data);
            });
        }
    };

    $scope.run();
}]);