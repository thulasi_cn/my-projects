var spamModule = angular.module('spamModule');

spamModule.factory('spamService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        var prepareSpamFilterQueryString = function (filter, queryParam) {

		    if (filter) {
		        var tmpfl = null;
		        if (filter instanceof Object) {
		            tmpfl = filter;
		        } else if (filter instanceof String) {

		            var rowSplitted = [];
		            if (filter.indexOf(';') > 0) {
		                var tmp = filter.split(';');
		                for (var i in tmp) {
		                    if (tmp[i])
		                        rowSplitted.push(tmp[i]);
		                }
		            } else {
		                rowSplitted.push(filter);
		            }
		            tmpfl = {};
		            for (var i in rowSplitted) {
		                var tmprow = rowSplitted[i];
		                var splRow = tmprow.split(':');
		                //var obj = {};
		                tmpfl[splRow[0]] = splRow[1];
		                //tmpfl.push(obj);
		            }
		        }

		        if (tmpfl) {
		            if (!queryParam){
		                queryParam = 'filter';
		            }
		            fl = '&'+queryParam+'=' + encodeURIComponent(JSON.stringify(tmpfl)) + '';
		            return fl;
		        }
		    }
		    return '';

		};
        return {
        	getSpamResults: function (limit, filterObject, lastRowObject) {
                var fl = '',
                    lr = '';
                    
                fl = prepareSpamFilterQueryString(filterObject);
                lr = prepareSpamFilterQueryString(lastRowObject, 'lastRow');

                var url = host + 'v2_1/spam/filter?limit=' + limit + fl + lr;
                return $http.get(url);
            },
            addSpam: function (spamAddModel) {
                return $http.post(host + 'v2_1/spam/add',spamAddModel);
            },
            deleteSpam: function (memberId) {
                return $http.delete(host + 'v2_1/spam/'+memberId);
            },
            getAuthorList: function (limit, filterObject, lastRowObject) {
            	var fl = '', lr = '';
                fl = prepareSpamFilterQueryString(filterObject);
                lr = prepareSpamFilterQueryString(lastRowObject, 'lastRow');
                var url = host + 'v2_1/spam/authors?limit=' + limit + fl + lr;
                return $http.get(url);
            },
            getAuthorData: function (sendObj){
            	var url = host + 'v2_1/spam/filter?filter='+encodeURIComponent(JSON.stringify(sendObj));
            	return $http.get(url);
            }
        }
    }
]);
