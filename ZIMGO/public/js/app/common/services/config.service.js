var serviceConfig = angular.module('service.config', []);

serviceConfig.factory('serviceConfig', ['$http', function ($http) {
    return {
        getHost: function () {
            if (GOWEB_CONF && GOWEB_CONF.apiUrl){
                return GOWEB_CONF.apiUrl;
            }else{
                // return 'http://' + window.location.hostname + ':80/olyfeWS/';
            }
        },
        getMediaResolveHost: function () {
            // return 'http://'+window.location.hostname+(window.location.port?':'+window.location.port:'')+'/resize';
            //return 'http://'+window.location.hostname+':9999/img';
            //return 'http://li1268-67.members.linode.com:9999/img';
            if (GOWEB_CONF && GOWEB_CONF.imageResizeUrl){ // global variable
                return GOWEB_CONF.imageResizeUrl+'/img';
            }else{
                return 'http://'+window.location.hostname+':9999/img';
            }          

        }
    };
}]);
