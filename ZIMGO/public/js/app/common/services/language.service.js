var commonModule = angular.module('commonModule');

commonModule.factory('languageService', ['amMoment','$rootScope','$translate','localStorageService',
  function(amMoment, $rootScope,$translate, localStorageService) {
    var langs = [{ name: 'Eng',id: 'en'}, {name: 'Kor',id: 'ko'}];
    var langKey = 'language';
    return {
      setCurrent: function(language) {
          localStorageService.set(langKey, language);
          this.language = language;
        //  $rootScope.selectedLanguage = language;
          $translate.use(language.id);
          amMoment.changeLocale(language.id);
      },
      getCurrent: function() {
          if (!this.language){
            //this.language = langs[0];
            var storedLang = localStorageService.get(langKey);
            if (!storedLang){
                storedLang = langs[0];
            }
            this.setCurrent(storedLang);
          }
          return this.language;
      },
      getAvailableLanguages: function() {
          return langs;
      },
      resolveLanguage: function(smallLang){
          var langObj = null;
          for (var i = 0; i < langs.length; i++) {
              if (langs[i].id === smallLang) { //stream.language){
                  langObj = langs[i];
                  break;
              }
          }
          return langObj;
      }
    }
  }
]);
