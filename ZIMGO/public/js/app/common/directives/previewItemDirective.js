var directives = angular.module('directives');

directives.directive('previewItem', [

    function () {
        'use strict';
        return {
            restrict: 'E',
            /*transclude: true,*/
            scope: {
                item: '=',
                itemType: '=',
                context: '=',
                previewContext: '='
            },
            template:   '<a href="" ng-click="openItem()">' +
                            '<span class="afterlogin_arrowicon"></span>' +
                        '</a>',
            controller: ['$rootScope', '$scope', '$log', '$modal', 'taxonomyService', '$window', '$timeout', '$facebook', 'languageService', function ($rootScope, $scope, $log, $modal, taxonomyService, $window, $timeout, $facebook, languageService) {


                $scope.resolvePreviewTemplate = function () {
                    var base = '/js/app/common/templates/';
                    if ($scope.itemType) {
                        if ($scope.itemType === "trendingphrases") {
                            return base + 'footer.item.trendingphrases.profile.view.html';
                        }
                        if ($scope.itemType === "contributor") {
                            return base + 'footer.item.contributors.profile.view.html';
                        }
                        if ($scope.itemType.indexOf('VIDEO') > -1) {
                            return base + 'video.player.modal.view.html';
                        }
                        if ($scope.itemType.indexOf('PHOTO') > -1) {
                            return base + 'photo.preview.modal.view.html';
                        }

                    }
                    return null;
                };

                $scope.openModalPreview = function () {
                    var templateUrl = $scope.resolvePreviewTemplate();
                    var modalInstance = $modal.open({
                        templateUrl: templateUrl,
                        controller: 'PlayVideoModalController',
                        size: 'md',
                        resolve: {
                            url: function () {
                                
                                if ($scope.itemType === "media-PHOTO" || $scope.itemType === "media-VIDEO") {
                                    return ($scope.item.VIDEO_URL ? $scope.item.VIDEO_URL.replace("https://www.youtube.com/v/", "https://www.youtube.com/embed/")+"?autoplay=1" : $scope.item.BIGIMG_URL);
                                } else {
                                    return "";
                                }

                            },
                            itemType: function () {
                                return $scope.itemType;
                            },
                            item: function () {
                                return $scope.item;
                            },
                            context: function(){
                                return $scope.context;
                            }
                        }
                    });

                    modalInstance.result.then(function (selectedItem) {}, function () {});
                };
                
                $scope.openItem = function () {
                    if ($scope.previewContext && $scope.previewContext==='modal'){
                        $scope.openItemModal();
                    }else{
                        $scope.openItemDirect();
                    }
                };

                $scope.openItemModal = function () {
                    if ($scope.itemType === "media-PHOTO" || $scope.itemType === "media-VIDEO" || $scope.itemType === "trendingphrases" || $scope.itemType === "contributor") {
                        $scope.openModalPreview();
                    } else {
                        $window.open(decodeURIComponent($scope.item.itemUrl));
                    }
                };
                
                $scope.openItemDirect = function () {
                    if ($scope.itemType === "media-PHOTO" || $scope.itemType === "media-VIDEO") {
                        $window.open($scope.item.BIGIMG_URL, "_blank");
                    }else if($scope.itemType === "trendingphrases"){
                        $window.open(decodeURIComponent($scope.item.PROFILE_URL), "_blank");   
                    }else if($scope.itemType === "contributor"){
                        $window.open(decodeURIComponent($scope.item.MESSAGE_JSON.profileUrl), "_blank");   
                    }
                };
            }]
        }
    }
]);
