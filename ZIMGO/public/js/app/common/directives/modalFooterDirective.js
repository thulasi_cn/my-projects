var directives = angular.module('directives');

directives.directive('modalItemFooter', [
	function () {
		'use strict';
		return {
            restrict: 'E',
            transclude: true,
            scope: {
                item: '=',
                taxonomyItem: '=',
                itemType: '=',
                context: '=',
            },
            template: '<ul class="list-inline resFtr">'+
                            '<li>'+
                                '<taxonomy-item taxonomy-item="taxonomyItem" item="item" item-type="itemType" context="context"></taxonomy-item>'+
                            '</li>'+
                            '<li>'+
                                '<share-item item="item" item-type="itemType"></share-item>'+
                            '</li>'+
                            '<li>'+
                                '<preview-item item="item" item-type="itemType" preview-context="\'direct\'"></preview-item>'+
                            '</li>'+
                        '</ul>',
           // templateUrl: '/js/app/common/templates/modal.footer.view.directive.html',
            controller: ['$rootScope', '$scope', '$log', '$modal', 'taxonomyService','$window','$timeout', '$facebook','languageService', function ($rootScope, $scope, $log, $modal, taxonomyService,$window, $timeout, $facebook, languageService) {

              /*  $scope.shares = ['Facebook', 'Twitter'];
                $scope.nwTaxonomy = {
                    "name" : "",
                    "color" : ""
                };
                angular.element("ngjs-color-picker li.selected").removeClass("selected");

            	$scope.openItem = function () {
                    if ($scope.itemType === "media-PHOTO" || $scope.itemType === "media-VIDEO") {
                        $window.open($scope.item.bigImgUrl);
                    }else if($scope.itemType === "trendingphrases"){
                        $window.open(decodeURIComponent($scope.item.topTaggedMessage.profileUrl));   
                    }else if($scope.itemType === "contributor"){
                        $window.open(decodeURIComponent($scope.item.profileUrl));   
                    }
                };

                // tagging related 
                $scope.taxonomies = $rootScope.taxonomies;
                $scope.selectedTaxonomies = [];
                $scope.preselectedTaxonomies = [];
                
                $scope.insertSelectedTaxonomy = function (taxonomy) {
                    var foundObj = _.find($scope.selectedTaxonomies, {
                        name: taxonomy.name
                    });
                    if (!foundObj) {
                        $scope.selectedTaxonomies.push(taxonomy);
                    } else {
                        var indexOf = $scope.selectedTaxonomies.indexOf(foundObj);
                        $scope.selectedTaxonomies.splice(indexOf, 1);
                    }
                };
                $scope.selectTaxonomy = function (taxonomy) {
                    $scope.insertSelectedTaxonomy(taxonomy);
                };

                $scope.addNewCategory = function(colorCode){
                    if(colorCode){
                        $scope.nwTaxonomy.color = colorCode;

                        if($scope.nwTaxonomy && $scope.nwTaxonomy.name !== "" && $scope.nwTaxonomy.color !== ""){
                            if($scope.validDuplicate()){
                                $scope.insertTaxonomy();
                            }else{
                                alert("This item is already present");
                            }
                        }
                    }
                };

                $scope.validDuplicate = function(){
                    var cnt = 0;
                        for(var i=0; i< $rootScope.taxonomies.length; i++ ){
                            if($scope.nwTaxonomy.name === $rootScope.taxonomies[i].name){
                                cnt++;
                                break;
                            }
                        }

                        if(cnt > 0){
                            return false;
                        }else{
                            return true;
                        }
                };
                
                $scope.removeNewCategory = function(){
                    angular.element(".taxonomy-tag-item-Nw").css({"background-color" : "#FFF"});
                    angular.element("ngjs-color-picker li.selected").removeClass("selected");
                    $scope.nwTaxonomy.name = "";
                    $scope.nwTaxonomy.color = "";
                };

                $scope.insertTaxonomy = function () {
                    
                    taxonomyService.insertTaxonomy($scope.nwTaxonomy)
                        .success(function (result) {
                            $scope.removeNewCategory();
                            $rootScope.taxonomies.splice(0, 0, result.result.result);
                        })
                        .error(function (data, status) {});
                };

                $scope.scroller = {
                    top: 0,
                    position: 'relative'
                };

                $scope.moveY = function (pixels) {
                    $scope.scroller.top += pixels;
                }

                $scope.saveTaggedItems = function () {
                    if ($scope.context==='search'){
                        $scope.saveSearchContextTaggedItems();   
                    }else{
                        $scope.saveTaxonomyContextTaggedItems();
                    }
                    
                };
                
                $scope.saveTaxonomyContextTaggedItems = function(){
                    if ((!$scope.selectedTaxonomies || $scope.selectedTaxonomies.length == 0) && $scope.preselectedTaxonomies.length==0) {
                        return;
                    }
                    
                    var taggedItem = $scope.item;
                    var itemType = $scope.itemType;
                    
                    var newSelectedTaxonomies = [];
                    var existingTaxonomies = [];
                    var removedTaxonomies = [];
                    angular.forEach($scope.preselectedTaxonomies, function (preselectedTaxonomy) {
                        var isSelected = _.find($scope.selectedTaxonomies, { id: preselectedTaxonomy.id });
                        if (!isSelected){
                            removedTaxonomies.push(preselectedTaxonomy);
                        }else{
                            existingTaxonomies.push(preselectedTaxonomy);
                        }
                    });
                    
                  //  console.log('To remove taxonomies: '+JSON.stringify(removedTaxonomies));
                    
                    angular.forEach($scope.selectedTaxonomies, function (taxonomy) {
                        var isExisting = _.find(existingTaxonomies, { id: taxonomy.id });
                        if (!isExisting){
                            newSelectedTaxonomies.push(taxonomy);   
                        }
                    });
                    
                    // console.log('To add taxonomies: '+JSON.stringify(newSelectedTaxonomies));
                    var shouldReload = false;
                    if (removedTaxonomies.length>0){
                        shouldReload = true;
                        var taxonimiedItemsToRemove = [];
                        angular.forEach(removedTaxonomies, function(removedTaxonomy){
                            var obj = {};
                            angular.extend(obj, $scope.item);
                            obj.category = removedTaxonomy.name;
                            obj.categoryId = removedTaxonomy.id;
                            obj.categoryColor = removedTaxonomy.color;
                            obj.categoryCreatedAt = removedTaxonomy.createdAt;
                            obj.content = JSON.stringify(obj.content);
                            taxonimiedItemsToRemove.push(obj);
                        });
                        
                     //   console.log('To remove taxonomies items: '+JSON.stringify(taxonimiedItemsToRemove));
                        // remove taxonomied item from this category/taxonomies 
                        taxonomyService.deleteTaxonomiedItems(taxonimiedItemsToRemove)
                        .success(function (result) {
                            if (result && result.message == 'success') {

                            }
                            $scope.closePopover();
                        })
                        .error(function (data, status) {
                            $log.debug('error: ' + JSON.stringify(data));
                        }); 
                        
                    }
                    
                    if (newSelectedTaxonomies.length>0){
                        shouldReload = true;
                        var taxonomiedItemsToInsert = [];
                        angular.forEach(newSelectedTaxonomies, function(newTaxonomy){
                            var obj = {};
                            angular.extend(obj, $scope.item);
                            obj.category = newTaxonomy.name;
                            obj.categoryId = newTaxonomy.id;
                            obj.categoryColor = newTaxonomy.color;
                            obj.categoryCreatedAt = newTaxonomy.createdAt;
                            obj.content = JSON.stringify(obj.content);
                            taxonomiedItemsToInsert.push(obj);
                        });
                      //  console.log('To add taxonomies items: '+JSON.stringify(taxonimiedItemsToInsert));
                                        
                                        
                        // insert as new taxonomies                        
                        taxonomyService.insertTaxonimiedItems(taxonomiedItemsToInsert)
                        .success(function (result) {
                            if (result && result.message == 'success') {

                            }
                            $scope.closePopover();
                        })
                        .error(function (data, status) {
                            $log.debug('error: ' + JSON.stringify(data));
                        });  
                    }
                    
                    if (shouldReload){
                        $timeout(function(){
                            taxonomyService.setReload({itemType: $scope.itemType});
                        }, 200);
                    }
                };
                
                $scope.saveSearchContextTaggedItems = function(){
                    if (!$scope.selectedTaxonomies || $scope.selectedTaxonomies.length == 0) {
                        return;
                    }

                    var taggedItem = $scope.item;
                    var itemType = $scope.itemType;

                    var olTaxonomiedItemList = [];
                    angular.forEach($scope.selectedTaxonomies, function (taxonomy) {
                        var obj = {
                            categoryId: taxonomy.id,
                            category: taxonomy.name,
                            categoryCreatedAt: taxonomy.createdAt,
                            categoryColor: taxonomy.color,
                            itemType: itemType,
                            sourceType: 'internal',
                            content: JSON.stringify(taggedItem)
                        };

                        switch (itemType) {
                            case 'trendingphrases':
                                obj.title = taggedItem.textItem;
                                obj.itemUrl = taggedItem.messages[0].messageLinkUrl;
                                obj.createdAt = (new Date()).getTime();
                                obj.referenceId = taggedItem.messages[0].referenceId;
                                obj.source = taggedItem.messages[0].source;
                                break;
                            case 'topic':
                                obj.title = taggedItem.textItem;
                                obj.itemUrl = taggedItem.messages[0].messageLinkUrl;
                                obj.createdAt = (new Date()).getTime();
                                obj.referenceId = taggedItem.messages[0].referenceId;
                                obj.source = taggedItem.messages[0].source;
                                break;
                            case 'contributor':
                                obj.title = taggedItem.userName;
                                obj.itemUrl = taggedItem.profileUrl;
                                obj.source = taggedItem.source;
                                obj.createdAt = (new Date()).getTime();
                                obj.referenceId = taggedItem.messages.referenceId;
                                break;
                            case 'media-PHOTO':
                                obj.itemUrl = taggedItem.imgUrl;
                                obj.title = taggedItem.title;
                                obj.createdAt = (new Date()).getTime();
                                obj.referenceId = taggedItem.referenceId;
                                obj.source = taggedItem.source;
                                break;
                            case 'media-VIDEO':
                                obj.itemUrl = taggedItem.imgUrl;
                                obj.title = taggedItem.title;
                                obj.createdAt = (new Date()).getTime();
                                obj.referenceId = taggedItem.referenceId;
                                obj.source = taggedItem.source;
                                break;
                        }

                        olTaxonomiedItemList.push(obj);
                    });
                    taxonomyService.insertTaxonimiedItems(olTaxonomiedItemList)
                        .success(function (result) {
                            if (result && result.message == 'success') {

                            }
                            $scope.closePopover();
                        })
                        .error(function (data, status) {
                            $log.debug('error: ' + JSON.stringify(data));
                        });  
                    
                };


                $scope.closePopover = function () {
                    $scope.isPopoverOpen = false;
                };

                $scope.closeSharePopover = function(){
                    $scope.issharePopoverOpen = false;
                };
               
                $scope.openTagPopover = function () {

                    var filter = $scope.context==='search'?$scope.getSearchContextFilter():$scope.getTaxonomyContextFilter();
                    
                    taxonomyService.filterTaxonomiedItems(filter)
                        .success(function (result) {
                            if (result && result.status == 200 && result.list) {
                              //  console.log('taxonomied items filtered: ' + JSON.stringify(result));
                                angular.forEach(result.list, function(item){
                                    angular.forEach($scope.taxonomies, function(taxonomy){
                                        if (taxonomy.id===item.categoryId){
                                            var ob_exists = _.find($scope.selectedTaxonomies, { id: taxonomy.id});
                                            if (!ob_exists){
                                                $scope.selectedTaxonomies.push(taxonomy);   
                                                ob_exists = _.find($scope.preselectedTaxonomies, { id: taxonomy.id});
                                                if (!ob_exists){
                                                    $scope.preselectedTaxonomies.push(taxonomy);
                                                }
                                            }
                                        }
                                    });
                                });
                            }
                            $scope.isPopoverOpen = true;
                        })
                        .error(function (data, status) {
                            // console.log('error: ' + JSON.stringify(data));
                        });
                    //$scope.isPopoverOpen = !$scope.isPopoverOpen;
                };
                $scope.isPopoverOpen = false;
                
                $scope.getSearchContextFilter = function(){
                    
                    var filter = {
                        from: 0,
                        limit: $scope.taxonomies.length + 1
                    };
                    var taggedItem = $scope.item;

                    switch ($scope.itemType) {
                        case 'trendingphrases':
                            filter.referenceId = taggedItem.messages[0].referenceId;
                            filter.title = taggedItem.textItem;
                            break;
                        case 'topic':
                            filter.referenceId = taggedItem.messages[0].referenceId;
                            filter.title = taggedItem.textItem;
                            break;
                        case 'contributor':
                            filter.referenceId = taggedItem.messages.referenceId;
                            filter.title = taggedItem.userName;
                            break;
                        case 'media-PHOTO':
                            filter.referenceId = taggedItem.referenceId;
                            filter.title = taggedItem.title;
                            break;
                        case 'media-VIDEO':
                            filter.referenceId = taggedItem.referenceId;
                            filter.title = taggedItem.title;
                            break;
                    }
                    return filter;
                    
                };

                $scope.getTaxonomyContextFilter = function(){
                    var filter = {
                        from: 0,
                        limit: $scope.taxonomies.length + 1
                    };
                    var taggedItem = $scope.item;

                    switch ($scope.itemType) {
                        case 'trendingphrases':
                            filter.referenceId = taggedItem.messages[0].referenceId;
                            filter.title = taggedItem.textItem;
                            break;
                        case 'topic':
                            filter.referenceId = taggedItem.messages[0].referenceId;
                            filter.title = taggedItem.textItem;
                            break;
                        case 'contributor':
                            filter.referenceId = taggedItem.messages.referenceId;
                            filter.title = taggedItem.userName;
                            break;
                        case 'media-PHOTO':
                            filter.referenceId = taggedItem.referenceId;
                            filter.title = taggedItem.title;
                            break;
                        case 'media-VIDEO':
                            filter.referenceId = taggedItem.referenceId;
                            filter.title = taggedItem.title;
                            break;
                    }
                    return filter;
                }
                // end of tagging related 





                $scope.commentItem = function () {

                };
                $scope.sharePopupOpen = function () {
                    $scope.issharePopoverOpen = true;
                };

                $scope.shareItem = function(chkItem){
                    var shareUrl = "";
                    if($scope.itemType === "contributor"){
                        shareUrl = $scope.item.profileUrl;
                    }else if($scope.itemType === "trendingphrases"){
                        shareUrl = (decodeURIComponent($scope.item.messages[0].messageLinkUrl));   
                    }else if ($scope.itemType === "media-PHOTO") {
                        shareUrl = $scope.item.imgUrl;
                    }

                    $facebook.ui(
                     {
                      method: 'share',
                      href: shareUrl,
                    }, function(response){
                        // console.info("res:",response);
                    });
                };

                $scope.getUrl = function(url){
                    return decodeURIComponent(url);
                };

                $scope.twitterShareURL = function(){

                    var itemType    = $scope.itemType;
                    var twtShareURL = '';

                    var obj = {};
                    var taggedItem = $scope.item;

                    switch (itemType) {
                        case 'trendingphrases':
                            obj.title = taggedItem.textItem;
                            obj.itemUrl = taggedItem.messages[0].messageLinkUrl;
                            obj.createdAt = (new Date()).getTime();
                            obj.referenceId = taggedItem.messages[0].referenceId;
                            obj.source = taggedItem.messages[0].source;
                            break;
                        case 'topic':
                            obj.title = taggedItem.textItem;
                            obj.itemUrl = taggedItem.messages[0].messageLinkUrl;
                            obj.createdAt = (new Date()).getTime();
                            obj.referenceId = taggedItem.messages[0].referenceId;
                            obj.source = taggedItem.messages[0].source;
                            break;
                        case 'contributor':
                            obj.title = taggedItem.userName;
                            obj.itemUrl = taggedItem.profileUrl;
                            obj.source = taggedItem.source;
                            obj.createdAt = (new Date()).getTime();
                            obj.referenceId = taggedItem.messages.referenceId;
                            break;
                        case 'media-PHOTO':
                            obj.itemUrl = taggedItem.imgUrl;
                            obj.title = taggedItem.title;
                            obj.createdAt = (new Date()).getTime();
                            obj.referenceId = taggedItem.referenceId;
                            obj.source = taggedItem.source;
                            break;
                        case 'media-VIDEO':
                            obj.itemUrl = taggedItem.imgUrl;
                            obj.title = taggedItem.title;
                            obj.createdAt = (new Date()).getTime();
                            obj.referenceId = taggedItem.referenceId;
                            obj.source = taggedItem.source;
                            break;
                    };

                    var selectedLanguage = languageService.getCurrent();
                    if(obj.source === 'twitter' || obj.source === 'TW' ){
                        twtShareURL = "https://twitter.com/intent/retweet?lang="+selectedLanguage.id+"&tweet_id="+obj.referenceId;
                    }else{
                        twtShareURL = "https://twitter.com/intent/tweet?lang="+selectedLanguage.id+"&url="+obj.itemUrl+'&text='+obj.title;
                    }

                    return twtShareURL;


                };*/
            }],
            link: ['scope', 'element', 'attrs', function (scope, element, attrs) {

            }]
        }
	}
]);