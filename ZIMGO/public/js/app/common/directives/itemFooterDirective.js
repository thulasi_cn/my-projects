var directives = angular.module('directives');

directives.directive('taxonomyItemFooter', [
    function () {
        'use strict';
        return {
            restrict: 'E',
            /*transclude: true,*/
            scope: {
                item: '=',
                taxonomyItem: '=',
                itemType: '=',
                context: '='
            },
            template: '<ul class="list-unstyled">'+
                            '<li>'+
                                '<ul class="list-inline resFtr">' +
                                    '<li>' +
                                         '<taxonomy-item taxonomy-item="taxonomyItem" item="item" item-type="itemType" context="context"></taxonomy-item> '+
                                    '</li>'+
                                    '<li>'+
                                        '<share-item item="item" item-type="itemType"></share-item> '+
                                    '</li> '+
                                    '<li> '+
                                        '<preview-item item="item" item-type="itemType" preview-context="\'modal\'"></preview-item> '+
                                    '</li>'+
                                '</ul>'+
                            '</li>'+
                        '</ul>',
            //templateUrl: '/js/app/common/templates/item.footer.view.directive.html',
            controller: ['$rootScope', '$scope', '$log', '$modal', 'taxonomyService','$window','$timeout', '$facebook','languageService', function ($rootScope, $scope, $log, $modal, taxonomyService,$window, $timeout, $facebook, languageService) {
                /*$scope.taggedByList = '';
                
                $scope.tagList = function(){
                    if ($scope.taggedBy && $scope.taggedBy.length && $rootScope.taxonomies){
                        var items = [];                        
                        _.forEach($rootScope.taxonomies, function(item){
                            for(var i = 0; i <$scope.taggedBy.length; i++){
                                console.info("itemSub:",$scope.taggedBy[i]);
                                if ($scope.taggedBy[i].indexOf(item.id)>-1){
                                    console.info(item.name);
                                    items.push(item);
                                } 
                            }
                        });
                        
                        $scope.taggedByList = items;
                    }    
                };
                
                $scope.tagList();*/
                
                /*$scope.$watch('taxonomyItem.categoryIds', function(newValue, oldValue){
                    if (newValue!==oldValue){
                        $scope.tagList();
                    }
                });*/
                
            }],
            link: function (scope, element, attrs) {

            }
        }
    }
]);

directives.directive('footerCatList', [function(){
    'use strict';
    return {
        restrict: 'E',
        scope: {
                taxonomyItem: '=',
                taggedBy: '='
            },
            template: '<ul class="list-unstyled taggedCatInFooter">'+
                    '<li class="taggedByList" ng-repeat="tagItem in taggedByList" ng-click="filterByFooterItem(tagItem);">'+ 
                        '<span ng-style="{background: tagItem.color}"></span>'+
                        '&nbsp;&nbsp;&nbsp;'+
                        '{{tagItem.name}}'+
                    '</li>'+
                '</ul>'+
                '<br class="clear">',
            controller: ['$rootScope', '$scope', '$log', '$modal', 'taxonomyService','$window','$timeout', '$facebook','languageService', function ($rootScope, $scope, $log, $modal, taxonomyService,$window, $timeout, $facebook, languageService) {
                    
                    $scope.tagList = function(){
                        if ($scope.taggedBy && $scope.taggedBy.length && $rootScope.taxonomies){
                            var items = [];                        
                            _.forEach($rootScope.taxonomies, function(item){
                                for(var i = 0; i <$scope.taggedBy.length; i++){
                                    if ($scope.taggedBy[i].indexOf(item.id)>-1){
                                        items.push(item);
                                    } 
                                }
                            });
                            
                            $scope.taggedByList = items;
                        }    
                    };
                    
                    $scope.tagList();

                    $scope.filterByFooterItem = function(item){
                        taxonomyService.updateFooterFilterItem(item);
                    };

                    $scope.$watch('taxonomyItem.categoryIds', function(newValue, oldValue){
                        if (newValue!==oldValue){
                            $scope.tagList();
                        }
                    });

                    $scope.$watch(function() { return $rootScope.taxonomies }, function(newValue, oldValue){
                        if (newValue!==oldValue){
                            $scope.tagList();
                        }
                    });
                }],
            link: function (scope, element, attrs) {

            }
    }
}]);