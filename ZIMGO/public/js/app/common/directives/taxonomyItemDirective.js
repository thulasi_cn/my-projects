var directives = angular.module('directives');

directives.directive('taxonomyItem', [

    function() {
        'use strict';
        return {
            restrict: 'E',
            /*transclude: true,*/
            scope: {
                item: '=',
                taxonomyItem: '=',
                itemType: '=',
                context: '=',
                taggedBy: '='
            },
            template: '<span class="footer-span" popover-template="\'/js/app/common/templates/taxonomy.popover.tag.item.html\'" popover-toggle="isPopoverOpen" popover-placement="{{popoverPos}}" options="options">' +
                '<a href="" class="popoverParent" ng-click="openTagPopover()">' +
                '<span class="landing_likeicon"></span>' +
                '</a>' +
                '</span>',
            controller: ['$rootScope', '$scope', '$log', '$modal', 'deviceDetector', 'taxonomyService', '$window', '$timeout', '$facebook', 'languageService', function($rootScope, $scope, $log, $modal, deviceDetector, taxonomyService, $window, $timeout, $facebook, languageService) {

                $rootScope.$on('modalOpenChk', function(event, args) {
                    if ($scope.isPopoverOpen) {
                        $scope.closePopover();
                        $scope.$digest();
                    }
                });

                $scope.options = {};
                if (deviceDetector.isDesktop()) {
                    $scope.popoverPos = "right";
                    $scope.options.size = 20;
                } else {
                    $scope.popoverPos = "bottom";
                    $scope.options.size = 35;
                }

                $scope.nwTaxonomy = {
                    "name": "",
                    "color": ""
                };
                angular.element("ngjs-color-picker li.selected").removeClass("selected");


                $scope.taxonomies = $rootScope.taxonomies;
                $scope.selectedTaxonomies = [];
                $scope.preselectedTaxonomies = [];
                $scope.preCategoryIds = [];

                $scope.insertSelectedTaxonomy = function(taxonomy) {
                    var foundObj = _.find($scope.selectedTaxonomies, {
                        name: taxonomy.name
                    });
                    if (!foundObj) {
                        $scope.selectedTaxonomies.push(taxonomy);
                    } else {
                        var indexOf = $scope.selectedTaxonomies.indexOf(foundObj);
                        $scope.selectedTaxonomies.splice(indexOf, 1);
                    }
                };
                $scope.selectTaxonomy = function(taxonomy) {
                    $scope.insertSelectedTaxonomy(taxonomy);
                };

                $scope.addNewCategory = function(colorCode) {
                    if (colorCode) {
                        $scope.nwTaxonomy.color = colorCode;

                        if ($scope.nwTaxonomy && $scope.nwTaxonomy.name !== "" && $scope.nwTaxonomy.color !== "") {
                            $scope.insertTaxonomy();
                        }
                    }
                };

                $scope.removeNewCategory = function() {
                    angular.element(".taxonomy-tag-item-Nw").css({
                        "background-color": "#FFF"
                    });
                    angular.element("ngjs-color-picker li.selected").removeClass("selected");
                    $scope.nwTaxonomy.name = "";
                    $scope.nwTaxonomy.color = "";
                };

                $scope.insertTaxonomy = function() {

                    taxonomyService.insertTaxonomy($scope.nwTaxonomy)
                        .success(function(result) {
                            $scope.removeNewCategory();
                            $rootScope.taxonomies.splice(0, 0, result.result.result);
                        })
                        .error(function(data, status) {});
                };

                $scope.scroller = {
                    top: 0,
                    position: 'relative'
                };

                $scope.moveY = function(pixels) {
                    $scope.scroller.top += pixels;
                }

                $scope.saveTaggedItems = function() {
                    if ($scope.context === 'search') {
                        $scope.saveSearchContextTaggedItems();
                    } else {
                        $scope.saveTaxonomyContextTaggedItems();
                    }

                };

                $scope.saveTaxonomyItem = function(taxonomyItem) {
                    var objToSave = taxonomyItem;
                    // insert as new taxonomies                        
                    taxonomyService.insertTaxonimiedItem(objToSave)
                        .success(function(result) {
                            if (result && result.message == 'success') {

                            }
                            $scope.closePopover();
                        })
                        .error(function(data, status) {
                            $log.debug('error: ' + JSON.stringify(data));
                        });
                };

                $scope.saveTaxonomyContextTaggedItems = function() {
                    if ((!$scope.selectedTaxonomies || $scope.selectedTaxonomies.length == 0) && $scope.preselectedTaxonomies.length == 0) {
                        return;
                    }

                    var taggedItem = $scope.item;
                    var itemType = $scope.itemType;

                    var newSelectedTaxonomies = [];
                    var existingTaxonomies = [];
                    var removedTaxonomies = [];

                    angular.forEach($scope.preselectedTaxonomies, function(preselectedTaxonomy) {
                        var isSelected = _.find($scope.selectedTaxonomies, {
                            id: preselectedTaxonomy.id
                        });
                        if (!isSelected) {
                            removedTaxonomies.push(preselectedTaxonomy);
                        } else {
                            existingTaxonomies.push(preselectedTaxonomy);
                        }
                    });

                    angular.forEach($scope.selectedTaxonomies, function(taxonomy) {
                        var isExisting = _.find(existingTaxonomies, {
                            id: taxonomy.id
                        });
                        if (!isExisting) {
                            newSelectedTaxonomies.push(taxonomy);
                        }
                    });

                    var shouldUpdate = false;
                    if ($scope.preselectedTaxonomies && $scope.preselectedTaxonomies.length > 0) {
                        // if there is existing categories we have to update the tagged item
                        shouldUpdate = true;
                    }

                    if ($scope.taxonomyItem) { // clear data appended by Angular - $scope.taxonomyItem exists only when user on Zimbang page
                        delete $scope.taxonomyItem.$$hashKey;
                        delete $scope.taxonomyItem.$index;
                    }

                    var objToSave = {};

                    if (shouldUpdate) {
                        if (!$scope.selectedTaxonomies || $scope.selectedTaxonomies.length === 0) {
                            // all categories are unchecked and we need to remove the item from previously selected categories
                            $scope.removeTaxonomyItem($scope.taxonomyItem);
                        } else {
                            // otherwise we just need to "update" item with new selected categoryIds - item is of TaxonomiedItem type and not content
                            $scope.taxonomyItem.categoryIds = _.map($scope.selectedTaxonomies, 'id');
                            angular.extend(objToSave, $scope.taxonomyItem);
                            objToSave.content = JSON.stringify(objToSave.content);

                            $scope.saveTaxonomyItem(objToSave);
                        }

                    } else {
                        // insert as new (can we have this here if this is in taxonomy (Zimbang section) context???)
                        /*objToSave.categoryIds = _.map($scope.selectedTaxonomies, 'id');
                        angular.extend(objToSave, $scope.taxonomyItem);*/

                        $scope.taxonomyItem.categoryIds = _.map($scope.selectedTaxonomies, 'id');
                        angular.extend(objToSave, $scope.taxonomyItem);

                        objToSave.content = JSON.stringify(objToSave.content);

                        $scope.saveTaxonomyItem(objToSave);
                    }
                };

                $scope.removeTaxonomyItem = function(taxonomyItem) {
                    taxonomyService.deleteTaxonomiedItem(taxonomyItem)
                        .success(function(result) {
                            if (result && result.result.message == 'success') {

                            }
                            $scope.closePopover();

                            //$scope.reload();
                            taxonomyService.setRemovedItem(taxonomyItem); // it is better to remove from local results then reload from backend for performance of the full system

                        })
                        .error(function(data, status) {
                            $log.debug('error: ' + JSON.stringify(data));
                        });
                };

                $scope.reload = function() {
                    $timeout(function() {
                        taxonomyService.setReload({
                            itemType: $scope.itemType
                        });
                    }, 50);
                };;


                $scope.saveTaxonomyContextTaggedItemsOld = function() {
                    if ((!$scope.selectedTaxonomies || $scope.selectedTaxonomies.length == 0) && $scope.preselectedTaxonomies.length == 0) {
                        return;
                    }

                    var taggedItem = $scope.item;
                    var itemType = $scope.itemType;

                    var newSelectedTaxonomies = [];
                    var existingTaxonomies = [];
                    var removedTaxonomies = [];
                    angular.forEach($scope.preselectedTaxonomies, function(preselectedTaxonomy) {
                        var isSelected = _.find($scope.selectedTaxonomies, {
                            id: preselectedTaxonomy.id
                        });
                        if (!isSelected) {
                            removedTaxonomies.push(preselectedTaxonomy);
                        } else {
                            existingTaxonomies.push(preselectedTaxonomy);
                        }
                    });

                    angular.forEach($scope.selectedTaxonomies, function(taxonomy) {
                        var isExisting = _.find(existingTaxonomies, {
                            id: taxonomy.id
                        });
                        if (!isExisting) {
                            newSelectedTaxonomies.push(taxonomy);
                        }
                    });

                    // console.log('To add taxonomies: '+JSON.stringify(newSelectedTaxonomies));
                    var shouldReload = false;
                    if (removedTaxonomies.length > 0) {
                        shouldReload = true;
                        var taxonimiedItemsToRemove = [];
                        angular.forEach(removedTaxonomies, function(removedTaxonomy) {
                            var obj = {};
                            angular.extend(obj, $scope.item);
                            obj.category = removedTaxonomy.name;
                            obj.categoryId = removedTaxonomy.id;
                            obj.categoryColor = removedTaxonomy.color;
                            obj.categoryCreatedAt = removedTaxonomy.createdAt;
                            obj.content = JSON.stringify(obj.content);
                            taxonimiedItemsToRemove.push(obj);
                        });

                        //   console.log('To remove taxonomies items: '+JSON.stringify(taxonimiedItemsToRemove));
                        // remove taxonomied item from this category/taxonomies 
                        taxonomyService.deleteTaxonomiedItems(taxonimiedItemsToRemove)
                            .success(function(result) {
                                if (result && result.message == 'success') {

                                }
                                $scope.closePopover();
                            })
                            .error(function(data, status) {
                                $log.debug('error: ' + JSON.stringify(data));
                            });

                    }

                    if (newSelectedTaxonomies.length > 0) {
                        shouldReload = true;
                        var taxonomiedItemsToInsert = [];

                        var obj = {};
                        angular.extend(obj, $scope.item);
                        obj.content = JSON.stringify(obj.content);
                        obj.categoryIds = _.map(newSelectedTaxonomies, 'id');
                        taxonomiedItemsToInsert.push(obj);


                        // insert as new taxonomies                        
                        taxonomyService.insertTaxonimiedItems(taxonomiedItemsToInsert)
                            .success(function(result) {
                                if (result && result.message == 'success') {

                                }
                                $scope.closePopover();
                            })
                            .error(function(data, status) {
                                $log.debug('error: ' + JSON.stringify(data));
                            });
                    }

                    if (shouldReload) {
                        $timeout(function() {
                            taxonomyService.setReload({
                                itemType: $scope.itemType
                            });
                        }, 200);
                    }
                };

                $scope.saveSearchContextTaggedItems = function() {
                    if (!$scope.selectedTaxonomies || $scope.selectedTaxonomies.length == 0 && $scope.preCategoryIds.length === 0) {
                        return;
                    }

                    var taggedItem = _.cloneDeep($scope.item);
                    var itemType = $scope.itemType;

                    // var olTaxonomiedItemList = [];
                    var selectedIds = _.map($scope.selectedTaxonomies, 'id');
                    if (_.isEqual(selectedIds, $scope.preCategoryIds)) {
                        // we don't need to update anything as there is nothing changed - both previously category and new are the same
                        return;
                    }

                    if (taggedItem) { // clear data appended by Angular - $scope.taxonomyItem exists only when user on Zimbang page
                        delete taggedItem.$$hashKey;
                        delete taggedItem.$index;
                    }

                    var obj = {
                        content: JSON.stringify(taggedItem),
                        itemType: itemType,
                        sourceType: 'internal',
                        categoryIds: (!$scope.selectedTaxonomies || $scope.selectedTaxonomies.length === 0) ? [] : selectedIds,
                        createdAt: (new Date()).getTime(),
                        source: taggedItem.SOURCE
                    };
                    switch (itemType) {
                        case 'trendingphrases':
                            obj.itemUrl = taggedItem.MESSAGE_LINK_URL;
                            obj.referenceId = taggedItem.MESSAGE_ID;
                            break;
                        case 'topic':
                            break;
                        case 'contributor':
                            obj.title = taggedItem.USER_NAME;
                            obj.itemUrl = taggedItem.PROFILE_URL;
                            obj.referenceId = taggedItem.USER_ID;
                            break;
                        case 'media-PHOTO':
                            obj.itemUrl = taggedItem.IMG_URL;
                            obj.referenceId = taggedItem.REFERENCE_ID;
                            break;
                        case 'media-VIDEO':
                            obj.itemUrl = taggedItem.IMG_URL;
                            obj.referenceId = taggedItem.REFERENCE_ID;
                            break;
                    }

                    //olTaxonomiedItemList.push(obj);
                    var taxonomiedItem = obj;


                    // we need to check if this is insert of new item or update
                    // if there is no selected Categories we need to call remove (as this item doesn't belong to any category)
                    // if there is at least one Category then we call insert and backend will save/update current item


                    if (!$scope.selectedTaxonomies || $scope.selectedTaxonomies.length === 0) {
                        // all categories are unchecked and we need to remove the item from previously selected categories
                        $scope.removeTaxonomyItem(taxonomiedItem);
                    } else {
                        // otherwise we just need to "update" item with new selected categoryIds - item is of TaxonomiedItem type and not content
                        //$scope.taxonomyItem.categoryIds = _.map($scope.selectedTaxonomies, 'id');
                        $scope.saveTaxonomyItem(taxonomiedItem);
                    }

                    $scope.preCategoryIds = obj.categoryIds;
                };


                $scope.closePopover = function() {
                    $scope.isPopoverOpen = false;
                };


                $scope.openTagPopover = function() {

                    var filter = $scope.getTaxonomyContextFilter(); //$scope.context==='search'?$scope.getSearchContextFilter():$scope.getTaxonomyContextFilter();

                    taxonomyService.filterTaxonomiedItems(filter)
                        .success(function(filterResult) {
                            if ($scope.taxonomies.length === 0) {
                                taxonomyService.filterTaxonomies({}, 0, 0)
                                    .success(function(result) {
                                        if (result && result.list && result.list.length > 0) {
                                            $rootScope.taxonomies = result.list;
                                            $scope.taxonomies = $rootScope.taxonomies;
                                            $scope.callPopup(filterResult);
                                        } else {
                                            $scope.callPopup(filterResult);
                                        }
                                    })
                                    .error(function(data, status) {});
                            } else {
                                $scope.callPopup(filterResult);
                            }
                        })
                        .error(function(data, status) {
                            $log.debug('error: ' + JSON.stringify(data));
                        });
                    //$scope.isPopoverOpen = !$scope.isPopoverOpen;
                };

                $scope.callPopup = function(result) {
                    if (result && result.status == 200 && result.list) {
                        //  console.log('taxonomied items filtered: ' + JSON.stringify(result));
                        angular.forEach(result.list, function(item) {
                            angular.forEach($scope.taxonomies, function(taxonomy) {
                                if (_.indexOf(item.categoryIds, taxonomy.id) > -1) {
                                    var ob_exists = _.find($scope.selectedTaxonomies, {
                                        id: taxonomy.id
                                    });
                                    if (!ob_exists) {
                                        $scope.selectedTaxonomies.push(taxonomy);
                                        ob_exists = _.find($scope.preselectedTaxonomies, {
                                            id: taxonomy.id
                                        });
                                        if (!ob_exists) {
                                            $scope.preselectedTaxonomies.push(taxonomy);
                                            $scope.preCategoryIds.push(taxonomy.id);
                                        }
                                    }
                                }
                            });
                        });
                    }
                    $scope.isPopoverOpen = true;
                };
                $scope.isPopoverOpen = false;

                /*$scope.getSearchContextFilter = function(){
                    
                    var filter = {
                        from: 0,
                        limit: $scope.taxonomies.length + 1
                    };
                    var taggedItem = $scope.item;

                    switch ($scope.itemType) {
                        case 'trendingphrases':
                            filter.referenceId = taggedItem.messages[0].referenceId;
                            filter.title = taggedItem.textItem;
                            break;
                        case 'topic':
                            filter.referenceId = taggedItem.messages[0].referenceId;
                            filter.title = taggedItem.textItem;
                            break;
                        case 'contributor':
                            filter.referenceId = taggedItem.messages.referenceId;
                            filter.title = taggedItem.userName;
                            break;
                        case 'media-PHOTO':
                            filter.referenceId = taggedItem.referenceId;
                            filter.title = taggedItem.title;
                            break;
                        case 'media-VIDEO':
                            filter.referenceId = taggedItem.referenceId;
                            filter.title = taggedItem.title;
                            break;
                    }
                    return filter;
                    
                };*/

                $scope.getTaxonomyContextFilter = function() {
                    var filter = {
                        from: 0,
                        limit: $scope.taxonomies.length + 1
                    };
                    var taggedItem = $scope.item;

                    switch ($scope.itemType) {
                        case 'trendingphrases':
                            filter.referenceId = taggedItem.MESSAGE_ID;
                            // filter.title = taggedItem.TEXT;
                            break;
                        case 'topic':
                            filter.referenceId = taggedItem.messages[0].referenceId;
                            filter.title = taggedItem.textItem;
                            break;
                        case 'contributor':
                            filter.referenceId = taggedItem.USER_ID;
                            filter.title = taggedItem.USER_NAME;
                            break;
                        case 'media-PHOTO':
                            filter.referenceId = taggedItem.REFERENCE_ID;
                            //filter.title = taggedItem.title;
                            break;
                        case 'media-VIDEO':
                            filter.referenceId = taggedItem.REFERENCE_ID;
                            //filter.title = taggedItem.title;
                            break;
                    }
                    return filter;
                }

            }]
        }
    }
]);
directives.directive('deleteTaxonomy', [
    function() {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                item: '='
            },
            template: '<i class="fa fa-times-circle-o" ng-click="removeTaxonomyItem();" tooltip-html-unsafe="Remove" tooltip-placement="bottom"></i>',
            controller: ['$scope', '$rootScope', 'taxonomyService', function($scope, $rootScope, taxonomyService) {

                $scope.removeTaxonomyItem = function() {
                    taxonomyService.deleteTaxonomiedItem($scope.item)
                        .success(function(result) {
                            taxonomyService.setRemovedItem($scope.item);
                        })
                        .error(function(data, status) {
                        });
                };

            }]
        }
    }
]);
