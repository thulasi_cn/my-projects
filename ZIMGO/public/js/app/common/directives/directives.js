var directives = angular.module('directives', []);

directives.directive('spamMark', [
    function () {
        'use strict';
        return {
            scope: {
                item: '=',
                search: '='
            },
            restrict: 'E',
            template: '<span class="footer-span" popover-template="\'/js/app/common/templates/item.spam.popover.html\'" popover-toggle="isPopoverOpen" popover-placement="left" options="{{options}}">' +
                '<div class="spamDiv popoverParent" tooltip-html-unsafe="{{ \'Mark as spam\' | translate }}" ng-click="toggleSpamPopover();"><i class="fa fa-ban" aria-hidden="true"></i></div>' +
                '</span>',
            controller: ['$scope', '$element', '$rootScope', 'spamMessagesService', 'commonSearchService', 'deviceDetector', function ($scope, $element, $rootScope, spamMessagesService, commonSearchService, deviceDetector) {

                $rootScope.$on('modalOpenChk', function(event, args){
                    if($scope.isPopoverOpen){
                            $scope.toggleSpamPopover();
                            $scope.$digest();
                    }
                });

                $scope.options = {};
                if(deviceDetector.isDesktop()){
                    $scope.popoverPos = "right";
                    $scope.options.size = 20;
                }else{
                    $scope.popoverPos = "bottom";
                    $scope.options.size = 35;
                }

                $scope.options.background = '#f9f2f4';

                $scope.isPopoverOpen = false;

                $scope.toggleSpamPopover = function(){
                    $scope.isPopoverOpen = !$scope.isPopoverOpen;
                };

                $scope.markAsSpam = function () {
                    $scope.toggleSpamPopover();
                    spamMessagesService.markAsSpam($scope.search.streamId, $scope.search.query, $scope.item)
                        .success(function (result) {
                            // reload everything
                            commonSearchService.setReload({shouldReload: true, source:''});
                        })
                        .error(function (data, status) {
                            // show some error somwhere???
                        });
                };
            }]
        }
    }
]);

directives.directive('spamAuthFooter', [
    function () {
        'use strict';
        return {
            scope: {
                authorId: '@',
                updateParent: '&'
            },
            restrict: 'E',
            template: '<div><button ng-click="openDetails()">Details</button><button class="delAuth" ng-click="removeDetail()">Remove</button></div>',
            controller: ['$scope', '$element', '$modal', 'spamService', '$filter', 'growl', function ($scope, $element, $modal, spamService, $filter, growl ) {

                $scope.allMsg = [];

                $scope.openDetails = function(){
                    var sendObj = {
                        "author_id" : $scope.authorId
                    };
                    spamService.getAuthorData(sendObj)
                        .success(function (result) {
                            if(result && result.list.length > 0){
                                $scope.allMsg = result.list;
                            }
                            $scope.openModal();
                        })
                        .error(function (data, status) {

                        });
                    
                };

                $scope.openModal = function(){
                    var modalInstance = $modal.open({
                        templateUrl: 'js/app/spam/templates/common/spam.author.modal.temp.html',
                        controller: 'spamAuthDetailModalController',
                        size: 'md',
                        resolve: {
                            allMsg: function () {
                                return $scope.allMsg;
                            }
                        }
                    });

                    modalInstance.result
                        .then(function(selectedItem) {
                        },
                        function() {
                            $log.debug('Modal dismissed at: ' + new Date());
                        });
                };

                $scope.removeDetail = function(){
                    var status = confirm( $filter('translate')('Are you sure about permanently deleting all items by the author of this content?') );
                    if (status === true) {
                        spamService.deleteSpam( $scope.authorId )
                        .success(function (result) {
                            
                            $scope.updatePerson($scope.authorId);
                            growl.success($filter('translate')("Spam filter has been deleted successfully."), { ttl: 10000 });
                        })
                        .error(function (data, status) {
                            console.log("data2:",data);
                        });
                    }
                };

                $scope.updatePerson = function(authorId) {
                    $scope.updateParent({ author : authorId });
                };
            }]
        }
    }
]);



directives.directive('showonhoverparent',
    function () {
        return {
            link: function (scope, element, attrs) {
                element.parent().bind('mouseenter', function () {
                    element.show();
                });
                element.parent().bind('mouseleave', function () {
                    element.hide();
                });
            }
        };
    });

/*
This directive allows us to pass a function in on an enter key to do what we want.
 */
directives.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                if (!scope.$$phase) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                }
                event.preventDefault();
            }
        });
    };
});


directives.directive('imageLoaded', [
    function () {
        'use strict';
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var cssClass = attrs.loadedclass;
                element.bind('load', function (e) {
                    angular.element(element).addClass(cssClass);
                });
            }
        }
    }
]);
directives.directive('fallbackImage', function () {
    var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function () {
                iElement.css("display", "none");
                angular.element(this).attr("src", iAttrs.fallbackSrc);
            });
        }
    };
    return fallbackSrc;
});


directives.directive('fallbackGridImage', function () {
    var fallbackSrc = {
        restrict: 'A',
        scope: {
            method: '&handlerMethod',
            card: '&handleModel'
        },
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function () {
                //angular.element(this).attr("src", iAttrs.fallbackSrc);
                scope.method()(scope.card());
            });
        }
    }
    return fallbackSrc;
});


directives.directive("countUp", function () {
    return {
        restrict: "A",
        require: "ngModel",
        scope: true,
        link: function (scope, element, attrs) {
            var animationLength, numDecimals;
            numDecimals = 0;
            animationLength = 4;
            if ((attrs.numDecimals != null) && attrs.numDecimals >= 0) {
                numDecimals = attrs.numDecimals;
            }
            if ((attrs.animationLength != null) && attrs.animationLength > 0) {
                animationLength = attrs.animationLength;
            }
            return scope.$watch(attrs.ngModel, function (newVal, oldVal) {
                if (oldVal == null) {
                    oldVal = 0;
                }
                if ((newVal != null) && newVal !== oldVal) {
                    return new countUp(attrs.id, oldVal, newVal, numDecimals, animationLength).start();
                }
            });
        }
    };
});

directives.directive('resizeTo', ['$window', '$parse', '$timeout', function ($window, $parse, $timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            var w = angular.element($window);
            scope.getWindowDimensions = function () {
                return {
                    'h': w.prop('outerHeight'),
                    'w': w.prop('outerWidth')
                };
            };

            scope.getToElemSize = function () {

                var resizeMinus = attr['resizeMinus'];
                var parsedAttr = attr['resizeTo'];
                var resElems = angular.element(parsedAttr);
                var toMinus = 0;

                if (resizeMinus) {
                    var resizeMinusElems = angular.element(resizeMinus);
                    if (resizeMinusElems) {
                        angular.forEach(resizeMinusElems, function (elem) {
                            var h = elem.clientHeight;
                            toMinus += h;
                        });
                    }
                }

                if (resElems && resElems.length > 0) {
                    var resElem = resElems[0];

                    var h = resElem.clientHeight;
                    scope.style = {
                        height: h - toMinus
                    }
                    if (!scope.$$phase) {
                        scope.$apply();
                    }
                }

            };

            scope.$on('imagesloaded:mainadd', function (e, item) {
                scope.getToElemSize();
            });

            w.bind('resize', function () {
                scope.getToElemSize();
            });
        }
    }
}]);

directives.directive('onErrorImgSrc', function(){
    return {
        link: function(scope, elem, attrs){
            elem.bind('error', function(){
                if(attrs.ngSrc !== attrs.onErrorImgSrc){
                    attrs.$set('ngSrc', attrs.onErrorImgSrc);
                }
            });
        }
    }
});

directives.directive('customVideoPlayer', ['$sce', function($sce){
    return {
        restrict: 'E',
        scope: {
            videos : '='
        },
        template: '<video preload="none" poster="{{ trustSrc(videos[0].poster) }}">' +
            '<source ng-repeat="item in videos" ng-src="{{ trustSrc(item.src) }}" type="video/{{ item.type }}" />' +
            '</video>' +
            '<div class="controls noselect" ng-if="!initVideo">' +
            '<a class="play" ng-click="toggle()" ng-if="!playing"> <span>&#9654;</span></a>' +
            '<a class="pause" ng-click="toggle()" ng-if="playing"><span">&#9616;&#9616;</span> </a>'+
            '<span>Watch how it works</span>'+
            '<div id="progress">'+  
                '<div id="progress_box">'+ 
                    '<span id="play_progress"></span>'+
                '</div>'+
                '</div>'+
            '</div>',
        link: function(scope, element, attrs){
            var video = element.find('video');
            element.addClass('player');
            scope.playing = false;
            scope.initVideo = false;
            
            scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
            };
            
            scope.frame = function (num) {
                if (video[0].readyState !== 0) {
                    video[0].currentTime += num;
                }
            };

            scope.toggle = function () {
                if (video[0].paused === true) {
                    video[0].play();
                    if(!video[0].hasAttribute("controls")){
                        scope.initVideo = true;
                        video[0].setAttribute("controls", "");
                    }
                    scope.playing = true;
                } else {
                    video[0].pause();
                    scope.playing = false;
                }
            };

            video[0].addEventListener('ended', function() {
                 scope.playing = false;
            }, false);

        }

    };
}]);

directives.directive('fullheigth', function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            return {
                'h': w.prop('outerHeight')
            };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;

            scope.style = function () {
                return {
                    'height': (newValue.h - 150) + 'px'
                };
            };

        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
});

(function (angular) {
    'use strict';

    var POPOVER_SHOW = 'popoverToggleShow';
    var POPOVER_HIDE = 'popoverToggleHide';

    var module = angular.module('popoverToggle', ['ui.bootstrap']);

    module.config(['$tooltipProvider', function ($tooltipProvider) {
        var triggers = {};
        triggers[POPOVER_SHOW] = POPOVER_HIDE;

        $tooltipProvider.setTriggers(triggers);
    }]);

    module.directive('popoverToggle', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: link
        };

        function link($scope, $element, $attrs) {
            $attrs.popoverTrigger = POPOVER_SHOW;
           
            $scope.$watch($attrs.popoverToggle, function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    $timeout(function () {
                        if (newValue) {
                            // $element.triggerHandler(POPOVER_SHOW);
                            if ($element[0])
                                $element[0].dispatchEvent(new CustomEvent(POPOVER_SHOW));
                        } else {
                            if ($element[0])
                                $element[0].dispatchEvent(new CustomEvent(POPOVER_HIDE));
                            // $element.triggerHandler(POPOVER_HIDE);
                        }
                    }, 100);
                }
            });
        }
    }]);

})(angular);


/* date of birth */
// https://github.com/rorymadden/angular-date-dropdowns
(function () {
    'use strict';

    var dd = angular.module('rorymadden.date-dropdowns', []);

    dd.factory('rsmdateutils', function () {
        var that = this,
            dayRange = [1, 31],
            months = [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December'
        ];

        function changeDate(date) {
            if (date.day > 28) {
                date.day--;
                return date;
            } else if (date.month > 11) {
                date.day = 31;
                date.month--;
                return date;
            }
        }

        return {
            checkDate: function (date) {
                var d;
                if (!date.day || date.month === null || date.month === undefined || !date.year) return false;

                d = new Date(Date.UTC(date.year, date.month, date.day));

                if (d && (d.getMonth() === date.month && d.getDate() === Number(date.day))) {
                    return d;
                }

                return this.checkDate(changeDate(date));
            },
            days: (function () {
                var days = [];
                while (dayRange[0] <= dayRange[1]) {
                    days.push(dayRange[0]++);
                }
                return days;
            }()),
            months: (function () {
                var lst = [],
                    mLen = months.length;

                for (var i = 0; i < mLen; i++) {
                    lst.push({
                        value: i,
                        name: months[i]
                    });
                }
                return lst;
            }())
        };
    });

    dd.directive('rsmdatedropdowns', ['rsmdateutils', function (rsmdateutils) {
        return {
            restrict: 'A',
            replace: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel'
            },
            controller: ['$scope', 'rsmdateutils', function ($scope, rsmDateUtils) {
                $scope.days = rsmDateUtils.days;
                $scope.months = rsmDateUtils.months;

                $scope.dateFields = {};

                $scope.dateFields.day = new Date($scope.model).getUTCDate();
                $scope.dateFields.month = new Date($scope.model).getUTCMonth();
                $scope.dateFields.year = new Date($scope.model).getUTCFullYear();

                // Initialize with current date (if set)
                $scope.$watch('model', function (newDate) {
                    if (newDate) {
                        $scope.dateFields.day = new Date(newDate).getUTCDate();
                        $scope.dateFields.month = new Date(newDate).getUTCMonth();
                        $scope.dateFields.year = new Date(newDate).getUTCFullYear();
                    }
                });

                $scope.checkDate = function () {
                    var date = rsmDateUtils.checkDate($scope.dateFields);
                    if (date) {
                        $scope.model = date;
                    }
                };
      }],
            template: '<div class="form-inline">' +
                '  <div class="form-group col-md-4 col-xs-2 col-sm-2 day select-style">' +
                '     <select name="dateFields.day" data-ng-model="dateFields.day" placeholder="Day" class="form-control" ng-options="day for day in days" ng-change="checkDate()" ng-disabled="disableFields" required><option selected value="">Day</option></select>' +
                '  </div>' +
                '  <div class="form-group col-md-5 col-xs-3 col-sm-3 month select-style">' +
                '    <select name="dateFields.month" data-ng-model="dateFields.month" placeholder="Month" class="form-control" ng-options="month.value as month.name for month in months" value="{{ dateField.month }}" ng-change="checkDate()" ng-disabled="disableFields" required><option selected value="">Month</option></select>' +
                '  </div>' +
                '  <div class="form-group col-md-3 col-xs-3 col-sm-3 year select-style">' +
                '    <select ng-show="!yearText" name="dateFields.year" data-ng-model="dateFields.year" placeholder="Year" class="form-control" ng-options="year for year in years" ng-change="checkDate()" ng-disabled="disableFields" required><option selected value="">Year</option></select>' +
                '    <input ng-show="yearText" type="text" name="dateFields.year" data-ng-model="dateFields.year" placeholder="Year" class="form-control" ng-disabled="disableFields">' +
                '  </div>' +
                '</div>',
            link: function (scope, element, attrs, ctrl) {
                var currentYear = parseInt(attrs.startingYear, 10) || new Date().getFullYear(),
                    numYears = parseInt(attrs.numYears, 10) || 100,
                    oldestYear = currentYear - numYears,
                    overridable = [
              'dayDivClass',
              'dayClass',
              'monthDivClass',
              'monthClass',
              'yearDivClass',
              'yearClass'
            ],
                    required;

                scope.years = [];
                scope.yearText = attrs.yearText ? true : false;

                if (attrs.ngDisabled) {
                    scope.$parent.$watch(attrs.ngDisabled, function (newVal) {
                        scope.disableFields = newVal;
                    });
                }

                if (attrs.required) {
                    required = attrs.required.split(' ');

                    ctrl.$parsers.push(function (value) {
                        angular.forEach(required, function (elem) {
                            if (!angular.isNumber(elem)) {
                                ctrl.$setValidity('required', false);
                            }
                        });
                        ctrl.$setValidity('required', true);
                    });
                }

                for (var i = currentYear; i >= oldestYear; i--) {
                    scope.years.push(i);
                }

                (function () {
                    var oLen = overridable.length,
                        oCurrent,
                        childEle;
                    while (oLen--) {
                        oCurrent = overridable[oLen];
                        childEle = element[0].children[Math.floor(oLen / 2)];

                        if (oLen % 2 && oLen != 2) {
                            childEle = childEle.children[0];
                        }

                        if (attrs[oCurrent]) {
                            angular.element(childEle).attr('class', attrs[oCurrent]);
                        }
                    }
                }());
            }
        };
  }]);
}());
