var directives = angular.module('directives');

directives.directive('sentimentOpinion', [
    function() {
        'use strict';
        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                model: '=ngModel',
            },
            template: '<span class="resSentiment" ng-click="openItem()"><span translate>Sentiment</span>' +
                '<button style ="background-color : {{$root.getHexCode(model.SENTIMENT_SCORE)}}">{{ model.SENTIMENT_SCORE | number:0 }}%</button></span>',
            controller: ['$scope', '$rootScope', '$modal', function($scope, $rootScope, $modal) {
                $scope.openItem = function() {
                 $modal.open({
                        templateUrl: '/js/app/common/templates/senitiment.opinion.model.view.html',
                        controller: 'SentimentOpinionController',
                        size: 'md',
                        resolve:{
                            taggedMessage: function () {
                                return $scope.model;
                            }
                        }
                    });

                };
            }]
        };
    }
]);
