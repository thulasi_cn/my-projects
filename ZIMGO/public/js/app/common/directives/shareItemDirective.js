var directives = angular.module('directives');

directives.directive('shareItem', ['$compile',

    function($compile) {
        'use strict';
        return {
            restrict: 'E',
            scope: {
                item: '=',
                itemType: '=',
                context: '=',
                taggedBy: '='
            },
            /*template:   '<span class="footer-span" popover-template="\'/js/app/common/templates/search.share.popover.html\'" popover-toggle="issharePopoverOpen" popover-placement="bottom">' +
                            '<a href="" ng-click="sharePopupOpen()">' +
                                '<span class="afterlogin_shareicon"></span>' +
                            '</a>' +
                        '</span>',*/
            controller: ['$rootScope', '$scope', '$log', '$modal', 'taxonomyService', '$window', '$timeout', '$facebook', 'languageService', 'tribeService', 'growl', function($rootScope, $scope, $log, $modal, taxonomyService, $window, $timeout, $facebook, languageService, tribeService, growl) {

                $scope.issharePopoverOpen = false;

                $rootScope.$on('modalOpenChk', function(event, args) {
                    if ($scope.issharePopoverOpen) {
                        $scope.closeSharePopover();
                        $scope.$digest();
                    }
                });

                $scope.shares = ['Facebook', 'Twitter', 'Clan'];

                $scope.closeSharePopover = function() {
                    $scope.issharePopoverOpen = !$scope.issharePopoverOpen;
                };

                $scope.sharePopupOpen = function() {
                    $scope.issharePopoverOpen = !$scope.issharePopoverOpen;
                };

                $scope.shareItem = function(chkItem) {
                    var shareUrl = "";
                    if ($scope.itemType === "contributor") {
                        shareUrl = $scope.item.MESSAGE_JSON.profileUrl;
                    } else if ($scope.itemType === "trendingphrases") {
                        shareUrl = (decodeURIComponent($scope.item.MESSAGE_LINK_URL));
                    } else if ($scope.itemType === "media-PHOTO") {
                        shareUrl = $scope.item.IMG_URL;
                    } else if ($scope.itemType === "media-VIDEO") {
                        shareUrl = $scope.item.VIDEO_URL;
                    }

                    $facebook.ui({
                        method: 'share',
                        href: shareUrl,
                    }, function(response) {
                        $log.debug("res:", response);
                    });
                };

                $scope.getUrl = function(url) {
                    return decodeURIComponent(url);
                };

                $scope.twitterShareURL = function() {

                    var itemType = $scope.itemType;
                    var twtShareURL = '';

                    var obj = {};
                    var taggedItem = $scope.item;
                    obj.createdAt = (new Date()).getTime();
                    obj.source = taggedItem.SOURCE;

                    switch (itemType) {
                        case 'trendingphrases':
                            obj.title = taggedItem.TEXT;
                            // obj.itemUrl = taggedItem.messages[0].messageLinkUrl;

                            obj.referenceId = taggedItem.MESSAGE_ID;
                            break;
                            /*case 'topic':
                                obj.title = taggedItem.textItem;
                                obj.itemUrl = taggedItem.messages[0].messageLinkUrl;
                                obj.referenceId = taggedItem.messages[0].referenceId;
                                obj.source = taggedItem.messages[0].source;
                                break;*/
                        case 'contributor':
                            obj.title = taggedItem.MESSAGE_JSON.userName;
                            obj.itemUrl = taggedItem.MESSAGE_JSON.profileUrl;
                            obj.source = taggedItem.MESSAGE_JSON.source;

                            obj.referenceId = taggedItem.MESSAGE_JSON.messageId;
                            break;
                        case 'media-PHOTO':
                            obj.itemUrl = taggedItem.IMG_URL;
                            obj.title = taggedItem.TITLE;
                            obj.referenceId = taggedItem.REFERENCE_ID;
                            break;
                        case 'media-VIDEO':
                            obj.itemUrl = taggedItem.VIDEO_URL;
                            obj.title = taggedItem.TITLE;
                            obj.referenceId = taggedItem.REFERENCE_ID;
                            break;
                    };

                    var selectedLanguage = languageService.getCurrent();
                    var testBlank = /(\s\s)+\s*/g;

                    if (itemType === 'trendingphrases' && (obj.source === 'twitter' || obj.source === 'TW')) {
                        twtShareURL = "https://twitter.com/intent/retweet?lang=" + selectedLanguage.id + "&tweet_id=" + obj.referenceId;
                    } else {
                        twtShareURL = "https://twitter.com/intent/tweet?lang=" + selectedLanguage.id + (obj.itemUrl ? "&url=" + obj.itemUrl : '') + (obj.title ? '&text=' + obj.title.replace(testBlank, ' ') : '');
                    }

                    return twtShareURL;

                };

                tribeService.loggedInTribeman().then(function(res) {
                    $scope.loggedInTribemanDetails = res.data;
                }, function(err) { console.log(err); });

                $scope.shareToClan = function() {
                    $modal.open({
                        templateUrl: '/js/app/common/templates/share.search.result.clan.view.html',
                        controller: function($scope, $modalInstance, tribeService, tribemanDetails, sharePostData, $state) {
                            $scope.close = function() { $modalInstance.dismiss('cancel'); };
                            tribeService.getClansByTribeman(tribemanDetails.tribemanId)
                                .success(function(clan) {
                                    $scope.myClanList = clan;
                                }).error(function(error) {});
                            $scope.shareToClan = function(clan) {
                                $scope.currentClan = clan;
                                var PostObj = {};
                                PostObj.postName = sharePostData.USER_NAME;
                                PostObj.postText = sharePostData.TEXT;
                                PostObj.postPicture = sharePostData.MEDIA_JSON[0].imgUrl;
                                PostObj.postSentiment = sharePostData.SENTIMENT_SCORE;
                                PostObj.postMood = 'HAPPY'; //sharePostData.MOOD;
                                PostObj.userScore = sharePostData.USER_SCORE;
                                PostObj.profileImageUrl = sharePostData.PROFILE_IMAGE_URL;
                                PostObj.userScreenName = sharePostData.USER_NAME;

                                /*PostObj.profileUrl = "string";
                                PostObj.rawLocation = "string";
                                PostObj.postVolume = 0;
                                PostObj.pictureInfo = "string";
                                PostObj.videoLink = "string";
                                PostObj.videoLinkInfo = "string";
                                PostObj.postHashtags = "string";
                                PostObj.socialIdentifier = sharePostData.SOURCE;
                                PostObj.streamId = "string";
                                PostObj.keyword = "";
                                PostObj.messageId = "string";
                                PostObj.messageLinkUrl = "string";
                                PostObj.rawMessage = "Test rawMessage";
                                PostObj.country = "Test string";
                                PostObj.state = "Test State";
                                PostObj.city = "Test City";*/

                                tribeService.createClanPost(PostObj, $scope.currentClan).success(function(res) {
                                    tribeService.setCommentRefresh({ 'refresh': true });
                                    $scope.close();
                                    growl.success("You search results has been successfully posted to Clan", {
                                        ttl: 5000
                                    });
                                    /*$state.go('main.base.tribe.main.main.categorie.tribes.clan.post', { tribeId: $scope.currentClan.tribeId, clanId: $scope.currentClan.clanId });*/
                                });
                            };

                        },
                        size: 'sm',
                        resolve: {
                            sharePostData: function() {
                                return $scope.item;
                            },
                            tribemanDetails: function() {
                                return $scope.loggedInTribemanDetails;
                            }
                        }
                    });
                };



            }],
            //link: ['scope', '$element', '$attrs', '$facebook', function(scope, $element, $attrs, $facebook){
            link: function(scope, element, attrs, $facebook) {



                var template = '<span class="footer-span" popover-template="\'search.share.popover.html\'" popover-toggle="issharePopoverOpen" popover-placement="bottom">' +
                    '<a href="" class="popoverParent" ng-click="closeSharePopover()">' +
                    '<span class="afterlogin_shareicon"></span>' +
                    '</a>' +
                    '</span>' +
                    '<script type="text/ng-template" id="search.share.popover.html">' +
                    '<div class="search-share-main">' +
                    '<div class="btn-div">' +
                    '<span translate>Share it with</span>' +
                    '<div class="btn-group  pull-right" role="group">' +
                    '<a>' +
                    '<div class="fa-timesWrap">' +
                    '<i class="fa fa-times" data-ng-click="closeSharePopover()" class="btn btn-xs" tooltip-html-unsafe="{{\'Cancel selection\' | translate }}" tooltip-placement="bottom"></i>' +
                    '</div>' +
                    '</a>' +
                    '</div>' +
                    '</div>' +
                    '<div class="search-share-repeat" ng-repeat = "type in shares">' +
                    '<div class="search-share-itemFb" ng-click="shareItem(type);" ng-if="type === \'Facebook\'">' +
                    '<i class="fa fa-facebook" aria-hidden="true"></i>' +
                    '</div>' +
                    '<div class="search-share-itemTw" ng-if="type === \'Twitter\'">' +
                    '<a ng-href="{{twitterShareURL()}}">' +
                    '<i class="fa fa-twitter" aria-hidden="true"></i>' +
                    '</a>' +
                    '</div>' +
                    '<div class="search-share-itemClan" ng-disable="loggedInTribemanDetails.tribemanId" ng-if="type === \'Clan\'">' +
                    '<a ng-click="shareToClan();">' +
                    '<img class="tribeShareImg" src="img/icons/tribe.png">' +
                    '</a>' +
                    '</div>' +
                    '</div>' +
                    '<div class="clear"></div>' +
                    '<div class="sharePopoverMsg" translate>You are required to be logged into Facebook or Twitter to share on these sites.</div>' +
                    '</div>' +
                    '</scrpt>';

                var compiled = $compile(template)(scope.$new());
                element.replaceWith(compiled);
            }
        }
    }
]);
