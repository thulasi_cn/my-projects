var navigationModule = angular.module('navigationModule');

navigationModule.controller('LanguageNavigationCtrl', ['$scope', '$rootScope', 'languageService','$cookies', function($scope, $rootScope, languageService,$cookies) {
      $scope.languages = languageService.getAvailableLanguages();
      $scope.selectedLanguage = languageService.getCurrent();
    //  amMoment.changeLocale($scope.selectedLanguage.id);

      $scope.selectLanguage = function (lang) {

          // $scope.changeHeaderSrc(lang);
          //$scope.clearQuery();
          languageService.setCurrent(lang);
          $scope.selectedLanguage = lang;
          $cookies.putObject("defaultLangSet", lang.id);
          // $rootScope.selectedLanguage = lang;
          // $translate.use(lang.id);
          // amMoment.changeLocale(lang.id);
          window.location.reload(true);
      };

      // $scope.changeHeaderSrc = function(lang){
      //   if(lang.name === "Eng"){
      //     document.getElementById("facebook-jssdk").src="//connect.facebook.net/en_US/sdk.js";
      //   }else if(lang.name === "Kor"){
      //     document.getElementById("facebook-jssdk").src="//connect.facebook.net/ko_KR/sdk.js";
      //   }
      // };

}]);
