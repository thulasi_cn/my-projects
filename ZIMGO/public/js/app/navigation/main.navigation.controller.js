var navigationModule = angular.module('navigationModule');

navigationModule.controller('MainNavigationCtrl', ['$scope', '$rootScope', '$state', 'UsersService', '$modal', '$translate', 'growl', '$filter', 'ngJws', '$location', '$cookies',
    'compileStreamService', 'amMoment', 'searchStremService', 'commonSearchService',
    '$timeout', 'viewStateService', 'webSocketStateService', 'stackSearchStreamsService',
    'languageService', '$interval', 'socialService', 'mediaMessagesService', 'limitService', 'deviceDetector', 'topicFilterService', '$location','tribeService',
    function($scope, $rootScope, $state, UsersService, $modal, $translate, growl, $filter, ngJws,
        $location, $cookies, compileStreamService, amMoment, searchStremService, commonSearchService,
        $timeout, viewStateService, webSocketStateService, stackSearchStreamsService, languageService, $interval, socialService, mediaMessagesService, limitService, deviceDetector, topicFilterService, $location,tribeService) {
        var tribeMan = localStorage.getItem('loggedInTribeman');
        var loggeInTribeMan = JSON.parse(localStorage.getItem("loggedInTribeman")) === "" ? true : false;

        if (tribeMan === null || loggeInTribeMan) {
            tribeService.loggedInTribeman().then(function(res) {
                tribeService.setLoggedInTribemanDetails(res.data);
                $rootScope.tribemanId = res.data.tribemanId;
                localStorage.setItem('loggedInTribeman', JSON.stringify(res.data));
            }, function(err) {
                console.log(err);
            });
        } else {
            var t = JSON.parse(tribeMan);
            $rootScope.tribemanId = t.tribemanId;
        }
        $scope.detectmob = function() {
            if (deviceDetector.isDesktop()) {
                return false;
            } else {
                var tltp = angular.element(".tooltip-inner,.tooltip-arrow");
                tltp.css('display', 'none');
                return true;
            }
        };
        if ($location.path().split('/')[1] == 'policy') {
            $scope.polNav = true;
        }

        socialService.setCookiesInFilter();

        $rootScope.checkHoldResult = false;
        $rootScope.shwLoad = false;
        $rootScope.callIntervalElm = [];

        if ($rootScope.$state.current.name === "main.base.content.main.main") {
            $scope.pageDetail = false;
        }
        // $rootScope.tourStart = false;

        $scope.activeAllSocialLinks = function() {
            if (angular.element('.main-search-sns.filter-sns li').length > 0) {
                setTimeout(function() {
                    angular.element('.main-search-sns.filter-sns li').trigger('click');
                }, 2000);
            }
        };
        $scope.checkedAutoRefresh = false;

        $scope.selectTimers = [{ id: 1, value: 30 }, { id: 2, value: 60 }, { id: 3, value: 90 }, { id: 4, value: 120, }, { id: 5, value: 150 }];
        $scope.setTimerController = $scope.selectTimers[0].value;

        // languageService.getCurrent();
        $scope.$watch(function() {
            return languageService.getCurrent();
        }, function(newValue, oldValue) {

            if ((newValue && oldValue) && newValue.id !== oldValue.id) {
                $scope.clearQuery();
            }
        });

        $scope.$watch(function() {
            return $rootScope.missingSocialAccounts;
        }, function(newValue, oldValue) {
            $scope.warning = 'Please connect to Twitter';
            if (newValue && newValue.length > 0) {
                if (newValue.length !== 2) {
                    var transLatedVal = $filter('translate')('Please connect Zimgo search with your ' + newValue[0] + ' account.');
                    growl.warning('<a ui-sref="main.users.base.profile" href="#/user/profile">' + transLatedVal + '</a>');
                }
            }

            $scope.openSocialLoginModal();
        });

        $scope.$watchCollection(function() {
            return commonSearchService.getSearchFilter();
        }, function(newVal, oldVal) {
            if (newVal && newVal !== oldVal && newVal.reload) {
                if (newVal.score === undefined && newVal.topic === undefined) {
                    $scope.mainDoSearch(true);
                } else {
                    $scope.postCompileStreamHandler(false);
                }
            }
        });

        $scope.toggleAutoRefresh = function() {
            return $scope.checkedAutoRefresh;
        };

        $scope.setTimer = function() {
            return $scope.setTimerController * 1000;
        };

        $scope.doLandingSearch = function(callback) {
            $timeout(function() {
                angular.element('.ng-isolate-scope.active a').trigger('click');
            }, 100);
        };

        $scope.mainDoSearch = function(type) {
            delete $scope.search.streamId;
            delete $scope.search.customerId;
            delete $scope.search.accountId;
            delete $scope.search.lang;
            commonSearchService.setSearchFilter({ "topic": undefined, "reload": false });
            if (GOWEB_CONF.analyticsId) {
                GA('send', {
                    hitType: 'event',
                    eventCategory: 'searchPage',
                    eventAction: 'search',
                    eventLabel: 'searchPageSearch'
                });
            }
            commonSearchService.setReset({
                shouldReset: true
            });

            $scope.doSearch(type);
        };

        $scope.$watch(function() {
            return commonSearchService.getDoSearch();
        }, function(newValue, oldValue) {
            $scope.search = {
                query: ''
            };
            if (newValue && newValue !== oldValue) {

                var langObj = languageService.resolveLanguage(newValue.language); //$scope.getLanguage(newValue.language);
                //  $scope.selectLanguage(langObj);
                //  languageService.setCurrent(langObj); // commented out to disable language switch in language

                $timeout(function() {
                    $scope.search.lang = langObj.id;
                    $scope.search.query = newValue.keyword;
                    $scope.search.customerId = newValue.customerId;
                    $scope.search.accountId = newValue.customerAccountId;
                    $scope.search.streamId = newValue.streamId;

                    var saveObj = {
                        "query": $scope.search.query,
                        "streamId": $scope.search.streamId
                    };

                    $cookies.putObject("searchVal", saveObj);
                    $scope.doSearch(true);
                }, 100);
            }
        });

        $scope.clearQuery = function(q) {
            q = q ? q : '';
            $scope.search = {
                query: q
            };
        };
        $scope.clearQuery();

        $scope.doSearch = function(type) {

            var setObj = commonSearchService.getSearchFilter();
            if (setObj) {
                setObj.reload = false;
                commonSearchService.setSearchFilter(setObj);
            }

            $scope.intervalRunCount = 5;
            stopInterval = undefined;
            if ($scope.search.streamId) {
                stackSearchStreamsService.add($scope.search);
                $scope.postCompileStreamHandler(type);
            } else {
                $scope.runCompileStream(type);
            }
        };

        $scope.postCompileStreamHandler = function(type) {
            //$rootScope.$broadcast('search:query', $scope.search);
            $timeout(function() {
                //console.log('run postCompileStreamHandler  - setCurrent in 50ms, $scope.search: ' + JSON.stringify($scope.search));
                commonSearchService.setCurrent($scope.search);
                if (type) {
                    $rootScope.checkHoldResult = false;
                    $rootScope.shwLoad = true;
                    $scope.callReload();
                    var countStrt = 0;
                    var dt1 = new Date().getTime();
                    //$rootScope.callIntervalElm.length = 0;
                    $rootScope.callIntervalElm = [];
                    var timer = $interval(function() {

                        if (countStrt < 60) {
                            if ($rootScope.callIntervalElm.length < 1) {
                                var dt2 = new Date().getTime();
                                countStrt += 5;
                                if (countStrt === 60) {
                                    $rootScope.checkHoldResult = true;
                                    // $rootScope.shwLoad = false;
                                }
                                console.log("time difference :" + (dt2 - dt1));
                                $scope.callReload();
                            } else {
                                $interval.cancel(timer);
                                $rootScope.checkHoldResult = true;
                                // $rootScope.shwLoad = false;
                            }
                        } else {
                            $interval.cancel(timer);
                        }
                    }, 5000);
                } else {
                    $rootScope.checkHoldResult = true;
                    $scope.callReload();
                }
            }, 100);

            /*
            if (!webSocketStateService.isConnected()) {
                webSocketStateService.clearChannels();
                webSocketStateService.handleWsConnection();
            } else {
                var previous = stackSearchStreamsService.getPrevious($scope.search);
                if (previous && previous.streamId) {
                    // TODO - create top last 10 searches and store in internal service collections for navigation
                    webSocketStateService.unsubscribeChannels(previous.streamId);
                }
            }
            webSocketStateService.subscribeToChannels($scope.search.streamId);
            */

        };

        $scope.callReload = function() {
            // $rootScope.callIntervalElm = [];
            limitService.setPhrasesLimit();
            limitService.setContributorsLimit();
            limitService.setMediaLimit();
            commonSearchService.setReload({
                shouldReload: true,
                source: 'main.navigation'
            });
        };

        var stopInterval;
        $scope.intervalRunCount = 0;

        $scope.runCompileStream = function(type) {
            // TODO this should be changed

            if (!$scope.search.query) return;

            //////////////
            var customerId = $scope.search.customerId;
            var accountId = $scope.search.accountId;
            if (!customerId)
                customerId = $rootScope.user ? $rootScope.user.customerId : null;

            if (!accountId)
                accountId = $rootScope.user ? $rootScope.user.accountId : null;

            compileStreamService.compileStream($scope.search.query, customerId, accountId, languageService.getCurrent().id)
                .success(function(result) {
                    $scope.search.streamId = result.streamId;

                    var saveObj = {
                        "query": $scope.search.query,
                        "streamId": result.streamId
                    };
                    $cookies.putObject("searchVal", saveObj);
                    $scope.search.lang = languageService.getCurrent().id; //$scope.selectedLanguage.id;

                    // TODO this should be changed
                    $scope.search.customerId = customerId;
                    $scope.search.accountId = accountId;
                    //////////
                    // add to stacked stream so we can have last searches and histst
                    stackSearchStreamsService.add($scope.search);

                    $scope.postCompileStreamHandler(type);

                    var IntervalTime = $scope.setTimer();
                    if (!stopInterval) {
                        stopInterval = $interval(function() {
                            if ($scope.checkedAutoRefresh) {
                                $scope.runCompileStream(type);
                                /*if (++$scope.intervalRunCount>=6){
                                    if (stopInterval){
                                        $interval.cancel(stopInterval);
                                        $scope.intervalRunCount = 0;
                                    }
                                }*/
                            }

                        }, IntervalTime);
                    }

                })
                .error(function(data, status) {
                    $scope.error = data.message;
                });
        };

        /* login related */
        $scope.openLoginModal = function(selectType) {
            viewStateService.setState('OpenModal');
            var modalInstance = $modal.open({
                templateUrl: 'js/app/authentication/templates/login.modal.view.html',
                controller: 'LoginModalCtrl',
                backdrop: 'static',
                keyboard: false,
                windowClass: 'login-modal-window',
                resolve: {
                    activeTab: function() {
                        return selectType;
                    }
                }
            });
            modalInstance.result.then(function(user) {
                viewStateService.setState(null);
                if (user) {
                    $state.go('main.base.content.main.main');
                } else {
                    $scope.error = 'You are not logged in. Plese try again';
                }
            }, function() {});
        };

        $scope.openRegisterModal = function() {
            var modalInstance = $modal.open({
                templateUrl: 'js/app/authentication/templates/registration.modal.view.html',
                controller: 'RegistrationModalCtrl',
                windowClass: 'registration-modal-window'
            });
            modalInstance.result.then(function(registerModel) {

            }, function() {});
        };

        if (viewStateService.getState() !== 'OpenModal') {
            if (UsersService.getRedirectStatus() === 'modalOpen') {
                $scope.openLoginModal('login');
            }
        }

        $scope.firstRecentSearch = function() {
            var srchObj = $cookies.getObject("searchVal");
            if (srchObj) {
                $timeout(function() {
                    $scope.search.lang = languageService.getCurrent().id;

                    $scope.search.query = srchObj.query;
                    $scope.search.streamId = srchObj.streamId;

                    $scope.doSearch(false);
                }, 50);
            } else {
                searchStremService.getRecent({}, 0, 1)
                    .success(function(result) {
                        if (result && result.list && result.list.length > 0) {
                            var stream = result.list[0];

                            // var langObj = languageService.resolveLanguage(stream.language); // $scope.getLanguage(stream.language);
                            //$scope.selectLanguage(langObj);
                            // languageService.setCurrent(langObj);
                            $scope.search.lang = languageService.getCurrent().id;

                            $scope.search.query = stream.keyword;
                            $scope.search.customerId = stream.customerId;
                            $scope.search.accountId = stream.customerAccountId;
                            $scope.search.streamId = stream.streamId;

                            stackSearchStreamsService.add($scope.search);

                            $scope.doSearch(false);
                        }
                    });
            }
        };

        $scope.socialConnectNetworks = socialService.getSocialNetworks();

        $scope.openSocialLoginModal = function() {
            if ($rootScope.authToken && $rootScope.missingSocialAccounts.length === 2) {
                if (viewStateService.getState() !== 'OpenModal') {
                    $scope.openLoginModal('register');
                }
            }
        };


        /**/
        $scope.activeSNS = function() {
            //alert('activeSNS');

            /*$scope.$on('$viewContentLoaded', function(){
                angular.element('.main-search-sns.filter-sns li').trigger('click');
            });*/
            //angular.element('.main-search-sns.filter-sns li').trigger('click');
        };

        $rootScope.$on('$includeContentLoaded', function() {

        });

        $scope.filterSocial = function(sn) {
            socialService.toggleAddFilter(sn);
        };

        $scope.isSnsSelected = function(sn) {
            return socialService.filterExists(sn);
        };

        $scope.checkSelection = function(sn) {
            if ($scope.isSnsSelected(sn) && (sn.id === 'YT' || sn.id === 'FL') && $scope.disableSocialPart) {
                return true;
            } else {
                return false;
            }
        };

        if (!viewStateService.getState() || viewStateService.getState() === "FirstRecentSearch") {
            viewStateService.setState('FirstRecentSearch');
            $scope.firstRecentSearch();
        }

        $scope.$on('$viewContentLoaded', function(event) {
            $rootScope.$broadcast('load-tour');
        });

    }
]);
