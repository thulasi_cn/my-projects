var searchModule = angular.module('searchModule');

searchModule.factory('socialService', [
    '$cookies',
    function ($cookies) {
        return {
            toggleAddFilter: function (sn) {

                this.selectedSns = this.selectedSns || [];
                if (this.filterExists(sn)) {
                    this.selectedSns = _.reject(this.selectedSns, _.omit(sn, ['$$hashKey']));
                    // this.selectedSns.splice(this.selectedSns.indexOf(sn),1);
                } else {
                    this.selectedSns.push(sn);
                }

                this.setFilterinCookies();
            },
            getFilter: function () {
                return this.selectedSns;
            },
            filterExists: function (sns) {
                this.selectedSns = this.selectedSns || [];
                //return this.selectedSns.indexOf(sns)>-1?true:false;
                var idxSns = _.findIndex(this.selectedSns, function (item) {
                    return item.id === sns.id;
                });
                return idxSns > -1;

            },
            getSocialNetworks: function () {
                return [{
                    name: 'twitter',
                    id: 'TW'
                }, {
                    name: 'facebook',
                    id: 'FB'
                }, {
                    name: 'youtube',
                    id: 'YT'
                }, {
                    name: 'flickr',
                    id: 'FL'
                }, {
                    name: 'blog',
                    id: 'BL'
                }];
            },
            setFilterinCookies: function () {
                $cookies.putObject("filter", this.selectedSns);
            },
            setCookiesInFilter: function () {
                if ($cookies.getObject("filter") !== undefined) {
                    this.selectedSns = $cookies.getObject("filter");
                }
            }
        }
}]);

searchModule.factory('topicFilterService', [
    function () {
        return {
            setCurrentTopicFilter : function (topicKeyword) {
                this.currentTopicFilter = topicKeyword;
            },
            getCurrentTopicFilter : function () {
                return this.currentTopicFilter;
            },
            setCurrentSentimentFilter : function (topicKeyword) {
                this.currentSentimentFilter = topicKeyword;
            },
            getCurrentSentimentFilter : function () {
                return this.currentSentimentFilter;
            },
            setCurrentLocationFilter : function (topicKeyword) {
                this.currentSentimentFilter = topicKeyword;
            },
            getCurrentLocationFilter : function () {
                return this.currentSentimentFilter;
            }
        };
}]);
searchModule.factory('activeTabService', [

    function () {
        var activeTab = null;
        return {
            set: function (atab) {
                activeTab = atab;
            },
            get: function () {
                return activeTab;
            }
        }
}]);

searchModule.factory('commonSearchService', ['$http','$location', function ($http,$location) {
    return {
        getActiveRoute: function () {
            return this.activeRoute;
        },
        setActiveRoute: function (activeRoute) {
            this.activeRoute = activeRoute;
        },
        getCurrent: function () {
            return this.currentSearchItem;
        },
        setCurrent: function (searchItem) {
            if ($location.path().split('/')[1] !== "interesting") {
                this.currentSearchItem = searchItem;
            } else {
                angular.element('#step-search').val('');
                searchItem.query = '';
                this.currentSearchItem = searchItem;
            }
        },
        addCurrentFilter: function (filterName, filterItem) {
            if (!this.currentSearchItem.filter)
                this.currentSearchItem.filter = {};

            if (_.isNull(filterItem)) {
                delete this.currentSearchItem.filter[filterName];
            } else {
                this.currentSearchItem.filter[filterName] = filterItem;
            }

        },
        setDoSearch: function (doSearchItem) {
            this.doSearchItem = doSearchItem;
        },
        getDoSearch: function () {
            return this.doSearchItem;
        },
        getReload: function () {
            return this.reload;
        },
        setReload: function (reload) {
            this.reload = reload;
        },
        setReset: function (reset) {
            this.reset = reset;
        },
        getReset: function () {
            return this.reset;
        },
        getSearchFilter: function(){
            return this.searchFilter;
        },
        setSearchFilter: function(filterObj){
            this.searchFilter = filterObj;
        }

    }
}]);

searchModule.factory('viewStateService', ['$http', function ($http) {
    return {
        getState: function () {
            return this.state;
        },
        setState: function (state) {
            this.state = state
        }
    }
}]);

searchModule.factory('stackSearchStreamsService', [
    function () {
        var limit = 10;
        return {
            getPrevious: function (searchStream) {
                if (this.searchStreams) {
                    var indexCurrent = this.searchStreams.indexOf(searchStream);
                    if (indexCurrent > 0) {
                        return this.searchStreams[indexCurrent - 1];
                    }
                    //return this.searchStreams[this.searchStreams.length-1];
                }
                return null;
            },
            getAll: function (searchStream) {
                return this.searchStreams;
            },
            add: function (searchStream) {
                if (!this.searchStreams) this.searchStreams = [];
                if (this.searchStreams.indexOf(searchStream) === -1) {
                    if (this.searchStreams.length === limit) {
                        this.searchStreams.shift();
                    }
                    this.searchStreams.push(searchStream);
                }
            }
        }
    }
]);

searchModule.factory('webSocketStateService', ['$http', '$log', 'ngJws', '$interval', function ($http, $log, ngJws, $interval) {
    var defaultBucketSize = 10;
    var maxBucketSize = 100;
    var publishEvery = 10 * 1000;
    var timerExecuteCount = 0;
    var maxTimerExecutionCount = 10;
    return {
        initTimer: function () {
            var that = this;
            timerExecuteCount = 0;
            that.interval = $interval(function () {
                ++timerExecuteCount;
                that.publishBufferedMessages(that);
                if (timerExecuteCount >= maxTimerExecutionCount) {
                    that.clearTimer();
                }
            }, publishEvery);
        },
        clearTimer: function () {
            var that = this;
            if (that.interval) {
                $interval.cancel(that.interval);
                timerExecuteCount = 0;
                that.interval = null;
            }
        },
        publishBufferedMessages: function (that) {
            that.publishBuffersByChannelId(that, 'media');
            that.publishBuffersByChannelId(that, 'trendingphrases');
            that.publishBuffersByChannelId(that, 'subtopics');
            that.publishBuffersByChannelId(that, 'influencers');
            that.publishBuffersByChannelId(that, 'sentiment');
            that.publishBuffersByChannelId(that, 'geolocations');
        },
        publishBuffersByChannelId: function (that, channelId) {
            if (that.listeners[channelId] && this.buffer[channelId].length) {

                //$log.debug('Channel: '+channelId+' listeners count: '+that.listeners[channelId].length+' this.buffer[channelId].count: '+this.buffer[channelId].length);

                for (var i = 0; i < that.listeners[channelId].length; i++) {
                    var hndlObj = that.listeners[channelId][i];
                    if (that.buffer[channelId][hndlObj.identifier]) {
                        var items = that.buffer[channelId][hndlObj.identifier].items;
                        if (items && items.length > 0) {

                            //$log.debug('Channel: '+channelId+' hndlObj.identifier: '+hndlObj.identifier+' items count: '+items.length);

                            var sorted = hndlObj.sortBufferFn(this.buffer[channelId][hndlObj.identifier].items);
                            hndlObj.fn(sorted.slice(0, hndlObj.bucketSize + 1));
                        }
                    }
                }
            }
        },
        getListeners: function () {
            return this.listeners;
        },
        addListener: function (identifier, channelId, handler, bucketSize, sortBufferFn) {

            if (!this.listeners) this.listeners = {};

            if (!this.listeners[channelId])
                this.listeners[channelId] = [];

            var hnObj = {
                identifier: identifier,
                fn: handler,
                bucketSize: bucketSize ? bucketSize : defaultBucketSize,
                sortBufferFn: sortBufferFn ? sortBufferFn : this.defaultSortBuffer
            }
            this.listeners[channelId].push(hnObj);

            this.bufferInit(channelId);
        },
        removeListeners: function (channelId) {
            if (this.listeners && this.listeners[channelId]) {
                delete this.listeners[channelId];
            }
        },
        addRawMessageListener: function (handler) {
            if (!this.rawListeners) this.rawListeners = [];
            this.rawListeners.push(handler);
        },
        getRawMessageListeners: function () {
            return this.rawListeners;
        },
        clearListeners: function () {
            this.listeners = [];
        },
        clearChannels: function () {
            this.subscribedChannels = [];
            this.lastTimeMessageReceived = 0;
            this.buffer = {};
            this.clearTimer();
        },
        unsubscribeChannels: function (streamId) {
            var that = this;
            that.unsubscribeChannel('media-' + streamId);
            that.unsubscribeChannel('trendingphrases-' + streamId);
            that.unsubscribeChannel('subtopics-' + streamId);
            that.unsubscribeChannel('influencers-' + streamId);
            that.unsubscribeChannel('sentiment-' + streamId);
            that.unsubscribeChannel('geolocations-' + streamId);

            that.clearTimer();
        },
        handleWsConnection: function () {
            var that = this;

            var callbacks = {
                OnOpen: function (aEvent) {
                    $log.debug('WS opened: ' + JSON.stringify(aEvent));
                    that.clearChannels();
                },
                // OnMessage callback
                OnMessage: function (aEvent, aToken) {
                    //  console.log('On Message: ' + JSON.stringify(aEvent) + ' aToken: ' + JSON.stringify(aToken));
                    that.handlOnWSMessage(that, aEvent, aToken);
                },
                OnError: function (aEvent, aToken) {
                    $log.debug('WS OnError: ' + JSON.stringify(aEvent) + ', aToken: ' + aToken);
                },
                OnClose: function (aEvent) {
                    $log.debug('WS OnClose: ' + JSON.stringify(aEvent));
                    that.clearChannels();
                }
            };

            ngJws.connect({}, callbacks);
        },
        close: function () {
            if (ngJws.baseSocket)
                ngJws.baseSocket.close();
        },
        isConnected: function () {
            return ngJws.baseSocket ? ngJws.baseSocket.isConnected() : false;
        },
        subscribeToChannels: function (streamId) {
            var that = this;
            setTimeout(function () {
                that.subscribeToChannel('media-' + streamId);
                that.subscribeToChannel('trendingphrases-' + streamId);
                that.subscribeToChannel('subtopics-' + streamId);
                that.subscribeToChannel('influencers-' + streamId);
                that.subscribeToChannel('sentiment-' + streamId);
                that.subscribeToChannel('geolocations-' + streamId);

                that.bufferInit('media');
                that.bufferInit('trendingphrases');
                that.bufferInit('subtopics');
                that.bufferInit('influencers');
                that.bufferInit('sentiment');
                that.bufferInit('geolocations');
                that.initTimer();
            }, 1000);
        },
        unsubscribeChannel: function (channelName) {
            ngJws.baseSocket.channelUnsubscribe(channelName, "1");
        },
        subscribeToChannel: function (channelName) {
            //First create channel in subscribedList and set incrementer to 0
            if (this.subscribedChannels[channelName] == undefined) {
                this.subscribedChannels[channelName] = 0;
            }

            //If there are less than 3 attempts of trying to subscribe, try to subscribe again
            if (this.subscribedChannels[channelName] < 3) {
                this.subscribedChannels[channelName] = this.subscribedChannels[channelName] + 1;
                ngJws.baseSocket.channelSubscribe(channelName, "1");
            }
        },
        defaultSortBuffer: function (bufferList) {
            var that = this;
            return bufferList;
        },
        bufferInit: function (channelId) {
            var that = this;
            if (!channelId) return;

            if (!this.buffer) this.buffer = {};
            if (!this.buffer[channelId]) this.buffer[channelId] = {};

            if (that.listeners[channelId]) {
                for (var i = 0; i < that.listeners[channelId].length; i++) {
                    var hndlObj = that.listeners[channelId][i];
                    if (!this.buffer[channelId][hndlObj.identifier]) {
                        this.buffer[channelId][hndlObj.identifier] = {};
                    }
                    if (!this.buffer[channelId][hndlObj.identifier].items) {
                        this.buffer[channelId][hndlObj.identifier].items = [];
                    }
                }
            }
        },
        bufferAndEmit: function (that, streamId, channelId, message) {
            var the = this;
            $log.debug('buffering streamId: ' + streamId + ' channelId: ' + channelId);
            if (that.listeners[channelId]) {

                for (var i = 0; i < that.listeners[channelId].length; i++) {
                    var hndlObj = that.listeners[channelId][i];
                    var identifier = hndlObj.identifier;

                    var indexOf = this.buffer[channelId][identifier].items.indexOf(message);
                    $log.debug('buffering streamId: ' + streamId + ' channelId: ' + channelId + ' identifier: ' + identifier + ', exist new message: ' + indexOf);
                    if (indexOf === -1) {
                        if (!this.buffer[channelId].count) this.buffer[channelId].count = 0;
                        ++this.buffer[channelId].count;
                        this.buffer[channelId][identifier].items.unshift(message);
                        this.buffer[channelId][identifier].items = hndlObj.sortBufferFn(this.buffer[channelId][identifier].items);
                        if (this.buffer[channelId][identifier].items.length >= maxBucketSize) {
                            this.buffer[channelId][identifier].items.pop();
                        }
                    }
                    /*
                                        var sorted = hndlObj.sortBufferFn(this.buffer[channelId][identifier].items);

                                        if (this.buffer[channelId].count >= hndlObj.bucketSize) {
                                            // hndlObj.fn(sorted.slice(0, hndlObj.bucketSize+1));
                                        }*/
                    if (this.buffer[channelId].count >= hndlObj.bucketSize) {
                        if (!hndlObj.isFiredFirstTime) {
                            hndlObj.fn(sorted.slice(0, hndlObj.bucketSize + 1));
                            hndlObj.isFiredFirstTime = true;
                        }
                    }
                }
            } else {
                $log.debug('There is no listeners for channelId: ' + channelId);
            }
        },
        getLastTimeMessageReceived: function () {
            return this.getLastTimeMessageReceived;
        },
        handlOnWSMessage: function (that, aEvent, aToken) {
            /*
             * Handle data response from the socket source channels.
             */
            //   var that = this;
            if (aToken.type == 'data') {
                //previousTimeMessageReceived = lastTimeMessageReceived;
                ++that.lastTimeMessageReceived;
                if (that.lastTimeMessageReceived === 1) {
                    /*if ($('.progress-bar').hasClass('display-none')) {
                        $('.progress-bar').removeClass('display-none');
                    }*/
                    //setCheckReceivingMesage();
                }
                var obj = JSON.parse(aEvent.data); //jQuery.parseJSON(aEvent.data);
                var channelId = obj.channelId;
                channelId = channelId.split("-");

                var rawListeners = that.getRawMessageListeners();
                if (rawListeners && rawListeners.length > 0) {
                    for (var i = 0; i < rawListeners.length; i++) {
                        rawListeners[i].handler(obj);
                    }
                }

                /*if (that.listeners[channelId[0]]){
                    for (var i=0;i<that.listeners[channelId[0]].length;i++){
                        var handler = that.listeners[channelId[0]][i];
                        handler(obj.data);
                    }
                    //that.listeners[channelId[0]].handler(obj.data);
                }*/
                var chnlId = channelId[0];
                channelId.shift();
                var streamId = channelId.join('-');

                that.bufferAndEmit(that, streamId, chnlId, JSON.parse(obj.data));

            } else if (aToken.type == "response") {
                /*
                 * Handle failed subscribe attempt and try to reestablish
                 * subscription to the specific channel
                 */
                var messageObj = JSON.parse(aEvent.data); //jQuery.parseJSON(aEvent.data);
                if (messageObj != null && messageObj.code == -1 && messageObj.reqType == "subscribe") {
                    var channelName = messageObj.msg.match(/'(.*?)'/);
                    //Try to resubscribe if first subscribe atempt failed
                    that.subscribeToChannel(channelName[1]);
                }
            }
        }
    }
}]);



// 어디

searchModule.factory('compileStreamService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            compileStream: function (q, customerId, userId, language) {
                /*
                http://xdsocial.com:8080/olyfeWS/stream/compile?q=web+socket&runWebSocket=true&useStorm=true&customerId=098f6bcd-4621-3373-8ade-4e832627b4f6&customerAccountId=f7a86af5-20dd-3ec2-9cbe-2e32905697f3&lang=en&callback=compileStreamCallback&_=1441735339527
                */
                var runWebSocket = false; // this will be false for now
                var queryString = host + 'stream/compile?q=' + encodeURIComponent(q).split(' ').join('+');
                queryString += '&useStorm=true&runWebSocket=' + runWebSocket + '&customerId=' + customerId + '&customerAccountId=' + userId + '&lang=' + language;

                return $http.get(queryString);
            }
        }
    }
]);




searchModule.factory('searchStremService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            getRecent: function (filter, from, limit) {
                var fl = '';
                fl = prepareFilterQueryString(filter);
                
                var queryString = host + 'search/stream/all?from=' + from + '&type='+'REGULAR'+'&limit=' + limit + fl;
                return $http.get(queryString);
            }
        }
    }
]);


searchModule.factory('locationMessagesService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {

            getFilteredMessages: function (streamId, limit, filterObject, lastRowObject) {
                var fl = '',
                    lr = '';
                fl = prepareFilterQueryString(filterObject);
                lr = prepareFilterQueryString(lastRowObject, 'lastRow');

                var url = host + 'v2_1/geo/filter?streamId=' + streamId + '&limit=' + limit + fl + lr;
                return $http.get(url);
            }

            /*
             getFilteredMessages: function (q, streamId, lang, limit, countries) {
                var cc = '';
                if (countries && countries.length) {
                    for (var i = 0; i < countries.length; i++) {
                        cc += '&countries[]=' + countries[i];
                    }
                } else {
                    cc = '&countries[]=US&countries[]=KR';
                }
                if (!limit) limit = 10;

                return $http.get(host + 'tagged/geo/filter?q=' + q + '&streamId=' + streamId + '&limit=' + limit + cc);
            },
            getMessages: function (from, limit, streamId) {
                return $http.get(host + 'tagged/geo/filter?from=' + from + '&limit=' + limit + '&streamId=' + streamId + '&countries[]=US&countries[]=KR');
            },
            getFilteredAggregatedMessages: function (from, limit, streamId, filter) {
                var fl = '&filter=' + encodeURIComponent(JSON.stringify(filter));
                //fl = prepareFilterQueryString(filter);
                //console.log('filter: ' + JSON.stringify(filter));
                // '&countries[]=US&countries[]=KR'

                return $http.get(host + 'tagged/geo/filter/group?from=' + from + '&limit=' + limit + '&streamId=' + streamId + fl);
            }
            */
        }
}]);

searchModule.factory('sentimentMessagesService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            /* get: function (q, streamId, lang) {
                 return $http.get(host + 'v2/sentiment/search?q=' + q + '&streamId=' + streamId + '&lang=' + lang);
             },*/
            getFilteredMessages: function (streamId, filter) {
                var fl = '';
                fl = prepareFilterQueryString(filter);
                return $http.get(host + '/v2_1/sentiment/filter?streamId=' + streamId + fl);
            },
            sentmentUpdate: function (obj) {
             var url = host + 'v2_1/sentiment/update';
                return $http.post(url, obj);
            }
        };
}]);

searchModule.factory('contributorsMessagesService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();

        return {
            /*getFilteredMessages: function (q, streamId, filter) {
                var fl = '';
                fl = prepareFilterQueryString(filter);
                return $http.get(host + 'tagged/influencers/filter?q=' + q + '&streamId=' + streamId + fl);
            }*/
            getFilteredMessages: function (streamId, limit, filterObject, lastRowObject) {
                var fl = '',
                    lr = '';
                fl = prepareFilterQueryString(filterObject);
                lr = prepareFilterQueryString(lastRowObject, 'lastRow');

                var url = host + 'v2_1/influencers/filter?streamId=' + streamId + '&limit=' + limit + fl + lr;
                return $http.get(url);
            }
        }
}]);

searchModule.factory('phrasesMessagesService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            /* getMessage: function (streamId, messageId) {
                 return $http.get(host + 'text/phrases/message?streamId=' + streamId + '&messageId=' + messageId);
             },
             getAllMessages: function (from, limit, filter, sort) {
                 var fl = '';
                 fl = prepareFilterQueryString(filter);
                 return $http.get(host + 'text/phrases/all?from=' + from + '&limit=' + limit + fl);
             },*/
            getFilteredMessages: function (streamId, limit, filterObject, lastRowObject) {
                var fl = '',
                    lr = '';
                    
                fl = prepareFilterQueryString(filterObject);
                lr = prepareFilterQueryString(lastRowObject, 'lastRow');

                var url = host + 'v2_1/text/phrases/filter?streamId=' + streamId + '&limit=' + limit + fl + lr;
                return $http.get(url);
            }
        }
}]);

searchModule.factory('spamMessagesService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            markAsSpam: function (streamId, searchKeyword, phraseItem) {
                var obj = {
                    authorId: phraseItem['USER_ID'],
                    authorName: phraseItem['USER_NAME'],
                    text: phraseItem['TEXT'],
                    searchKeyword: searchKeyword,
                    itemId: phraseItem['MESSAGE_ID'],
                    source: phraseItem['SOURCE'],
                    streamId: streamId
                };
                var url = host + 'v2_1/spam/add';
                return $http.post(url, obj);
            }
        }
}]);

searchModule.factory('mediaMessagesService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        // http://prod1.gobpu.com:8080/olyfeWS/media/search?q=social&limit=20&type=photo
        var host = serviceConfig.getHost();
        return {
            /* getMessages: function (from, limit, keyword, streamId) {
                 var fl = '';
                 //fl = prepareFilterQueryString(filter);
                 return $http.get(host + 'media/search?q=' + encodeURIComponent(keyword) + '&from=' + from + '&limit=' + limit + fl);
             },*/
            /* getFilteredMessages: function (from, limit, keyword, filter) {
                 var fl = '';
                 fl = prepareFilterQueryString(filter);
                 return $http.get(host + 'media/filter?' + (keyword ? ('q=' + encodeURIComponent(keyword) + '&') : '') + 'from=' + from + '&limit=' + limit + fl);
             }*/

            getFilteredMessages: function (streamId, limit, filterObject, lastRowObject) {
                var fl = '',
                    lr = '';
                fl = prepareFilterQueryString(filterObject);
                lr = prepareFilterQueryString(lastRowObject, 'lastRow');

                var url = host + 'v2_1/media/filter?streamId=' + streamId + '&limit=' + limit + fl + lr;
                return $http.get(url);
            }
        }
}]);

searchModule.factory('topicsMessagesService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            /*getMessage: function (streamId, messageId) {
                return $http.get(host + 'text/topics/message?streamId=' + streamId + '&messageId=' + messageId);
            },
            getAllMessages: function (from, limit, filter, sort) {
                var fl = '';
                fl = prepareFilterQueryString(filter);
                return $http.get(host + 'text/topics/all?from=' + from + '&limit=' + limit + fl);
            },
            getFilteredMessages: function (keyword, streamId, from, limit, filter, sort) {
                var fl = '';
                fl = prepareFilterQueryString(filter);
                return $http.get(host + 'text/topics/filter?q='+keyword+'&streamId='+streamId+'&from=' + from + '&limit=' + limit + fl);
            }*/

            getFilteredMessages: function (streamId, limit, filterObject, lastRowObject) {
                var fl = '',
                    lr = '';
                fl = prepareFilterQueryString(filterObject);
                lr = prepareFilterQueryString(lastRowObject, 'lastRow');

                var url = host + 'v2_1/text/topics/filter?streamId=' + streamId + '&limit=' + limit + fl + lr;
                return $http.get(url);
            }
        }
}]);

searchModule.factory('rawMessagesService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            getMessage: function (streamId, messageId) {
                return $http.get(host + 'raw/message?streamId=' + streamId + '&messageId=' + messageId);
            },
            getFiteredMessages: function (from, limit, filter, sort) {
                var fl = '';
                fl = prepareFilterQueryString(filter);
                return $http.get(host + 'raw/filter?from=' + from + '&limit=' + limit + fl);
            }
        }
}]);



searchModule.factory('taggedMessagesService', ['$http', '$log', 'serviceConfig',
    function ($http, $log, serviceConfig) {
        var host = serviceConfig.getHost();
        var prepareFilterString = function (filter) {
            var flt = '';
            //console.log("filter: " + filter);

            if (filter && filter.indexOf(':') > 0) {
                var pos = filter.indexOf(':');
                flt += '&' + filter.substring(0, pos) + '=' + encodeURIComponent(filter.substring(pos + 1, filter.length));
            }
            return flt;
        };

        var prepareSortString = function (sort) {

        };
        return {
            getMessage: function (streamId, messageId) {
                return $http.get(host + 'tagged/message?streamId=' + streamId + '&messageId=' + messageId);
            },
            getMessages: function () {
                //return the promise directly.
                return $http.get(host + 'tagged/all');
            },
            getPagedMessages: function (from, limit, filter, sort) {
                //return the promise directly.
                var fl = prepareFilterString(filter);
                // var so = prepareSortString(sort);
                return $http.get(host + 'tagged/all?from=' + from + '&limit=' + limit + fl);
            },
            getFiteredMessages: function (from, limit, filter, sort) {
                var fl = '';
                fl = prepareFilterQueryString(filter);
                $log.debug('filter: ' + JSON.stringify(filter));
                $log.debug('fl: ' + fl);

                return $http.get(host + 'tagged/all?from=' + from + '&limit=' + limit + fl);
            },
            getStrictFiteredMessages: function (from, limit, filter) {
                if (!filter) throw new Error('filter is required');
                return $http.get(host + 'tagged/all?from=' + from + '&limit=' + limit + '&filter=' + filter);
            }
        }
}]);

searchModule.factory('limitService', [
    function () {
        return {
            setPhrasesLimit: function () {
                this.phraseLimit = 6;
                this.phraseLastCnt = 0;
            },
            getPhraseDetails: function () {
                var returnObj = {
                    "limit": this.phraseLimit,
                    "phraseLastCnt": this.phraseLastCnt
                };
                return returnObj;
            },
            setContributorsLimit: function () {
                this.contributorsLimit = 6;
                this.contributorsLastCnt = 0;
            },
            getContributorsDetails: function () {
                var returnObj = {
                    "limit": this.contributorsLimit,
                    "contributorsLastCnt": this.contributorsLastCnt
                };
                return returnObj;
            },
            setMediaLimit: function () {
                this.mediaLimit = 6;
                this.mediaLastCnt = 0;
            },
            getMediaDetails: function () {
                var returnObj = {
                    "limit": this.mediaLimit,
                    "mediaLastCnt": this.mediaLastCnt
                };
                return returnObj;
            },
            getRange: function () {
                this.range = 6;
                return this.range;
            }
        }
    }
]);
