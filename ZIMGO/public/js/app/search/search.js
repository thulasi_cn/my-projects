var searchModule = angular.module('searchModule', ['ui.router']);

searchModule.directive('mediaList', function () {
    return {
        scope: {
            listModel: '='
        },
        template:   '<ul class="media-list list-unstyled">'+
                        '<li class="media-list-item" ng-repeat="mediaItem in listModel" >'+
                            '<div class="media-list-item-container">'+
                                '<img data-ng-src="{{ getPreparedUrl(mediaItem.imgUrl) }} " alt="" onerror="this.style.display = \'none\'; this.src=\'img/errorImg.png\';" />'+
                            '</div>'+
                        '</li>' +
                    '</ul>',
        controller: ['$scope','serviceConfig', function($scope, serviceConfig){
             $scope.getPreparedUrl = function (bigImageUrl) {
                var hostPart = serviceConfig.getMediaResolveHost() + '?h=250&w=324&src=';
                var encodedPart = encodeURIComponent(bigImageUrl);
                var url = hostPart + encodedPart;
                return url;
            };
        }]
    };
});


searchModule.config(
  ['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

      $stateProvider
      .state('main.base.content', {
          abstract: true,
          url: 'main',
          views: {
              'content': {
                  templateUrl: '/js/app/main/templates/content/main.base.content.view.html',
                  controller: 'SearchContentResultBaseCtrl'
              }
          }
      })
      .state('main.base.content.main', {
          url: '',
          abstract: true,
          views: {
              'navbar@main': {
                  templateUrl: '/js/app/navigation/templates/search.navigation.view.html',
                  controller: 'MainNavigationCtrl'
              }
              ,'search@main': {
                  // templateUrl: '/js/app/search/templates/content/main.content.navigation.items.view.html',
                  controller: 'MainContentSearchNavigationItemsCtrl'
              }
              // ,
              //   'leftcontainer@main':{
              //    templateUrl: '/js/app/search/templates/content/search.trending.topics.float.view.html'
              // }
          }

      })
      .state('main.base.content.main.main', {
          url: '',
          views: {
              'main@main.base.content': {
                  templateUrl: '/js/app/search/templates/content/results/search.result.main.view.html',
                  controller: 'SearchContentResultMainCtrl'
              }
          },
          resolve: {
              activeRoute: ['commonSearchService', function(commonSearchService){
                  var activeRoute = 'main';
                  commonSearchService.setActiveRoute(activeRoute);
                  return activeRoute;
              }]
          }
      })
      .state('main.base.content.main.trendingphrases', {
          url: '/phrases',
          views: {
              'main@main.base.content': {
                  templateUrl: '/js/app/search/templates/content/results/phrases/search.result.trendingphrases.view.html',
                  controller: 'SearchPhrasesContentResultPhrasesCtrl'
              }
          },
          resolve: {
              activeRoute: ['commonSearchService', function(commonSearchService){
                  var activeRoute = 'phrases';
                  commonSearchService.setActiveRoute(activeRoute);
                  return activeRoute;
              }]
          }
      })
      .state('main.base.content.main.contributors', {
          url: '/contributors',
          views: {
              'main@main.base.content': {
                  templateUrl: '/js/app/search/templates/content/results/contributors/search.result.contributors.view.html',
                  controller: 'SearchContributorsContentResultContributorsCtrl'
              }
          },
          resolve: {
              activeRoute: ['commonSearchService', function(commonSearchService){
                  var activeRoute = 'contributors';
                  commonSearchService.setActiveRoute(activeRoute);
                  return activeRoute;
              }]
          }
      })
      .state('main.base.content.main.media', {
          url: '/media',
          views: {
              'main@main.base.content': {
                  templateUrl: '/js/app/search/templates/content/results/media/search.result.media.view.html',
                  controller: 'SearchMediaContentResultMediaCtrl'
              }
          },
          resolve: {
              activeRoute: ['commonSearchService', function(commonSearchService){
                  var activeRoute = 'media';
                  commonSearchService.setActiveRoute(activeRoute);
                  return activeRoute;
              }]
          }
      })
      .state('main.base.landingbase', {
          abstract: true,
          url: '',
          views: {
              'content': {
                  templateUrl: '/js/app/main/templates/main.landing.base.view.html'
              },
              'search@main': {
                  templateUrl: '/js/app/search/templates/landing/landing.search.view.html',
                  controller: 'MainNavigationCtrl'
              },
              'navbar@main': {
                  templateUrl: '/js/app/navigation/templates/landing.navigation.view.html',
                  controller: 'MainNavigationCtrl'
              }
          }
      })
      .state('main.base.landingbase.main', {
          url: '',
          views: {
              '': {
                  templateUrl: '/js/app/main/templates/main.view.html',
                  controller: 'MainCtrl'
              },
              'cloud': {
                  templateUrl: '/js/app/search/templates/landing/cloud.view.html',
                  controller: 'MainCloudCtrl'
              },
              'results': {
                  templateUrl: '/js/app/search/templates/landing/results.view.html',
                  controller: 'MainSearchResultCtrl'
              },
              'mainads': {
                  templateUrl: '/js/app/as/templates/main.as.view.html',
                  controller: 'MainAdsCtrl'
              },
              'leftads': {
                  templateUrl: '/js/app/as/templates/left.as.view.html'
              },
              'rightads': {
                  templateUrl: '/js/app/as/templates/right.as.view.html'
              }
          }
      });


    }
  ]
);
