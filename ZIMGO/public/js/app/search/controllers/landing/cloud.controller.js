var app = angular.module('mainModule');

app.controller('MainCloudCtrl', ['$scope', '$log', '$rootScope', 'commonSearchService', 'searchStremService', 'languageService' ,'topicsMessagesService','webSocketStateService',
    function ($scope, $log, $rootScope, commonSearchService, searchStremService, languageService, topicsMessagesService,webSocketStateService) {

        $scope.prvClicked = undefined;

        /*$scope.words = [
          {text: "Lorem", weight: 13 },
          {text: "Ipsum", weight: 10.5, handlers:{ 'click': function(item, i){ $scope.onCloudItemClick(item, i); }} },
          {text: "Dolor", weight: 9.4, html: {title: "Dolor, 5%"}},
          {text: "Sit", weight: 8},
          {text: "Amet", weight: 6.2},
          {text: "Consectetur", weight: 5},
          {text: "Adipiscing", weight: 5},
          {text: "Elit", weight: 5},
          {text: "Nam et", weight: 5},
          {text: "Leo", weight: 4},
          {text: "Sapien", weight: 4},
          {text: "Pellentesque", weight: 3},
          {text: "habitant", weight: 3},
          {text: "morbi", weight: 3},
          {text: "tristisque", weight: 3},
          {text: "senectus", weight: 3},
          {text: "et netus", weight: 3},
          {text: "et malesuada", weight: 3},
          {text: "fames", weight: 2},
          {text: "ac turpis", weight: 2},
          {text: "egestas", weight: 2},
          {text: "Aenean", weight: 2},
          {text: "vestibulum", weight: 2},
          {text: "elit", weight: 2},
          {text: "sit amet", weight: 2},
          {text: "metus", weight: 2},
          {text: "adipiscing", weight: 2},
          {text: "ut ultrices", weight: 2}
        ];*/
    
        // $scope.colors = ["#800026", "#bd0026", "#e31a1c", "#fc4e2a", "#fd8d3c", "#feb24c", "#fed976"];
        $scope.colors = ["#383f49", "#697185", "#aeb2be"];
        $scope.font_wt = ["25px", "14px", "12px"]
        $scope.onCloudItemClick = function(item, i){
           //   alert(item.currentTarget.textContent);
        };
        
        $scope.$watch(function () {
            return commonSearchService.getCurrent();
        }, function (newValue, oldValue) {
            //alert('hey, myVar has changed!');
            $log.debug('running getcurrent for cloud controller: newValue: '+newValue+' oldValue: '+oldValue);
            if (newValue && newValue.streamId && newValue.streamId !== (oldValue && oldValue.streamId?oldValue.streamId:null)) {
                $scope.run(newValue);
                $log.debug('landing Cloud newValue: ' + JSON.stringify(newValue));
            }
        });
        
        $scope.run = function(item){
            $scope.search = item;
            $scope.selectedLocation = {};
            
           // var filter = { streamId: $scope.search.streamId, keyword: $scope.search.query};
            searchStremService.getRecent({}, 0, 20)
            .success(function(result){
              if (result && result.list && result.list.length > 0) {
                  var res = [];
                    for (var i=0;i<result.list.length;i++){
                        var w = {
                            text: result.list[i].keyword,
                            stream_id: result.list[i].streamId,
                            weight: i,
                            handlers:{ 'click': function(item, i){ $scope.onCloudItemClick(item, i); }},
                            active: false
                        }
                        res.push(w);
                    }
                  $scope.words = res;
                  if($scope.prvClicked !== undefined){
                    $scope.words[$scope.prvClicked].active = true;
                  }

                  $scope.showPopular();
              }
            });
            /*topicsMessagesService.getFilteredMessages($scope.search.streamId, 30, null, null)
            .success(function (result) {
                if (result && result.list && result.list.length > 0) {
                    var res = [];
                    for (var i=0;i<result.list.length;i++){
                        var w = {
                            text: result.list[i].TEXT_ITEM,
                            stream_id: result.list[i].STREAM_ID,
                            weight: result.list[i].SCORE,
                            handlers:{ 'click': function(item, i){ $scope.onCloudItemClick(item, i); }} 
                        }
                        res.push(w);
                    }
                    $scope.words = res;
                    $scope.showPopular();
                }
            })
            .error(function (data, status) {
            });*/
        };
        
        $scope.showPopular = function(){
          /*$scope.words.sort(function(a, b) { 
              return (b.weight - a.weight); 
          });*/

          for(var i = 0; i< $scope.words.length; i++){
            if(i === 0){
              $scope.words[i].color = $scope.colors[0];
              $scope.words[i].font_wt = $scope.font_wt[0];
            }else if(i % 2 !== 0){
              $scope.words[i].color = $scope.colors[1];
              $scope.words[i].font_wt = $scope.font_wt[1];
            }else{
              $scope.words[i].color = $scope.colors[2];
              $scope.words[i].font_wt = $scope.font_wt[2];
            }
          }
        };

        $scope.handleWSData = function (wsData) {
            $log.debug('cloud subtopics ws data: ' + JSON.stringify(wsData));
        };
       // webSocketStateService.addListener('subtopics', $scope.handleWSData);
        webSocketStateService.addListener('cloud.subtopics','subtopics', $scope.handleWSData, 30);

        $scope.getStyle = function(model){
          return { "color" : model.color,
                    "font-size" : model.font_wt
                };
        };

        $scope.setPopularIssues = function(obj, index){
          $scope.prvClicked = index;
          commonSearchService.setCurrent({
            "query": obj.text,
            "streamId": obj.stream_id,
            "customerId": commonSearchService.getCurrent().customerId,
            "accountId": commonSearchService.getCurrent().accountId,
            "lang": languageService.getCurrent().id
          });
        };
        
}]);