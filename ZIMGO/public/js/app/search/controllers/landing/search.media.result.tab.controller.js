var app = angular.module('searchModule');

app.controller('MediaSearchResultCtrl', ['$scope', '$rootScope', '$state', 'mediaMessagesService', '$sce', '$modal',
                                         'serviceConfig',
    function ($scope, $rootScope, $state, mediaMessagesService, $sce, $modal, serviceConfig) {

        //$scope.mediaResolveHost = 'http://xdsocial.com/'; //'http://prod1.gobpu.com/'

        $scope.getPreparedUrl = function (bigImageUrl) {
            var hostPart = serviceConfig.getMediaResolveHost() + '?h=209&w=378&src=';
            var encodedPart = encodeURIComponent(bigImageUrl);
            var url = hostPart + encodedPart;
            return url;
        };
        
        $scope.getPreviewUrl = function (bigImageUrl) {
            var hostPart = serviceConfig.getMediaResolveHost() + '?h=209&w=378&src=';
            var encodedPart = encodeURIComponent(bigImageUrl);
            var url = hostPart + encodedPart;
            return url;
        };

        $scope.getVideoPreparedUrl = function (videoUrl) {
            return $sce.trustAsResourceUrl(videoUrl);
        };

        $scope.resolvePreviewTemplate = function (itemType) {
            var base = '/js/app/common/templates/';
            if (itemType) {
                if (itemType === "trendingphrases") {
                    return base + 'footer.item.trendingphrases.profile.view.html';
                }
                if (itemType === "contributor") {
                    return base + 'footer.item.contributors.profile.view.html';
                }
                if (itemType.indexOf('VIDEO') > -1) {
                    return base + 'video.player.modal.view.html';
                }
                if (itemType.indexOf('PHOTO') > -1) {
                    return base + 'photo.preview.modal.view.html';
                }

            }
            return null;
        };

        $scope.playVideo = function (videoObj, itemType) {
            // console.log('clicked preview: ' + videoObj);
            var templateUrl = $scope.resolvePreviewTemplate(itemType);
            var modalInstance = $modal.open({
                templateUrl: templateUrl, //'js/app/common/templates/video.player.modal.view.html',
                controller: 'PlayVideoModalController',
                size: 'lg',
                resolve: {
                    url: function () {
                        if (itemType.indexOf('VIDEO') > -1) {
                            return (videoObj.VIDEO_URL.replace("https://www.youtube.com/v/", "https://www.youtube.com/embed/"))+"?autoplay=1";
                        }else if (itemType.indexOf('PHOTO') > -1){
                            return videoObj;
                        }
                        
                    },
                    item: function () {
                        return videoObj;
                    },
                    itemType: function () {
                        return itemType;
                    },
                    context: function () {
                        return 'landing';
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.getSourceImgUrl = function (source) {
            var url = 'img/social/plain/small/ID.png';
            var sourceId = "twitter";
            switch (source) {
                case 'FB':
                    sourceId = 'facebook';
                    break;
                case 'YT':
                    sourceId = 'youtube';
                    break;
                case 'FL':
                    sourceId = 'flickr';
                    break;
                case 'BL':
                    sourceId = 'blog';
                    break;
                default:
                    sourceId = "twitter";
                    break;
            }
            return url.split('ID').join(sourceId);

        };

    }]);
