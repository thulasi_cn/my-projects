var app = angular.module('searchModule');

app.controller('MainSearchResultCtrl', ['$scope', '$rootScope', '$state', 'mediaMessagesService', '$sce', '$modal', 'activeTabService', 'searchStremService', 'commonSearchService', 'viewStateService', '$interval', 'socialService', '$timeout', 'serviceConfig',
    function ($scope, $rootScope, $state, mediaMessagesService, $sce, $modal, activeTabService, searchStremService, commonSearchService, viewStateService, $interval, socialService, $timeout, serviceConfig) {

        $scope.run = function (item) {
            if (item === $scope.search || item.streamId === ($scope.search && $scope.search.streamId ? $scope.search.streamId : null)) {
                return;
            }
            $scope.search = item;
            // fo search and filter the data
            $scope.filterData($scope.handleResults);
        };

        $scope.$watch(function () {
            return commonSearchService.getCurrent();
        }, function (newValue, oldValue) {
            //alert('hey, myVar has changed!');
            //  console.log('running getcurrent for landing controller: newValue: ' + newValue + ' oldValue: ' + oldValue + ' $scope.search: ' + $scope.search);
            if (newValue && newValue.streamId && newValue.streamId !== (oldValue && oldValue.streamId ? oldValue.streamId : null)) {
                //if (newValue!==$scope.search && newValue.streamId!==($scope.search.streamId$scope.search.streamId){
                $scope.run(newValue);
                //  console.log('landing result newValue: ' + JSON.stringify(newValue));
                //}
            }
        });

        $scope.$watch(function () {
            return commonSearchService.getReload();
        }, function (newValue, oldValue) {
            if (newValue && newValue.shouldReload) {
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.$watchCollection(function () {
            return socialService.getFilter();
        }, function (newValue, oldValue) {
            if (newValue !== oldValue) { //newValue.source===$rootScope.$state.current.name) {
                //$scope.run(commonSearchService.getCurrent());
                $scope.filterTabs();
            }
        });

        $scope.errorImages = [];
        var stopCheckErrorImages;
        $scope.handleImageLoadError = function (card) {
            //  alert(card);
            $scope.errorImages.push(card);
            if (!angular.isDefined(stopCheckErrorImages)) {
                stopCheckErrorImages = $interval(function () {
                    $scope.removeFromExistingImages($scope.errorImages);
                    $scope.errorImages = [];
                    stopCheckErrorImages = undefined;
                }, 200);
            }
        };

        $scope.removeFromExistingImages = function (toBeRemoved) {

            var result = [],
                found;
            var a = $scope.mediaMessages,
                b = toBeRemoved;
            for (var i = 0; i < a.length; i++) {
                found = false;
                // find a[i] in b
                for (var j = 0; j < b.length; j++) {
                    if (a[i].referenceId === b[j].referenceId) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    result.push(a[i]);
                }
            }
            $scope.mediaMessages = result;
        };


        $rootScope.mainClass = function () {
            return 'landing';
        };

        $scope.filterTabs = function () {
            var sns = socialService.getFilter();

            angular.forEach($scope.tabs, function (tab) {
                var sn = _.find(sns, {
                    name: tab.id
                });
                if (!sn) {
                    tab.visible = false;
                } else {
                    tab.visible = true;
                }
            });

            angular.forEach($scope.tabs, function (v, k) {
                v.active = false;
            });
            //  $timeout(function () {
            //    $scope.tabs[0].active = true;
            //   }, 10);

            // $timeout(function () {
            var visibleTabs = _.find($scope.tabs, {
                visible: true
            });
            if (visibleTabs) {
                $scope.onClickTab(visibleTabs);
            } else {
                angular.forEach($scope.tabs, function (v, k) {
                    v.visible = true;
                });
                $scope.onClickTab($scope.tabs[0]);
            }
            // }, 10);

        };

        /*  $scope.active = {
              facebook:true
          };*/

        //  $scope.isActive = true;

        $scope.tabs = [
            {
                id: 'facebook',
                small: 'FB',
                title: 'Facebook',
                image: 'facebook.png',
                src: '/js/app/search/templates/landing/media.result.view.html',
                active: false,
                visible: true
            }, {
                id: 'twitter',
                small: 'TW',
                title: 'Twitter',
                image: 'twitter.png',
                src: '/js/app/search/templates/landing/media.result.view.html',
                active: false,
                visible: true
            },
            {
                id: 'flickr',
                small: 'FL',
                title: 'Flickr',
                image: 'flickr.png',
                src: '/js/app/search/templates/landing/media.result.view.html',
                active: false,
                visible: true
            },
            {
                id: 'youtube',
                small: 'YT',
                title: 'YouTube',
                image: 'youtube.png',
                src: '/js/app/search/templates/landing/media.result.view.html',
                active: false,
                visible: true
            },
            {
                id: 'blog',
                small: 'BL',
                title: 'Naver blog',
                image: 'blog.png',
                src: '/js/app/search/templates/landing/media.result.view.html',
                active: false,
                visible: true
            }];



        /* $scope.$watch('activeTab', function (newValue, oldValue) {
             //alert('hey, myVar has changed!');
             if (newValue !== oldValue) {
                 activeTabService.set(newValue);
                 $scope.filterData($scope.handleResults);
                 //$scope.$broadcast('onCurrentTabChanged', newValue);
             }
         });*/

        // $scope.activeTab = $scope.tabs[0];



        $scope.limit = 9;
        $scope.search = null;

        $scope.handleResults = function (result) {
            $scope.errorImages = [];
            setTimeout(function () {
                $scope.$apply(function () {
                    //  console.log('tw res: '+JSON.stringify(result));
                    if (result && result.list) {
                        $scope.mediaMessages = result.list;
                    } else {
                        $scope.mediaMessages = [];
                    }

                });
            }, 10);
        };

        $scope.filterData = function (callback) {
            $scope.search = commonSearchService.getCurrent();
            if (!$scope.search) return;

            var keyword = $scope.search.query;
            var sourceType = $scope.activeTab[$scope.activeTab.id].small;
            $scope.from = 0;
            var filter = {
                source: sourceType
            };
//  getFilteredMessages: function (streamId, limit, filterObject, lastRowObject) {
            $rootScope.shwLoad = true;
            mediaMessagesService.getFilteredMessages($scope.search.streamId, $scope.limit, filter, null)
                .success(function (result) {
                    $rootScope.shwLoad = false;
                    callback(result);
                });
        };

        $scope.onClickTab = function (tab) {

            /*var activeTab = _.find($scope.tabs, {
                active: true
            });*/
            if ($scope.activeTab && $scope.activeTab.id) {
                // var s = angular.element('#' + $scope.activeTab.id).scope().$parent;
             //   s.$destroy();
            }

            angular.forEach($scope.tabs, function (v, k) {
                v.active = false;
            });

            $scope.activeTab = {};
            tab.active = true;
            $scope.activeTab[tab.id] = tab;
            $scope.activeTab.id = tab.id;

            activeTabService.set($scope.activeTab);
            $scope.mediaMessages = [];
            //  $timeout(function(){
            $scope.filterData($scope.handleResults);
            //  },10);
            //commonSearchService.setReload({shouldReload:true, source: 'landing'});
        };

        $scope.activeTab = {};
        $scope.onClickTab($scope.tabs[0]);

        /*// card template related


        $scope.getPreparedUrl = function (bigImageUrl) {
            var hostPart = serviceConfig.getMediaResolveHost() + '?h=209&w=378&src=';
            var encodedPart = encodeURIComponent(bigImageUrl);
            var url = hostPart + encodedPart;
            return url;
        };

        $scope.getVideoPreparedUrl = function (videoUrl) {
            return $sce.trustAsResourceUrl(videoUrl);
        };

        $scope.playVideo = function (videoUrl) {
            console.log('clicked preview: ' + videoUrl);

            var modalInstance = $modal.open({
                templateUrl: 'js/app/common/templates/video.player.modal.view.html',
                controller: 'PlayVideoModalController',
                size: 'lg',
                resolve: {
                    url: function () {
                        return videoUrl;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.getSourceImgUrl = function (source) {
            var url = 'img/social/plain/small/ID.png';
            var sourceId = "twitter";
            switch (source) {
                case 'FB':
                    sourceId = 'facebook';
                    break;
                case 'YT':
                    sourceId = 'youtube';
                    break;
                case 'FL':
                    sourceId = 'flickr'
                    break;
                case 'BL':
                    sourceId = 'blog'
                    break;
                default:
                    sourceId = "twitter";
                    break;
            }
            return url.split('ID').join(sourceId);

        };*/
}]);
