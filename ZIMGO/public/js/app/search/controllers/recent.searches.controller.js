var app = angular.module('searchModule');
app.controller('RecentSearchesCtrl', ['$scope', '$log', '$rootScope', '$state', 'searchStremService','commonSearchService',
    function ($scope, $log, $rootScope, $state, searchStremService, commonSearchService) {

        $scope.selectedStream = null;
        
        $scope.loadRecentSearches = function () {
            $log.debug('run recent searches');
            searchStremService.getRecent({}, 0, 10)
                .success(function (result) {
                    if (result && result.list && result.list.length > 0) {
                        $scope.result = result.list;
                    }
                });

        }

        $scope.loadRecentSearches();

        $scope.selectStream = function(stream) {            
            commonSearchService.setDoSearch(stream);
            $scope.selectedStream = stream;
        }
}]);
