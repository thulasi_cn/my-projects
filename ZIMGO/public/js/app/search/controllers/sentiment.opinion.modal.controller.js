var searchModule = angular.module('searchModule');

searchModule.controller('SentimentOpinionController', ['$scope', '$state', '$modalInstance', 'taggedMessage', 'sentimentMessagesService', 'commonSearchService', '$timeout', function($scope, $state, $modalInstance, taggedMessage, sentimentMessagesService, commonSearchService, $timeout) {
    setTimeout(function() {
        $scope.$broadcast('reCalcViewDimensions');
    }, 50);
    $scope.sentimentData = taggedMessage;
    $scope.mood;
    $scope.close = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.priceSlider = {
        value: $scope.sentimentData.SENTIMENT_SCORE,
        options: {
            floor: 0,
            ceil: 100,
            step: 1,
            showSelectionBar: true,
            getSelectionBarColor: function(value) {
                if (value <= 30)
                    return 'red';
                if (value <= 60)
                    return 'orange';
                if (value <= 90)
                    return 'yellow';
                return '#2AE02A';
            }
        }
    };
    $scope.sentimentModelSbmt = function() {
        var sentimentObj = {
            "sentimentUpdate": {
                "sentimentScore": $scope.priceSlider.value,
                "mood": $scope.mood
            },
            "taggedMessage": $scope.sentimentData
        };
        sentimentMessagesService.sentmentUpdate(sentimentObj).then(function(res) {
            if (res) {
                var currentState = $state;
                commonSearchService.setReload({
                    shouldReload: true,
                    source: currentState.current.name
                });
                $scope.close();
            }

        }, function(error) {});
    };
}]);
