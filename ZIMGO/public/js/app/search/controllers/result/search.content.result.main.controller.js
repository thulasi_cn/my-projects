var app = angular.module('searchModule');

app.controller('SearchContentResultMainCtrl', ['$scope', '$rootScope', '$state',
    function ($scope, $rootScope, $state) {
        if (GOWEB_CONF.analyticsId) {
                GA('send', {
                  hitType: 'event',
                  eventCategory: 'searchPage',
                  eventAction: 'view',
                  eventLabel: 'searchPageView'
                });
              }
        $scope.selectedTab = "trending_phrase";
        $rootScope.disableSocialPart = true;
        $scope.loadTabTphrases = true;

        $scope.getSourceImgUrl = function(source){
            var url = 'img/social/plain/small/ID.png';
            var sourceId = "twitter";
            switch(source){
                case 'FB': 
                    sourceId = 'facebook';
                    break;
                case 'YT':
                    sourceId = 'youtube';
                    break;
                case 'FL':
                    sourceId = 'flickr'
                    break;
                case 'BL':
                    sourceId = 'blog'
                    break;
                default:
                    sourceId = "twitter";
                    break;
            }
            return url.split('ID').join(sourceId);            
        };

        $scope.toggleTab = function(type, $event){
             $scope.selectedTab = type;

             if($scope.selectedTab === "trending_phrase" || $scope.selectedTab === "opinion_leader"){
                $rootScope.disableSocialPart = true;
            }else{
                $rootScope.disableSocialPart = false;
            }

             $scope.loadTabOLeaders = ($rootScope.callIntervalElm.indexOf("opinion_leader") != -1 || $rootScope.callIntervalElm.indexOf("contributors") != -1) ? true : false;
             $scope.loadTabImedia   = ($rootScope.callIntervalElm.indexOf("image_video") != -1 || $rootScope.callIntervalElm.indexOf("media") != -1) ? true : false;
             $scope.loadTabTphrases = ($rootScope.callIntervalElm.indexOf("trending_phrase") != -1 || $rootScope.callIntervalElm.indexOf("phrases") != -1) ? true : false;

             $scope.loadTabOLeaders = ( $scope.loadTabOLeaders || $scope.selectedTab == "opinion_leader" ) ? true : $scope.loadTabOLeaders;
             $scope.loadTabImedia   = ( $scope.loadTabImedia || $scope.selectedTab == "image_video" ) ? true : $scope.loadTabImedia;
             $scope.loadTabTphrases = ( $scope.loadTabTphrases || $scope.selectedTab == "trending_phrase" ) ? true : $scope.loadTabTphrases;

        };

        $scope.$watch(function () {
            return $rootScope.callIntervalElm;
        }, function (newValue, oldValue) {
            var defaultTab = "trending_phrase";

            if ((newValue && oldValue) && newValue.length !== oldValue.length) {
                //$scope.clearQuery();
                 
                 $scope.loadTabOLeaders = (newValue.indexOf("opinion_leader") != -1 || newValue.indexOf("contributors") != -1) ? true : false;
                 $scope.loadTabImedia   = (newValue.indexOf("image_video") != -1 || newValue.indexOf("media") != -1) ? true : false;
                 $scope.loadTabTphrases = (newValue.indexOf("trending_phrase") != -1 || newValue.indexOf("phrases") != -1) ? true : false;

                 if(!newValue.length){
                     $scope.selectedTab = "trending_phrase";
                     $scope.loadTabOLeaders = ( $scope.loadTabOLeaders || $scope.selectedTab == "opinion_leader" ) ? true : $scope.loadTabOLeaders;
                     $scope.loadTabImedia   = ( $scope.loadTabImedia || $scope.selectedTab == "image_video" ) ? true : $scope.loadTabImedia;
                     $scope.loadTabTphrases = ( $scope.loadTabTphrases || $scope.selectedTab == "trending_phrase" ) ? true : $scope.loadTabTphrases;
                 }

            }

        });

        $scope.$on('fetch-stop', function(event, args) {
            $scope.selectedTab = args.q;
            
            $scope.callIntervalElmDebug = $rootScope.callIntervalElm;

        });
}]);
