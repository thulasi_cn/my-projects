var app = angular.module('searchModule');

app.controller('SearchTrendingTopicsCtrl', ['$scope', '$rootScope',
    '$state', 'topicsMessagesService', '$timeout',
    'commonSearchService', 'webSocketStateService', 'socialService', 'localStorageService',
    function($scope, $rootScope, $state, topicsMessagesService, $timeout, commonSearchService, webSocketStateService, socialService, localStorageService) {

        $scope.selectedItem = null;
        $scope.isShow = true;

        $scope.$watch(function() {
            
            return commonSearchService.getReload();
        }, function(newValue, oldValue) {
            if (newValue && newValue.shouldReload && (newValue.source.indexOf('main.base.content.main') > -1) && newValue.reloadSource !== 'topics') {
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.$watchCollection(function() {
            return socialService.getFilter();
        }, function(newValue, oldValue) {
            if (newValue !== oldValue) { //newValue.source===$rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });


        $scope.run = function(item) {
            if (!item) return;
            $scope.result = null;
            //      console.log('Loading Topics: '+item.query);
            $scope.search = item;

            var filter = {
                streamId: $scope.search.streamId,
                keyword: $scope.search.query
            };

            angular.extend(filter, item.filter ? item.filter : {});

            if (filter.subtopics) { // don't want to refresh ourself
                delete filter.subtopics;
            }

            if (socialService.getFilter().length > 0) {
                filter.sources = _.map(socialService.getFilter(), 'id');
            }

            topicsMessagesService.getFilteredMessages($scope.search.query, $scope.search.streamId, 0, 10, filter)
                .success(function(result) {
                    if (result && result.list && result.list.length > 0) {
                        $scope.result = result.list;
                    }
                })
                .error(function(data, status) {});
        };

        $scope.handleWSData = function(wsData) {
            //       console.log('subtopics ws data: ' + JSON.stringify(wsData));
        };
        webSocketStateService.addListener('main.subtopics', 'subtopics', $scope.handleWSData, 10);

        $scope.selectTopic = function(topic) {
            $scope.selectedItem = topic;
            commonSearchService.addCurrentFilter('subtopics', topic.textItem);
            commonSearchService.setReload({
                shouldReload: true,
                reloadSource: 'topics',
                source: 'main.base.content.main',
                state: 'filter'
            });
        };

        $scope.toggleRelatedTopicSlide = function() {
            if ($scope.isShow == true) {
                $scope.isShow = false;
            } else {
                $scope.isShow = true;
            }
        };
        
        // // Related Topics

        // $scope.relateList = angular.element(document.getElementById('relateLists'));

        // $scope.marginLeft = 0;

        // $scope.myStyle = {
        //     'margin-left': '0px'
        // };

        // //precheck
        // if ($scope.marginLeft === 0) {
        //     $scope.leftdisabled = true;
        // } else {
        //     $scope.leftdisabled = false;
        // }

        // if ($scope.marginLeft === -(($scope.relateList.find('li').length- 1) * 5)) {
        //     $scope.rightdisabled = true;
        // } else {
        //     $scope.rightdisabled = false;
        // }

        // //mover right
        // $scope.moveRight = function() {
        //     $scope.marginLeft = +$scope.myStyle['margin-left'].replace('px', '') + -5;

        //     if ($scope.marginLeft === 0) {
        //         $scope.leftdisabled = true;
        //     } else {
        //         $scope.leftdisabled = false;
        //     }

        //     if ($scope.marginLeft === -(($scope.relateList.find('li').length - 1) * 5)) {
        //         $scope.rightdisabled = true;
        //     } else {
        //         $scope.rightdisabled = false;
        //     }

        //     $scope['margin-left'] = $scope.marginLeft + 'px';
        //     return $scope['margin-left'];
        // };

        // //move left
        // $scope.moveLeft = function() {
        //     $scope.marginLeft = +$scope.myStyle['margin-left'].replace('px', '') + 5;

        //     if ($scope.marginLeft === 0) {
        //         $scope.leftdisabled = true;
        //     } else {
        //         $scope.leftdisabled = false;
        //     }

        //     if ($scope.marginLeft === -(($scope.relateList.find('li').length - 1) * 5)) {
        //         $scope.rightdisabled = true;
        //     } else {
        //         $scope.rightdisabled = false;
        //     }

        //     $scope['margin-left'] = $scope.marginLeft + 'px';
        //     return $scope['margin-left'];
        // };
    }
]);
