var app = angular.module('searchModule');

app.controller('SearchContentResultBaseCtrl', ['$scope', '$rootScope', '$state', '$timeout',
    function ($scope, $rootScope, $state, $timeout) {

        $rootScope.mainClass = function () {
            return 'main-search';
        }

        $scope.checkUser = function () {
            $timeout(function () {
                if (!$rootScope.user) {
                    $state.go('main.base.landingbase.main');
                }
            }, 100);
        };

        $scope.checkUser();

}]);
