var app = angular.module('searchModule');

app.controller('SearchMediaContentResultMediaCtrl', ['$scope', '$log', '$rootScope', '$state',
                                                    'mediaMessagesService', '$sce', '$timeout',
                                                    'commonSearchService', '$modal', 'webSocketStateService',
                                                    'serviceConfig','socialService',
    function ($scope, $log, $rootScope, $state, mediaMessagesService, $sce, $timeout, commonSearchService, $modal, webSocketStateService, serviceConfig, socialService) {

        $scope.$watch(function () {
            return commonSearchService.getReload();
        }, function (newValue, oldValue) {
            if (newValue && newValue.shouldReload && newValue.source === $rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.$watchCollection(function () {
            return socialService.getFilter();
        }, function (newValue, oldValue) {
            if (newValue!==oldValue) { //newValue.source===$rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.pagingData = {
            currentPage: -1,
            from: 0,
            limit: 9,
            busy: false,
            disabled: false
        };

        $scope.clearPagingData = function () {
            $scope.pagingData = {
                currentPage: -1,
                from: 0,
                limit: 9,
                busy: false,
                disabled: false
            };
        };

        $scope.run = function (item) {
            if (!item) return;

            $scope.clearPagingData();
            $scope.result = null;
            $scope.search = item;            
            $scope.loadMore();
        };

        $scope.getNextPage = function () {
            $scope.pagingData.currentPage += 1;
            $scope.pagingData.from = $scope.pagingData.currentPage * $scope.pagingData.limit;
        };

        $scope.loadPagedData = function () {
            $scope.pagingData.busy = true;
            var filter = {
                customerId: $scope.search.customerId,
                customerAccountId: $scope.search.accountId
            };
            
            if (socialService.getFilter().length>0){
                filter.sources = _.map(socialService.getFilter(), 'name');
            }
            
            mediaMessagesService.getFilteredMessages($scope.pagingData.from, $scope.pagingData.limit, $scope.search.query, filter)
                .success(function (result) {
                    if (result && result.list && result.list.length > 0) {
                        $scope.result = $scope.result ? $scope.result.concat(result.list) : result.list;
                        $scope.pagingData.busy = false;
                    } else {
                        $scope.pagingData.disabled = true;
                    }

                })
                .error(function (data, status) {});
        };

        $scope.loadMore = function () {
            if (!$scope.search) return;
            $scope.getNextPage();
            $scope.loadPagedData();
        };

        $scope.getPreparedUrl = function (bigImageUrl) {
            var hostPart = serviceConfig.getMediaResolveHost() + '?h=200&w=278&src=';
            var encodedPart = encodeURIComponent(bigImageUrl);
            var url = hostPart + encodedPart;
            return url;
        };

        $scope.getVideoPreparedUrl = function (videoUrl) {
            return $sce.trustAsResourceUrl(videoUrl);
        };

        $scope.playVideo = function (videoUrl) {
            $log.debug('clicked preview: ' + videoUrl);

            var modalInstance = $modal.open({
                templateUrl: 'js/app/common/templates/video.player.modal.view.html',
                controller: 'PlayVideoModalController',
                size: 'lg',
                resolve: {
                    url: function () {
                        return videoUrl;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.getSourceImgUrl = function (source) {
            var url = 'img/social/plain/small/ID.png';
            var sourceId = "twitter";
            switch (source) {
                case 'FB':
                    sourceId = 'facebook';
                    break;
                case 'YT':
                    sourceId = 'youtube';
                    break;
                case 'FL':
                    sourceId = 'flickr'
                    break;
                case 'BL':
                    sourceId = 'blog'
                    break;
                default:
                    sourceId = "twitter";
                    break;
            }
            return url.split('ID').join(sourceId);
        };
}]);
