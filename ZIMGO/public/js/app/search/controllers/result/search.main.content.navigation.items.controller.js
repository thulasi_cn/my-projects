var app = angular.module('searchModule');

app.controller('MainContentSearchNavigationItemsCtrl', ['$scope', '$rootScope', '$state','activeTabService','searchStremService','commonSearchService',
    function ($scope, $rootScope, $state, activeTabService, searchStremService,commonSearchService) {
                  
        $scope.reloadCurrent = function(sourcePage){
         /*   var state = $rootScope.$state;
            var currentState = $state;*/
            commonSearchService.setReload({shouldReload: true, source: sourcePage});
            $scope.selected = sourcePage;
        };
        
        $scope.selected = 'main.base.content.main.main';
        
        $scope.$watch(function () {
            return commonSearchService.getReload();
        }, function (newValue, oldValue) {
            if (newValue && newValue.shouldReload && newValue.source==='main.navigation' && newValue!==oldValue) {
                var state = $rootScope.$state;
                var currentState = $state;
                commonSearchService.setReload({shouldReload: true, source: currentState.current.name});
            }
        });
        
        $scope.searchItem = null;
        $scope.$watch(function () {
            return commonSearchService.getCurrent();
        }, function (newValue, oldValue) {
             if (newValue && newValue.streamId && newValue.streamId !== (oldValue && oldValue.streamId?oldValue.streamId:null)){
                var state = $rootScope.$state;
                var currentState = $state;
                commonSearchService.setReload({shouldReload: true, source: currentState.current.name});
            }
        });       
        
}]);
