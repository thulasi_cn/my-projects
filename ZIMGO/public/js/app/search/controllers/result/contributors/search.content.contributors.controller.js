var app = angular.module('searchModule');

app.controller('SearchContributorsContentResultContributorsCtrl', ['$scope', '$log', '$rootScope', '$state',
                                                    'contributorsMessagesService', '$sce', '$timeout',
                                                    'commonSearchService', '$modal', 'webSocketStateService',
                                                    'serviceConfig', 'activeRoute', 'socialService',
    function ($scope, $log, $rootScope, $state, contributorsMessagesService, $sce, $timeout, commonSearchService,
               $modal, webSocketStateService, serviceConfig, activeRoute, socialService) {

        $log.debug('full contributor activeRoute: ' + activeRoute);

        $scope.$watch(function () {
            return commonSearchService.getReload();
        }, function (newValue, oldValue) {
            if (newValue && newValue.shouldReload && newValue.source === $rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });
        
        $scope.$watchCollection(function () {
            return socialService.getFilter();
        }, function (newValue, oldValue) {
            if (newValue!==oldValue) { //newValue.source===$rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.pagingData = {
            currentPage: -1,
            from: 0,
            limit: 12,
            busy: false,
            disabled: false
        }

        $scope.clearPagingData = function () {
            $scope.pagingData = {
                currentPage: -1,
                from: 0,
                limit: 12,
                busy: false,
                disabled: false
            }
        };

        $scope.run = function (item) {
            if (!item) return;
            
            $scope.clearPagingData();
            $scope.result = null;
            $scope.search = item;

            $scope.loadMore();
        };

        $scope.getNextPage = function () {
            $scope.pagingData.currentPage += 1;
            $scope.pagingData.from = $scope.pagingData.currentPage * $scope.pagingData.limit;
        };

        $scope.loadPagedData = function () {
            $scope.pagingData.busy = true;
            var filter = {
                from: $scope.pagingData.from,
                limit: $scope.pagingData.limit,
                lang: $scope.search.lang
            };
            
            if (socialService.getFilter().length>0){
               filter.sources = _.map(socialService.getFilter(), 'id');
            }
            
            contributorsMessagesService.getFilteredMessages($scope.search.query, $scope.search.streamId, filter)
                .success(function (result) {
                    if (result && result.list && result.list.length > 0) {
                        $scope.result = $scope.result ? $scope.result.concat(result.list) : result.list;
                        $scope.pagingData.busy = false;
                    } else {
                        $scope.pagingData.disabled = true;
                    }

                })
                .error(function (data, status) {});
        };

        $scope.loadMore = function () {
            if (!$scope.search) return;
            $scope.getNextPage();
            $scope.loadPagedData();
        };

        /*     $scope.run = function(item){
               $scope.result = null;
               console.log('Loading Contributors: '+item.query);
               $scope.search = item;

               var filter = {
                   from: 0,
                   limit: 12,
                   lang: $scope.search.lang
               };
                contributorsMessagesService.getFilteredMessages($scope.search.query, $scope.search.streamId, filter)
                       .success(function (result) {
                           if (result && result.list && result.list.length > 0) {
                               $scope.result = result.list;
                           }
                       })
                       .error(function (data, status) {

                       });
           };*/

        $scope.getSourceImgUrl = function (source) {
            var url = 'img/social/plain/small/ID.png';
            var sourceId = "twitter";
            switch (source) {
                case 'FB':
                    sourceId = 'facebook';
                    break;
                case 'YT':
                    sourceId = 'youtube';
                    break;
                case 'FL':
                    sourceId = 'flickr'
                    break;
                case 'BL':
                    sourceId = 'blog'
                    break;
                default:
                    sourceId = "twitter";
                    break;
            }
            return url.split('ID').join(sourceId);
        }
    }
]);
