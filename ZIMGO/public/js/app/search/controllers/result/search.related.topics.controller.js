var app = angular.module('navigationModule');


app.controller('SearchRelatedTopicsCtrl', ['$scope', '$controller', '$rootScope', '$state', 'topicsMessagesService', 'UsersService', '$modal', '$translate', 'ngJws', '$location',
    'compileStreamService', 'amMoment', 'searchStremService', 'commonSearchService',
    '$timeout', 'viewStateService', 'webSocketStateService', 'stackSearchStreamsService',
    'languageService', '$interval', 'socialService', 'mediaMessagesService', 'topicFilterService',
    function($scope, $controller, $rootScope, $state, topicsMessagesService, UsersService, $modal, $translate, ngJws,
        $location, compileStreamService, amMoment, searchStremService, commonSearchService,
        $timeout, viewStateService, webSocketStateService, stackSearchStreamsService, languageService, $interval, socialService, mediaMessagesService, topicFilterService) {

        // $controller('MainNavigationCtrl', {$scope: $scope}); //This works
        $scope.startSlick = function() {
            $scope.slickConfig = {
                dots: false,
                autoplay: false,
                infinite: false,
                speed: 300,
                // slidesToShow: 8,
                method: {},
                touchMove: false,
                initOnload: true,
                data: $scope.result,
                enabled: true,
                mobileFirst: true,
                variableWidth: true,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1
                        }
                    }, {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }, {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            };
        };
        $scope.startSlick();
        $scope.slickConfig.enabled = false;

        $scope.selectedItem = null;
        $scope.isShow = true;

        $scope.$watch(function() {
            return commonSearchService.getReload();
        }, function(newValue, oldValue) {
            // debugger;
            if (newValue && newValue.shouldReload && (newValue.source.indexOf('main.base.content.main') > -1) && newValue.reloadSource !== 'topics') {
                if (topicFilterService.getCurrentSentimentFilter() === undefined && topicFilterService.getCurrentTopicFilter() === undefined) {
                    $scope.run(commonSearchService.getCurrent());
                }
            }
        });

        $scope.$watchCollection(function() {
            return socialService.getFilter();
        }, function(newValue, oldValue) {
            if (newValue !== oldValue) { //newValue.source===$rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });


        $scope.run = function(item) {
            $scope.slickConfig.enabled = false;

            if (angular.element('#relatedTopicSlider').hasClass('slick-slider')) {
                angular.element('#relatedTopicSlider').slick('unslick');
            }

            if (!item) return;
            if ($rootScope.checkHoldResult) {
                $scope.result = null;
            }
            //      console.log('Loading Topics: '+item.query);
            $scope.search = item;


            var filter = {}; // we don't filter topics

            //  angular.extend(filter, item.filter ? item.filter : {});


            /* if (filter.subtopics) { // don't want to refresh ourself
                 delete filter.subtopics;
             }*/

            /*if (socialService.getFilter().length > 0) {
                filter.source = _.map(socialService.getFilter(), 'id'); // there is no filter by source
            }*/

            var lastRow = null; // 
            $scope.result = null;
            topicsMessagesService.getFilteredMessages($scope.search.streamId, 20, filter, lastRow)
                .success(function(result) {
                    if (result && result.list && result.list.length > 0) {
                        $scope.result = result.list;
                        var liWd = angular.element(document.getElementById('relateLists')).find("li:first").outerWidth(),
                            len = $scope.result.length,
                            divWd = angular.element(document.getElementById('relateLists')).outerWidth();
                        if ((liWd * len) < divWd) {
                            $scope.rightdisabled = true;
                        }

                        $timeout(function() {
                            $scope.slickConfig.enabled = true;
                            $scope.startSlick();
                            $rootScope.$broadcast('load-tour');
                        }, 1000);
                    }
                })
                .error(function(data, status) {});
        };

        $scope.handleWSData = function(wsData) {
            //       console.log('subtopics ws data: ' + JSON.stringify(wsData));
        };
        webSocketStateService.addListener('main.subtopics', 'subtopics', $scope.handleWSData, 10);

        $scope.selectTopic = function(topic) {
            $scope.selectedItem = topic;
            commonSearchService.addCurrentFilter('subtopics', topic.textItem);
            commonSearchService.setReload({
                shouldReload: true,
                reloadSource: 'topics',
                source: 'main.base.content.main',
                state: 'filter'
            });
        };

        $scope.toggleRelatedTopicSlide = function() {
            if ($scope.isShow == true) {
                $scope.isShow = false;
            } else {
                $scope.isShow = true;
            }
        };



        // Related Topics

        $scope.relateList = angular.element(document.getElementById('relateLists'));

        $scope.marginLeft = 0;

        $scope.myStyle = {
            'margin-left': '0px'
        };

        //precheck
        if ($scope.marginLeft === 0) {
            $scope.leftdisabled = true;
        } else {
            $scope.leftdisabled = false;
        }

        if ($scope.marginLeft === -(($scope.relateList.find('li').length - 1) * 105)) {
            $scope.rightdisabled = true;
        } else {
            $scope.rightdisabled = false;
        }

        //mover right
        $scope.moveRight = function() {
            if (!$scope.rightdisabled) {
                $scope.marginLeft = +$scope.myStyle['margin-left'].replace('px', '') + -105;

                if ($scope.marginLeft === 0) {
                    $scope.leftdisabled = true;
                } else {
                    $scope.leftdisabled = false;
                }

                if ($scope.marginLeft === -(($scope.relateList.find('li').length - 1) * 105)) {
                    $scope.rightdisabled = true;
                } else {
                    $scope.rightdisabled = false;
                }

                $scope['margin-left'] = $scope.marginLeft + 'px';
                $scope.myStyle['margin-left'] = $scope['margin-left'];
                return $scope['margin-left'];
            }
        };

        //move left
        $scope.moveLeft = function() {
            if (!$scope.leftdisabled) {
                $scope.marginLeft = +$scope.myStyle['margin-left'].replace('px', '') + 105;

                if ($scope.marginLeft === 0) {
                    $scope.leftdisabled = true;
                } else {
                    $scope.leftdisabled = false;
                }

                if ($scope.marginLeft === -(($scope.relateList.find('li').length - 1) * 105)) {
                    $scope.rightdisabled = true;
                } else {
                    $scope.rightdisabled = false;
                }

                $scope['margin-left'] = $scope.marginLeft + 'px';
                $scope.myStyle['margin-left'] = $scope['margin-left'];
                return $scope['margin-left'];
            }
        };

        $scope.refreshSearchResult = function(val) {

            //alert(val);
            // $scope.search.filter = {topic:''};

            $scope.search.query = val;
            if (GOWEB_CONF.analyticsId) {
                GA('send', {
                    hitType: 'event',
                    eventCategory: 'searchPage',
                    eventAction: 'relatedKeyWord',
                    eventLabel: 'searchPageRelatedKeyWord'
                });
            }
            commonSearchService.setSearchFilter({ "score": undefined, "topic": undefined, "reload": true });
            topicFilterService.setCurrentTopicFilter(undefined);
            topicFilterService.setCurrentSentimentFilter(undefined);
            commonSearchService.setCurrent($scope.search);

            // $scope.search.query = val;
            // $scope.mainDoSearch(true);
        };
        $scope.relFilt = '';
        $scope.filtered = true;
        $scope.relatedFilter = function(val) {
            $scope.filtered = false;
            $scope.relFilt = val;
            topicFilterService.setCurrentTopicFilter(val);
            commonSearchService.setSearchFilter({ "topic": val, "score": topicFilterService.getCurrentSentimentFilter(), "reload": true });
        };
        $scope.clearTopicFilter = function() {
            $scope.filtered = true;
            $scope.relFilt = '';
            topicFilterService.setCurrentTopicFilter(undefined);
            commonSearchService.setSearchFilter({ "topic": undefined, "score": topicFilterService.getCurrentSentimentFilter(), "reload": true });
        };
    }
]);
