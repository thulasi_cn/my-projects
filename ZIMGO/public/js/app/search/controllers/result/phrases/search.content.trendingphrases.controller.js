var app = angular.module('searchModule');

app.controller('SearchPhrasesContentResultPhrasesCtrl', ['$scope', '$log', '$rootScope', '$state',
                                                    'phrasesMessagesService', '$sce', '$timeout',
                                                    'commonSearchService', '$modal', 'webSocketStateService',
                                                    'serviceConfig', 'socialService',
    function ($scope, $log, $rootScope, $state, phrasesMessagesService, $sce, $timeout, commonSearchService, $modal, webSocketStateService, serviceConfig, socialService) {

        $log.debug('trending phrase in full route: activeRoute: ' + commonSearchService.getActiveRoute());

        $scope.$watch(function () {
            return commonSearchService.getReload();
        }, function (newValue, oldValue) {
            if (newValue && newValue.shouldReload && newValue.source === $rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });
        
        $scope.$watchCollection(function () {
            return socialService.getFilter();
        }, function (newValue, oldValue) {
            if (newValue!==oldValue) { //newValue.source===$rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.pagingData = {
            currentPage: -1,
            from: 0,
            limit: 12,
            busy: false,
            disabled: false
        };

        $scope.clearPagingData = function () {
            $scope.pagingData = {
                currentPage: -1,
                from: 0,
                limit: 12,
                busy: false,
                disabled: false
            };
        };

        $scope.run = function (item) {
            if (!item) return;
            $scope.result = null;
            
            $scope.search = item;
            $scope.clearPagingData();
            $scope.loadMore();
        };

        $scope.getNextPage = function () {
            $scope.pagingData.currentPage += 1;
            $scope.pagingData.from = $scope.pagingData.currentPage * $scope.pagingData.limit;
        };

        $scope.loadPagedData = function () {
            $scope.pagingData.busy = true;
            var filter = {
                keyword: $scope.search.query,
                streamId: $scope.search.streamId,
                lang: $scope.search.lang
            };
            
            if (socialService.getFilter().length>0){
                filter.sources = _.map(socialService.getFilter(), 'id');
            }
            
            phrasesMessagesService.getFilteredMessages($scope.search.query, $scope.search.streamId, $scope.pagingData.from, $scope.pagingData.limit, filter)
                .success(function (result) {
                    if (result && result.list && result.list.length > 0) {
                        $scope.result = $scope.result ? $scope.result.concat(result.list) : result.list;
                        $scope.pagingData.busy = false;
                    } else {
                        $scope.pagingData.disabled = true;
                    }

                })
                .error(function (data, status) {});
        };

        $scope.loadMore = function () {
            if (!$scope.search) return;
            $scope.getNextPage();
            $scope.loadPagedData();
        };

        $scope.getSourceImgUrl = function (source) {
            var url = 'img/social/plain/small/ID.png';
            var sourceId = "twitter";
            switch (source) {
                case 'FB':
                    sourceId = 'facebook';
                    break;
                case 'YT':
                    sourceId = 'youtube';
                    break;
                case 'FL':
                    sourceId = 'flickr'
                    break;
                case 'BL':
                    sourceId = 'blog'
                    break;
                default:
                    sourceId = "twitter";
                    break;
            }
            return url.split('ID').join(sourceId);
        }

    }
]);
