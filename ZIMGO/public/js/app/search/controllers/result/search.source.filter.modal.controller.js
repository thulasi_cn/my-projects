var app = angular.module('searchModule');

app.controller('SearchSourceFilterModalCtrl', ['$scope', '$timeout','$modal','$state','$modalInstance',
    function ($scope, $timeout, $modal, $state,$modalInstance) {
        
        $scope.selectSourceFilter = function(item){
            $modalInstance.close(item);    
        }
        
    }]);