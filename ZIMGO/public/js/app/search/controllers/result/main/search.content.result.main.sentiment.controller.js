var app = angular.module('searchModule');

app.controller('SearchMainContentResultSentimentCtrl', ['$scope', '$rootScope', '$filter',
    '$state', 'sentimentMessagesService', '$timeout',
    'commonSearchService', 'webSocketStateService', 'socialService', 'topicFilterService',
    function($scope, $rootScope, $filter, $state, sentimentMessagesService, $timeout, commonSearchService, webSocketStateService, socialService, topicFilterService) {

        commonSearchService.setSearchFilter({ "score": undefined, "reload": false });
        // $scope.checkedPositive = false;
        // $scope.checkedNegative = false;
        // $scope.checkedNeutral = false;
        // $scope.neutralShow = false;

        $scope.$watch(function() {
            return commonSearchService.getReload();
        }, function(newValue, oldValue) {
            if (newValue && newValue.shouldReload && commonSearchService.getActiveRoute() === 'main') { //&& newValue.source === $rootScope.$state.current.name) {
                if (topicFilterService.getCurrentSentimentFilter() === undefined && topicFilterService.getCurrentTopicFilter() === undefined) {
                    $scope.checkedPositive = false;
                    $scope.checkedNegative = false;
                    $scope.checkedNeutral = false;
                    $scope.neutralShow = false;
                    $scope.run(commonSearchService.getCurrent());
                    // console.log('Reload sentiment newValue: ' + JSON.stringify(newValue));
                }
            }
        });

        $scope.$watchCollection(function() {
            return socialService.getFilter();
        }, function(newValue, oldValue) {
            if (newValue !== oldValue) { //newValue.source===$rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.run = function(item) {
            if (!item) return;

            $scope.result = 0;
            $scope.search = item;

            /*if (item.filter){
                $scope.filtered(item);
            }else{
                $scope.regular(item);
            }*/

            $scope.filtered(item);
        };

        $scope.filtered = function(item) {
            var filter = {};
            angular.extend(filter, item.filter ? item.filter : {});

            if (socialService.getFilter().length > 0) {
                filter.source = _.map(socialService.getFilter(), 'id');
            }


            sentimentMessagesService.getFilteredMessages($scope.search.streamId, filter)
                .success(function(result) {
                    if (result && result.SCORE) {
                        $scope.result = result.SCORE;
                        $scope.negative = 100 - $filter('number')($scope.result, 0);
                        $scope.positive = $filter('number')($scope.result, 0)
                            //  console.log('negative: '+$scope.negative+' positive: '+$scope.positive);
                    }
                })
                .error(function(data, status) {

                });
        };

        /*$scope.regular = function(item){
            
            sentimentMessagesService.get($scope.search.query, $scope.search.streamId, item.language)
                .success(function (result) {
                    if (result && result.list && result.list.length > 0) {
                        $scope.result = result.list[0];
                        $scope.negative = 100- $filter('number')($scope.result.score,0);
                        $scope.positive = $filter('number')($scope.result.score,0)
                   }
                })
                .error(function (data, status) {

                });
        };*/



        // $scope.result = {
        //     score: 0
        // };

        $scope.handleWSData = function(wsData) {};

        $scope.filterSentiment = function(val) {
            topicFilterService.setCurrentSentimentFilter(val);
            if (!val) {
                $scope.neutralShow = false;
            } else {
                $scope.neutralShow = true;
            }
            $scope.toggleSentimentBtn(val);

            commonSearchService.setSearchFilter({ "score": val, "topic": topicFilterService.getCurrentTopicFilter(), "reload": true });


        };

        $scope.toggleSentimentBtn = function(val) {
            if (val === 'positive') {
                $scope.checkedPositive = !$scope.checkedPositive;
                $scope.checkedNegative = false;
                $scope.checkedNeutral = false;
            } else if (val === 'negative') {
                $scope.checkedNegative = !$scope.checkedNegative;
                $scope.checkedPositive = false;
                $scope.checkedNeutral = false;
            } else if (val === 'neutral') {
                $scope.checkedNeutral = !$scope.checkedNeutral;
                $scope.checkedPositive = false;
                $scope.checkedNegative = false;
            } else {
                $scope.checkedPositive = false;
                $scope.checkedNegative = false;
                $scope.checkedNeutral = false;
            }
        };
        webSocketStateService.addListener('main.sentiment', 'sentiment', $scope.handleWSData, 1);
    }
]);
