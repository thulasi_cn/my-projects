var app = angular.module('searchModule');

app.controller('SearchMainContentResultContributorsCtrl', ['$scope', '$rootScope', '$state', '$modal',
                                                           'contributorsMessagesService', '$timeout', 'commonSearchService',
                                                           'webSocketStateService', 'socialService', 'limitService','$document', '$window',
    function ($scope, $rootScope, $state, $modal, contributorsMessagesService, $timeout, commonSearchService, webSocketStateService, socialService, limitService, $document, $window) {

        $scope.initPagingData = function () {
            $scope.pagingData = {
                from: 0,
                limit: limitService.getRange(),
                total: null,
                isCompleted: false
            };
            $scope.result = [];
        }
        $scope.initPagingData();

        $scope.$watch(function () {
            return commonSearchService.getReload();
        }, function (newValue, oldValue) {
            if (newValue && newValue.shouldReload && commonSearchService.getActiveRoute() === 'main') { //newValue.source===$rootScope.$state.current.name) {
                    if ($rootScope.callIntervalElm.indexOf("opinion_leader") === -1) {
                    // $scope.scrollDown = false;
                    var getLimit = limitService.getContributorsDetails();
                    $scope.pageLimit = getLimit.limit;
                    $scope.lastCnt = getLimit.contributorsLastCnt;
                    
                }                
                $scope.initPagingData();
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.$watchCollection(function () {
            return socialService.getFilter();
        }, function (newValue, oldValue) {
            if (newValue !== oldValue) { //newValue.source===$rootScope.$state.current.name) {
                // $scope.scrollDown = false;
                var getLimit = limitService.getContributorsDetails();
                $scope.pageLimit = getLimit.limit;

                $scope.initPagingData();

                $scope.lastCnt = getLimit.contributorsLastCnt;
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.updateLimit = function () {
            //  $scope.result = []; // we should append data while paging not getting whole list each time
            $scope.pageLimit = $scope.pageLimit + limitService.getRange();
            // $scope.scrollDown = true;
            $scope.run(commonSearchService.getCurrent());
        };

        angular.element($document).on('scroll', function(){
            if($scope.result.length > 0){
                if((angular.element($document).height() - angular.element($window).height()) === angular.element($window).scrollTop()){
                    $scope.updateLimit();
                }
            }
        });

        $scope.handleResult = function (result) {

            var diff = _.differenceBy(result.list, $scope.result, 'USER_ID');
            if (diff && diff.length > 0) {
                _.forEach(result.list, function (resItem) {
                    if (resItem.MESSAGE_JSON) {
                        resItem.MESSAGE_JSON = JSON.parse(resItem.MESSAGE_JSON); //$scope.getParsedJsonMedia(resItem.MESSAGE_JSON);
                    }
                });

                $scope.result = $scope.result.concat(result.list);
            }
            $scope.pagingData.lastRow = $scope.result[$scope.result.length - 1];
        };

        $scope.run = function (item) {
            if (!item) return;

            if ($rootScope.checkHoldResult) {
                // $scope.result =[]; //null; // comment out for testing by Ninel - need to understand why we need this
            }
            $scope.search = item;

            var filter = {};
            angular.extend(filter, item.filter ? item.filter : {});

            // quick and irty fix for influencers - we need to prefix filter with 'cu'
            if (filter){
                for (var prop in filter){
                    if (!_.startsWith(prop, 'cu.')){
                        filter['cu.'+prop] = filter[prop];
                        delete filter[prop];
                    }
                }
            }
            
            if (socialService.getFilter().length > 0) {
              //  filter['cu.source'] = _.map(socialService.getFilter(), 'id');
                  filter['cti.source'] = _.map(socialService.getFilter(), 'id');
            }
            
            
            
            var lastRow = {};
            if ($scope.pagingData.lastRow) {
                // lastRow['cu.SOURCE']= $scope.pagingData.lastRow.SOURCE;

                lastRow['cu.SCORE'] = $scope.pagingData.lastRow.SCORE;
                lastRow['cu.USER_ID'] = $scope.pagingData.lastRow.USER_ID;
            }

            if(!$rootScope.shwLoad){
                $rootScope.shwLoad = true;
            }

            contributorsMessagesService.getFilteredMessages($scope.search.streamId, $scope.pagingData.limit, filter, lastRow)
                .success(function (result) {
                    if($rootScope.checkHoldResult){
                        if($rootScope.shwLoad){
                            $rootScope.shwLoad = false;
                        }
                    }
                    if (result && result.list && result.list.length > 0) {
                        if ($rootScope.callIntervalElm.indexOf("opinion_leader") === -1) {
                            $rootScope.callIntervalElm.push("opinion_leader");
                            if($rootScope.shwLoad && $rootScope.callIntervalElm.length > 0){

                                $rootScope.shwLoad = false;
                                $rootScope.$broadcast('fetch-stop',{q:'opinion_leader'});
                            }
                        }

                        $scope.handleResult(result);

                        $scope.lastCnt = $scope.result.length;
                        /*if ($scope.scrollDown) {
                            $timeout(function () {
                                angular.element(document.body).animate({
                                    scrollTop: $(document).height()
                                }, 2000);
                            }, 1000);
                        }*/
                    }
                })
                .error(function (data, status) {

                });
        };

        $scope.handleWSData = function (wsData) {
            //      console.log('influencers ws data: ' + JSON.stringify(wsData));
            //   $scope.result = wsData;
        };
        // webSocketStateService.addListener('influencers', $scope.handleWSData);        
        webSocketStateService.addListener('main.influencers', 'influencers', $scope.handleWSData, 10);

        /*$scope.selectItem = function(sourceItem){
            $scope.isPopoverOpen= false;
        }
        $scope.openSourceFilter=function(){
            $scope.isPopoverOpen = !$scope.isPopoverOpen;
        }
        $scope.isPopoverOpen = false;*/

        // Open Item when clicked on the link on item footer
        /*$scope.openItem = function(item){
            // as it is included via data-ng-include we need to access it via $parent
            alert(item.item.userId);
        }*/
}]);
