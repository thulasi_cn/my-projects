var app = angular.module('searchModule');

app.controller('SearchMainContentResultTrendingphrasesCtrl', ['$scope', '$log', '$rootScope',
    '$state', 'phrasesMessagesService', '$timeout',
    'commonSearchService', 'webSocketStateService',
    'socialService', 'limitService', '$document', '$window', 'topicFilterService',
    function($scope, $log, $rootScope, $state, phrasesMessagesService, $timeout, commonSearchService, webSocketStateService,
        socialService, limitService, $document, $window, topicFilterService) {

        $log.debug('trending phrase in main route: activeRoute: ' + commonSearchService.getActiveRoute());

        $scope.initPagingData = function() {
            $scope.pagingData = {
                from: 0,
                limit: limitService.getRange(),
                total: null,
                isCompleted: false,
                lastRow: null
            };
            $scope.result = [];
        }
        $scope.initPagingData();

        $scope.$watch(function() {
            return commonSearchService.getReload();
        }, function(newValue, oldValue) {
            if (newValue && newValue.shouldReload && commonSearchService.getActiveRoute() === 'main') { //newValue.source === $rootScope.$state.current.name) {
                if ($rootScope.callIntervalElm.indexOf("trending_phrase") === -1) {
                    // $scope.scrollDown = false;
                    var getLimit = limitService.getPhraseDetails();
                    $scope.pageLimit = getLimit.limit;

                    $scope.lastCnt = getLimit.phraseLastCnt;
                    $log.debug("$scope.lastCnt:", $scope.lastCnt);
                }
                $scope.initPagingData();
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.$watchCollection(function() {
            return socialService.getFilter();
        }, function(newValue, oldValue) {
            if (newValue !== oldValue) { //newValue.source===$rootScope.$state.current.name) {
                // $scope.scrollDown = false;
                var getLimit = limitService.getPhraseDetails();
                $scope.pageLimit = getLimit.limit;

                $scope.initPagingData();

                $scope.lastCnt = getLimit.phraseLastCnt;
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.getParsedJsonMedia = function(mediaJsonStr) {
            var items = [];
            if (mediaJsonStr) {
                mediaJsonStr.forEach(function(i) {
                    var media = JSON.parse(i);
                    items.push(media);
                });
            }
            return items;
        };

        $scope.updateLimit = function() {
            // $scope.result = []; // we should append data while paging not getting whole list each time
            $scope.pageLimit = $scope.pageLimit + limitService.getRange();
            // $scope.scrollDown = true;
            $scope.run(commonSearchService.getCurrent());
        };

        angular.element($document).on('scroll', function() {
            if ($scope.result.length > 0) {
                if ((angular.element($document).height() - angular.element($window).height()) === angular.element($window).scrollTop()) {
                    //$scope.updateLimit();
                }
            }
        });

        $scope.removeDuplicatesFromNew = function(existingList, resultList) {
            var newList = [];
            if (existingList) {
                _.forEach(resultList, function(resItem) {
                    var found = _.find(existingList, function(item) {
                        return item['MESSAGE_ID'] === resItem['MESSAGE_ID'];
                    });
                    if (!found) {
                        newList.push(resItem);
                    }
                });
            } else {
                newList = resultList;
            }
            return newList;
        };


        $scope.handleResult = function(result) {

            var imCleanList = _.uniqBy(result.list, 'MESSAGE_ID');

            var newList = $scope.removeDuplicatesFromNew($scope.result, imCleanList);

            var diff = _.differenceBy(newList, $scope.result, 'MESSAGE_ID');
            if (diff && diff.length > 0) {
                if (newList) {
                    _.forEach(result.list, function(resItem) {
                        if (resItem['MEDIA_JSON']) {
                            resItem['MEDIA_JSON'] = $scope.getParsedJsonMedia(resItem['MEDIA_JSON']);
                        }
                    });
                }
                $scope.result = $scope.result.concat(newList);
            }
            $scope.pagingData.lastRow = $scope.result[$scope.result.length - 1];
        };

        $scope.run = function(item) {
            if (!item) return;

            // check if we are at the end of results
            /*if ($scope.pagingData.total && $scope.pagingData.from >= $scope.pagingData.total) {
                $scope.pagingData.isCompleted = true;
                return;
            }*/

            if (item.filter && item.filter.phrases) { // don't want to refresh ourself
                delete item.filter.phrases;
            }

            if ($rootScope.checkHoldResult) {
                // $scope.result =[]; //null; // comment out for testing by Ninel - need to understand why we need this
            }

            //  console.log('Loading TrendingPhrases: ' + item.query);
            $scope.search = item;

            //  stream_id: $scope.search.streamId
            var filter = {};
            angular.extend(filter, item.filter ? item.filter : {});

            // quick and irty fix for influencers - we need to prefix filter with 'cu'
            /*if (filter){
                for (var prop in filter){
                    if (!_.startsWith(prop, 'CTI.')){
                        filter['CTI.'+prop] = filter[prop];
                        delete filter[prop];
                    }
                }
            }*/

            if (socialService.getFilter().length > 0) {
                filter['SOURCE'] = _.map(socialService.getFilter(), 'id');
            }



            var lastRow = {};
            if ($scope.pagingData.lastRow) {
                //    lastRow.TRENDING_SCORE = $scope.pagingData.lastRow.TRENDING_SCORE;  
                //    lastRow.USER_ID = $scope.pagingData.lastRow.USER_ID;
               
                // for old phrase frequency sorter
              //  lastRow['CTP.FREQ'] = $scope.pagingData.lastRow['PHRASE_SCORE'];
              //  lastRow['CTP.MESSAGE_ID'] = $scope.pagingData.lastRow['MESSAGE_ID'];
                
                // for new created_at
                
                lastRow['CREATED_AT'] = $scope.pagingData.lastRow['CREATED_AT'];
            }

            if (!$rootScope.shwLoad) {
                $rootScope.shwLoad = true;
            }

            if (commonSearchService.getSearchFilter() && commonSearchService.getSearchFilter().score) {
                filter.sentiment_polarity = commonSearchService.getSearchFilter().score;
                filter.topic = commonSearchService.getSearchFilter().topic;
            } else {
                if (filter && filter.sentiment_polarity)
                    delete filter.sentiment_polarity;
            }

            if (commonSearchService.getSearchFilter() && commonSearchService.getSearchFilter().topic) {
                filter.topic = commonSearchService.getSearchFilter().topic;
            } else {
                if (filter && filter.topic)
                    delete filter.topic;
            }
            phrasesMessagesService.getFilteredMessages($scope.search.streamId, $scope.pagingData.limit, filter, lastRow)
                .success(function(result) {
                    if ($rootScope.checkHoldResult) {
                        if ($rootScope.shwLoad) {
                            $rootScope.shwLoad = false;
                        }
                    }
                    if (result && result.list && result.list.length > 0) {
                        if ($rootScope.callIntervalElm.indexOf("trending_phrase") === -1) {
                            $rootScope.callIntervalElm.push("trending_phrase");
                            if ($rootScope.shwLoad && $rootScope.callIntervalElm.length > 0) {

                                $rootScope.shwLoad = false;
                                $rootScope.$broadcast('fetch-stop', {
                                    q: 'trending_phrase'
                                });
                            }
                        }

                        $scope.handleResult(result);

                        $scope.lastCnt = $scope.result.length;

                        /*if ($scope.scrollDown) {
                            $timeout(function () {
                                angular.element(document.body).animate({
                                    scrollTop: $(document).height()
                                }, 2000);
                            }, 1000);
                        }*/
                    }
                })
                .error(function(data, status) {

                });
        }


        $scope.handleWSData = function(wsData) {
            //   console.log('trendingphrases ws data: ' + JSON.stringify(wsData));
            // $scope.result = wsData;
        };
        // webSocketStateService.addListener('main.trendingphrases', 'trendingphrases', $scope.handleWSData, 10);

        $scope.selectItem = function(item) {
            $scope.selectedItem = item;
            commonSearchService.addCurrentFilter('phrases', item.textItem);
            commonSearchService.setReload({
                shouldReload: true,
                source: 'main.base.content.main.main'
            });
        }

        $scope.prepareWSData = function(wsDataArray) {
            var res = [];
            if (wsDataArray) {
                for (var i = 0; i < wsDataArray.length; i++) {
                    // phrase.topTaggedMessage.profile_image_url
                    // phrase.topTaggedMessage.profileUrl
                    // phrase.topTaggedMessage.userName
                    // phrase.topTaggedMessage.createdAt
                    // phrase.topTaggedMessage.source
                    // {phrase.messages[0].messageLinkUrl
                    //  phrase.topTaggedMessage.text
                    // 
                    var wsObj = wsDataArray[i];
                    var obj = {
                        topTaggedMessage: {}
                    };
                    obj.topTaggedMessage.profile_image_url = wsObj.


                    res.push(obj);
                }
            }
            return res;
        };



    }
]);
