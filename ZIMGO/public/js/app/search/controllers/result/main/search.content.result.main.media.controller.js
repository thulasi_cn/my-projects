var app = angular.module('searchModule');

app.controller('SearchMainContentResultMediaCtrl', ['$scope', '$rootScope', '$state',
                                                    'mediaMessagesService', '$sce', '$timeout',
                                                    'commonSearchService', '$modal', 'webSocketStateService',
                                                    'serviceConfig', 'socialService', 'limitService','$document', '$window',
    function ($scope, $rootScope, $state, mediaMessagesService, $sce, $timeout, commonSearchService, $modal, webSocketStateService, serviceConfig, socialService, limitService, $document, $window) {

        $scope.initPagingData = function () {
            $scope.pagingData = {
                from: 0,
                limit: limitService.getRange(),
                total: null,
                isCompleted: false
            };
            $scope.result = [];
        }
        $scope.initPagingData();

        $scope.$watch(function () {
            return commonSearchService.getReload();
        }, function (newValue, oldValue) {
            if (newValue && newValue.shouldReload && (commonSearchService.getActiveRoute() === 'main')) { //newValue.source===$rootScope.$state.current.name) {
                if ($rootScope.callIntervalElm.indexOf("image_video") === -1) {
                    // $scope.scrollDown = false;
                    var getLimit = limitService.getMediaDetails();
                    $scope.pageLimit = getLimit.limit;                  

                    $scope.lastCnt = getLimit.mediaLastCnt;
                }
                $scope.initPagingData();
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.$watchCollection(function () {
            return socialService.getFilter();
        }, function (newValue, oldValue) {
            if (newValue !== oldValue) { //newValue.source===$rootScope.$state.current.name) {
                // $scope.scrollDown = false;
                var getLimit = limitService.getMediaDetails();
                $scope.pageLimit = getLimit.limit;

                $scope.initPagingData();

                $scope.lastCnt = getLimit.mediaLastCnt;
                $scope.run(commonSearchService.getCurrent());
            }
        });

        $scope.updateLimit = function () {
            //  $scope.result = []; // we should append data while paging not getting whole list each time
            $scope.pageLimit = $scope.pageLimit + limitService.getRange();
            // $scope.scrollDown = true;
            $scope.run(commonSearchService.getCurrent());
        };

        angular.element($document).on('scroll', function(){
            if($scope.result.length > 0){
                if((angular.element($document).height() - angular.element($window).height()) === angular.element($window).scrollTop()){
                    $scope.updateLimit();
                }
            }
        });

        $scope.handleResult = function (result) {
            var diff = _.differenceBy(result.list, $scope.result, 'REFERENCE_ID');
            if (diff && diff.length > 0) {
                $scope.result = $scope.result.concat(result.list);
            }
            $scope.pagingData.lastRow = result.list[result.list.length - 1];

        };

        $scope.run = function (item) {
            if (!item) return;

            // check if we are at the end of results
            if ($scope.pagingData.total && $scope.pagingData.from >= $scope.pagingData.total) {
                $scope.pagingData.isCompleted = true;
                return;
            }


            if ($rootScope.checkHoldResult) {
                // $scope.result =[]; //null; // comment out for testing by Ninel - need to understand why we need this
            }
            //        console.log('Loading Media: '+item.query);
            $scope.search = item;

            var filter = {}; // we don't filter media items except by social networ source
            //angular.extend(filter, item.filter ? item.filter : {});

            if (socialService.getFilter().length > 0) {
                filter.source = _.map(socialService.getFilter(), 'id');
            }

            var lastRow = {};
            if ($scope.pagingData.lastRow) {
                lastRow.REFERENCE_ID = $scope.pagingData.lastRow.REFERENCE_ID.toString();
                // lastRow.SCORE = $scope.pagingData.lastRow.SCORE;

            }

            if(!$rootScope.shwLoad){
                $rootScope.shwLoad = true;
            }
            
            mediaMessagesService.getFilteredMessages($scope.search.streamId, $scope.pagingData.limit, filter, lastRow)
                .success(function (result) {
                    if($rootScope.checkHoldResult){
                        if($rootScope.shwLoad){
                            $rootScope.shwLoad = false;
                        }
                    }
                    if (result && result.list && result.list.length > 0) {
                        if ($rootScope.callIntervalElm.indexOf("image_video") === -1) {
                            $rootScope.callIntervalElm.push("image_video");
                            if ($rootScope.shwLoad && $rootScope.callIntervalElm.length > 0) {

                                $rootScope.shwLoad = false;
                                $rootScope.$broadcast('fetch-stop', {
                                    q: 'image_video'
                                });
                            }
                        }

                        $scope.handleResult(result);

                        $scope.lastCnt = $scope.result.length;
                        /*if ($scope.scrollDown) {
                            $timeout(function () {
                                angular.element(document.body).animate({
                                    scrollTop: $(document).height()
                                }, 2000);
                            }, 1000);
                        }*/
                    }
                })
                .error(function (data, status) {

                });
        }

        $scope.handleWSData = function (wsData) {};
        //function (identifier, channelId, handler, bucketSize, sortBufferFn)
        webSocketStateService.addListener('main.media', 'media', $scope.handleWSData, 10);


        $scope.getPreparedUrl = function (bigImageUrl) {
            if (bigImageUrl) {
                var hostPart = serviceConfig.getMediaResolveHost() + '?h=200&w=278&src=';
                var encodedPart = encodeURIComponent(bigImageUrl);
                var url = hostPart + encodedPart;
                return url;
            } else {
                return '';
            }

        };

        $scope.getVideoPreparedUrl = function (videoUrl) {
            return $sce.trustAsResourceUrl(videoUrl);
        };

        $scope.startVideo = function(){

        };

        $scope.playVideo = function (videoObj) {
            //      console.log('clicked preview: ' + videoUrl);
            var modalInstance = $modal.open({
                templateUrl: 'js/app/common/templates/video.player.modal.view.html',
                controller: 'PlayVideoModalController',
                size: 'md',
                resolve: {
                    url: function () {
                        return (videoObj.VIDEO_URL.replace("https://www.youtube.com/v/", "https://www.youtube.com/embed/"))+"?autoplay=1";
                    },
                    itemType: function () {
                        return 'media-VIDEO';
                    },
                    item: function () {
                        return videoObj;
                    },
                    context: function(){
                        return "ONLY-VIDEO";
                    }
                }

            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {
                
            });
        };

        $scope.getSourceImgUrl = function (source) {
            var url = 'img/social/plain/small/ID.png';
            var sourceId = source;
            switch (source) {
                case 'FB':
                    sourceId = 'facebook';
                    break;
                case 'YT':
                    sourceId = 'youtube';
                    break;
                case 'FL':
                    sourceId = 'flickr'
                    break;
                case 'BL':
                    sourceId = 'blog'
                    break;
                default:
                    sourceId = source;
                    break;
            }
            return url.split('ID').join(sourceId);
        }
}]);
