var app = angular.module('searchModule');

app.controller('SearchMainContentResultLocationsCtrl', ['$scope', '$log', '$rootScope', 
                                                        '$state', 'locationMessagesService', '$timeout',
                                                        'commonSearchService','webSocketStateService','socialService',
    function ($scope, $log, $rootScope, $state, locationMessagesService, $timeout,commonSearchService,webSocketStateService,socialService) {

        $scope.result = {};
        $scope.selectedLocation = {};
        $scope.limit = 0;
        $scope.filterResult = [];
        
        $scope.$watch(function () {
            return commonSearchService.getReload();
        }, function (newValue, oldValue) {
            if (newValue && newValue.shouldReload && commonSearchService.getActiveRoute()==='main' && newValue.reloadSource!=='location'){ //&& newValue.source===$rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
              
            }
        });
        
        $scope.toggled = function(open) {
           // $log.log('Dropdown is now: ', open);
          };
        
        $scope.$watchCollection(function () {
            return socialService.getFilter();
        }, function (newValue, oldValue) {
            if (newValue!==oldValue) { //newValue.source===$rootScope.$state.current.name) {
                $scope.run(commonSearchService.getCurrent());
            }
        });
        
        $scope.run = function(item){
            /*
            if (!item) return;
            
            $scope.result = null;
            $log.debug('Loading Locations: '+item.query);
            $scope.search = item;
            $scope.selectedLocation = {};
            
            var filter = {};
            angular.extend(filter, item.filter?item.filter:{});            
            
            if (socialService.getFilter().length>0){
                filter.sources = _.map(socialService.getFilter(), 'id');
            }*/
            
            
            if (!item) return;

            if ($rootScope.checkHoldResult) {
                // $scope.result =[]; //null; // comment out for testing by Ninel - need to understand why we need this
            }
            $scope.search = item;
            // $scope.selectedLocation = {};

            var filter = {};
            // angular.extend(filter, item.filter ? item.filter : {});

            if (socialService.getFilter().length > 0) {
                filter['source'] = _.map(socialService.getFilter(), 'id');
            }
            
            delete filter['STATE'];

           /* var lastRow = {};
            if ($scope.pagingData.lastRow) {
                // lastRow['cu.SOURCE']= $scope.pagingData.lastRow.SOURCE;

                lastRow['cu.SCORE'] = $scope.pagingData.lastRow.SCORE;
                lastRow['cu.USER_ID'] = $scope.pagingData.lastRow.USER_ID;
            }*/

           /* if(!$rootScope.shwLoad){
                $rootScope.shwLoad = true;
            }*/
            

            // we don't need to limit items - we load all locations at once - so we don't need lastRow for pagination

            locationMessagesService.getFilteredMessages($scope.search.streamId, -1, filter, null)
                    .success(function (result) {
                        if (result && result.list && result.list.length > 0) {
                            //$scope.result = result.list[0];
                            $scope.result = $scope.getCalculatedPercentage(result.list);
                        }
                    })
                    .error(function (data, status) {

                    });
        };
        
        $scope.getCalculatedPercentage = function(items){
            var totalCount = 0;
            angular.forEach(items, function(item){
                totalCount+=item.SCORE;
            });
            
            angular.forEach(items, function(item){
                item.PERCENTAGE = Math.ceil(item.SCORE * 100/totalCount); // round to bigger number
            });
            
            var list = _.orderBy(items, ['PERCENTAGE'], ['desc']);
            list = _.filter(list, function(item){
                return item.STATE!=' ' && item.STATE!='0';
            });
        
            return list;
        };

        $scope.handleWSData = function(wsData){
            $log.debug('geolocations ws data: '+JSON.stringify(wsData));
         //    $scope.result = wsData;
        };
        
      //  webSocketStateService.addListener('main.geolocations','geolocations', $scope.handleWSData, 10);

        $scope.selectLocation = function (location) {
            $scope.selectedLocation = location;
            
           //var st = location.state.split(',')[1].trim();
            commonSearchService.addCurrentFilter('STATE', location.STATE);
            commonSearchService.setReload({shouldReload: true, reloadSource: 'location', source:'main.base.content.main.main', state:'filter' });
        }
        
        $scope.clearSelection = function(){
            $scope.selectedLocation  = null;
            
            commonSearchService.addCurrentFilter('STATE', null);
            commonSearchService.setReload({shouldReload: true, reloadSource: 'location', source:'main.base.content.main.main', state:'filter' });
            $scope.run(commonSearchService.getCurrent());
        }
}]);
