var searchModule = angular.module('searchModule');

searchModule.controller('PlayVideoModalController', ['$scope', '$timeout', '$sce', 'url', '$modalInstance', 'item', 'itemType', 'context',
        function ($scope, $timeout, $sce, url, $modalInstance, item, itemType, context) {

        $scope.modalViewContext = context;
        $scope.modalItemType = itemType;
        $scope.modalItem = item;
        if ($scope.modalItemType === "media-PHOTO" || $scope.modalItemType === "media-VIDEO") {
            $scope.url = $sce.trustAsResourceUrl(url);
        }

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };

}]);
