var taxonomyModyle = angular.module('taxonomyModule', ['ui.router']);

taxonomyModyle.config(
  ['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {


      $stateProvider
        .state('main.base.taxonomy', {
          abstract: true,
          url: 'interesting',
          views: {
            'content': {
              templateUrl: '/js/app/main/templates/content/main.base.content.view.html',
              controller: 'SearchContentResultBaseCtrl'
                /* this will check for user and redirect to landing and will set main-class */
            },
            'navbar@main': {
              templateUrl: '/js/app/navigation/templates/search.navigation.view.html',
              controller: 'MainTaxonomyNavigationCtrl'
              // controller: 'MainTaxonomyNavigationCtrl'
                // update from: 'MainNavigationCtrl'
            }
            ,
            'search@main': {
              // templateUrl: '/js/app/taxonomy/templates/main.content.navigation.items.view.html',
              controller: 'MainTaxonomyContentSearchNavigationItemsCtrl'
                // update from: MainContentSearchItemsCtrl
            }
          }
        })
        .state('main.base.taxonomy.main', {
          url: '',
          abstract: true,
          views: {
            'main@main.base.taxonomy': {
              templateUrl: '/js/app/taxonomy/templates/main.taxonomy.base.view.html',
              controller: 'TaxonomySearchContentResultMainCtrl'
                // update from: SearchContentResultMainCtrl
            },
            'taxonomytoolbar@main.base.taxonomy.main': {
                templateUrl: '/js/app/taxonomy/templates/main.taxonomy.toolbar.items.view.html',
                controller: 'TaxonomyToolbarCtrl'
            }
          }

        })
        .state('main.base.taxonomy.main.main', {
          url: '',
          views: {
            '@main.base.taxonomy.main': {
                templateUrl: '/js/app/taxonomy/templates/content/main/taxonomy.result.main.view.html'
            }
          },
          resolve: {
            activeRoute: ['commonSearchService', function(commonSearchService) {
              var activeRoute = 'main';
              commonSearchService.setActiveRoute(activeRoute);
              return activeRoute;
            }]
          }
        })
        .state('main.base.taxonomy.main.trendingphrases', {
          url: '/phrases',
          views: {
              '@main.base.taxonomy.main': {
                  templateUrl: '/js/app/taxonomy/templates/content/results/phrases/taxonomy.result.trendingphrases.view.html',
                  controller: 'TaxonomyContentResultPhrasesCtrl'
              }
          },
          resolve: {
              activeRoute: ['commonSearchService', function(commonSearchService){
                  var activeRoute = 'trendingphrases';
                  commonSearchService.setActiveRoute(activeRoute);
                  return activeRoute;
              }]
          }
      })
      .state('main.base.taxonomy.main.contributors', {
          url: '/contributors',
          views: {
              '@main.base.taxonomy.main': {
                  templateUrl: '/js/app/taxonomy/templates/content/results/contributors/taxonomy.result.contributors.view.html',
                  controller: 'TaxonomyContentResultContributorsCtrl'
              }
          },
          resolve: {
              activeRoute: ['commonSearchService', function(commonSearchService){
                  var activeRoute = 'contributor';
                  commonSearchService.setActiveRoute(activeRoute);
                  return activeRoute;
              }]
          }
      })
      .state('main.base.taxonomy.main.media', {
          url: '/media',
          views: {
              '@main.base.taxonomy.main': {
                  templateUrl: '/js/app/taxonomy/templates/content/results/media/taxonomy.result.media.view.html',
                  controller: 'TaxonomyContentResultMediaCtrl'
              }
          },
          resolve: {
              activeRoute: ['commonSearchService', function(commonSearchService){
                  var activeRoute = 'media';
                  commonSearchService.setActiveRoute(activeRoute);
                  return activeRoute;
              }]
          }
      });
    }
  ]
);
