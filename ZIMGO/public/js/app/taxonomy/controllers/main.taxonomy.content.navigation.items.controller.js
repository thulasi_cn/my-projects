var taxonomyModule = angular.module('taxonomyModule');

taxonomyModule.controller('MainTaxonomyContentSearchNavigationItemsCtrl', ['$scope', '$rootScope', '$state', 'commonSearchService', function ($scope, $rootScope, $state, commonSearchService) {

    $scope.reloadCurrent = function (sourcePage) {
        /*   var state = $rootScope.$state;
           var currentState = $state;*/
        commonSearchService.setReload({
            shouldReload: true,
            source: sourcePage
        });
        $scope.selected = sourcePage;
    };

    $scope.selected = $state.current.name;// 'main.base.taxonomy.main.main';

    $scope.$watch(function () {
        return commonSearchService.getReload();
    }, function (newValue, oldValue) {
        if (newValue && newValue.shouldReload && newValue.source === 'main.taxonomy' && newValue !== oldValue) {
            var state = $rootScope.$state;
            var currentState = $state;
            commonSearchService.setReload({
                shouldReload: true,
                source: currentState.current.name
            });
            // $scope.selected = currentState.current.name;
        }
    });

    $scope.searchItem = null;
    $scope.$watch(function () {
        return commonSearchService.getCurrent();
    }, function (newValue, oldValue) {
        if (newValue && newValue.streamId && newValue.streamId !== (oldValue && oldValue.streamId ? oldValue.streamId : null)) {
            var state = $rootScope.$state;
            var currentState = $state;
            //  $scope.selected = currentState.current.name;
            commonSearchService.setReload({
                shouldReload: true,
                source: currentState.current.name
            });
        }
    });

    commonSearchService.setReload({
        shouldReload: true,
        source: $state.current.name
    });

    /* $scope.refreshCurrent = function () {
         commonSearchService.setReload({shouldReload: true, });
     };*/
}]);
