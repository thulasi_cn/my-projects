var taxonomyModule = angular.module('taxonomyModule');

taxonomyModule.controller('MainTaxonomyNavigationCtrl', ['$scope','$controller','$rootScope', '$state', 'taxonomyService', 'commonSearchService', 'socialService',
    function ($scope, $controller, $rootScope, $state, taxonomyService, commonSearchService, socialService) {

    socialService.setCookiesInFilter();

    $scope.pageDetail = true;

    $scope.$watch(function(){
        return taxonomyService.getFilterReload();
    }, function(newValue, oldValue){
        if(newValue !== oldValue){
           if(newValue && newValue.q === ""){
                $scope.clearQuery();
                commonSearchService.setCurrent($scope.search);
            } 
        }
        
    });

    $controller('MainNavigationCtrl', {$scope: $scope}); //This works
   

    $scope.activeAllSocialLinks = function(){
        if(angular.element('.main-search-sns.filter-sns li').length > 0){
            setTimeout(function(){
                angular.element('.main-search-sns.filter-sns li').trigger('click');
            }, 2000);
        }
    };
    $scope.selectTimers = [{id:1,value:30},{id:2,value:60},{id:3,value:90},{id:4,value:120,},{id:5,value:150}];
    $scope.setTimerController = $scope.selectTimers[0].value;

    $scope.clearQuery = function (q) {
        q = q ? q : '';
        $scope.search = {
            query: q
        };
    };
    $scope.clearQuery();
    commonSearchService.setCurrent($scope.search);

    $scope.mainDoSearch = function () {
        //$scope.loadMore();
        commonSearchService.setCurrent($scope.search);
        taxonomyService.setFilterReload({
            q : $scope.search,
          list: []
        });
    };
    
    $scope.socialConnectNetworks = socialService.getSocialNetworks();        
        $scope.filterSocial = function(sn){
            socialService.toggleAddFilter(sn);
            taxonomyService.setFilterReload({
                q : $scope.search,
              list: []
            });
        };
        
        $scope.isSnsSelected = function(sn){
            if((sn.name === 'youtube' || sn.name === 'flickr') && $rootScope.disableSocialPart){
                return false;
            }else{
                return socialService.filterExists(sn);
            }
        };
}]);
