var taxonomyModule = angular.module('taxonomyModule');

taxonomyModule.controller('TaxonomyAddUpdateModalController', ['$scope', '$rootScope', '$state','$controller', '$modalInstance', 'currentTaxonomy','taxonomyService','commonSearchService','$modal', function ($scope, $rootScope, $state, $controller, $modalInstance, currentTaxonomy,taxonomyService,commonSearchService,$modal) {
    
    $controller('TaxonomyToolbarCtrl', {$scope: $scope,
                                        $state:$state,
                                        $rootScope:$rootScope,
                                        taxonomyService:taxonomyService,
                                        commonSearchService:commonSearchService,
                                        $modal:$modal});

    if (currentTaxonomy && currentTaxonomy.name) {
        $scope.isUpdate = true;
        $scope.prvTaxonomy = currentTaxonomy.name;
    }
    $scope.showColorPicker = true;
    $scope.isDuplicate = false;
    $scope.$watch(function(){
        if(angular.element("form[name='categoryForm'] .taxonomyNm span").length > 0){
            angular.element(".modal-mainDiv").addClass('changeHt');
        }else{
            angular.element(".modal-mainDiv").removeClass('changeHt');
        }
    });
    $scope.currentTaxonomy = currentTaxonomy;

    $scope.isEmpty = function(){
        return $scope.currentTaxonomy.color === undefined;
    };
    $scope.updateCurrentTaxonomy = function(item){
        $scope.currentTaxonomy = item;
        $scope.isUpdate = true;
        $scope.prvTaxonomy = item.name;
    }
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.ok = function () {

        if($scope.validDuplicate()){
            $scope.isDuplicate = false;
            if($scope.isUpdate){
                $scope.updateTaxonomy($scope.currentTaxonomy);
            }else{
                $scope.insertTaxonomy($scope.currentTaxonomy);
            }

        }else{
            $scope.isDuplicate = true;
        }    
        
    };


    $scope.validDuplicate = function(){
        var chk = true, cnt = 0;
        if($scope.prvTaxonomy){
            if($scope.prvTaxonomy === $scope.currentTaxonomy.name){
                chk = false;
            }else{
                chk = true;
            }
        }else{
            chk = true;
        }
        if(chk){
            for(var i=0; i< $rootScope.taxonomies.length; i++ ){
                if($scope.currentTaxonomy.id !== $rootScope.taxonomies[i].id){
                    if($scope.currentTaxonomy.name === $rootScope.taxonomies[i].name){
                        cnt++;
                        break;
                    }
                }
            }

            if(cnt > 0){
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }
    };

    $scope.delete = function () {
        $modalInstance.close({
            action: $scope.isUpdate ? 'delete' : null,
            item: $scope.currentTaxonomy
        });
    };

    $scope.createNew = function(){
        $scope.isUpdate = false;
        $scope.prvTaxonomy = "";
        $scope.currentTaxonomy = {};
        $scope.showColorPicker = false;

        setTimeout(function() {
            $scope.categoryForm.$setPristine();
            $scope.categoryForm.$setUntouched();
            $scope.showColorPicker = true;
            $scope.$apply(); //this triggers a $digest
          }, 50);

    };
}]);
