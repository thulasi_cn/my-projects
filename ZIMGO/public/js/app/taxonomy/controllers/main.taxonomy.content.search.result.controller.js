var taxonomyModule = angular.module('taxonomyModule');

taxonomyModule.controller('TaxonomySearchContentResultMainCtrl', ['$scope', '$rootScope', '$state',function($scope, $rootScope, $state){
	   	if (GOWEB_CONF.analyticsId) {
	       GA('send', {
	          hitType: 'event',
	          eventCategory: 'taxonomyPage',
	          eventAction: 'view',
	          eventLabel: 'taxonomyPageView'
	        });
	   }
}]);