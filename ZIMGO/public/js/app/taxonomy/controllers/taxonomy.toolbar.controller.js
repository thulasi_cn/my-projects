var taxonomyModule = angular.module('taxonomyModule');

taxonomyModule.controller('TaxonomyToolbarCtrl', ['$scope', '$rootScope', '$state', '$modal',
'taxonomyService', 'commonSearchService','$translate','languageService',
    function ($scope, $rootScope, $state, $modal, taxonomyService, commonSearchService, $translate, languageService) {
        
        $scope.currentTaxonomy = {};
        $scope.totalCat = 0;
        $scope.isVisible = false;
        $scope.catList = angular.element(document.getElementById('categoryLists'));
        $scope.showClear = false;
        $scope.filterList = [];
        $scope.$watch(function(){ return $scope.showClear}, function(newValue, oldValue){
            $rootScope.$broadcast('checkCategoryFilter', {'q' : newValue});
        });

        $translate('Do you want to delete category and all related items?').then(function (translation) {
          $scope.translatedTextDeleteTaxonomyCnfrm = translation;
        });

        $scope.$watch(function(){
            return taxonomyService.getFilterReload();
        }, function(newValue, oldValue){
            if(newValue !== oldValue){
                if (newValue && newValue.q !== "") {
                    if(commonSearchService.getCurrent().query !== ""){
                        $scope.showClear = true;
                         angular.forEach($scope.result, function(category){
                            category.selected = false;
                          });
                         $scope.filterList.length = 0;
                    }
                }
            }
        });

        $scope.$watch(function() {return taxonomyService.getFooterFilterItem()}, function(newValue, oldValue){
            if(newValue){
                $scope.editFilter(newValue.name, 'edit');
            }
        });

        $scope.$watch( function(){ return $rootScope.taxonomies; }, function(newValue, oldValue){
            if(newValue && newValue !== oldValue){
                if(newValue.length > 0){
                    $scope.result = newValue;
                }else{
                    $scope.result = undefined;
                }
            }
        });
        $scope.marginLeft = 0;

        $scope.myStyle = {
            'margin-left': '0px'
        };

        $scope.chkVisible = function(){
            $scope.isVisible = !$scope.isVisible;
        };

        //precheck
        if ($scope.marginLeft === 0) {
            $scope.leftdisabled = true;
        } else {
            $scope.leftdisabled = false;
        }

        if ($scope.marginLeft === -(($scope.catList.find('li').length- 1) * 105)) {
            $scope.rightdisabled = true;
        } else {
            $scope.rightdisabled = false;
        }


        //mover right
        $scope.moveRight = function() {
            $scope.marginLeft = +$scope.myStyle['margin-left'].replace('px', '') + -105;

            if ($scope.marginLeft === 0) {
                $scope.leftdisabled = true;
            } else {
                $scope.leftdisabled = false;
            }

            if ($scope.marginLeft === -(($scope.catList.find('li').length - 1) * 105)) {
                $scope.rightdisabled = true;
            } else {
                $scope.rightdisabled = false;
            }

            $scope['margin-left'] = $scope.marginLeft + 'px';
            $scope.myStyle['margin-left'] = $scope['margin-left'];
            return $scope['margin-left'];
        };

        //move left
        $scope.moveLeft = function() {
            $scope.marginLeft = +$scope.myStyle['margin-left'].replace('px', '') + 105;

            if ($scope.marginLeft === 0) {
                $scope.leftdisabled = true;
            } else {
                $scope.leftdisabled = false;
            }

            if ($scope.marginLeft === -(($scope.catList.find('li').length - 1) * 105)) {
                $scope.rightdisabled = true;
            } else {
                $scope.rightdisabled = false;
            }

            $scope['margin-left'] = $scope.marginLeft + 'px';
            $scope.myStyle['margin-left'] = $scope['margin-left'];
            return $scope['margin-left'];
        };

        $scope.$watch(function () {
            return $scope.currentTaxonomy
        }, function (newValue, oldValue) {
         //   if (newValue != oldValue) {
                taxonomyService.setCurrent($scope.currentTaxonomy);
           // }
        });

        taxonomyService.setCurrent($scope.currentTaxonomy);

        $rootScope.$on('categoryListUpdate', function (event, args) {
            if(args.type === "insert"){
                $scope.insertTaxonomy(args.currentTaxonomy);
            }else if(args.type === "update"){
                $scope.updateTaxonomy(args.currentTaxonomy);
            }
        });
        $scope.openTaxonomyModalView = function (taxonomy) {
            var modalInstance = $modal.open({
                templateUrl: 'js/app/taxonomy/templates/common/taxonomy.add.update.modal.view.html',
                controller: 'TaxonomyAddUpdateModalController',
                windowClass: 'addCatPop-modal-window',
                //scope: $scope,
                resolve: {
                    currentTaxonomy: function () {
                        return taxonomy;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                if (selectedItem.action && selectedItem.action === 'insert') {
                    $scope.insertTaxonomy(selectedItem.item);
                } else if (selectedItem.action && selectedItem.action === 'update') {
                    $scope.updateTaxonomy(selectedItem.item);
                } else if (selectedItem.action && selectedItem.action === 'delete') {
                    $scope.deleteTaxonomy(selectedItem.item);
                } else {
                    // console.log('unknown taxonomy item: ' + JSON.stringify(selectedItem));
                }
            }, function () {});
        };

        $scope.addTaxonomy = function () {
            //$scope.currentTaxonomy = {};
            $scope.openTaxonomyModalView({});
        };

        $scope.insertTaxonomy = function (taxonomy) {

            if (!taxonomy || !taxonomy.name || !taxonomy.color) {
                // some error 
                return;
            }
            taxonomyService.insertTaxonomy(taxonomy)
                .success(function (result) {
                    if (GOWEB_CONF.analyticsId) {
                         GA('send', {
                          hitType: 'event',
                          eventCategory: 'taxonomyPage',
                          eventAction: 'createCategory',
                          eventLabel: 'taxonomyPageCreateCategory'
                        });
                     }
                    $scope.currentTaxonomy = result.result;
                    if($scope.createNew){
                        $scope.createNew();
                    }
                    $scope.loadPagedData(true);
                })
                .error(function (data, status) {});
        };

        $scope.updateTaxonomy = function (taxonomy) {
            if (!taxonomy || !taxonomy.id || !taxonomy.name || !taxonomy.color) {
                // some error 
                return;
            }
            taxonomyService.updateTaxonomy(taxonomy)
                .success(function (result) {
                    $scope.currentTaxonomy = result.result;
                    if($scope.createNew){
                        $scope.createNew();
                    }
                    $scope.loadPagedData(true);
                })
                .error(function (data, status) {});
        };


        $scope.deleteTaxonomy = function (taxonomy) {
            
            function characterFromDecimalNumericEntity(str) {
                var strArr = [];
                var transform = function(str1){

                    var decNumEntRex = /^\&#(\d+);$/;
                    var match = decNumEntRex.exec(str1);
                    var codepoint = match ? parseInt(match[1], 10) : null;
                    var character = codepoint ? String.fromCharCode(codepoint) : null;

                    return character;
                }

                if(str.split(";").length > 1){
                    strArr = str.split(/;| /).map(function(char){
                        return char ? (transform(char+';')) : ' ';
                    });
                }
                
                return strArr.length ? strArr.join('') : str;
            }

            var selectedLanguage = languageService.getCurrent();
            var strmy = (selectedLanguage.id == 'en') ? $scope.translatedTextDeleteTaxonomyCnfrm : 
                            characterFromDecimalNumericEntity($scope.translatedTextDeleteTaxonomyCnfrm);

            var r = confirm(strmy);
            
            if (r==true){
                if (taxonomy.id) {
                    taxonomyService.deleteTaxonomy(taxonomy.id)
                        .success(function (result) {
                        if (GOWEB_CONF.analyticsId) {
                           GA('send', {
                              hitType: 'event',
                              eventCategory: 'taxonomyPage',
                              eventAction: 'deletCategory',
                              eventLabel: 'taxonomyPageDeletCategory'
                            });
                         }
                            if (result && result.result === 1) {

                            }
                            if($scope.createNew){
                                $scope.createNew();
                            }
                            $scope.loadPagedData(true);
                            //$scope.updateCurrentTaxonomy({});
                        })
                        .error(function (data, status) {});
                } else {
                    // do we need growl here???   
                }
            }
        };

        $scope.loadPagedData = function (isReload) {
            
            taxonomyService.filterTaxonomies($scope.search, 0, 0)
                .success(function (result) {
                    if (result && result.list && result.list.length > 0) {
                        $scope.result = result.list;
                        $rootScope.taxonomies = $scope.result;
                        // $scope.filterCl(result.list);
                    }else{
                        $scope.result = undefined;
                        $rootScope.taxonomies = [];
                        // $rootScope.$digest();
                        //     $scope.filterCl([]);
                    }
                    if(isReload){
                        $scope.filterCl([]);
                    }
                    $scope.showClear = false;
                })
                .error(function (data, status) {});
        };

      //  $scope.result = $rootScope.taxonomies;
        $scope.loadPagedData(false);

        $scope.toggleSelectItem = function (item) {
            if ($scope.currentTaxonomy===item){
                $scope.currentTaxonomy = {};
            }else{
                $scope.currentTaxonomy = item;
            }
        };

        $scope.removeItem = function (item) {
            $scope.deleteTaxonomy(item);
        };

        $scope.updateItem = function (item) {
            $scope.updateCurrentTaxonomy(item);
        };

        $scope.moveX = function (pixels) {
            var v = 0;
            for(var i = 0; i < $scope.catList.find('li').length; i++){
                v = $scope.catList[i].clientWidth;
            }
            $scope.scroller.left += pixels;
        };
        /*$scope.scroller = {
            position: 'relative',
            left: 0
        };*/

        $scope.showCommands = function () {
            $scope.commandVisible = true;
        };

        $scope.hideCommands = function () {
            $scope.commandVisible = false;
        };

        $scope.searchCategories = function(){
            var selectedCategoryList = [];
            $scope.filterList.length = 0;
          angular.forEach($scope.result, function(category){
            if (category.selected){
                $scope.filterList.push(category.name);
                selectedCategoryList.push(category.id);
            } 
          });
          $scope.filterCl(selectedCategoryList);
          $scope.chkVisible();
        };

        $scope.filterCl = function(selectedCategoryList){
            if(selectedCategoryList && selectedCategoryList.length > 0){
                taxonomyService.setFilterReload({
                    q : "",
                  list: selectedCategoryList
                });
                $scope.showClear = true;
              }else{
                taxonomyService.setFilterReload({
                    q : "",
                  list: []
                });
                $scope.showClear = false;
              }
        };

        $scope.clearCategories = function(){
            angular.forEach($scope.result, function(category){
                category.selected = false;
              });

            $scope.filterList.length = 0;
            commonSearchService.setCurrent({
                query: ''
            });
            taxonomyService.setFilterReload({
                q : "",
              list: []
            });
            if($scope.isVisible){
                $scope.chkVisible();
            }
            $scope.showClear = false;
            taxonomyService.resetFooterFilterItem();
        };

        $scope.editFilter = function(item, type){
            $scope.filterList.length = 0;
            var selectedCategoryList = [];
            angular.forEach($scope.result, function(category){
                if(type === 'del'){
                    if(category.name === item){
                        category.selected = false;
                    }
                }else{
                    if(category.name === item){
                        category.selected = true;
                    }else{
                        category.selected = false;
                    }
                }
            if (category.selected){
                $scope.filterList.push(category.name);
                selectedCategoryList.push(category.id);
            } 
          });

            if(type === 'del'){
                taxonomyService.resetFooterFilterItem();
            }
            $scope.filterCl(selectedCategoryList);
        };

}]);
