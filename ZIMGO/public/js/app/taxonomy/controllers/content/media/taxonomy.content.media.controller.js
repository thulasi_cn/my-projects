var taxonomyModule = angular.module('taxonomyModule');

taxonomyModule.controller('TaxonomyContentResultMediaCtrl', [ '$scope', '$rootScope', '$state', 'taxonomyService','commonSearchService','serviceConfig', '$modal','$sce', function($scope, $rootScope, $state, taxonomyService, commonSearchService, serviceConfig, $modal, $sce){
    
    $scope.$watch(function () {
        return taxonomyService.getCurrent();
    }, function (newValue, oldValue) {
        if (newValue !== oldValue) {
            $scope.run(newValue);
        }
    });

    $scope.$watch(function () {
        return taxonomyService.getReload();
    }, function (newValue, oldValue) {
        if (newValue !== oldValue && newValue.itemType === 'contributor') {
            $scope.run(taxonomyService.getCurrent());
        }
    });
    
    $scope.$watch(function () {
            return commonSearchService.getReload();
        }, function (newValue, oldValue) {
            if (newValue && newValue.shouldReload && newValue.source === $rootScope.$state.current.name) {
                $scope.run();
            }
        });


    $scope.pagingData = {
        currentPage: -1,
        from: 0,
        limit: 12,
        busy: false,
        disabled: false
    };

    $scope.clearPagingData = function () {
        $scope.pagingData = {
            currentPage: -1,
            from: 0,
            limit: 12,
            busy: false,
            disabled: false
        };
    };

    $scope.getNextPage = function () {
        $scope.pagingData.currentPage += 1;
        $scope.pagingData.from = $scope.pagingData.currentPage * $scope.pagingData.limit;
    };

    $scope.loadPagedData = function () {
        $scope.pagingData.busy = true;


        var filter = {itemTypes: ['media-PHOTO','media-VIDEO' ]};
        
        if ($scope.item.id) {
            filter.categoryIds = [$scope.item.id];
        }
        filter.from = $scope.pagingData.currentPage;
        filter.limit = $scope.pagingData.limit;
        
        if (commonSearchService.getCurrent()){
            var search = commonSearchService.getCurrent();
            if (search.query){
                filter.query = search.query;   
            }
        }

        taxonomyService.filterTaxonomiedItems(filter)
            .success(function (result) {
                if (result && result.list && result.list.length > 0) {
                    var resContentItems = [];
                    angular.forEach(result.list, function (item) {
                        item.content = JSON.parse(item.content);
                        resContentItems.push(item);
                    });
                    $scope.result = $scope.result ? $scope.result.concat(resContentItems) : resContentItems;
                    $scope.pagingData.busy = false;
                } else {
                    $scope.pagingData.disabled = true;
                }
            })
            .error(function (data, status) {

            });
    };

    $scope.loadMore = function () {
        $scope.getNextPage();
        $scope.loadPagedData();
    };

    $scope.run = function (item) {
        $scope.result = null;
        $scope.item = item || taxonomyService.getCurrent() || {};
        $scope.clearPagingData();
        $scope.loadMore();
    };

   // $scope.run();
    
     $scope.getPreparedUrl = function (bigImageUrl) {
            var hostPart = serviceConfig.getMediaResolveHost() + '?h=200&w=278&src=';
            var encodedPart = encodeURIComponent(bigImageUrl);
            var url = hostPart + encodedPart;
            return url;
        };

        $scope.getVideoPreparedUrl = function (videoUrl) {
            return $sce.trustAsResourceUrl(videoUrl);
        };

        $scope.playVideo = function (videoUrl) {         
            var modalInstance = $modal.open({
                templateUrl: 'js/app/common/templates/video.player.modal.view.html',
                controller: 'PlayVideoModalController',
                size: 'lg',
                resolve: {
                    url: function () {
                        return videoUrl;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };
    
}]);