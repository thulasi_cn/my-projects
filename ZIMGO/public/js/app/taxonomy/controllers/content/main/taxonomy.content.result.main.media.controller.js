var app = angular.module('taxonomyModule');

app.controller('TaxonomyMainContentResultMediaCtrl', ['$scope', '$controller', '$rootScope', '$state',
                                                    'mediaMessagesService', '$sce', '$timeout',
                                                    'commonSearchService', '$modal', 'taxonomyService',
                                                    'serviceConfig', 'socialService',
    function ($scope, $controller, $rootScope, $state, mediaMessagesService, $sce, $timeout, commonSearchService, $modal,
        taxonomyService, serviceConfig, socialService) {

        $scope.filterActivate = false;

        // $scope.$watch(function () {
        //     return taxonomyService.getCurrent();
        // }, function (newValue, oldValue) {
        //     if (newValue !== oldValue) {
        //         $scope.run(newValue);
        //     }
        // });

        $scope.$watch(function () {
            return taxonomyService.getFilterReload();
        }, function (newValue, oldValue) {
            // if (newValue !== oldValue) {
                if (newValue && newValue.list.length > 0) {
                    $scope.run(newValue.list);
                } else {
                    $scope.run("");
                }
            // }
        });

        $scope.$watch(function () {
            return taxonomyService.getReload();
        }, function (newValue, oldValue) {
            if (newValue !== oldValue && (newValue.itemType === 'media-PHOTO' || newValue.itemType === 'media-VIDEO')) {
                $scope.run(taxonomyService.getCurrent());
            }
        });

        $scope.$watch(function () {
            return taxonomyService.getRemovedItem();
        }, function (newValue, oldValue) {
            if (newValue && newValue !== oldValue && (newValue.itemType === 'media-PHOTO' || newValue.itemType === 'media-VIDEO')) {
                //$scope.run(taxonomyService.getCurrent());
                var removed = _.remove($scope.result, function (i) {
                    return i.referenceId === newValue.referenceId;
                });
            }
        });

        $scope.$on('checkCategoryFilter', function (event, args) {
            $scope.filterActivate = args.q;
        });

        $scope.run = function (item) {
            // $scope.result = null;
            //if (!item) return;
            $scope.item = item || taxonomyService.getCurrent() || {};

            $scope.result = null;

            var filter = {
                itemTypes: ['media-PHOTO', 'media-VIDEO']
            };

            if (angular.isArray($scope.item)) {
                filter.categoryIds = item;
            } else {
                if ($scope.item.id) {
                    filter.categoryIds = [$scope.item.id];
                }
            }

            if (commonSearchService.getCurrent().query !== "") {
                filter.query = commonSearchService.getCurrent().query;
            }

            if (socialService.getFilter().length > 0) {
                filter.source = _.map(socialService.getFilter(), 'id');
            }

            $rootScope.shwLoad = true;

            taxonomyService.filterTaxonomiedItems(filter)
                .success(function (result) {
                    if($rootScope.taxonomySearchListArr.indexOf("image_video") < 0){
                        $rootScope.taxonomySearchListArr.push("image_video");
                    }
                    $rootScope.shwLoad = false;
                    if (result && result.list && result.list.length > 0) {
                        var resContentItems = [];
                        angular.forEach(result.list, function (item) {
                            item.content = JSON.parse(item.content);
                            resContentItems.push(item);
                        });
                        $scope.result = resContentItems;
                    }
                })
                .error(function (data, status) {

                });
        };

        $scope.run(taxonomyService.getFilterReload() ? (taxonomyService.getFilterReload().list.length > 0 ? taxonomyService.getFilterReload().list : taxonomyService.getFilterReload()) : "");

        $scope.getPreparedUrl = function (bigImageUrl) {
            if (bigImageUrl) {
                var hostPart = serviceConfig.getMediaResolveHost() + '?h=200&w=278&src=';
                var encodedPart = encodeURIComponent(bigImageUrl);
                var url = hostPart + encodedPart;
                return url;
            } else {
                return "";
            }

        };

        $scope.getVideoPreparedUrl = function (videoUrl) {
            return $sce.trustAsResourceUrl(videoUrl);
        };

        $scope.playVideo = function (videoObj) {

            var modalInstance = $modal.open({
                templateUrl: 'js/app/common/templates/video.player.modal.view.html',
                controller: 'PlayVideoModalController',
                size: 'md',
                resolve: {
                    url: function () {
                        return (videoObj.VIDEO_URL.replace("https://www.youtube.com/v/", "https://www.youtube.com/embed/"))+"?autoplay=1";
                    },
                    itemType: function () {
                        return 'media-VIDEO';
                    },
                    item: function () {
                        return videoObj;
                    },
                    context: function(){
                        return "ONLY-VIDEO";
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {});
        };
}]);
