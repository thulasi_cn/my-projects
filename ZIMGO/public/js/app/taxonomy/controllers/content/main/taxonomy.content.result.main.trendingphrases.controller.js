var app = angular.module('searchModule');

app.controller('TaxonomyMainContentResultTrendingphrasesCtrl', ['$scope', '$controller', '$rootScope',
                                                              '$state', 'phrasesMessagesService', '$timeout',
                                                              'commonSearchService', 'taxonomyService', 'socialService',
    function ($scope, $controller, $rootScope, $state, phrasesMessagesService, $timeout, commonSearchService, taxonomyService, socialService) {

        $scope.filterActivate = false;

        taxonomyService.setFilterReload(undefined);

        // $scope.$watch(function () {
        //     return taxonomyService.getCurrent();
        // }, function (newValue, oldValue) {
        //     if (newValue !== oldValue) {
        //         $scope.run(newValue);
        //     }
        // });

        $scope.$watch(function () {
            return taxonomyService.getReload();
        }, function (newValue, oldValue) {
            if (newValue !== oldValue && newValue.itemType === 'trendingphrases') {
                $scope.run(taxonomyService.getCurrent());
            }
        });

        $scope.$watch(function () {
            return taxonomyService.getFilterReload();
        }, function (newValue, oldValue) {
            // if (newValue !== oldValue) {
                if (newValue && newValue.list.length > 0) {
                    $scope.run(newValue.list);
                } else {
                    $scope.run("");
                }
            // }
        });
        
        $scope.$watch(function () {
            return taxonomyService.getRemovedItem();
        }, function (newValue, oldValue) {
            if (newValue && newValue !== oldValue && newValue.itemType === 'trendingphrases') {
                //$scope.run(taxonomyService.getCurrent());
                var removed = _.remove($scope.result, function(i){
                    return i.referenceId===newValue.referenceId;
                });
            }
        });

        $scope.$on('checkCategoryFilter',function(event, args){
            $scope.filterActivate = args.q;
        });
        
       /* $scope.$watchCollection(function () {
            return socialService.getFilter();
        }, function (newValue, oldValue) {
            if (newValue !== oldValue) {
                $scope.scrollDown = false;
                var getLimit = limitService.getPhraseDetails();
                $scope.pageLimit = getLimit.limit;

                $scope.initPagingData();

                $scope.lastCnt = getLimit.phraseLastCnt;
                $scope.run(commonSearchService.getCurrent());
            }
        });*/
        

        $scope.run = function (item) {
            // $scope.result = null;
            //if (!item) return;
            $scope.item = item || taxonomyService.getCurrent() || {};

            $scope.result = null;
            //console.log('Loading TAXONOMY Contributors: ' + item);

            var filter = {
                itemType: 'trendingphrases'
            };
            if (angular.isArray($scope.item)) {
                filter.categoryIds = item;
            } else {
                if ($scope.item.id) {
                    filter.categoryIds = [$scope.item.id];
                }
            }

            if (commonSearchService.getCurrent().query !== "") {
                filter.query = commonSearchService.getCurrent().query;
            }
            
            if (socialService.getFilter().length > 0) {
                filter.source = _.map(socialService.getFilter(), 'id');
            }
            
            $rootScope.shwLoad = true;

            taxonomyService.filterTaxonomiedItems(filter)
                .success(function (result) {
                    if($rootScope.taxonomySearchListArr.indexOf("trending_phrase") < 0){
                        $rootScope.taxonomySearchListArr.push("trending_phrase");
                    }
                    $rootScope.shwLoad = false;
                    if (result && result.list && result.list.length > 0) {
                        var resContentItems = [];
                        angular.forEach(result.list, function (item) {
                            item.content = JSON.parse(item.content);
                            resContentItems.push(item);
                        });
                        $scope.result = resContentItems;
                    }
                })
                .error(function (data, status) {

                });
        };

        $scope.run(taxonomyService.getFilterReload() ? (taxonomyService.getFilterReload().list.length > 0 ? taxonomyService.getFilterReload().list : taxonomyService.getFilterReload()) : "");

        /*  if (taxonomyService.getCurrent()){
              $scope.item = taxonomyService.getCurrent();   
          }
          */
        // $scope.run();

        // console.log('trending phrase in main route: activeRoute: '+commonSearchService.getActiveRoute());

        /* $scope.$watch(function () {
             return commonSearchService.getReload();
         }, function (newValue, oldValue) {
             if (newValue && newValue.shouldReload && commonSearchService.getActiveRoute()==='main'){ //newValue.source === $rootScope.$state.current.name) {
                 $scope.run(commonSearchService.getCurrent());

             }
         });*/

        /* $scope.run = function (item) {
             
             if (!item) return;
             
             if (item.filter && item.filter.phrases){ // don't want to refresh ourself
                 delete item.filter.phrases;
             }
             
             $scope.result = null;
             console.log('Loading TrendingPhrases: ' + item.query);
             $scope.search = item;

             var filter = {
                 keyword: $scope.search.query,
                 streamId: $scope.search.streamId,
                 lang: $scope.search.lang
             };
             angular.extend(filter, item.filter?item.filter:{});
             
             phrasesMessagesService.getFilteredMessages($scope.search.query, $scope.search.streamId, 0, 4, filter)
                 .success(function (result) {
                     if (result && result.list && result.list.length > 0) {
                         $scope.result = result.list;

                     }
                 })
                 .error(function (data, status) {

                 });
         }*/
        /*  $scope.result = [];

          $scope.handleWSData = function (wsData) {
              console.log('trendingphrases ws data: ' + JSON.stringify(wsData));
          };
          webSocketStateService.addListener('main.trendingphrases','trendingphrases', $scope.handleWSData, 10);
          
          $scope.selectItem = function(item){
              $scope.selectedItem = item;            
              commonSearchService.addCurrentFilter('phrases', item.textItem);
              commonSearchService.setReload({shouldReload: true, source:'main.base.content.main.main' });
          }*/
}]);
