var taxonomyModyle = angular.module('taxonomyModule');


taxonomyModyle.factory('taxonomyService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            filterTaxonomies: function (filter) {
                //var fl = '';
                //fl = prepareFilterQueryString(filter);
                var queryString = host + 'taxonomy/filter';
                return $http.get(queryString);
            },
            insertTaxonomy: function (taxonomy) {
                return $http.post(host + 'taxonomy/', JSON.stringify(taxonomy));
            },
            updateTaxonomy: function (taxonomy) {
                return $http.put(host + 'taxonomy/', JSON.stringify(taxonomy));
            },
            deleteTaxonomy: function (id) {
                return $http.delete(host + 'taxonomy/' + id);
            },
            insertTaxonimiedItem: function (taxonomiedItem) {
                return $http.post(host + 'taxonomy/item', taxonomiedItem);
            },
            insertTaxonimiedItems: function (taxonomiedItemList) {
                return $http.post(host + 'taxonomy/item/list', taxonomiedItemList);
            },
            filterTaxonomiedItems: function (filter) {
                var queryString = host + 'taxonomy/item/filter?filter=' + encodeURIComponent(JSON.stringify(filter));
                return $http.get(queryString);
            },
            deleteTaxonomiedItem: function (taxonomiedItem) {
                console.log(taxonomiedItem);
                delete taxonomiedItem.content;
                return $http.post(host + 'taxonomy/item/remove', taxonomiedItem);
            },
            /*deleteTaxonomiedItems: function (taxonomiedItemList, categoryIds) {
                var cids = categoryIds.join(',');
                return $http.post(host + 'taxonomy/item/list/remove/' + cids, taxonomiedItemList);
            },*/
            setCurrent: function (taxonomy) {
                this.currentTaxonomy = taxonomy;
            },
            getCurrent: function () {
                return this.currentTaxonomy;
            },
            setReload: function (reload) {
                this.reload = reload;
            },
            getReload: function () {
                return this.reload;
            },
            setFilterReload : function(categoryObj){
                this.filterReload = categoryObj;
            },
            getFilterReload: function () {
                return this.filterReload;
            },
            setRemovedItem: function(taxonomiedItem){
                this.taxonomiedItem = taxonomiedItem;
            },
            getRemovedItem: function(){
                return this.taxonomiedItem;
            },
            updateFooterFilterItem: function(item){
                this.footerFilter = item;
            },
            getFooterFilterItem: function(){
                return this.footerFilter;
            },
            resetFooterFilterItem: function(){
                this.footerFilter = undefined;
            }
        }
    }
]);
