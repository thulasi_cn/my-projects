var maintenanceModule = angular.module('maintenanceModule', ['ui.router']);

maintenanceModule.config(
  ['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {


      $stateProvider
        .state('main.base.maintenance', {
          abstract: true,
          url: 'maintenance',
          views: {
            'content': {
              templateUrl: '/js/app/main/templates/content/main.base.content.view.html'
            }
          }
        })
        .state('main.base.maintenance.main', {
          url: '',
          abstract: true,
          views: {
            'main@main.base.maintenance': {
              templateUrl: '/js/app/maintenance/templates/main.maintenance.base.view.html'
            }
          }

        })
        .state('main.base.maintenance.main.main', {
          url: '',
          views: {
            '@main.base.maintenance.main': {
                templateUrl: '/js/app/maintenance/templates/maintenance.result.main.view.html'
            }
          }
        });
    }
  ]
);
