var auth = angular.module('authentication');

auth.controller('LoginModalCtrl', ['AuthService', 'UsersService', '$rootScope', '$scope', '$window', '$log', '$filter', '$timeout', '$modal', 'growl', '$facebook', '$auth', 'serviceConfig', 'localStorageService', '$state', '$modalInstance', 'defaultErrorMessageResolver', 'activeTab', 'languageService',
    function(AuthService, UsersService, $rootScope, $scope, $window, $log, $filter, $timeout, $modal, growl, $facebook, $auth, serviceConfig, localStorageService, $state, $modalInstance, defaultErrorMessageResolver, activeTab, languageService) {

        $scope.activeTab = activeTab;

        if ($rootScope.authToken) {
            $scope.subTab = 'registerSocial';
        } else {
            $scope.subTab = 'registerDetails';
        }
        $scope.success = '';
        $scope.error = '';
        $scope.showForget = false;
        $scope.regProcesing = false;
        $scope.forgetPassProcesing = false;
        $scope.loginFrmShw = true;

        $scope.recoveryEmail = '';

        $scope.registerModel = {};

        $scope.registerModel = {
            gender: 'male'
        };

        /* login related */
        $scope.openRegisterModal = function() {
            var modalInstance = $modal.open({
                templateUrl: 'js/app/authentication/templates/registration.modal.view.html',
                controller: 'RegistrationModalCtrl',
                windowClass: 'registration-modal-window'
            });
            modalInstance.result.then(function(registerModel) {

            }, function() {});
        };

        // this will open the Forget Password Modal
        $scope.openForgetPasswordModal = function() {
            var modalInstance = $modal.open({
                templateUrl: 'js/app/authentication/templates/forgetpass.modal.view.html',
                controller: 'ForgetPassModalCtrl',
                windowClass: 'forget-modal-window'
            });
            modalInstance.result.then(function(registerModel) {

            }, function() {});
        };

        $scope.showForgetSec = function() {
            if ($scope.showForget === false) {
                $scope.showForget = true;
            } else {
                $scope.showForget = false;
            }
        };
        // Forget Password Form Handler
        $scope.forgetPass = function() {
            $log.debug('Called forgetpass Form');
        };

        $scope.$watch('success', function(newValue, oldValue) {
            //alert('hey, myVar has changed!');
            if (newValue) {
                growl.success($scope.success);
                $scope.success = null;
            }
        });

        $scope.$watch('error', function(newValue, oldValue) {
            if (newValue) {
                growl.error($scope.error, {
                    // ttl: 60000
                });
                $scope.error = null;
            }
        });

        $scope.loggedUser = {};

        //$scope.registerModel = {};
        var shouldRemberMe = true;
        $scope.loginModel = {
            rememberme: shouldRemberMe
        };

        $scope.handlePreLogin = function() {
            var token = localStorageService.get('authToken');
            var rememberme = localStorageService.get('rememberme');
            if (token && rememberme) { // && token) {
                $state.go('main.base.content.main.main');
            } else {
                $rootScope.logout('noModalOpen');
            }
        };

        $scope.handlePreLogin();
        $scope.loginErrorCount = 3;

        $scope.passMatch = function() {
            // console.log(angular.element('#verifypassword').val(), angular.element('#password').val());
            if (angular.element('#verifypassword').val() == '') {
                return false;
            } else if (angular.element('#verifypassword').val() !== angular.element('#password').val()) {
                $scope.passMismatch = true;
                setTimeout(function() {
                    angular.element('.help-block.has-error.error-msg').empty();
                    angular.element('#password').css('border-color', '#a94442');
                    angular.element('#password').css('box-shadow', 'inset 0 1px 1px rgba(0,0,0,.075)');
                    angular.element('#verifypassword').css('border-color', "#3c763d");
                    angular.element('#verifypassword').css('box-shadow', "inset 0 1px 1px rgba(0,0,0,.075)");
                }, 75);

            } else {
                angular.element('#password').css('border-color', '#3c763d');
                angular.element('#verifypassword').css('border-color', "#3c763d");
                $scope.passMismatch = false;
            }
        };
        $scope.conformPassMatch = function () {
            if(angular.element('#verifypassword').val() == angular.element('#password').val()){
                $scope.passMismatch = false;
                angular.element('#password').css('border-color', '#3c763d');
                    angular.element('#password').css('box-shadow', 'inset 0 1px 1px rgba(0,0,0,.075)');
            }
           
        };

        $scope.login = function() {

            /* AuthService.customerAccountAuthenticate($scope.loginModel).
             success(function (data, status) {
                 
                 $rootScope.user = data.list[0];
                 delete $rootScope.user.password;
                 
                 $rootScope.authToken = $rootScope.user.accountId;
                 if ($scope.loginModel.rememberme) {
                      //$cookieStore.put('authToken', $rootScope.authToken);
                      localStorageService.set('user', $rootScope.user);
                     // localStorageService.set('authToken', $rootScope.authToken);
                      localStorageService.set('rememberme', $scope.loginModel.rememberme);                        
                  }
                  $modalInstance.close($rootScope.user);
             }).
             error(function (data, status) {
                 if (status === 403) {
                     $scope.error = JSON.stringify(data.message);
                 } else {
                     $scope.error = 'Username and password not correct!'; // JSON.stringify(status);
                 }
                 //$modalInstance.close($scope.existingProject);
                 //$rootScope.logout();
             });*/

            AuthService.authenticate($scope.loginModel).
            success(function(data, status) {
                $scope.loginCountOnDashboard();
                var ele = angular.element('.growl-container.growl-fixed.top-right').children();
                if (ele.hasClass('alert-error')) {
                    ele.css('display', 'none');
                }
                //$scope.success = 'Successfully loged in'; //status;
                //console.log('data: ' + JSON.stringify(data) + ' status: ' + JSON.stringify(status));
                $rootScope.authToken = data.token;
                if ($scope.loginModel.rememberme) {
                    //$cookieStore.put('authToken', $rootScope.authToken);
                    localStorageService.set('authToken', $rootScope.authToken);
                    localStorageService.set('rememberme', $scope.loginModel.rememberme);
                }
                UsersService.getUser().
                success(function(data, status) {

                    if (GOWEB_CONF.analyticsId) {
                        GA('send', {
                            hitType: 'event',
                            eventCategory: 'landing',
                            eventAction: 'userLogin',
                            eventLabel: 'landingUserLogin'
                        });
                    }
                    $rootScope.user = data;
                    Intercom('boot', {
                        app_id: 'hyxd20av',
                        name: $rootScope.user.name, // Full name
                        email: $rootScope.user.accountName, // Email address,
                        language_override: languageService.getCurrent().id
                    });

                    //$location.path("/");
                    //$state.go('main.base.landingbase.main');
                    $rootScope.getSocialAccounts();
                    if ($scope.subTab !== 'registerSocial') {
                        $modalInstance.close($rootScope.user);
                    }
                }).
                error(function(data, status) {
                    $scope.error = JSON.stringify(status);
                    $rootScope.logout('noModalOpen');
                });
            }).
            error(function(data, status) {
                var ele = angular.element('.growl-container.growl-fixed.top-right').children();
                if (ele.hasClass('alert-error')) {
                    ele.css('display', 'none');
                }

                if (status === 403) {
                    $scope.error = JSON.stringify(data.message);
                } else if ($scope.loginErrorCount === 1) {
                    $scope.loginErrorCount = 3;
                    $scope.showForgetSec();
                    $scope.forgetPassProcesing = false;
                    $scope.recoveryEmail = $scope.loginModel.username;
                    angular.element('.frgBtn').removeAttr('disabled');
                    $scope.loginModel = '';
                    $scope.loginForm.$setUntouched();
                    $scope.loginForm.$setPristine();
                    $scope.loginFrmShw = false;

                    // $scope.error = 'Username and password not correct, click on the Sign up tab in the login dialog to register.';
                } else {
                    $scope.loginErrorCount--;
                    $scope.error = 'Login attempt failed, you have ' + $scope.loginErrorCount + ' more tries left';

                }
                $rootScope.logout('noModalOpen');
            });
            // }
        };

        $scope.loginCountOnDashboard = function() {
            var today = new Date();
            currntDt = today.toISOString().substring(0, 10),
                currntDtArr = currntDt.split("-"),
                pushObj = {
                    "data": [{
                        "Date": currntDtArr[0] + currntDtArr[1] + currntDtArr[2],
                        "Users": "1"
                    }],
                    "onduplicate": {
                        "Users": "add"
                    },
                    "color": {
                        "Users": "#52ff7f"
                    },
                    "type": {
                        "Users": "line"
                    }
                };

            AuthService.analyticsPush("https://app.cyfe.com/api/push/57bbf051363690698204902467010", pushObj);
        };

        defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
            errorMessages['username'] = 'Please ensure the passwords match.';
        });

        $scope.validateUsername = function() {
            //  $scope.registrationForm.$setDirty();
            $scope.registrationForm.username.$validate();
        };

        $scope.register = function() {
            $scope.regProcesing = true;
            $scope.registerModel.username = $.trim($scope.registrationForm.username.$viewValue);

            // if (GOWEB_CONF.invitationList && GOWEB_CONF.invitationList.length > 0 && GOWEB_CONF.invitationList.indexOf($scope.registerModel.username) === -1 && $scope.registerModel.username.indexOf("@bpuholdings.com") === -1) {
            //     $scope.regProcesing = false;
            //     $scope.error = $filter('translate')("We are so sorry, at this point the Beta is by invitation only, only authorised emails may register. If you would like to participate in our Beta program, please send email to beta@bpuholdings.com, include your full name, phone number and email address so we can review your participation or sign up at Zimgo.cloud to be put on the waiting list. Although approval can be denied at this time, we will add your name to the list of our next test group.");
            //     return;
            // }

            AuthService.register($scope.registerModel).
            success(function(data, status) {
                $scope.regProcesing = false;
                $scope.registrationForm.$invalid = false;
                $scope.success = 'Successfully register user.';
                setTimeout(function() {
                    $scope.subTab = 'registerSocial';
                    $scope.loginAfterRegister();
                    // $modalInstance.close();
                }, 2000);
            }).
            error(function(data, status) {
                $scope.regProcesing = false;
                $modalInstance.close();
                $scope.error = JSON.stringify(data.message);
            });
        };

        $scope.loginAfterRegister = function() {
            $scope.loginModel.username = $scope.registerModel.username;
            $scope.loginModel.password = $scope.registerModel.password;
            $scope.loginModel.rememberme = shouldRemberMe;
            $scope.login();
        };

        $scope.connectToTw = function() {
            var selectedLanguage = languageService.getCurrent(),
                lan = "";
            var authUrl = serviceConfig.getHost() + 'auth/twitter?lang=' + selectedLanguage.id + '&token=' + $rootScope.authToken;

            $auth.authenticate('twitter', {
                    token: $rootScope.authToken,
                    url: authUrl
                })
                .then(function(response) {
                    console.log("response:", response);
                    $window.location.reload();
                });
        };

        $scope.requestFbConnect = false;
        $scope.$on('fb.auth.authResponseChange', function() {
            $scope.status = $facebook.isConnected();
            if ($scope.status) {
                if ($scope.requestFbConnect) {
                    var authResponse = $facebook.getAuthResponse();
                    var accessToken = authResponse.accessToken;
                    $facebook.api('/me').then(function(user) {
                        UsersService.connectFbAccount({
                                accessToken: authResponse.accessToken,
                                expiresIn: authResponse.expiresIn,
                                userId: authResponse.userID,
                                name: user.name,
                                socialType: 'FB'
                            })
                            .success(function(data, status) {
                                window.location.reload();
                            })
                            .error(function(data, status) {
                                $scope.error = JSON.stringify(data.message);
                            });

                    });
                }

            }
        });

        $scope.connectToFb = function() {
            $scope.requestFbConnect = true;
            $facebook.login('email, public_profile, user_friends').then(function(response) {
                console.log("success login:", response);
                if (response && response.authResponse) {
                    $modalInstance.close($rootScope.user);
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            });
        };

        $scope.closeModal = function() {
            $scope.loginErrorCount = 3;
            $rootScope.logout('noModalOpen');
            $modalInstance.close($rootScope.user);
        };

        $scope.passwordRecoveryRequest = function() {
            $scope.forgetPassProcesing = true;

            if (!$scope.recoveryEmail) {
                $scope.error = 'Please provide the existing email to recover';
                return;
            }

            var emailObj = {
                email: $scope.recoveryEmail,
                language: languageService.getCurrent().id
            };

            AuthService.passwordRecoveryRequest(emailObj).
            success(function(data, status) {
                $scope.forgetPassProcesing = false;
                // show some message
                $scope.success = 'Please check your email for the new password.';
            }).
            error(function(data, status) {
                $scope.forgetPassProcesing = false;
                // show error message
                if (data)
                    $scope.error = data.message;
            });
        }

    }
]);
