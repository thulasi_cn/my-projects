var auth = angular.module('authentication');

auth.controller('RegistrationModalCtrl', ['AuthService', '$scope', '$timeout', '$modal', 'UsersService', 'growl', '$state', '$modalInstance', 'defaultErrorMessageResolver',
    function (AuthService, $scope, $timeout, $modal, UsersService, growl, $state, $modalInstance, defaultErrorMessageResolver) {

        $scope.success = '';
        $scope.error = '';
        
        $scope.registerModel = {
            gender: 'male'  
        };

        $scope.$watch('success', function (newValue, oldValue) {
            //alert('hey, myVar has changed!');
            if (newValue) {
                growl.success($scope.success, {
                    ttl: 5000
                });
                $scope.success = null;
            }
        });

        $scope.$watch('error', function (newValue, oldValue) {
            if (newValue) {
                growl.error($scope.error, {
                    ttl: 20000
                });
                $scope.error = null;
            }
        });

        defaultErrorMessageResolver.getErrorMessages().then(function (errorMessages) {
            errorMessages['username'] = 'Please ensure the passwords match.';
        });

        $scope.registerModel = {};

        $scope.validateUsername = function () {
            //  $scope.registrationForm.$setDirty();
            // $scope.registrationForm.username.$validate();
        };

        $scope.register = function () {

            $scope.registerModel.username = $scope.registrationForm.username.$viewValue;

            AuthService.register($scope.registerModel).
            success(function (data, status) {
                $scope.success = 'Successfully register user. Please login.'; //status;
                // console.log('data: ' + JSON.stringify(data) + ' status: ' + JSON.stringify(status));
                //$scope.loggedUser = data;
                setTimeout(function () {
                    $modalInstance.close();
                    //$state.go('login');
                }, 5000);
            }).
            error(function (data, status) {
                $modalInstance.close();
                $scope.error = JSON.stringify(data.message);
                //$rootScope.logout();
            });
        };
    }]);
