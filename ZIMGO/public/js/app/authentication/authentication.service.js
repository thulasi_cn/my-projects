var auth = angular.module('authentication');

auth.factory('AuthService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            authenticate: function (loginModel) {
                return $http.post(host + 'auth/authenticate',loginModel);
            },
            register: function (registrationModel) {
                return $http.post(host + 'auth/register',registrationModel);
            },
            changePassword: function(changePasswordModel){
                return $http.post(host + 'auth/changepassword',changePasswordModel);
            },
            customerAccountAuthenticate: function(loginModel){
                var hash = md5(loginModel.password);
                return $http.get(host + 'customer/login?userName='+loginModel.username+'&password='+hash);
            },
            verifyuserexists: function(username){
                return $http.post(host + 'auth/verifyuserexists', username);
            },
            passwordRecoveryRequest: function(emailObj){
                return $http.post(host + 'auth/passwordrecovery/request', emailObj);
            },
            analyticsPush: function(sendUrl, pushObj){
                 $.ajax({
                    type: "POST",
                    url : sendUrl,
                    data: JSON.stringify(pushObj),
                    dataType: 'json',
                    success: function(res){
                    },
                    error: function(res){
                    }
                });
            }
        }
}]);