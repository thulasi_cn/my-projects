var directives = angular.module('authentication');

directives.directive('confirmPassword', ['defaultErrorMessageResolver', function(defaultErrorMessageResolver) {
    defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
        errorMessages['confirmPassword'] = 'Please ensure the passwords match';
    });

    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            confirmPassword: '=confirmPassword'
        },
        link: function(scope, element, attributes, ngModel) {
            ngModel.$validators.confirmPassword = function(modelValue) {
                return modelValue === scope.confirmPassword;
            };

            scope.$watch('confirmPassword', function() {
                ngModel.$validate();
            });
        }
    };
}]);

directives.directive('verifyUserExistsAsync', ['defaultErrorMessageResolver', 'AuthService', '$q', function(defaultErrorMessageResolver, AuthService, $q) {


    var emailPattern = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;

    return {
        restrict: 'A',
        require: 'ngModel',
        scope: false,
        link: function(scope, element, attributes, ngModel) {

            element.on('blur', function(evt) {
                //  ngModel.$validate();
                // if (ngModel.$valid) {
                var modelValue = element.val();

                if (!modelValue) {
                    defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
                        errorMessages['verifyUserExists'] = 'This value is required!';
                    });

                    ngModel.$setValidity('verifyUserExists', false);
                    return false;
                }

                if (!emailPattern.test(modelValue)) {
                    defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
                        errorMessages['verifyUserExists'] = 'Provide username in email format!';
                    });
                    ngModel.$setValidity('verifyUserExists', false);
                    return false;
                }

                scope.$apply(function() {
                        AuthService.verifyuserexists(modelValue)
                            .success(function(data, status) {
                                defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
                                    errorMessages['verifyUserExistsAsync'] = 'Duplicate username, please try with some other username';
                                });
                                ngModel.$setValidity('verifyUserExistsAsync', false);
                            })
                            .error(function(data, status) {
                                /* console.log(JSON.stringify(data.message));
                                 ngModel.$setValidity('verifyUserExists', false);*/
                                //        reject();
                            });

                        // return defer.promise;
                    })
                    // };
            });

            /*  ngModel.$asyncValidators.verifyUserExistsAsync = function (modelValue, viewValue) {
                  var defer = $q.defer();

                  AuthService.verifyuserexists(viewValue)
                      .success(function (data, status) {
                          defaultErrorMessageResolver.getErrorMessages().then(function (errorMessages) {
                              errorMessages['verifyUserExistsAsync'] = 'Provided username already exists. Please use another';
                          });
                         resolve(!data);
                          if (!data) {
                              defer.resolve();
                              return modelValue;
                          } else {
                              defer.reject();
                              return undefined;
                          }
                     })
                      .error(function (data, status) {
                        reject();
                      });

                  return defer.promise;
              };*/
        }
    };
}]);

directives.directive('verifyUserExists', ['defaultErrorMessageResolver', 'AuthService', function(defaultErrorMessageResolver, AuthService) {


    var emailPattern = /^[a-z0-9._]+@[a-z\-]+\.[a-z.]{2,5}$/;

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attributes, ngModel) {
            ngModel.$validators.verifyUserExists = function(modelValue, viewValue) {
                if (!modelValue) {
                    defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
                        errorMessages['verifyUserExists'] = 'This value is required!';
                    });

                    ngModel.$setValidity('verifyUserExists', false);
                    return false;
                }

                if (!emailPattern.test(modelValue)) {
                    defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
                        errorMessages['verifyUserExists'] = 'Provide username in email format!';
                    });
                    ngModel.$setValidity('verifyUserExists', false);
                    return false;
                }

                AuthService.verifyuserexists(viewValue)
                    .success(function(data, status) {
                        if (data) {
                            var cLang = JSON.parse(localStorage.getItem('ls.language'));
                            defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
                                angular.element('#emailExist').css('display', 'none');
                                if (cLang.id === 'ko') {
                                    errorMessages['verifyUserExists'] = '중복된 사용자명입니다, 다른 사용자명으로 시도하십시오.';
                                } else if (cLang.id === 'en') {
                                    errorMessages['verifyUserExists'] = 'Duplicate username, please try with some other username.';
                                }
                            });
                            ngModel.$setValidity('verifyUserExists', !data);
                            return !data;
                        } else if (data === false) {
                            angular.element('#emailExist').css('display', 'block');
                        }

                    })
                    .error(function(data, status) {
                        // console.log(JSON.stringify(data.message));
                        ngModel.$setValidity('verifyUserExists', false);
                    });

            };
        }
    };
}]);

directives.directive('validatePasswordCharacters', ['defaultErrorMessageResolver', function(defaultErrorMessageResolver) {

    defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages) {
        errorMessages['passwordCharacters'] = '';
    });
    var REQUIRED_PATTERNS = [
        /\d+/, //numeric values
        /[a-z]+/, //lowercase values
        /[A-Z]+/, //uppercase values
        // /\W+/, //special characters
        /^\S+$/ //no whitespace allowed
    ];

    return {
        require: 'ngModel',
        link: function($scope, element, attrs, ngModel) {
            ngModel.$validators.passwordCharacters = function(value) {
                var status = true;
                angular.forEach(REQUIRED_PATTERNS, function(pattern) {
                    status = status && pattern.test(value);
                });
                return status;
            };
        }
    }
}]);
