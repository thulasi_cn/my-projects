// Forget Password Controller
var forget = angular.module('authentication');

forget.controller('ForgetPassModalCtrl', ['AuthService', '$scope', '$log', '$timeout', '$modal', 'UsersService', 'growl', '$state', '$modalInstance', 'defaultErrorMessageResolver',
    function(AuthService, $scope, $log, $timeout, $modal, UsersService, growl, $state, $modalInstance, defaultErrorMessageResolver) {

        $scope.success = '';
        $scope.error = '';

        // Success Message
        $scope.$watch('success', function(newValue, oldValue) {
            if (newValue) {
                growl.success($scope.success, {
                    ttl: 5000
                });
                $scope.success = null;
            }
        });

        // Error Message
        $scope.$watch('error', function(newValue, oldValue) {
            if (newValue) {
                growl.error($scope.error, {
                    ttl: 20000
                });
                $scope.error = null;
            }
        });

        // Forget Password Form Handler
        $scope.forgetPass = function() {
            $log.debug('Called forgetpass Form');
        };
    }
]);
