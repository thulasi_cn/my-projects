var policyModule = angular.module('policyModule', ['ui.router']);

policyModule.config(
    ['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('main.policy', {
                    abstract: true,
                    url: 'policy',
                    templateUrl: '/js/app/policy/templates/policy.base.view.html',
                    views: {
                        'navbar@main': {
                            templateUrl: '/js/app/policy/templates/policy.main.navigation.view.html',
                            controller: 'MainNavigationCtrl'
                        },
                        'content@main': {
                            templateUrl: '/js/app/policy/templates/policy.base.view.html',
                        }
                    }
                }).state('main.policy.en', {
                    url: '/en',
                    views: {
                        'content@main.policy': {
                            templateUrl: '/js/app/policy/templates/en_policy.html'
                        }                        
                    }
                })
                .state('main.policy.kop', {
                    url: '/ko_policy',
                    views: {
                        'content@main.policy': {
                            templateUrl: '/js/app/policy/templates/ko_policy.html'
                        }                        
                    }
                })
                .state('main.policy.kot', {
                    url: '/ko_terms',
                    views: {
                        'content@main.policy': {
                            templateUrl: '/js/app/policy/templates/ko_terms.html'
                        }                        
                    }
                }).state('main.policy.koprt', {
                    url: '/ko_protection',
                    views: {
                        'content@main.policy': {
                            templateUrl: '/js/app/policy/templates/ko_protection.html'
                        }                        
                    }
                })
        }
    ]);
