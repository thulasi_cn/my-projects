var mainModule = angular.module('mainModule', ['ui.router']);

mainModule.config(
  ['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

      if(GOWEB_CONF.getMode){
        //$urlRouterProvider.otherwise('/maintenance');
        $urlRouterProvider.when('/*', '/maintenance');
        $urlRouterProvider.when('/main', '/maintenance');
        $urlRouterProvider.when('/', '/maintenance');

      }else{
        $urlRouterProvider.otherwise('/');
      }


      $stateProvider
        .state('main', {
          abstract: true,
          url: '/',
          views: {
            '': {
              templateUrl: '/js/app/main/templates/main.abstract.view.html',
            }
          }
        })
        .state('main.base', {
          abstract: true,
          url: '',
          views: {
            'content': {
              templateUrl: '/js/app/main/templates/main.base.view.html'
            }
          }
        })

    }
  ]
);
