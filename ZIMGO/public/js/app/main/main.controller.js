var app = angular.module('mainModule');

app.controller('MainCtrl', ['$scope', '$rootScope', '$state', '$cookies', '$filter', '$translate', 'UsersService','ngJws','$timeout',
    function ($scope, $rootScope, $state, $cookies, $filter, $translate, UsersService,ngJws,$timeout) {

    	$scope.$on('load-tour', function(event, args) {
    		$scope.callTour();
		});

		$scope.CompletedEvent = function (scope) {
            console.log("Completed Event called");
          };

          $scope.ExitEvent = function (scope) {
          	document.querySelector("#step-langChng").classList.remove('changeColor');
          	document.querySelector("#step-zzimbang").classList.remove('changeColor');
          };

          $scope.ChangeEvent = function (targetElement, scope) {
              document.querySelector("#step-langChng").classList.remove('changeColor');
              document.querySelector("#step-zzimbang").classList.remove('changeColor');
          };

          $scope.BeforeChangeEvent = function (targetElement, scope) {
          	$rootScope.tourInstance = this;
          };

          $scope.AfterChangeEvent = function (targetElement, scope) {
              document.querySelector("#"+targetElement.id).classList.add('changeColor');
          };

          $scope.filterArr = [
	          "Change your display language",
	          "Go to Zzimbang section to find your saved searches and organise them into categories",
	          "Search social platforms for the information you are looking for",
	          "Filter search results by one or more selected social platforms",
	          "Overall sentiment score of the search results",
	          "Topics related to search results",
	          "Trending phrases within the search results",
	          "Opinion leaders within the search results",
	          "Videos and images within the search results"
          ];

          $scope.callFilter = function(str){
          		return ($filter('translate')(str));
			};

          $scope.callFilter();

        	$scope.callTour = function(){
        		$timeout(function(){
        			if($state.current.name === 'main.base.content.main.main'){
	        			$scope.IntroOptions = {
				        	steps:[
					          {
					              element: document.querySelector('#step-langChng'),
					              intro: $scope.callFilter($scope.filterArr[0]),
					              position: 'bottom'
					          },
					          {
					              element: document.querySelector('#step-zzimbang'),
					              intro: $scope.callFilter($scope.filterArr[1]),
					              position: 'bottom'
					          },
					          {
					              element: document.querySelector('#step-search'), 
					              intro: $scope.callFilter($scope.filterArr[2]),
					              position: 'bottom'
					          },
					          {
					              element: document.querySelector('#step-social'), 
					              intro: $scope.callFilter($scope.filterArr[3]),
					              position: 'bottom'
					          },
					          {
					              element: document.querySelector('#step-sentiment'), 
					              intro: $scope.callFilter($scope.filterArr[4]),
					              position: 'bottom'
					          },
					          {
					              element: document.querySelector('#step-topics'), 
					              intro: $scope.callFilter($scope.filterArr[5]),
					              position: 'bottom'
					          },
					          {
					              element: document.querySelector('#step-phrase'), 
					              intro: $scope.callFilter($scope.filterArr[6]),
					              position: 'bottom'
					          },
					          {
					              element: document.querySelector('#step-leaders'), 
					              intro: $scope.callFilter($scope.filterArr[7]),
					              position: 'bottom'
					          },
					          {
					              element: document.querySelector('#step-media'), 
					              intro: $scope.callFilter($scope.filterArr[8]),
					              position: 'bottom'
					          }
					        ],
					          showStepNumbers: false,
					          exitOnOverlayClick: true,
					          exitOnEsc: true,
					          nextLabel: '<strong>'+$filter('translate')('Continue')+'</strong>',//Continue!
					          prevLabel: '<span style="color:green">'+$filter('translate')('Previous')+'</span>',//Previous
					          skipLabel: $filter('translate')('End tour'),//Exit
					          doneLabel: $filter('translate')('Thanks')//Thanks
					      };
					  }
        		}, 1000);

        		
    			$rootScope.callInit = function(funcObj){
    				$timeout(function(){
	    				if(!$cookies.getObject("autoTour") || $cookies.getObject("autoTour") === false){
		    					$cookies.putObject("autoTour", true);
		    					if($state.current.name === 'main.base.content.main.main'){
			    					funcObj();
			    				}
		    			}
					},2000);
				};
			};
        			
        	
       
}]);