var usersModule = angular.module('usersModule', ['ui.router', 'service.config']);

usersModule.config(
  ['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('main.users', {
                    abstract: true,
                    url: 'user',
                    templateUrl: '/js/app/users/templates/user.base.view.html',
                    views:{
                        'navbar@main': {
                            templateUrl: '/js/app/users/templates/user.main.navigation.view.html'
                        },
                        'content@main': {
                            templateUrl: '/js/app/users/templates/user.base.view.html',
                            controller: 'UserBaseCtrl'
                        }
                    }
                })
                .state('main.users.base',{
                    abstract: true,
                    url: '', views:{
                        'search@main': {
                            templateUrl: '/js/app/users/templates/user.head.view.html',
                            controller: 'UserHeadCtrl'
                        }
                    }
                })
                // .state('main.users.base.manage', {
                //     url: '/manage',
                //     views: {
                //         'content@main': {
                //             templateUrl: '/js/app/users/templates/user.manage.view.html',
                //             controller: 'UsersManageController'
                //         }
                //     }
                // })
                .state('main.users.base.profile', {
                    url: '/profile',
                    views: {
                        'content@main.users': {
                            templateUrl: '/js/app/users/templates/user.profile.view.html',
                            controller: 'UserProfileCtrl'
                        },
                        'leftuser@main.users.base.profile':{
                            templateUrl: '/js/app/users/templates/user.profile.leftuser.view.html'
                        },
                        'content@main.users.base.profile':{
                            templateUrl: '/js/app/users/templates/user.profile.content.view.html'
                        }
                    }
                })
            //     .state('main.roles', {
            //         abstract: true,
            //         url: 'roles',
            //         templateUrl: '/js/common/templates/base.html'
            //     })
            //     .state('main.roles.manage', {
            //         url: '/manage',
            //         views: {
            //             'main@main': {
            //                 templateUrl: '/js/app/users/templates/roles.manage.view.html',
            //                 controller: 'RolesManageController'
            //             }
            //         }
            //     })
            // .state('main.accounts', {
            //         abstract: true,
            //         url: 'accounts',
            //         templateUrl: '/js/common/templates/base.html'
            //     })
            //     .state('main.accounts.manage', {
            //         url: '/manage',
            //         views: {
            //             'main@main': {
            //                 templateUrl: '/js/app/users/templates/account.manage.view.html',
            //                 controller: 'AccountManageController'
            //             }
            //         }
            //     })
    }]);
