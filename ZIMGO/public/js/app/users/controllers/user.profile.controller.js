var app = angular.module('usersModule');

app.controller('UserProfileCtrl', ['UsersService', 'UsersConenctService', 'AuthService', '$scope', '$log', '$rootScope', '$controller', '$state',
    '$facebook', '$auth', 'growl', '$window', '$timeout', 'serviceConfig', '$timeout', 'languageService', 'AuthService',
    function(UsersService, UsersConenctService, AuthService, $scope, $log, $rootScope, $controller, $state, $facebook, $auth, growl, $window, $timeout, serviceConfig, timeout, languageService, AuthService) {
        if (GOWEB_CONF.analyticsId) {
            //GA('send', 'profilePageview');
            //GA('send', 'event', [eventCategory], [eventAction], [eventLabel], [eventValue], [fieldsObject]);
            GA('send', {
                hitType: 'event',
                eventCategory: 'profiles',
                eventAction: 'view',
                eventLabel: 'ProfilesHit'
            });

        }
        $scope.success = '';
        $scope.error = '';
        $scope.warning = '';
        $scope.activeContent = 'sns';
        $scope.changePasswordModel = {};
        $scope.formProcessing = false;
        $scope.issocialCOnnectPopoverOpen = false;

        $scope.$watch('success', function(newValue, oldValue) {
            //alert('hey, myVar has changed!');
            if (newValue) {
                growl.success($scope.success, { ttl: 3000 });
                $scope.success = null;
            }
        });

        $scope.$watch('error', function(newValue, oldValue) {
            if (newValue) {
                growl.error($scope.error, { ttl: 5000 });
                $scope.error = null;
            }
        });

        $controller('MainNavigationCtrl', { $scope: $scope }); //This works

        $scope.openPasswordForm = function() {
            $scope.activeContent = 'changePassword';
        };

        $scope.closeForm = function() {
            $scope.activeContent = 'sns';
            $scope.changePasswordModel = {};
            $scope.formProcessing = false;
        };


        $scope.changePass = function(theFrmRef) {
            $scope.formProcessing = true;

            AuthService.changePassword($scope.changePasswordModel).
            success(function(data, status) {
                $scope.formProcessing = false;
                $scope.success = 'Password changed. You\'ll be redirected to the login with new password';
                $scope.changePasswordModel = {};

                theFrmRef.$setPristine();
                theFrmRef.$setUntouched();

                $scope.activeContent = 'sns';
                $timeout(function() { $rootScope.logout('modalOpen'); }, 3000);
            }).
            error(function(data, status) {
                $scope.formProcessing = false;
                $scope.changePasswordModel = {};

                theFrmRef.$setPristine();
                theFrmRef.$setUntouched();

                $scope.error = 'Wrong credentials, please try again';

            });
        };

        $scope.socialAccounts = {};

        $scope.handleSocialAccounts = function(data) {
            if (data && data.length > 0) {
                $scope.socialAccounts = _.groupBy(data, 'socialType');

                if (!$scope.socialAccounts.TW) {
                    // $scope.warning = 'Please connect Zimgo search with your Twitter account';
                    // growl.warning($scope.warning);
                } else if (!$scope.socialAccounts.FB) {
                    // $scope.warning = 'Please connect Zimgo search with your Facebook account';
                    // growl.warning($scope.warning);
                } else {
                    $scope.error = null;
                }
            } else {
                $scope.socialAccounts = {};
                $scope.error = 'Please connect to Twitter and Facebook';
                growl.error($scope.error, { ttl: 10000 });
        var ele = angular.element('.growl-container.growl-fixed.top-right').children();
                if (ele.hasClass('alert-warning')){
                    ele.css('display', 'none');
                }

            }

            $rootScope.getSocialAccounts(data);
        };

        $rootScope.getSocialAccounts();

        $scope.getSocialAccounts = function() {
            UsersService.getSocialAccounts()
                .success(function(data, status) {
                    $scope.handleSocialAccounts(data);
                })
                .error(function(data, status) {
                    $scope.error = JSON.stringify(data.message);
                });
        };

        $scope.requestFbConnect = false;

        $scope.getSocialAccounts();

        $scope.$on('fb.auth.authResponseChange', function() {
            $scope.status = $facebook.isConnected();
            if ($scope.status) {
                if ($scope.requestFbConnect) {
                    var authResponse = $facebook.getAuthResponse();
                    var accessToken = authResponse.accessToken;
                    $facebook.api('/me').then(function(user) {
                        UsersService.connectFbAccount({
                                accessToken: authResponse.accessToken,
                                expiresIn: authResponse.expiresIn,
                                userId: authResponse.userID,
                                name: user.name,
                                socialType: 'FB'
                            })
                            .success(function(data, status) {
                                $scope.handleSocialAccounts(data);
                                $window.location.reload();
                            })
                            .error(function(data, status) {
                                //$scope.error = JSON.stringify(data.message);
                            });

                    });
                }

            }
        });

        $scope.toggleSocialPopover = function() {
            $scope.issocialCOnnectPopoverOpen = !$scope.issocialCOnnectPopoverOpen;
        };

        $scope.disconnectFbAccount = function() {
            var account = $scope.socialAccounts.FB;
            UsersService.disconnectFbAccount({
                    userId: account[0].socialUserId,
                    socialType: 'FB'
                })
                .success(function(data, status) {
                    $scope.handleSocialAccounts(data);
                })
                .error(function(data, status) {
                    $scope.error = JSON.stringify(data.message);
                });

            $scope.requestFbConnect = false;
            $facebook.logout();
        }
        $scope.connectFbAccount = function() {
            $scope.requestFbConnect = true;
            $facebook.login('email, public_profile, user_friends'); //manage_pages');
        };

        $scope.disconnectTwAccount = function() {
            var twAccount = $scope.socialAccounts.TW;
            UsersService.disconnectTwAccount({
                    userId: twAccount[0].socialUserId,
                    socialType: 'TW'
                }).success(function(data, status) {
                    $scope.handleSocialAccounts(data);
                })
                .error(function(data, status) {
                    $scope.error = JSON.stringify(data.message);
                });
        }
        $scope.connectTwAccount = function() {
            var selectedLanguage = languageService.getCurrent(),
                lan = "";
            var authUrl = serviceConfig.getHost() + 'auth/twitter?lang=' + selectedLanguage.id + '&token=' + $rootScope.authToken;
            /*,
                 url: authUrl,
                 loginRedirect: '/#/user/profile'*/
            console.log("authUrl:", authUrl);
            $auth.authenticate('twitter', {
                    token: $rootScope.authToken,
                    url: authUrl
                })
                .then(function(response) {
                    console.log("response:", response);
                    $window.location.reload();
                });

        };

        $scope.disconnectAccount = function(socialNetwork) {
            switch (socialNetwork) {
                case 'TW':
                    $scope.disconnectTwAccount();
                    break;
                case 'FB':
                    $scope.disconnectFbAccount();
                    break;
                default:

            }
        };

        $scope.connectAccount = function(socialNetwork) {
            switch (socialNetwork) {
                case 'TW':
                    $scope.connectTwAccount();
                    break;
                case 'FB':
                    $scope.connectFbAccount();
                    break;
                default:
                    break;

            }
        };


        $scope.changePassword = function() {

            var modalOptions = {
                templateUrl: 'js/app/users/templates/user.profile.change.password.view.html',
                controller: 'UserProfileChangePasswordController',
                size: 'lg'
            };
            var modalInstance = $modal.open(modalOptions);

            modalInstance.result
                .then(function(selectedItem) {
                        //$rootScope.logout();
                        AuthService.changePassword(selectedItem).
                        success(function(data, status) {
                            $scope.success = 'Successfully updated password. You will be redirected to login...'; //status;

                            $timeout(function() {
                                $rootScope.logout();
                                //$state.go('login');
                            }, 3000);
                        }).
                        error(function(data, status) {
                            $scope.error = JSON.stringify(data.message);
                        });
                    },

                    function() {
                        $log.debug('Modal dismissed at: ' + new Date());
                    });
        };

    }
]);
