var usersModule = angular.module('usersModule');

usersModule.controller('UserProfileChangePasswordController', ['UsersService', '$rootScope', '$scope', 'growl', 'AuthService', '$state', '$window', '$modal', '$modalInstance',
    function (UsersService, $rootScope, $scope, growl, AuthService, $state, $window, $modal, $modalInstance) {

        $scope.changePasswordModel = {};
        $scope.updatePassword = function(){
            $scope.ok();
        };

        $scope.ok = function () {
            $modalInstance.close($scope.changePasswordModel);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }]);
