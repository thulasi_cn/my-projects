var usersModule = angular.module('usersModule');


usersModule.factory('UsersConenctService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            connectFbAccount: function(data){
                return $http.post(host+'user/social/fb', data);
            },
            connectTwAccount: function(){
                return $http.post(host+'user/social/tw');
            },
            disconnectFbAccount: function(data){
                return $http.post(host+'user/social/fb/disconnect', data);
            },
            disconnectTwAccount: function(data){
                return $http.post(host+'user/social/tw/disconnect', data);
            }
        }
    }]);

usersModule.factory('UsersService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost(),
            checkModalStatus;
        return {
            getUser: function(){
                 return $http.get(host + 'user/');
            },
            getSocialAccounts: function(){
                return $http.get(host+'user/social');
            },
            connectFbAccount: function(data){
                return $http.post(host+'user/social/fb', data);
            },
            connectTwAccount: function(){
                return $http.post(host+'user/social/tw');
            },
            disconnectFbAccount: function(data){
                return $http.post(host+'user/social/fb/disconnect', data);
            },
            disconnectTwAccount: function(data){
                return $http.post(host+'user/social/tw/disconnect', data);
            },
            getFilteredUsers:  function(from, limit, filter){
                fl = prepareFilterQueryString(filter);
                return $http.get(host + 'user/all?from=' + from + '&limit=' + limit + fl);
            },
            createUser: function(userData){
                return $http.post(host + 'user/', userData);
            },
            updateUser: function(userData){
                return $http.put(host + 'user/', userData);
            },
            removeUser: function(userId){
                return $http.delete(host + 'user/'+userId);
            },
            logoutType: function(val){
                checkModalStatus = val;
            },
            getRedirectStatus: function(){
                return checkModalStatus;
            }
        }
    }]);

usersModule.factory('AccountService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            getAccounts: function(){
                 return $http.get(host + 'user/account');
            },
            createAccount: function(data){
                return $http.post(host + 'user/account', data);
            },
            removeAccount: function(id){
                return $http.delete(host + 'user/account/'+id);
            },
            updateAccount: function(data){
                return $http.put(host + 'user/account/', data);
            }
        }
    }]);

usersModule.factory('RolesService', ['$http', 'serviceConfig',
    function ($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            getRoles: function(){
                 return $http.get(host + 'user/roles');
            },
            createRole: function(roleData){
                return $http.post(host + 'user/roles', roleData);
            },
            removeRole: function(roleId){
                return $http.delete(host + 'user/roles/'+roleId);
            }
        }
    }]);
