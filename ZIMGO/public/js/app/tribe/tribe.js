var tribeModule = angular.module('tribeModule', ['ui.router']);

tribeModule.config(
    ['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state('main.base.tribe', {
                    abstract: true,
                    url: 'tribe',
                    views: {
                        'content': {
                            templateUrl: '/js/app/main/templates/content/main.base.content.view.html',
                            controller: 'SearchContentResultBaseCtrl'
                        },
                        'navbar@main': {
                            templateUrl: '/js/app/navigation/templates/tribe.navigation.view.html',
                            controller: 'MainTribeNavigationCtrl'
                        }
                    }
                })
                .state('main.base.tribe.main', {
                    url: '',
                    abstract: true,
                    views: {
                        'main@main.base.tribe': {
                            templateUrl: '/js/app/tribe/templates/main.tribe.base.view.html'
                        }
                    }
                })
                .state('main.base.tribe.main.main', {
                    url: '',
                    views: {
                        '@main.base.tribe.main': {
                            templateUrl: '/js/app/tribe/templates/tribe/tribe.result.main.view.html',
                            controller: 'TribeContentResultMainCtrl'
                        }
                    }
                })
                .state('main.base.tribe.main.main.tribeslist', {
                    url: '/top',
                    views: {
                        'tribeMain': {
                            templateUrl: '/js/app/tribe/templates/tribe/tribe.main.topTen.list.view.html',
                            controller: 'TopTenTribeResultCtrl'
                        }
                    }
                })
                .state('main.base.tribe.main.main.searchResult', {
                    url: '/searchResult',
                    views: {
                        'tribeMain': {
                            templateUrl: '/js/app/tribe/templates/tribe.search.result.view.html',
                            controller: 'SearchResultCtrl'
                        }
                    }
                })
                .state('main.base.tribe.main.main.categorie', {
                    url: '/categories/:categorieID',
                    views: {
                        'tribeMain': {
                            templateUrl: '/js/app/tribe/templates/tribe/tribe.categorie.tribes.view.html',
                            controller: 'TribeCategorieCtrl'
                        }
                    }
                })
                .state('main.base.tribe.main.main.categorie.tribes', {
                    url: '/tribeProfile/:tribeId',
                    views: {
                        '@main.base.tribe.main': {
                            templateUrl: '/js/app/tribe/templates/tribe/tribe.profile.view.html',
                            controller: 'TribeClanListCtrl'
                        }
                    }
                })
                /*.state('main.base.tribe.main.main.tribes.clans', {
                    url: '/tirbeClans',
                    views: {
                        'tribeProfileMain': { 
                            templateUrl: '/js/app/tribe/templates/tribe/categories/tribe.clans.view.html',
                            controller: 'TribeClansController'
                        }
                    }
                })
                .state('main.base.tribe.main.main.tribes.tribemans', {
                    url: '/tribeMans',
                    views: {
                        'tribeProfileMain': { 
                            templateUrl: '/js/app/tribe/templates/tribe/categories/tribe.tribemans.view.html',
                            controller: 'TribeTribemansController'
                        }
                    }
                })
                .state('main.base.tribe.main.main.tribes.photos', {
                    url: '/tribePhotos',
                    views: {
                        'tribeProfileMain': { 
                            templateUrl: '/js/app/tribe/templates/tribe/categories/tribe.photos.view.html',
                            controller: 'TribePhotosController'
                        }
                    }
                })
                .state('main.base.tribe.main.main.tribes.events', {
                    url: '/tribeEvents',
                    views: {
                        'tribeProfileMain': { 
                            templateUrl: '/js/app/tribe/templates/tribe/categories/tribe.events.view.html',
                            controller: 'TribeEventsController'
                        }
                    }
                })*/
                .state('main.base.tribe.main.main.categorie.tribes.clan', {
                    url: '/clan/:clanId',
                    views: {
                        '@main.base.tribe.main': {
                            templateUrl: '/js/app/tribe/templates/clan/clan.main.view.html',
                            controller: 'TribeClanCtrl'
                        }
                    }
                })
                .state('main.base.tribe.main.main.categorie.tribes.clan.post', {
                    url: '/post',
                    views: {
                        'clanMain': {
                            templateUrl: '/js/app/tribe/templates/clan/categories/clan.post.view.html'
                        }
                    }
                }).state('main.base.tribe.main.main.categorie.tribes.clan.tribemans', {
                    url: '/tribemans',
                    views: {
                        'clanMain': {
                            templateUrl: '/js/app/tribe/templates/clan/categories/clan.tribemans.view.html'
                        }
                    }
                }).state('main.base.tribe.main.main.categorie.tribes.clan.photos', {
                    url: '/photos',
                    views: {
                        'clanMain': {
                            templateUrl: '/js/app/tribe/templates/clan/categories/clan.photos.view.html'
                        }
                    }
                }).state('main.base.tribe.main.main.categorie.tribes.clan.events', {
                    url: '/events',
                    views: {
                        'clanMain': {
                            templateUrl: '/js/app/tribe/templates/clan/categories/clan.events.view.html'
                        }
                    }
                }).state('main.base.tribe.main.main.categorie.tribes.clan.postDetails', {
                    url: '/post/:postID',
                    views: {
                        'clanMain': {
                            templateUrl: '/js/app/tribe/templates/clan/categories/clan.post.details.view.html'
                        }
                    }
                })
                .state('main.base.tribe.main.profile', {
                    url: '/loggedInTribeman/:tribemanId',
                    views: {
                        '@main.base.tribe.main': {
                            templateUrl: '/js/app/tribe/templates/profile/profile.view.html',
                            controller: 'TribeProfileCtrl'
                        }
                    }
                }).state('main.base.tribe.main.friendProfile', {
                    url: '/tribemanFriend/:tribemanId',
                    views: {
                        '@main.base.tribe.main': {
                            templateUrl: '/js/app/tribe/templates/profile/profile.friend.profile.view.html',
                            controller: 'FriendProfileCtrl'
                        }
                    }
                });
        }
    ]
);
