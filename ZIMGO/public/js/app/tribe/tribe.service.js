var tribeModule = angular.module('tribeModule');

tribeModule.factory('tribeService', ['$http', 'serviceConfig',
    function($http, serviceConfig) {
        var host = serviceConfig.getHost();
        return {
            /*----------   Tribeman    ----------*/
            loggedInTribeman: function() {
                var url = host + 'v2_1/tribemans/logedUser';
                return $http.get(url);
            },
            getTribemanDetails: function(tribemanId) {
                var url = host + 'v2_1/tribemans/' + tribemanId;
                return $http.get(url);
            },
            getTribemanFriendsCount: function(tribemanId) {
                var url = host + 'v2_1/tribemans/friends/' + tribemanId;
                return $http.get(url);
            },
            getTribemanPictureCount: function(tribemanId) {
                var url = host + 'v2_1/tribemans/picturesCount/' + tribemanId;
                return $http.get(url);
            },
            loggedInTribemanProfileUpdate: function(tribmanId, obj) {
                var url = host + 'v2_1/tribemans/' + tribmanId;
                return $http.post(url, obj);
            },
            addFriend: function(friendId) {
                var url = host + 'v2_1/tribemans/friend/' + friendId;
                var obj = { "friendId": friendId };
                return $http.post(url, obj);
            },
            unFriend: function(friendId) {
                var url = host + 'v2_1/tribemans/friend/' + friendId;
                return $http.delete(url);
            },
            sendMessage: function(messageObj) {
                var url = host + 'v2_1/messages';
                return $http.post(url, messageObj);
            },
            getAllLoggedInUserMessage: function() {
                var url = host + 'v2_1/messages';
                return $http.get(url);
            },
            getActiveTribemansByTribe: function(tribeId) {
                var url = host + 'v2_1/memberships/tribe?tribeId=' + tribeId;
                return $http.get(url);
            },
            getTopContributors: function() {
                var url = host + 'v2_1/memberships/tribe/user/contributors';
                return $http.get(url);
            },

            /*----------   Category    ----------*/
            createCategory: function(catObj) {
                var url = host + 'v2_1/tribes/tribecategory';
                return $http.post(url, catObj);
            },
            getCategory: function(limit) {
                var url = host + 'v2_1/tribes/tribecategory?limit=' + limit;
                return $http.get(url);
            },
            getRandomCategory: function() {
                var url = host + 'v2_1/tribes/tribecategory/unique';
                return $http.get(url);
            },
            getOneCategory: function(catId) {
                /*var filter = { 'CATEGORY_NAME': catName },
                    limit = 1,
                    fl = prepareFilterQueryString(filter);*/
                var url = host + 'v2_1/tribes/tribecategory/' + catId;
                return $http.get(url);
            },
            getTribesCatAndTribeman: function(tribemanId, catId) {
                // var url = host + 'v2_1/tribes/tribemanId=' + tribemanId + '&categoryId=' + catId;
                var url = host + 'v2_1/tribes/list/' + tribemanId + '/' + catId;
                return $http.get(url);
            },

            /*----------   Tribe    ----------*/
            searchTribe: function(tribeObj) {
                console.log(tribeObj);
                var filter = { "keyword": tribeObj, "postName": "Test Post clan12" }; //{ "tribeName": tribeObj.tribeSearch }
                //{ "keyword": "city", "limit": 10, "postName": '', "tribeName":tribeObj.tribeSearch };
                var fl = prepareFilterQueryString(filter);
                var url = host + 'v2_1/tribesearch?' + fl;
                return $http.get(url, tribeObj);
            },
            createTribes: function(tribeObj) {
                var url = host + 'v2_1/tribes';
                return $http.post(url, tribeObj);
            },
            getTopTribes: function(limit, lastRow, filter) {
                var url = host + 'v2_1/tribes/top';
                return $http.get(url);
            },
            getTribesByCatId: function(limit, categoryId) {
                var filter = { "tribe_category_id": categoryId },
                    fl = prepareFilterQueryString(filter),
                    url = host + 'v2_1/tribes?limit=' + limit + fl;
                return $http.get(url);
            },
            getTribeByTribe: function(tribeId) {
                var url = host + 'v2_1/tribes/information/' + tribeId;
                return $http.get(url);
            },
            getTribe: function(limit) {
                var url = host + 'v2_1/tribes?limit=' + limit;
                return $http.get(url);
            },
            getTribeByTribeId: function(tribeId) {
                var url = host + 'v2_1/tribes/' + tribeId;
                return $http.get(url);
            },
            getRelatedTribeByTribe: function(tribe) {
                var url = host + 'v2_1/tribes/related/' + tribe.tribeId;
                return $http.get(url);
            },
            /* deleteTribe : function () {
                 var url = host + 'v2_1/tribes/tribe1/85f7a2ca-4ef5-4e37-819d-d986ef791de3';
                 return $http.delete(url);
             },*/
            getTribeMansByTribe: function(tribeName) {
                var url = host + 'v2_1/tribemans/tribe?tribeName=' + tribeName;
                return $http.get(url);
            },
            /*getPicturesByTribe: function(limit, tribeId) {
                var filter = { "tribe_Id": tribeId },
                    fl = prepareFilterQueryString(filter);

                var url = host + 'v2_1/tribes/events/tribe?limit=' + limit + fl;
                return $http.get(url);
            },*/
            getEventsByTribe: function(tribeName) {
                var url = host + 'v2_1/tribemans/tribe?tribeName=' + tribeName;
                return $http.get(url);
            },
            createTribeEvent: function(eventObj) {
                var url = host + 'v2_1/tribes/events';
                return $http.post(url, eventObj);
            },
            getTribeEvents: function(limit) {
                var url = host + 'v2_1/tribes/events?limit=' + limit;
                return $http.get(url);
            },
            getActiveTribemens: function(limit) {
                var url = host + 'v2_1/memberships/tribe/user/activity?isActive=true&limit=' + limit;
                return $http.get(url);
            },
            getTribesByTribemanId: function(tribemanId) {
                var url = host + 'v2_1/memberships/tribe/user?tribemanId=' + tribemanId;
                return $http.get(url);
            },
            checkJoinTribe: function(tribeId) {
                var url = host + 'v2_1/memberships/checkt/' + tribeId;
                return $http.get(url);
            },
            unJoinTribe: function(tribeId) {
                var url = host + 'v2_1/memberships?tribeId=' + tribeId;
                return $http.delete(url);
            },
            /*----------   Clan    ----------*/
            joinClan: function(joinObj) {
                var url = host + 'v2_1/memberships';
                return $http.post(url, joinObj);
            },
            checkJoinClan: function(clanId) {
                var url = host + 'v2_1/memberships/check/' + clanId;
                return $http.get(url);
            },
            unJoinClan: function(clanId) {
                var url = host + 'v2_1/memberships?clanId=' + clanId;
                return $http.delete(url);
            },
            createClan: function(clanObj) {
                var url = host + 'v2_1/clans';
                return $http.post(url, clanObj);
            },
            getClanById: function(clanId) {
                var url = host + 'v2_1/clans/' + clanId;
                return $http.get(url);
            },
            getClansByTribe: function(limit, tribeId) {
                var filter = { "tribe_Id": tribeId },
                    fl = prepareFilterQueryString(filter),
                    url = host + 'v2_1/clans?limit=' + limit + fl;
                return $http.get(url);
            },
            getClansByTribeman: function(tribemanId) {
                // var filter = { "created_by_username": tribemanName },
                //     fl = prepareFilterQueryString(filter),
                var url = host + 'v2_1/memberships/clan/user?tribemanId=' + tribemanId;
                return $http.get(url);
            },
            // clanUpdate: function() {
            //     var url = host + 'v2_1/clans/clan2/f13e53c6-d29e-43b1-a10f-7700b1d6e1cf/' + "tribeId=fc4fd91d-656e-4d01-97d1-2d4ca90b2788";
            //     return $http.put(url);
            // },
            // deleteClan: function() {
            //     var url = host + 'v2_1/clans/first clan/2a0f796d-99cf-434d-b7df-51044f534952';
            //     return $http.delete(url);
            // },
            createClanPost: function(postObj, clanObj) {
                var url = host + 'v2_1/post/' + clanObj.clanId;
                return $http.post(url, postObj);
            },
            getPostsByClan: function(clanObj) {
                var url = host + 'v2_1/posts/byClan/' + clanObj.clanId + '?limit=' + 10;
                return $http.get(url);
            },
            getPostsByLoggedInUser: function() {
                var url = host + 'v2_1/tribemans/userFeed';
                return $http.get(url);
            },
            deleteClanPost: function(postObj) {
                var url = host + 'v2_1/posts/remove';
                return $http({ method: 'DELETE', url: url, data: postObj, headers: { 'Content-type': 'application/json;charset=utf-8' } });

            },
            createPostComment: function(postCommentObj) {
                var url = host + 'v2_1/posts/comments';
                return $http.post(url, postCommentObj);
            },
            getClanPostComment: function(postId) {
                var url = host + 'v2_1/posts/' + postId + '/comments';
                return $http.get(url);
            },

            deletePostComment: function(postId, commentId) {
                var url = host + 'v2_1/posts/' + postId + '/comments/' + commentId;
                return $http.delete(url);
            },
            updatePostComment: function(commentObj) {
                var postId = commentObj.postId,
                    commentId = commentObj.commentId,
                    updateCommentString = 'COMMENT=' + commentObj.comment + ',POST_COMMENT_SENTIMENT=' + commentObj.postCommentSentiment + ',POST_COMMENT_MOOD=' + commentObj.postCommentMood;
                var url = host + 'v2_1/posts/' + postId + '/comments/' + commentId + '/' + updateCommentString;
                return $http.put(url);
            },
            likeComment: function(likeObj) {
                var url = host + 'v2_1/comments/likes';
                return $http.post(url, likeObj);
            },
            replyComment: function(cmtObj) {
                var url = host + 'v2_1/comments/'+cmtObj.cmtId;
                return $http.post(url, cmtObj);
            },
            getCommentReplys: function(cmtObj){
                var url = host + 'v2_1/posts/' + cmtObj.postId + '/'+cmtObj.commentId;
                return $http.get(url);
            },
            postRating: function(ratingsObj) {
                var url = host + 'v2_1/posts/ratings';
                return $http.post(url, ratingsObj);
            },
            createClanEvent: function(eventObj) {
                var url = host + 'v2_1/clans/events';
                return $http.post(url, eventObj);
            },
            getClanEvents: function(limit, clanId) {
                var filter = { "clan_id": clanId },
                    fl = prepareFilterQueryString(filter);
                var url = host + 'v2_1/clans/events?limit=' + limit + fl;
                return $http.get(url);
            },
            // createTribeMans: function() {
            //     var url = host + 'v2_1/tribemans';
            //     return $http.post(url, {
            //         "tribemanName": "tribeman3",
            //         "clanName": "clan1",
            //         "clanId": "f38ca0f3-a5cd-49c0-aa3b-79ebde9e2bf7"
            //     });
            // },
            getTribeMansByClan: function(clanId) {
                var url = host + 'v2_1/tribemans/clan?clanId=' + clanId;
                return $http.get(url);
            },
            getTribeMans: function(limit, language) {
                var url = host + 'v2_1/tribemans?limit=' + limit + '&lang=' + language;
                return $http.get(url);
            },
            createPicture: function(pictureObj) {
                var url = host + 'v2_1/clans/pictures';
                /*return $http.post(url, {
                    "tribemanName": "tribeman5",
                    "clanName": "clan2",
                    "clanId": "f13e53c6-d29e-43b1-a10f-7700b1d6e1cf",
                    "picture": "", 
                    "picturePath": "string", "hashtags": "string", "location": "string", "latitude": "string", "longitude": "string"
                });*/
                return $http.post(url, pictureObj);

            },
            getclanPictures: function(limit, clanId) {
                var filter = { "clan_id": clanId },
                    fl = prepareFilterQueryString(filter);
                var url = host + 'v2_1/clans/pictures?limit=' + limit + fl;
                return $http.get(url);
            },
            deleteClanPictures: function() {
                var url = host + 'v2_1/clans/pictures/932cf872-62f2-4f70-9231-897d53361d31';
                return $http.delete(url);
            },
            addBookmarkClan: function(bObj) {
                var url = host + 'v2_1/clans/bookmarks';
                return $http.post(url, bObj);
            },
            getBookmarkListByTribeman: function(tribeManId) {
                var url = host + 'v2_1/clans/bookmarks?tribemanId=' + tribeManId;
                return $http.get(url);
            },
            deleteBookmarkClan: function(bookmarkId) {
                var url = host + 'v2_1/clans/bookmarks/' + bookmarkId;
                return $http.delete(url);
            },

            /*------  Profile page      -------*/
            getTribemanFriends: function(tribeman_id) {
                var filter = { "tribeman_id": tribeman_id },
                    fl = prepareFilterQueryString(filter);
                var url = host + 'v2_1/tribemans/friend?' + fl;
                return $http.get(url);
            },
            addTribemanPicture: function(tribemanPicture) {
                var url = host + 'v2_1/tribemans/pictures';
                return $http.post(url, tribemanPicture);
            },
            getTribemanPicture: function(limit, tribeman_id) {
                var filter = { "tribeman_id": tribeman_id },
                    fl = prepareFilterQueryString(filter);
                var url = host + 'v2_1/tribemans/pictures?limit=' + limit + fl;
                return $http.get(url);
            },
            getTribemanPictureDetails: function(tribemanPictureId) {
                var url = host + 'v2_1/tribemans/singleTribemanPicture/' + tribemanPictureId;
                return $http.get(url);
            },
            deleteTribemanPicture: function(tribemanPictureId) {
                var url = host + 'v2_1/tribemans/pictures/' + tribemanPictureId;
                return $http.delete(url);
            },
            getRequestList: function() {
                var url = host + 'v2_1/tribemans/showFriendRequest';
                return $http.get(url);
            },
            accectFriendRequest: function(requestId) {
                var reqObj = { 'tribemanFriendListId': requestId };
                var url = host + 'v2_1/tribemans/acceptFriendRequest?tribemanFriendListId=' + requestId;
                return $http.post(url, reqObj);
            },
            rejectFriendRequest: function(requestId) {
                var reqObj = { 'tribemanFriendListId': requestId };
                var url = host + 'v2_1/tribemans/rejectFriendRequest?tribemanFriendListId=' + requestId;
                return $http.post(url, reqObj);
            },


            /*------  Invite Friend Clan    ------*/

            frindListUnderClan: function(tribemanId, clanId) {
                var url = host + 'v2_1/invites?tribemanId=' + tribemanId + '&clanId=' + clanId;
                return $http.get(url);
            },
            unmemberClansList: function(friendId) {
                var url = host + 'v2_1/check/friend/' + friendId;
                return $http.get(url);
            },
            inviteFriendToClan: function(inviteObj) {
                var url = host + 'v2_1/invites';
                return $http.post(url, inviteObj);
            },
            getAllInvites: function() {
                var url = host + 'v2_1/invites/showInvites';
                return $http.get(url);
            },
            acceptClanInviteRequest: function(invId) {
                var url = host + 'v2_1/invites/accept?id='+invId;
                return $http.post(url);
            },
            rejectClanInviteRequest: function(invId) {
                var url = host + 'v2_1/invites/reject?id='+invId;
                return $http.post(url);
            },




            /*------  Common Services      ------*/

            setTribeReload: function(val) {
                this.tribeReload = val;
            },
            getTribeReload: function() {
                return this.tribeReload;
            },
            setListRefresh: function(val) {
                this.listRefresh = val;
            },
            getListRefresh: function() {
                return this.listRefresh;
            },
            setCommentRefresh: function(val) {
                this.commentsRfresh = val;
            },
            getCommentRefresh: function() {
                return this.commentsRfresh;
            },
            setLoggedInTribemanDetails: function(tribeman) {
                this.tribemanDetails = tribeman;
            },
            getLoggedInTribemanDetails: function() {
                return this.tribemanDetails;
            }
        };
    }
]);
tribeModule.factory('CommonTribeService', [function() {
    return {
        setClanReload: function(val) {
            this.shouldReload = val.shouldReload;
        },
        getClanReload: function() {
            return this.shouldReload;
        }
    };
}]);
