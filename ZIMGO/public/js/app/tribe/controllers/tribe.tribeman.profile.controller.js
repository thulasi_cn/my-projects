var app = angular.module('tribeModule');
app.controller('TribeProfileCtrl', ['$scope', '$rootScope', '$modal', 'tribeService', '$stateParams', '$state', 'growl', function($scope, $rootScope, $modal, tribeService, $stateParams, $state, growl) {
    $scope.$watch(function() {
        return tribeService.getListRefresh();
    }, function(newValue, oldValue) {
        if (newValue) {
            $scope.getLoggedInTribemanDetails();
        }
    });
    $rootScope.shwLoad = true;


    $scope.pendingRequestList = function() {
        tribeService.getRequestList().then(function(res) {
            $scope.pendingFriendRequests = res.data;
        }, function(error) {});
    };
    $scope.pendingRequestList();
    $scope.inviteTribe = function(inviteFriend) {
        var modalInstance = $modal.open({
            templateUrl: 'js/app/tribe/templates/profile/profile.inviteTribe.modal.view.html',
            controller: 'inviteTribeCtrl',
            size: 'md',
            resolve: {
                friend: function() {
                    return inviteFriend;
                }
            }

        });
        modalInstance.result.then(function(registerModel) {}, function() {});
    };
    $scope.loggedInTribeman = $stateParams;
    $scope.getLoggedInTribemanDetails = function() {
        tribeService.getTribemanDetails($scope.loggedInTribeman.tribemanId).success(function(res) {
            $scope.tribemanDetails = res;
            $rootScope.shwLoad = false;
        });
    };
    $scope.getLoggedInTribemanDetails();
    tribeService.getTribemanFriendsCount($scope.loggedInTribeman.tribemanId).success(function(res) {
        $scope.friendsCount = res[0].count;
    });
    tribeService.getTribemanPictureCount($scope.loggedInTribeman.tribemanId).success(function(res) {
        $scope.tribemanPictureCount = res[0].count;
    });
    $scope.friendsList = [];
    $scope.tribemanFriendsList = function() {
        tribeService.getTribemanFriends($scope.loggedInTribeman.tribemanId).success(function(res) {
            if (res && res.length > 0) {
                $scope.friendsList = res;
            } else {}
        });
    };
    $scope.tribemanFriendsList();
    $scope.myTribsList = function() {
        tribeService.getTribesByTribemanId($scope.loggedInTribeman.tribemanId)
            .success(function(result) {
                if (result && result.length > 0) {
                    $scope.tribeListData = result;
                } else {}
            }).error(function(data, status) {});
    };
    $scope.myTribsList();
    /*$scope.goToCatPage = function(tribe) {
        $rootScope.shwLoad = true;
        tribeService.getOneCategory(tribe.tribeCategory).success(function(category) {
            var categoryRes = category.list[0];
            localStorage.setItem("CurrentCatagory", JSON.stringify(categoryRes));
            $rootScope.shwLoad = false;
            $state.go('main.base.tribe.main.main.categorie', { categorieID: categoryRes.categoryId });
        });
    };
    $scope.goToTribePage = function(tribe) {
        localStorage.setItem("CurrentTribe", JSON.stringify(tribe));
        $state.go('main.base.tribe.main.main.categorie.tribes', { categorieID: tribe.tribeCategoryId, tribeId: tribe.tribeId });

    };*/

    $scope.changeProfilePc = function() {
        var modalInstance = $modal.open({
            templateUrl: 'js/app/tribe/templates/profile/profile.updateProfile.modal.view.html',
            controller: 'updateProfileCtrl',
            windowClass: 'tribeMessage-modal-window'
        });
        modalInstance.result.then(function(registerModel) {}, function() {});
    };
    $scope.getAllLoggedInUserMessage = function() {
        tribeService.getAllLoggedInUserMessage()
            .success(function(msg) {
                $scope.messageList = msg;
            }).error(function(data, status) {});
    };
    $scope.getAllLoggedInUserMessage();
    $scope.getAllLoggedInUserInvitations = function() {
        tribeService.getAllInvites().success(function(invites) {
            $scope.invitesList = invites;
        });
    };
    $scope.getAllLoggedInUserInvitations();
    $scope.tribemanPictureList = function() {
        tribeService.getTribemanPicture(20, $scope.loggedInTribeman.tribemanId).success(function(res) {
            if (res && res.list && res.list.length > 0) {
                $scope.tribemanPicutures = res.list;
                $rootScope.shwLoad = false;
            }
        });
    };
    $scope.tribemanPictureList();

    $scope.acceptFrndRequest = function(requestId) {
        tribeService.accectFriendRequest(requestId).then(function(res) {
            console.log(res);
            growl.success("You Successfully Accepted the friend request...!", {
                ttl: 5000
            });
            $scope.pendingRequestList();
            $scope.tribemanFriendsList();
            $state.go($state.current, {}, { reload: true });
        }, function(error) { console.log(error); });
    };
    $scope.rejectFrndRequest = function(requestId) {
        tribeService.rejectFriendRequest(requestId).then(function(res) {
            growl.success("You Successfully Rejected the friend request...!", {
                ttl: 5000
            });
            $scope.pendingRequestList();
            $scope.tribemanFriendsList();
            $state.go($state.current, {}, { reload: true });
        }, function(error) { console.log(error); });
    };

    $scope.viewSlider = function(pics) {
        var modalInstance = $modal.open({
            templateUrl: 'js/app/tribe/templates/profile/profile.gallery.view.html',
            controller: function($scope, $modalInstance) {
                $scope.tribemanPicutures = pics;
                $scope.close = function() { $modalInstance.dismiss('cancel'); };
            },
            // size: 'md'
            windowClass: 'picture-slider-window     '


        });
        modalInstance.result.then(function(registerModel) {}, function() {});
    };
    $scope.postDetails = function(post) {
        $modal.open({
            templateUrl: "/js/app/tribe/templates/clan/categories/clan.post.details.view.html",
            controller: 'PostDetailController',
            windowClass: 'post-details-window',
            // size: 'sm',
            resolve: {
                postData: function() {
                    return post;
                }
            }
        });
    };
    $scope.acceptClanInvt = function(invId) {
        tribeService.acceptClanInviteRequest(invId).then(function(res) {
            growl.success("You Successfully Accepted the clan request...!", {
                ttl: 5000
            });
            $scope.getAllLoggedInUserInvitations();
        }, function(error) { console.log(error); });
    };
    $scope.rejectClanInvt = function(invId) {
        tribeService.rejectClanInviteRequest(invId).then(function(res) {
            growl.success("You Successfully Rejected the clan request...!", {
                ttl: 5000
            });
            $scope.getAllLoggedInUserInvitations();
        }, function(error) { console.log(error); });
    };
}]);
