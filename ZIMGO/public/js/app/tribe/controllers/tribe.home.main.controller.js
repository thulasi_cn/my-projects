var app = angular.module('tribeModule');
app.controller('TribeContentResultMainCtrl', ['$scope', '$rootScope', '$modal', '$location', 'tribeService', 'languageService', '$timeout', function($scope, $rootScope, $modal, $location, tribeService, languageService, $timeout) {
    $rootScope.shwLoad = true;
    $rootScope.$on('modalOpenChk', function(event, args) {
        if ($scope.isPopoverOpen) {
            $scope.toggleSpamPopover();
            $scope.$digest();
        }
    });
    $scope.tribeList = [];
    $scope.clanList = [];
    $scope.tribeCategoriesList = [];
    $scope.isPopoverOpen = false;
    $scope.toggleSpamPopover = function() {
        $scope.isPopoverOpen = !$scope.isPopoverOpen;
    };
    $scope.getCategoryList = function() {
        tribeService.getCategory(20).success(function(res) {
            if (res && res.list && res.list.length > 0) {
                $scope.tribeCategoriesList = res.list;
                // $scope.tribeListFn($scope.tribeCategoriesList);
                $scope.getActiveTribemansList();
            }
        });
    };
    tribeService.loggedInTribeman().then(function(res){$scope.loggedInTribemanId = res.data.tribemanId;},function(error){console.log(error);});
    /* $scope.tribeListFn = function(catList) {
         angular.forEach(catList, function(cat, i) {
             tribeService.getTribesByCatId(10, cat.categoryName)
                 .success(function(trbs) {
                     if (trbs && trbs.list && trbs.list.length > 0) {
                         angular.forEach(trbs.list, function(trb, i) {
                             $scope.tribeList.push(trb);
                         });
                         // $scope.clanListFn();
                     }
                 }).error(function(data, status) {});
         });
     };*/
    /*$timeout($scope.clanListFn = function() {
        angular.forEach($scope.tribeList, function(trb, i) {
            tribeService.getClansByTribe(10, trb.tribeId)
                .success(function(clans) {
                    if (clans && clans.list && clans.list.length > 0) {
                        angular.forEach(clans.list, function(cln, i) {
                            $scope.clanList.push(cln);
                        });
                    }
                }).error(function(data, status) {});
            $scope.getActiveTribemansList();
        });
    }, 2000);*/

    /*$scope.filterTribes = function(catName) {
        var filteredTribe = _.filter($scope.tribeList, { tribeCategory: catName });
        return filteredTribe;
    };*/
    // $scope.filterClans = function(tribeID) {
    //     return _.filter($scope.clanList, { tribeId: tribeID });
    // };

    $scope.getActiveTribemansList = function() {
        var currentLang = languageService.getCurrent();
        tribeService.getActiveTribemens(4)
            .success(function(result) {
                if (result && result.length > 0) {
                    $scope.tribeMansList = result;
                    $rootScope.shwLoad = false;
                }
            }).error(function(data, status) {});
    };
    $scope.getCategoryList();


    /*$scope.openCreateTribeModel = function() {
        var modalInstance = $modal.open({
            templateUrl: 'js/app/tribe/templates/tribe/tribe.create.modal.view.html',
            windowClass: 'tribeCreate-modal-window'
        });
        modalInstance.result.then(function(registerModel) {}, function() {});
    };
    $scope.isActive = function(catID) {
        if ($location.path().split('/')[2] === "categories") {
            var active = (catID === $location.path().split('/')[3]);
            return active;
        }
    };
    $scope.isTribeActive = function(catID) {
        if ($location.path().split('/')[2] === "tribes") {
            var active = (catID === parseInt($location.path().split('/')[3]));
            return active;
        }
    };*/
    $scope.setCurrentCatagory = function(cat) {
        localStorage.setItem("CurrentCatagory", JSON.stringify(cat));
    };
    /*$scope.setNavTribe = function(ct, tr) {
        localStorage.setItem("CurrentCatagory", JSON.stringify(ct));
        localStorage.setItem("CurrentTribe", JSON.stringify(tr));
    };
    $scope.setCTCAll = function(ct, tr, cl) {
        localStorage.setItem("CurrentCatagory", JSON.stringify(ct));
        localStorage.setItem("CurrentTribe", JSON.stringify(tr));
        localStorage.setItem("CurrentClan", JSON.stringify(cl));
    };*/

    tribeService.getTopContributors().then(function(res) { $scope.topContributorLis = res.data; }, function(error) {console.log(error);});
}]);
