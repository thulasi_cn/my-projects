var app = angular.module('tribeModule');
app.controller('TribePhotosController',  ['$scope', '$rootScope', 'languageService', 'tribeService', '$state', function($scope, $rootScope, languageService, tribeService, $state) {
    $rootScope.shwLoad = true;

    $scope.currentCategory = JSON.parse(localStorage.getItem("CurrentCatagory"));
    $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
    $scope.currentClan = JSON.parse(localStorage.getItem("CurrentClan"));
    
    $scope.clanPictureList = function() {
        tribeService.getclanPictures(6).success(function(res) {
            if (res && res.list && res.list.length > 0) {
                $scope.clanPicutures = res.list;
                $rootScope.shwLoad = false;
            }
        });
    };
    $scope.clanPictureList();
}]);
