var app = angular.module('tribeModule');
app.controller('TribeClansController', ['$scope', '$rootScope', 'tribeService', '$stateParams', '$location', function($scope, $rootScope, tribeService, $stateParams, $location) {
    $rootScope.shwLoad = true;
    $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
    console.log($stateParams.tribesID)
    $scope.result = [];
    $scope.run = function() {
        tribeService.getClansByTribe(10, $stateParams.tribesID)
            .success(function(result) {
                if (result && result.list && result.list.length > 0) {
                    $scope.result = result.list;
                    $rootScope.shwLoad = false;
                }
            })
            .error(function(data, status) {

            });

    };
    $scope.run();

    $scope.setClan = function(cClan) {
        localStorage.setItem("CurrentClan", JSON.stringify(cClan));
    };

}]);
