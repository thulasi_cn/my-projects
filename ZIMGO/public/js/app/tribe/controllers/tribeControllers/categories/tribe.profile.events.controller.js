var app = angular.module('tribeModule');
app.controller('TribeEventsController', ['$scope', '$rootScope', 'languageService', 'tribeService', '$state', function($scope, $rootScope, languageService, tribeService, $state) {

    $scope.currentCategory = JSON.parse(localStorage.getItem("CurrentCatagory"));
    $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
    $scope.currentClan = JSON.parse(localStorage.getItem("CurrentClan"));
    
    $rootScope.shwLoad = true;
    $scope.eventsList = function() {
        tribeService.getTribeEvents(10).success(function(result) {
            if (result && result.list && result.list.length > 0) {
                $scope.result = result.list;
                $rootScope.shwLoad = false;
            }
        });
    };
    $scope.eventsList();
}]);
