var app = angular.module('tribeModule');
app.controller('TribeTribemansController',  ['$scope', 'languageService', 'tribeService', function($scope, languageService, tribeService) {

    $scope.currentCategory = JSON.parse(localStorage.getItem("CurrentCatagory"));
    $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
    $scope.currentClan = JSON.parse(localStorage.getItem("CurrentClan"));

    $scope.tribemansList = function() {
        var currentLang = languageService.getCurrent();
        tribeService.getTribeMansByTribe($scope.currentTribe.tribeName).success(function(result) {
                if (result && result.list && result.list.length > 0) {
                    $scope.tribeMansList = result.list;
                }
            }).error(function(data, status) {});

    };
    $scope.tribemansList();
}]);
