var tribeModule = angular.module('tribeModule');

tribeModule.controller('tribeCreateController', ['$scope', '$modalInstance', 'languageService', 'tribeName', 'tribeList', 'catList', 'position', 'tribeService', '$state', '$rootScope', '$stateParams', function($scope, $modalInstance, languageService, tribeName, tribeList, catList, position, tribeService, $state, $rootScope, $stateParams) {
    $scope.tribeName = tribeName;
    $scope.poistion = position;
    var loggeInTribeMan = JSON.parse(localStorage.getItem("loggedInTribeman")) === "" ? true : false;
    $scope.tribemanData = JSON.parse(localStorage.getItem("loggedInTribeman"));

    $scope.getTribeList = function(categoryId) {
        $rootScope.shwLoad = false;
        //tribeService.getTribesByCatId(20, categoryName)
        tribeService.getTribesCatAndTribeman($scope.tribemanData.tribemanId, categoryId).success(function(result) {
                if (result && result.length > 0) {
                    $scope.showTribeSelect = true;
                    $rootScope.shwLoad = false;
                    $scope.noTribe = false;
                    $scope.tribeListData = result;
                } else {
                    $scope.noTribe = true;
                    $scope.showTribeSelect = false;
                }
                $rootScope.shwLoad = false;

            })
            .error(function(data, status) {});
    };
    if ($scope.poistion === "homePage") {
        if ($state.current.name !== 'main.base.tribe.main.main.categorie') {
            $scope.showTribeSelect = false;
            $scope.showCategory = true;
            $scope.noTribe = false;

        } else {
            if (!loggeInTribeMan) {
                $scope.showTribeSelect = true;
                $scope.showCategory = false;
                $scope.tribeman = JSON.parse(localStorage.getItem("loggedInTribeman"));
                tribeService.getTribesCatAndTribeman($scope.tribeman.tribemanId, $stateParams.categorieID).success(function(res) {
                    if (res && res && res.length > 0) {
                        $scope.tribeListData = res;
                    } else {
                        $scope.noTribeTrbman = true;
                        $scope.showTribeSelect = false;
                    }
                });
            } else {
                $scope.showTribeSelect = false;
                $scope.showCategory = true;
                $scope.noTribe = false;
            }
        }

    } else {
        $scope.showCategory = false;
        $scope.showTribeSelect = false;
        $scope.selectedTribe = true;
        $scope.tribeListData = tribeList;
    }
    $scope.catList = catList;
    $scope.close = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.tribeCreate = function(tribe) {
        var tribeObj = {};
        var currentLang = languageService.getCurrent();
        tribeObj.tribeName = tribe.tribename;
        tribeObj.description = tribe.description;
        tribeObj.tribeCategory = tribe.tribeCategory.categoryName;
        tribeObj.tribeCategoryId = tribe.tribeCategory.categoryId;
        tribeObj.language = currentLang.id;
        tribeObj.icon = tribe.icon.data;
        tribeObj.tribeKeyword = '';
        tribeService.createTribes(tribeObj)
            .success(function(result) {
                tribeService.setTribeReload(true);
                tribeService.setListRefresh({ 'catTribe': true });
                localStorage.setItem("CurrentTribe", JSON.stringify(result));
                $scope.closeImg();
                $state.go('main.base.tribe.main.main.categorie.tribes', { categorieID: result.tribeCategoryId, tribeId: result.tribeId });
                $scope.close();
            }).error(function(data, status) {});
    };
    $scope.chooseClanIcn = false;
    $scope.clanCreate = function(clan) {
        if (clan.icon.data !== '') {
            $scope.chooseClanIcn = false;
            var clanObj = {};
            clanObj.clanName = clan.clanName;
            clanObj.description = clan.description;
            clanObj.icon = clan.icon.data;
            if ($scope.showTribeSelect) {
                clanObj.tribeId = clan.tribeId;
            } else {
                clanObj.tribeId = $scope.tribeListData[0].tribe.tribeId;
            }
            tribeService.createClan(clanObj).success(function(result) {
                tribeService.setListRefresh({ 'tribeClan': true });
                // tribeService.setTribeReload(true);
                $scope.close();
                localStorage.setItem("CurrentClan", JSON.stringify(result));
                $state.go('main.base.tribe.main.main.categorie.tribes.clan.post', { categorieID: result.clanCategory, tribeId: result.tribeId, clanId: result.clanId });
            }).error(function(data, status) {});
        }else{
            $scope.chooseClanIcn = true;
        }


    };

    $scope.closeImg = function() {
        angular.element('.tribeImgDiv img').attr('src', '');
        angular.element('.tribeImgDiv').css('display', 'none');
    };
}]);
