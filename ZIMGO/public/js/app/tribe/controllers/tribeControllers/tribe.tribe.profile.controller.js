var tribeModule = angular.module('tribeModule');

tribeModule.controller('TribeClanListCtrl', ['$scope', '$state', '$rootScope', 'tribeService', '$stateParams', '$location', 'growl','$modal', function($scope, $state, $rootScope, tribeService, $stateParams, $location, growl,$modal) {


    $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
    $scope.tribeId = $stateParams.tribeId;

    $rootScope.shwLoad = true;
    $scope.$watch(function() {
        return tribeService.getListRefresh();
    }, function(newValue, oldValue) {
        if (newValue && newValue.tribeEvent && newValue.tribeClan) {
            $scope.run();
        }
    });

    $scope.setTribe = function(cTribe) {
        localStorage.setItem("CurrentTribe", JSON.stringify(cTribe));
    };
    $scope.checkJoinTribeVal = function() {
        tribeService.checkJoinTribe($scope.tribeId).then(function(res) {
            $scope.checkJoinTribe = res.data.isSubscribed;
        }, function(error) {});

    };
    $scope.checkJoinTribeVal();

    $scope.joinTribe = function(tribe) {
        var joinObj = {};
        $rootScope.shwLoad = true;

        joinObj.tribeId = tribe.tribeId;
        tribeService.joinClan(joinObj).success(function(res) {
            $scope.checkJoinTribeVal();
            growl.success("You have Joined this tribe successfully", {
                ttl: 5000
            });
            $scope.run();
            $state.go($state.current, {}, { reload: true });
            tribeService.setLoggedInTribemanDetails(res.tribeman);
            localStorage.setItem('loggedInTribeman', JSON.stringify(res.tribeman));
            $rootScope.shwLoad = false;

        });
    };
    $scope.unJoinTribe = function(tribe) {
        $rootScope.shwLoad = true;
        tribeService.unJoinTribe(tribe.tribeId).success(function(res) {
            $scope.checkJoinTribeVal();
            growl.success("You have Un Joined this tribe successfully", {
                ttl: 5000
            });
            $scope.run();
            $state.go($state.current, {}, { reload: true });
            $rootScope.shwLoad = false;

        });
    };
    var loggeInTribeMan = JSON.parse(localStorage.getItem("loggedInTribeman")) === "" ? true : false;
    if (loggeInTribeMan) {
        $scope.hideJoinBtn = true;
    } else {
        $scope.hideJoinBtn = false;
        $scope.tribeman = JSON.parse(localStorage.getItem("loggedInTribeman"));
    }
    tribeService.getOneCategory($stateParams.categorieID).success(function(category) {
        localStorage.setItem("CurrentCatagory", JSON.stringify(category));
        $scope.currentCategory = category;
    });
    $scope.result = [];
    $scope.run = function() {
        tribeService.getTribeByTribe($scope.tribeId)
            .success(function(result) {
                $scope.getRelatedTribes(result.tribe);
                $scope.creatorOfTribe = result.creatorOfTribe;
                $scope.tribeMembershipCount = result.membershipCount;
                $scope.tribeDetails = result.tribe;
                $scope.clansList = result.clans;
                $scope.tribemansList = result.tribemans;
                $scope.photosList = result.pictures;
                $scope.eventsList = result.tribeEvents;
                // $scope.activeTribeMansList = result.activeTribemans;
                $scope.getActiveTribemansList();
                $scope.crtClnTrbObj = { 'tribeName': $scope.tribeDetails.tribeName, 'tribeId': $scope.tribeDetails.tribeId }
            })
            .error(function(data, status) {});
    };
    $scope.getActiveTribemansList = function() {
        tribeService.getActiveTribemens(10)
            .success(function(result) {
                if (result && result.length > 0) {
                    $scope.activeTribeMansList = result;
                    $rootScope.shwLoad = false;
                } else {
                    $rootScope.shwLoad = false;
                }
            }).error(function(data, status) { $rootScope.shwLoad = false; });
    };
    $scope.getRelatedTribes = function(tribe) {
        tribeService.getRelatedTribeByTribe(tribe)
            .success(function(result) {
                $scope.relatedTribe = result.list;
            });
    };
    $scope.run();
    $scope.isActive = function(viewLocation) {
        var active = (viewLocation === $location.path().split('/')[4]);
        return active;
    };

    $scope.setClan = function(cClan) {
        localStorage.setItem("CurrentClan", JSON.stringify(cClan));
    };
 $scope.showSlider = function(pics) {
        var modalInstance = $modal.open({
            templateUrl: 'js/app/tribe/common/templates/photo.slider.view.html',
            controller: function($scope, $modalInstance) {
                $scope.tribemanPicutures = pics;
                $scope.close = function() { $modalInstance.dismiss('cancel'); };
            },
            // size: 'md'
            windowClass: 'picture-slider-window     '


        });
        modalInstance.result.then(function(registerModel) {}, function() {});
    };
}]);
