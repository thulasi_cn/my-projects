var app = angular.module('tribeModule');

app.controller('TopTenTribeResultCtrl', ['$scope', '$rootScope', 'commonSearchService', 'tribeService', 'languageService','$timeout', function($scope, $rootScope, commonSearchService, tribeService, languageService,$timeout) {
    $scope.initPagingData = function() {
        $scope.pagingData = {
            limit: 6,
            lastRow: null
        };
        $scope.result = [];
    };
    $scope.initPagingData();
    $rootScope.shwLoad = true;
    /*$scope.tribeMansList = [];*/
    /*$scope.$watch(function() {
        return commonSearchService.getReload();
    }, function(newValue, oldValue) {
        if (newValue) {
            $scope.run(commonSearchService.getCurrent());
        }
    });*/
    /*$scope.$watch(function() {
        return tribeService.getTribeReload();
    }, function(newValue, oldValue) {
        if (newValue && newValue === true) {
            $scope.run();
        }
    });*/
    $scope.run = function() {
        /*var currentLang = languageService.getCurrent();
        var lastRow = {};
        var filter = { "language": "en" };*/
        /* if ($scope.pagingData.lastRow) {
             lastRow.TRIBE_NAME = $scope.pagingData.lastRow.tribeName;
             lastRow.TRIBE_ID = $scope.pagingData.lastRow.tribeId;
         }*/
        tribeService.getTopTribes()
            .success(function(result) {
                if (result && result.length > 0) {
                    $scope.result = result;
                    $timeout(function(){$rootScope.shwLoad = false;},1000);
                    
                    /*} else {
                        angular.forEach(result.list, function(element, index) {
                            $scope.result.push(element);
                        });
                        $rootScope.shwLoad = false; }*/
                } else {
                    $rootScope.shwLoad = false;
                }
            })
            .error(function(data, status) {

            });

    };
    $scope.run();
    $scope.setTribe = function(cTribe) {
        localStorage.setItem("CurrentTribe", JSON.stringify(cTribe));
        // tribeService.setCurrentTribe(cTribe);
    };
    /*$scope.loadMore = function() {
        $rootScope.shwLoad = true;
        $scope.pagingData.lastRow = $scope.result[$scope.result.length - 1];
        $scope.run();
    };*/
}]);
