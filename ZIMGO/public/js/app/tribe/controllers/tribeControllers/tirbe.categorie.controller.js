var tribeModule = angular.module('tribeModule');

tribeModule.controller('TribeCategorieCtrl', ['$scope', '$rootScope', 'tribeService', '$stateParams', function($scope, $rootScope, tribeService, $stateParams) {
    $rootScope.shwLoad = true;
    $scope.$watch(function() {
        return tribeService.getListRefresh();
    }, function(newValue, oldValue) {
        if (newValue && newValue.catTribe) {
            $scope.run();
        }
    });
    /*$scope.currentCategory = JSON.parse(localStorage.getItem("CurrentCatagory"));*/
    tribeService.getOneCategory($stateParams.categorieID).success(function(category) {
        $scope.currentCategory = category;
        $scope.run();
    });
    $scope.result = [];
    $scope.run = function() {
        tribeService.getTribesByCatId(10, $stateParams.categorieID)
            .success(function(result) {
                if (result && result.length > 0) {
                    $scope.result = result;
                    $rootScope.shwLoad = false;
                }
                $rootScope.shwLoad = false;
            })
            .error(function(data, status) {});
    };

    $scope.setTribe = function(cTribe) {
        localStorage.setItem("CurrentTribe", JSON.stringify(cTribe));
    };
}]);
