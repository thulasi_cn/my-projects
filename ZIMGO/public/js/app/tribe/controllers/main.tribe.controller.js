var tribeModule = angular.module('tribeModule');

tribeModule.controller('MainTribeNavigationCtrl', ['$scope', '$rootScope', 'tribeService', 'socialService',
    function($scope, $rootScope, tribeService, socialService) {
        socialService.setCookiesInFilter();
        $rootScope.shwLoad = true;
        $scope.socialConnectNetworks = socialService.getSocialNetworks();
        $scope.filterSocial = function(sn) {
            socialService.toggleAddFilter(sn);
        };
        $scope.isSnsSelected = function(sn) {
            if ((sn.name === 'youtube' || sn.name === 'flickr') && $rootScope.disableSocialPart) {
                return false;
            } else {
                return socialService.filterExists(sn);
            }
        };
        $scope.mainTribeSearch = function() {
            tribeService.searchTribe($scope.tribeSearch).success(function(res) {
                console.log(res);
            });
        };
        var tribeMan = localStorage.getItem('loggedInTribeman');
        var loggeInTribeMan = JSON.parse(localStorage.getItem("loggedInTribeman")) === "" ? true : false;

        if (tribeMan === null || loggeInTribeMan) {
            tribeService.loggedInTribeman().then(function(res) {
                tribeService.setLoggedInTribemanDetails(res.data);
                $scope.tribemanId = res.data.tribemanId;
                localStorage.setItem('loggedInTribeman', JSON.stringify(res.data));
            }, function(err) {
                console.log(err);
            });
        } else {
            var t = JSON.parse(tribeMan);
            $scope.tribemanId = t.tribemanId;

        }

        $scope.checkSelection = function(sn) {
            if ($scope.isSnsSelected(sn) && (sn.id === 'YT' || sn.id === 'FL') && $scope.disableSocialPart) {
                return true;
            } else {
                return false;
            }
        };

    }
]);
