var app = angular.module('tribeModule');
app.controller('TribeClanCtrl', ['$scope', '$state', '$rootScope', '$modal', '$location', 'tribeService', '$stateParams', 'growl', function($scope, $state, $rootScope, $modal, $location, tribeService, $stateParams, growl) {
    $rootScope.shwLoad = false;
    // $scope.currentCategory = JSON.parse(localStorage.getItem("CurrentCatagory"));
    // $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
    $scope.currentClan = $stateParams; //JSON.parse(localStorage.getItem("CurrentClan"));
    // $scope.currentClanDetails = JSON.parse(localStorage.getItem("CurrentClan"));
    var loggeInTribeMan = JSON.parse(localStorage.getItem("loggedInTribeman")) === "" ? true : false;

    tribeService.getOneCategory($stateParams.categorieID).success(function(category) {
        $scope.currentCategory = category;
    });
    tribeService.getTribeByTribeId($stateParams.tribeId)
        .success(function(result) {
            $scope.currentTribe = result.tribe;
        }).error(function(data, status) {});
    tribeService.getClanById($stateParams.clanId)
        .then(function(clan) {
            $scope.currentClanCreatorDetails = clan.data.creatorOfClan;
            $scope.currentClanDetails = clan.data.clan;
        }, function(error) { console.log(error); });



    if (loggeInTribeMan) {
        $scope.hideJoinBtn = true;
    } else {
        $scope.hideJoinBtn = false;
        $scope.tribeman = JSON.parse(localStorage.getItem("loggedInTribeman"));
        $scope.tribemanId = (JSON.parse(localStorage.getItem("loggedInTribeman"))).tribemanId;
    }
    $scope.isBookmark = true;
     $scope.checkBookmark = function() {
        tribeService.getBookmarkListByTribeman($scope.tribeman.tribemanId).then(function(res) {
            angular.forEach(res.data.list, function(element, index) {
                if(element.CLAN_ID === $scope.currentClan.clanId){$scope.isBookmark = false;}
            });
        }, function(error) { console.log(error); });
    };
    $scope.checkBookmark();


    $scope.pictureDetails = function() {
        var modalInstance = $modal.open({
            templateUrl: 'js/app/tribe/templates/clan/categories/clan.photos.details.modal.view.html',
            windowClass: 'tribeCreate-modal-window'
        });
        modalInstance.result.then(function(registerModel) {}, function() {});
    };
    $scope.isMember = function() {
        tribeService.checkJoinClan($scope.currentClan.clanId).then(function(res) {
            $scope.isSubscribed = res.data.isSubscribed;
        });
    };
    $scope.isMember();
    /*if (!$scope.hideJoinBtn) {
        $scope.getBookmarks = function() {
            tribeService.getBookmarkClan($scope.tribemanId).success(function(res) {});
        };
        $scope.getBookmarks();
    }*/
    $scope.bookmarkClan = function() {
        var bookmarkObj = {};
        bookmarkObj.clanName = $scope.currentClanDetails.clanName;
        bookmarkObj.clanId = $scope.currentClan.clanId;
        bookmarkObj.tribemanId = $scope.tribemanId;
        tribeService.addBookmarkClan(bookmarkObj).success(function(res) {
            growl.success("You have Bookmarked this clan successfully", {
                ttl: 5000
            });
            $scope.checkBookmark();
        });
    };

    $scope.isActive = function(viewLocation) {
        var path = $location.path().split('/');
        var active = ((viewLocation === path[path.length - 1]) || (viewLocation === path[path.length - 2]));
        return active;
    };
    $scope.joinClan = function() {
        var joinObj = { 'clanId': $scope.currentClan.clanId };
        tribeService.joinClan(joinObj).success(function(res) {
            growl.success("You have Joined this clan successfully", {
                ttl: 5000
            });
            $state.go($state.current, {}, { reload: true });
            $scope.isMember();
            $scope.hideJoinBtn = false;
            tribeService.setLoggedInTribemanDetails(res.tribeman);
            localStorage.setItem('loggedInTribeman', JSON.stringify(res.tribeman));
        });
    };
    $scope.unJoinClan = function() {
        tribeService.unJoinClan($scope.currentClan.clanId).success(function(res) {
            growl.success("You have Unjoined this clan successfully", {
                ttl: 5000
            });
            $state.go($state.current, {}, { reload: true });
            $scope.isMember();
            $scope.hideJoinBtn = true;
        });
    };
    $scope.getActiveTribemansList = function() {
        tribeService.getActiveTribemens(10)
            .success(function(result) {
                if (result && result.length > 0) {
                    $scope.activeTribeMansList = result;
                    $rootScope.shwLoad = false;
                }
            }).error(function(data, status) {});
    };
    $scope.getActiveTribemansList();

}]);
