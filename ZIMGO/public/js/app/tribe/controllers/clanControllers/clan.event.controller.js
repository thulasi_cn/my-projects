var app = angular.module('tribeModule');
app.controller('EventController', ['$scope', '$rootScope', 'languageService', 'tribeService', '$state', 'CommonTribeService', '$stateParams', function($scope, $rootScope, languageService, tribeService, $state, CommonTribeService, $stateParams) {
    CommonTribeService.setClanReload({
        shouldReload: false
    });
    $scope.$watchCollection(function() {
        return CommonTribeService.getClanReload();
    }, function(newValue, oldValue) {
        if (newValue !== oldValue) {
            if (newValue && newValue.shouldReload) {
                $scope.eventsList();
            }
        }
    });

    /*    $scope.currentCategory = JSON.parse(localStorage.getItem("CurrentCatagory"));
        $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
        $scope.currentClan = $stateParams;//JSON.parse(localStorage.getItem("CurrentClan"));
        $scope.currentClanDetails = JSON.parse(localStorage.getItem("CurrentClan"));*/

    $rootScope.shwLoad = true;
    $scope.eventsList = function() {
        tribeService.getClanEvents(10, $scope.currentClan.clanId).success(function(result) {
            if (result && result.list && result.list.length > 0) {
                $scope.result = result.list;
                $rootScope.shwLoad = false;
            } else {
                $rootScope.shwLoad = false;
            }
        });
    };
    $scope.eventsList();
}]);
