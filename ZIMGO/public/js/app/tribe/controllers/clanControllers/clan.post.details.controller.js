var app = angular.module('tribeModule');
app.controller('PostDetailController', ['$scope', 'tribeService', '$rootScope', 'postData','$modalInstance', function($scope, tribeService, $rootScope, postData,$modalInstance) {
    $scope.postDetail = postData;
    // $scope.$watch(function() {
    //     return tribeService.getCommentRefresh();
    // }, function(newValue, oldValue) {
    //     if (newValue) {
    //         $scope.getPostCommentsList();
    //     }
    // });
    $scope.close = function() { $modalInstance.dismiss('cancel'); };

    var postid = postData.postId;
    $scope.postRating = 1;
    $scope.commentMood = '';
    $scope.moodIcon = false;

    $scope.chooseCommetMood = function(mood) {
        $scope.moodIcon = true;
        $scope.moodExpError = false;
        $scope.commentMood = mood.replace(' ', '');
    };
    $scope.getPostCommentsList = function() {
        tribeService.getClanPostComment(postid)
            .success(function(comments) {
                $scope.commentsList = comments;
            }).error(function(data, status) {});
    };
    $scope.moodExpError = false;
    $scope.createClanPostComment = function() {
        if ($scope.commentMood !== '') {
            $rootScope.shwLoad = true;
            var commentObj = {};
            commentObj.postId = postid;
            commentObj.postCommentSentiment = "50";
            commentObj.postCommentMood = $scope.commentMood;
            commentObj.comment = $scope.commentText;
            tribeService.createPostComment(commentObj)
                .success(function(result) {
                    $scope.moodIcon = false;
                    $scope.moodExpError = false;
                    $rootScope.shwLoad = false;
                    $scope.commentText = '';
                    $scope.commentMood = '';
                    $scope.getPostCommentsList();
                }).error(function(data, status) {});
        } else {
            $scope.moodExpError = true;
            $scope.moodeError = "Please choose the Mood...!!";
        }
    };
    $scope.rateFunction = function(rating) {
        var postRating = {};
        postRating.postId = postid;
        postRating.rating = rating;
        tribeService.postRating(postRating)
            .success(function(res) {
                console.log(res);
            }).error(function(data, status) {});
    };
    $scope.clickLike = true;
    $scope.commentLike = function(comment) {
        var likeObj = {
            "postId": comment.postId,
            "commentId": comment.commentId,
            "replyId": comment.replyId
        };
        tribeService.likeComment(likeObj).then(function(res) {
            console.log(res);
            $scope.clickLike = false;
        }, function(error) {});
    };

}]);
