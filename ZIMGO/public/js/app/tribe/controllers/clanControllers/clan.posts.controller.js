var app = angular.module('tribeModule');
app.controller('PostsController', ['$scope', '$rootScope', 'tribeService', '$stateParams','$modal', function($scope, $rootScope, tribeService, $stateParams,$modal) {
    var loggeInTribeMan = JSON.parse(localStorage.getItem("loggedInTribeman")) === "" ? true : false;
    setTimeout(function() {
        $scope.$broadcast('reCalcViewDimensions');
    }, 50);
    if (!loggeInTribeMan) {
        $scope.tribeman = JSON.parse(localStorage.getItem("loggedInTribeman"));
    }
    // $scope.$watch(function() {
    //     return tribeService.getCommentRefresh();
    // }, function(newValue, oldValue) {
    //     if (newValue) {
    //         $scope.postListByClan();
    //     }
    // });
    $scope.commentMood = '';
    $scope.moodIcon = false;
    $scope.chooseCommetMood = function(mood) {
        $scope.moodIcon = true;
        $scope.moodExpError = false;
        $scope.commentMood = mood.replace(' ', '');
    };
    $scope.postSentiment = {
        value: 5,
        options: {
            floor: 0,
            ceil: 100,
            step: 1,
            showSelectionBar: true,
            getSelectionBarColor: function(value) {
                if (value <= 30)
                    return 'red';
                if (value <= 60)
                    return 'orange';
                if (value <= 90)
                    return 'yellow';
                return '#2AE02A';
            }
        }
    };

    $rootScope.shwLoad = true;
    // $scope.currentCategory = JSON.parse(localStorage.getItem("CurrentCatagory"));
    // $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
    // $scope.currentClan = $stateParams; //JSON.parse(localStorage.getItem("CurrentClan"));
    // $scope.currentClanDetails = JSON.parse(localStorage.getItem("CurrentClan"));

    $scope.postListByClan = function() {
        $rootScope.shwLoad = true;
        tribeService.getPostsByClan($stateParams)
            .success(function(result) {
                if (result && result.length > 0) {
                    $scope.postList = result;
                    $rootScope.shwLoad = false;
                } else {
                    $rootScope.shwLoad = false;
                }
            }).error(function(data, status) {});
    };
    $scope.postListByClan();

    $scope.postClan = function(postData) {
        if ($scope.commentMood !== '') {
            var PostObj = {};
            PostObj.postName = postData.title;
            PostObj.postText = postData.description;
            PostObj.postPicture = (postData.postPicture !== undefined) ? postData.postPicture.data : '';
            PostObj.postSentiment = $scope.postSentiment.value;
            PostObj.postMood = $scope.commentMood;

            // PostObj.userScore = 0;
            // PostObj.profileImageUrl = "string";
            // PostObj.profileUrl = "string";
            // PostObj.userScreenName = "string";
            // PostObj.rawLocation = "string";
            // PostObj.postVolume = 0;
            // PostObj.pictureInfo = "string";
            // PostObj.videoLink = "string";
            // PostObj.videoLinkInfo = "string";
            // PostObj.postHashtags = "string";
            // PostObj.socialIdentifier = "fb";
            // PostObj.streamId = "string";
            // PostObj.keyword = "";
            // PostObj.messageId = "string";
            // PostObj.messageLinkUrl = "string";
            // PostObj.rawMessage = "Test rawMessage";
            // PostObj.country = "Test string";
            // PostObj.state = "Test State";
            // PostObj.city = "Test City";

            tribeService.createClanPost(PostObj, $scope.currentClan).success(function(res) {
                $scope.postSentiment.value = 5;
                $scope.moodIcon = false;
                $scope.moodExpError = false;
                $scope.postData = '';
                $scope.closeImg();
                $scope.postListByClan();
            });
        } else {
            $scope.moodExpError = true;
            $scope.moodeError = "Please choose the Mood...!!";
        }
    };
    $scope.closeImg = function() {
        angular.element('.tribeIcons img').attr('src', '');
        angular.element('.tribeImgDiv').css('display', 'none');
    };
   $scope.createPostComment = function(pId,text) {
            $rootScope.shwLoad = true;
            var commentObj = {};
            commentObj.postId = pId;
            commentObj.postCommentSentiment = "50";
            commentObj.postCommentMood = 'HAPPY';
            commentObj.comment = text;
            tribeService.createPostComment(commentObj)
                .success(function(result) {
                    tribeService.setCommentRefresh({ 'refresh': true });
                    $rootScope.shwLoad = false;
                    // $scope.postListByClan();
                }).error(function(data, status) {});

    };
    $scope.postDetails = function(post) {
        $modal.open({
            templateUrl: "/js/app/tribe/templates/clan/categories/clan.post.details.view.html",
            controller: 'PostDetailController',
            size: 'md',
            resolve: {
                postData: function() {
                    return post;
                }
            }
        });
    };

}]);


