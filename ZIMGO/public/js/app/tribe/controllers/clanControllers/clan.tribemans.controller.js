var app = angular.module('tribeModule');
app.controller('TribeMansController', ['$scope', 'tribeService', '$stateParams', function($scope, tribeService, $stateParams) {
    /*$scope.currentCategory = JSON.parse(localStorage.getItem("CurrentCatagory"));
    $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
    $scope.currentClan = $stateParams; //JSON.parse(localStorage.getItem("CurrentClan"));
    $scope.currentClanDetails = JSON.parse(localStorage.getItem("CurrentClan"));*/

    $scope.tribemansList = function() {
        tribeService.getTribeMansByClan($scope.currentClan.clanId).success(function(result) {
            if (result && result.length > 0) {
                $scope.tribeMansList = result;
            }
        }).error(function(data, status) {});
    };
    $scope.tribemansList();
}]);
