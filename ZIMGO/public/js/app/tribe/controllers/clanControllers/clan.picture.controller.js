var app = angular.module('tribeModule');
app.controller('pictureController', ['$scope', '$rootScope', 'languageService', 'tribeService', '$state','$stateParams','$modal', function($scope, $rootScope, languageService, tribeService, $state,$stateParams,$modal) {
    $rootScope.shwLoad = true;

    /*$scope.currentCategory = JSON.parse(localStorage.getItem("CurrentCatagory"));
    $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
    $scope.currentClan = $stateParams;//JSON.parse(localStorage.getItem("CurrentClan"));
    $scope.currentClanDetails = JSON.parse(localStorage.getItem("CurrentClan"));*/

    $scope.addPicture = function(picture) {
        $rootScope.shwLoad = true;
        var pictureData = {};
        pictureData.pictureName = picture.pictureName;
        pictureData.picture = picture.picture.data;
        pictureData.clanName = $scope.currentClanDetails.clanName;
        pictureData.clanId = $scope.currentClan.clanId;
        pictureData.tribeId = $scope.currentTribe.tribeId;
        pictureData.picturePath = '';
        pictureData.hashtags = '';
        pictureData.location = '';
        pictureData.latitude = '';
        tribeService.createPicture(pictureData).success(function(res) {
            $scope.pictureData.pictureName = '';
            angular.element('.tribeImgDiv').css('display', 'none');
            angular.element('.trIcon').css('display', 'none');
            $scope.clanPictureList();
            $rootScope.shwLoad = false;
        });
    };
    $scope.closeImg = function() {
        angular.element('.tribeImgDiv img').attr('src', '');
        angular.element('.tribeImgDiv').css('display', 'none');
    };
    $scope.clanPictureList = function() {
        tribeService.getclanPictures(9,$scope.currentClan.clanId).success(function(res) {
            if (res && res.list && res.list.length > 0) {
                $scope.clanPicutures = res.list;
                $rootScope.shwLoad = false;
            }else {
                $rootScope.shwLoad = false;
            }
        });
    };
    $scope.clanPictureList();

    
    $scope.showSlider = function(pics) {
        var modalInstance = $modal.open({
            templateUrl: 'js/app/tribe/common/templates/photo.slider.view.html',
            controller: function($scope, $modalInstance) {
                $scope.tribemanPicutures = pics;
                $scope.close = function() { $modalInstance.dismiss('cancel'); };
            },
            // size: 'md'
            windowClass: 'picture-slider-window     '


        });
        modalInstance.result.then(function(registerModel) {}, function() {});
    };
}]);
