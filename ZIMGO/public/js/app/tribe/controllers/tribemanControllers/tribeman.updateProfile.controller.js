var app = angular.module('tribeModule');
app.controller('updateProfileCtrl', ['$scope', '$rootScope', 'tribeService', '$modalInstance', '$state', function($scope, $rootScope, tribeService, $modalInstance, $stateParams, $state) {
    $scope.tribemanDetails = JSON.parse(localStorage.getItem('loggedInTribeman'));
    $scope.close = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.closeImg = function() {
        angular.element('.tribeIcons img').attr('src', '');
        angular.element('.tribeImgDiv').css('display', 'none');
    };
    $scope.tribemanProfilePicUpdate = function(picBlob) {
        if (picBlob.data !== '') {
            var obj = {};
            obj.tribemanId = $scope.tribemanDetails.tribemanId;
            obj.profilePic = picBlob.data;
            obj.tribemanName = $scope.tribemanDetails.tribemanName;
            tribeService.loggedInTribemanProfileUpdate($scope.tribemanDetails.tribemanId, obj).then(function(res) {
                localStorage.setItem('loggedInTribeman', JSON.stringify(res.data));
                tribeService.setListRefresh({ 'profilePicUpdate': true });
                $scope.close();

            }, function(error) { /* body... */ });
        } else {
            $scope.imgChoosError = "Please select picture.";
        }
    };
}]);
