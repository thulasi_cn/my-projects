var app = angular.module('tribeModule');
app.controller('tribemanPostController', ['$scope', '$rootScope', 'tribeService', '$stateParams', function($scope, $rootScope, tribeService, $stateParams) {
    // $scope.$watch(function() {
    //     return tribeService.getCommentRefresh();
    // }, function(newValue, oldValue) {
    //     if (newValue) {
    //         $scope.postListByLoggedInUser();
    //     }
    // });
    $rootScope.shwLoad = true;
    $scope.feedData = {};
    $scope.loggedInTribeman = $stateParams;
    $scope.loggedInTribemanDetails = JSON.parse(localStorage.getItem('loggedInTribeman'));
    $scope.isLoggedin = ($scope.loggedInTribeman.tribemanId === $scope.loggedInTribemanDetails.tribemanId) ? true : false;

    $scope.commentMood = '';
    $scope.moodIcon = false;
    $scope.chooseCommetMood = function(mood) {
        $scope.moodIcon = true;
        $scope.moodExpError = false;
        $scope.commentMood = mood.replace(' ', '');
    };


    $scope.postListByLoggedInUser = function() {
        tribeService.getPostsByLoggedInUser()
            .success(function(result) {
                if (result && result.length > 0) {
                    $scope.postList = result;
                    $rootScope.shwLoad = false;
                } else {
                    $rootScope.shwLoad = false;
                }
            }).error(function(data, status) {});
    };
     //$scope.postListByLoggedInUser();

    $scope.postSentiment = {
        value: 5,
        options: {
            floor: 0,
            ceil: 100,
            step: 1,
            showSelectionBar: true,
            getSelectionBarColor: function(value) {
                if (value <= 30)
                    return 'red';
                if (value <= 60)
                    return 'orange';
                if (value <= 90)
                    return 'yellow';
                return '#2AE02A';
            }
        }
    };

    $scope.closeImg = function() {
        angular.element('.tribeIcons img').attr('src', '');
        angular.element('.tribeImgDiv').css('display', 'none');
    };
    tribeService.getClansByTribeman($scope.loggedInTribemanDetails.tribemanId)
        .success(function(clans) {
            $scope.myClanList = clans;
        }).error(function(data, status) {});

    $scope.postFeed = function(postData) {
        if ($scope.commentMood !== '') {
            var PostObj = {};
            PostObj.postName = postData.title;
            PostObj.postText = postData.description;
            PostObj.postPicture = (postData.postPicture !== undefined) ? postData.postPicture.data : '';
            PostObj.postSentiment = $scope.postSentiment.value;
            PostObj.postMood = $scope.commentMood;
            PostObj.profileImageUrl = $scope.loggedInTribemanDetails.profilePic;
            PostObj.userScreenName = $scope.loggedInTribemanDetails.tribemanName;
            tribeService.createClanPost(PostObj, { "clanId": postData.clanId }).success(function(res) {
                $scope.postSentiment.value = 5;
                $scope.moodIcon = false;
                $scope.moodExpError = false;
                $scope.feedData = {};
                $scope.closeImg();
                $scope.postListByLoggedInUser();
            });
        } else {
            $scope.moodExpError = true;
            $scope.moodeError = "Please choose the Mood...!!";
        }
    };
    $scope.createPostComment = function(pId, text) {
        $rootScope.shwLoad = true;
        var commentObj = {};
        commentObj.postId = pId;
        commentObj.postCommentSentiment = "50";
        commentObj.postCommentMood = 'HAPPY';
        commentObj.comment = text;
        tribeService.createPostComment(commentObj)
            .success(function(result) {
                $scope.postListByLoggedInUser();
            }).error(function(data, status) {});

    };
}]);
