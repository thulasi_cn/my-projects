var app = angular.module('tribeModule');
app.controller('tribemanFriendsController', ['$scope', '$rootScope', 'tribeService','$stateParams', function($scope, $rootScope, tribeService,$stateParams) {
    $scope.loggedInTribeman = $stateParams;
    $rootScope.shwLoad = true;$scope.friendsDataList = [];
        $scope.tribemanFriendsList = function() {
        tribeService.getTribemanFriends($scope.loggedInTribeman.tribemanId).success(function(res) {
            if (res && res.length > 0) {
                $scope.friendsDataList = res;
                $rootScope.shwLoad = false;
            }
        });
    };
    $scope.tribemanFriendsList();
}]);
