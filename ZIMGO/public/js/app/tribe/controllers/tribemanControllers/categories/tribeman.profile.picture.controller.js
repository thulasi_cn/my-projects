var app = angular.module('tribeModule');
app.controller('tribemanPictureController', ['$scope', '$modal', '$rootScope', 'tribeService', '$stateParams', function($scope, $modal, $rootScope, tribeService, $stateParams) {
    $rootScope.shwLoad = true;
    // $scope.loggedInTribeman = JSON.parse(localStorage.getItem("loggedInTribeman")); //$stateParams;
    $scope.picChooseError = false;
    $scope.pictureData = {};
    $scope.loggedInTribeman = $stateParams;
    $scope.loggedInTribemanDetails = JSON.parse(localStorage.getItem('loggedInTribeman'));
    $scope.isLoggedin = ($scope.loggedInTribeman.tribemanId === $scope.loggedInTribemanDetails.tribemanId) ? true : false;

    $scope.addPicture = function(pictureFormData) {
        if (pictureFormData.picture.data !== null) {
            $scope.picChooseError = false;
            $rootScope.shwLoad = true;
            var pictureData = {};
            pictureData.tribemanId = $scope.loggedInTribeman.tribemanId;
            pictureData.picture = pictureFormData.picture.data;
            pictureData.imageTitle = pictureFormData.imageTitle;
            pictureData.description = pictureFormData.description;
            pictureData.album = '';
            pictureData.longitude = '';
            pictureData.picturePath = '';
            pictureData.hashtags = '';
            pictureData.location = '';
            pictureData.latitude = '';

            tribeService.addTribemanPicture(pictureData).success(function(res) {
                $scope.pictureData = {};
                $scope.closeImg();
                $scope.tribemanPictureList();
                $rootScope.shwLoad = false;
            });
        } else {
            alert('Please select the picture.');
            $scope.picChooseError = true;
        }

    };
    $scope.closeImg = function() {
        angular.element('.tribeImgDiv img').attr('src', '');
        angular.element('.tribeImgDiv').css('display', 'none');
    };
    $scope.tribemanPictureList = function() {
        tribeService.getTribemanPicture(20, $scope.loggedInTribeman.tribemanId).success(function(res) {
            if (res && res.list && res.list.length > 0) {
                $scope.tribemanPicutures = res.list;
                $rootScope.shwLoad = false;
            }
        });
    };
    $scope.tribemanPictureList();

    $scope.deleteTribemanPicture = function(picId) {
        tribeService.deleteTribemanPicture(picId).success(function(res) {
            $scope.tribemanPictureList();
        });
    };
    $scope.pictureDetails = function(pictureId) {
        var modalInstance = $modal.open({
            templateUrl: 'js/app/tribe/templates/clan/categories/clan.photos.details.modal.view.html',
            controller: 'tribemanPictureDetailsController',
            windowClass: 'tribeCreate-modal-window',
            resolve: {
                pictureId: function() {
                    return tribeService.getTribemanPictureDetails(pictureId).then(function(res) {
                        return res.data;
                    });
                },
                tribemanPicturesList: function() {
                    return $scope.tribemanPicutures;
                },
                loggedInTribeman: function() {
                    return $scope.loggedInTribemanDetails;
                }
            }
        });
        modalInstance.result.then(function(registerModel) {}, function() {});
    };
}]);
app.controller('tribemanPictureDetailsController', ['$scope', 'pictureId', 'tribemanPicturesList', '$modalInstance', 'loggedInTribeman', function($scope,pictureId, tribemanPicturesList, $modalInstance, loggedInTribeman) {
    $scope.loggedInTribemanDetails = loggedInTribeman;
    $scope.tribemanPicturesList = tribemanPicturesList;
    $scope.picDetails = pictureId;

    $scope.close = function() {
        $modalInstance.dismiss('cancel');
    };
}]);
