var app = angular.module('tribeModule');
app.controller('tribemanMytribesController', ['$scope', 'tribeService', '$stateParams', function($scope, tribeService, $stateParams) {

    $scope.loggedInTribeman = $stateParams;
    $scope.setClan = function(cClan) {
        localStorage.setItem("CurrentClan", JSON.stringify(cClan));
    };
    $scope.myClansList = function() {
        tribeService.getClansByTribeman($scope.loggedInTribeman.tribemanId)
            .success(function(result) {
                if (result && result.length > 0) {
                    $scope.clanListData = result;
                }
            }).error(function(data, status) {});
    };
    $scope.myClansList();
    
}]);
