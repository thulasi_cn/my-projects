var app = angular.module('tribeModule');
app.controller('FriendProfileCtrl', ['$scope', '$rootScope', '$modal', 'tribeService', '$stateParams', '$state', 'growl', function($scope, $rootScope, $modal, tribeService, $stateParams, $state, growl) {
    $rootScope.shwLoad = true;
    $scope.friend = $stateParams;
    $scope.loggedInTribemanDetails = JSON.parse(localStorage.getItem('loggedInTribeman'));

    tribeService.getTribemanDetails($scope.friend.tribemanId).success(function(res) {
        $scope.friendDetails = res;
    });
    tribeService.getTribemanFriendsCount($scope.friend.tribemanId).success(function(res) {
        $scope.friendsCount = res[0].count;
    });
    tribeService.getTribemanPictureCount($scope.friend.tribemanId).success(function(res) {
        $scope.tribemanPictureCount = res[0].count;
    });
    $scope.addFriend = function() {
        tribeService.addFriend($scope.friend.tribemanId).success(function(res) {
            growl.success("Your Friend request has been sent successfully to the user..!", {
                ttl: 5000
            });
            // $scope.tribemanPictureCount = res[0].count;
        }, function(error) {
            growl.error("Failed to add friend.", {
                ttl: 5000
            });
        });
    };
    $scope.sendMessage = function() {
        var modalInstance = $modal.open({
            templateUrl: 'js/app/tribe/templates/profile/profile.sendMessage.modal.view.html',
            controller: 'sendMessageCtrl',
            // size: 'md'
            windowClass: 'tribeMessage-modal-window'
        });
        modalInstance.result.then(function(registerModel) {}, function() {});
    };
    $scope.friendsList = [];
    $scope.tribemanFriendsList = function() {
        tribeService.getTribemanFriends($scope.friend.tribemanId).success(function(res) {
            if (res && res.length > 0) {
                $rootScope.shwLoad = false;
                $scope.friendsList = res;
                $scope.filterFrnd = _.filter($scope.friendsList, function(a) {
                    return (a.tribemanId === $scope.friend.tribemanId);
                });
                $scope.addFrndBtn = ($scope.filterFrnd.length !== 0) ? true : false;
            } else {
                $rootScope.shwLoad = false;
            }
        });
    };
    $scope.tribemanFriendsList();
    /* $scope.myTribsList = function() {
         tribeService.getTribesByTribemanId($scope.friend.tribemanId)
             .success(function(result) {
                 if (result && result.length > 0) {
                     $rootScope.shwLoad = false;
                     $scope.tribeListData = result;
                 } else {
                     $rootScope.shwLoad = false;
                 }
             }).error(function(data, status) {});
     };
     $scope.myTribsList();
     $scope.goToCatPage = function(tribe) {
         $rootScope.shwLoad = true;
         tribeService.getOneCategory(tribe.tribeCategory).success(function(category) {
             var categoryRes = category.list[0];
             localStorage.setItem("CurrentCatagory", JSON.stringify(categoryRes));
             $rootScope.shwLoad = false;
             $state.go('main.base.tribe.main.main.categorie', { categorieID: categoryRes.categoryId });
         });
     };
     $scope.goToTribePage = function(tribe) {
         localStorage.setItem("CurrentTribe", JSON.stringify(tribe));
         $state.go('main.base.tribe.main.main.tribes', { tribeId: tribe.tribeId });
     };*/
    $scope.unFriend = function() {
        tribeService.unFriend($scope.friend.tribemanId).success(function(res) {
            growl.success("You Unfriend this user successfully..!!", {
                ttl: 5000
            });
            $scope.tribemanFriendsList();
        }, function(error) {
            growl.error("Failed to unFriend.", {
                ttl: 5000
            });
        });
    };

}]);
