var app = angular.module('tribeModule');
app.controller('sendMessageCtrl', ['$scope', '$rootScope', 'tribeService', '$stateParams', '$modalInstance', function($scope, $rootScope, tribeService, $stateParams, $modalInstance) {
    $scope.friend = $stateParams;
    setTimeout(function() {
        $scope.$broadcast('reCalcViewDimensions');
    }, 50);
    tribeService.getTribemanDetails($scope.friend.tribemanId).success(function(res) {
        $scope.friendDetails = res;
    });
    $scope.close = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.commentMood = '';
    $scope.moodIcon = false;
    $scope.chooseCommetMood = function(mood) {
        $scope.moodIcon = true;
        $scope.moodExpError = false;
        $scope.commentMood = mood.replace(' ', '');
    };
    $scope.messsageSentiment = {
        value: 5,
        options: {
            floor: 0,
            ceil: 100,
            step: 1,
            showSelectionBar: true,
            getSelectionBarColor: function(value) {
                if (value <= 30)
                    return 'red';
                if (value <= 60)
                    return 'orange';
                if (value <= 90)
                    return 'yellow';
                return '#2AE02A';
            }
        }
    };
    $scope.moodExpError = false;
    $scope.sensMessage = function(sndMsg) {
        var messageObj = { "userToId": $scope.friend.tribemanId, "message": sndMsg, "mood": $scope.commentMood, sentiment: $scope.messsageSentiment.value };
        tribeService.sendMessage(messageObj).success(function(res) {
            $scope.close();
        });
    };
}]);
