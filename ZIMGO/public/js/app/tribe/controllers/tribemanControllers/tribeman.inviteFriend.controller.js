var app = angular.module('tribeModule');
app.controller('inviteTribeCtrl', ['$scope', '$rootScope', 'tribeService', '$modalInstance', 'growl', 'friend', function($scope, $rootScope, tribeService, $modalInstance, growl, friend) {
    $scope.tribemanDetails = JSON.parse(localStorage.getItem('loggedInTribeman'));
    $scope.close = function() { $modalInstance.dismiss('cancel'); };
    $scope.selectFrnd = true;
    /*tribeService.getClansByTribeman($scope.tribemanDetails.tribemanId)
        .success(function(clans) {
            $scope.myClanList = clans;
        }).error(function(data, status) {});*/
    var selectedFriend = friend;
    tribeService.unmemberClansList(selectedFriend.tribemanId)
        .success(function(clans) {
             $scope.myClanList = clans;
        }).error(function(data, status) {});

    $scope.clanSelect = function(clan) {
        $scope.selectedClan = clan;
        /*$scope.selectFrnd = false;
        tribeService.frindListUnderClan($scope.tribemanDetails.tribemanId, $scope.selectedClan.clanId).then(function(res) {
            $scope.friendsList = res.data;
        });*/
        $scope.inviteData.inviteText = "Hi " + selectedFriend.tribemanName + ", I'd like to invite you to this clan " + $scope.selectedClan.clanName;
    };
    /*$scope.tribeFriend = function(friend) {
        $scope.showFrndDetails = true;
        $scope.selectedFriend = friend;

        $scope.inviteData.inviteText = "Hi " + $scope.selectedFriend.tribemanName + ", I'd like to invite you to this clan " + $scope.selectedClan.clanName;
    };*/
    $scope.inviteTribe = function(inviteData) {
        var inviteObj = {
            "userToId": selectedFriend.tribemanId,
            "clanId": inviteData.clan.clanId,
            "message": inviteData.inviteText
        };
        tribeService.inviteFriendToClan(inviteObj).then(function(res) {
            growl.success("Your invitation has been successfully sent", {
                ttl: 5000
            });
            $scope.close();
        }, function(error) {
            growl.error("Something went wrong.", {
                ttl: 5000
            });
        });
    };
}]);
