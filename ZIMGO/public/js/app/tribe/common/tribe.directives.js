var tribeModule = angular.module('tribeModule');

tribeModule.directive('createTribe', [function() {
    'use strict';
    return {
        restrict: 'AE',
        required: 'ngModal',
        scope: {
            tname: '@',
            btn: '@',
            tribemanid: '@',
            position: "@",
            tribeid: "@"
        },
        template: '<span ng-click="createTribe()">Create {{btn}}</span>',
        controller: ['$scope', '$modal', 'tribeService', 'languageService', function($scope, $modal, tribeService, languageService) {

            $scope.tribeListData = [];
            $scope.catList = [];
            if ($scope.position === 'tribeProfilepage') {
                tribeService.getTribeByTribeId($scope.tribeid)
                    .success(function(result) {
                        $scope.tribeListData[0] = result;
                    }).error(function(data, status) {});

                /*tribeService.getTribesByTribemanId($scope.tribemanid)
                    .success(function(result) {
                        if (result && result.length > 0) {
                            $scope.tribeListData = result;
                        }
                    }).error(function(data, status) {});*/
            } else if ($scope.btn === 'Clan') {
                tribeService.getTribesByTribemanId($scope.tribemanid)
                    .success(function(result) {
                        if (result && result.length > 0) {
                            $scope.tribeListData = result;
                        }
                    }).error(function(data, status) {});
            }
            $scope.getCategoryList = function() {
                $scope.catList = [];
                tribeService.getCategory(20).success(function(res) {
                    if (res && res.list && res.list.length > 0) {
                        angular.forEach(res.list, function(element, index) {
                            $scope.catList.push(element.category);
                        });
                    }
                });
            };
            $scope.getCategoryList();
            $scope.createTribe = function() {
                var template;
                $scope.getCategoryList();
                if ($scope.tname === "Tribe") {
                    template = '/js/app/tribe/templates/tribe/tribe.create.modal.view.html';
                } else if ($scope.tname === "Clan") {
                    template = '/js/app/tribe/templates/clan/categories/clan.create.view.html';
                }
                $modal.open({
                    templateUrl: template,
                    controller: 'tribeCreateController',
                    size: 'md',
                    resolve: {
                        tribeName: function() {
                            return $scope.tname;
                        },
                        tribeList: function() {
                            return $scope.tribeListData;
                        },
                        catList: function() {
                            return $scope.catList;
                        },
                        position: function() {
                            return $scope.position;
                        }
                    }
                });
            };
        }]
    };
}]).directive("ngFileModel", [function() {
    return {
        scope: {
            ngFileModel: "=",
            imgid: "@"
        },
        link: function(scope, element, attributes) {
            var fileTypes = ['jpg', 'jpeg', 'png', 'ico', 'mp4', 'mp3'];
            element.bind("change", function(changeEvent) {
                var reader = new FileReader();
                var fileData = changeEvent.target.files[0];
                var extension = fileData.name.split('.').pop().toLowerCase(),
                    isSuccess = fileTypes.indexOf(extension) > -1;
                if (isSuccess) {
                    reader.onload = function(loadEvent) {
                        if (fileData.size <= 2024 * 1024) {
                            angular.element('#tribeIcoErr').css('display', 'none');
                            angular.element('#' + scope.imgid).parent('.tribeImgDiv').css('display', 'block');
                            angular.element('.trIcon').css('display', 'none');
                            angular.element('#' + scope.imgid).attr('src', loadEvent.target.result);
                            scope.$apply(function() {
                                scope.ngFileModel = {
                                    lastModified: fileData.lastModified,
                                    lastModifiedDate: fileData.lastModifiedDate,
                                    name: fileData.name,
                                    size: fileData.size,
                                    type: fileData.type,
                                    data: loadEvent.target.result
                                };
                            });
                        } else {
                            angular.element('#tribeIcoErr').css('display', 'block').text("File size must be at below 10MB");
                            angular.element('#' + scope.imgid).parent('.tribeImgDiv').css('display', 'none');
                        }
                    };
                    reader.readAsDataURL(fileData);
                } else {
                    angular.element('#' + scope.imgid).parent('.tribeImgDiv').css('display', 'none');
                    angular.element('#tribeIcoErr').css('display', 'block').text("This type of files are not allowed");
                }
            });

        }
    };
}]).directive('createTribeEvent', [function() {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            ename: '@',
        },
        template: '<button ng-click="createTribeEvent();">Create {{ename}} Event <i class="fa fa-calendar pull-right" aria-hidden="true"></i>',
        controller: ['$scope', '$modal', 'CommonTribeService', '$rootScope', function($scope, $modal, CommonTribeService, $rootScope) {
            $rootScope.eventClicked = $scope.ename;
            $scope.createTribeEvent = function() {
                $modal.open({
                    templateUrl: 'js/app/tribe/templates/clan/createEvent.modal.view.html',
                    controller: function($scope, tribeService, $state, $modalInstance, CommonTribeService, $rootScope) {
                        CommonTribeService.setClanReload({
                            shouldReload: true
                        });
                        $scope.clickName = $rootScope.eventClicked;
                        $scope.currentTribe = JSON.parse(localStorage.getItem("CurrentTribe"));
                        $scope.currentClan = JSON.parse(localStorage.getItem("CurrentClan"));

                        $scope.close = function() {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.eventCreate = function(event) {
                            var eventData = event;
                            if ($rootScope.eventClicked === "Tribe") {
                                eventData.tribeName = $scope.currentTribe.tribeName;
                                eventData.tribeId = $scope.currentTribe.tribeId;
                                tribeService.createTribeEvent(eventData).success(function(res) {
                                    CommonTribeService.setClanReload({
                                        shouldReload: true
                                    });
                                    tribeService.setListRefresh({ 'tribeEvent': true });
                                    $scope.close();
                                    /*$state.transitionTo('main.base.tribe.main.main.tribes.events', {
                                        reload: true,
                                        inherit: false,
                                        notify: true
                                    });*/
                                });
                            } else if ($rootScope.eventClicked === "Clan") {
                                eventData.clanName = $scope.currentClan.clanName;
                                eventData.clanId = $scope.currentClan.clanId;
                                tribeService.createClanEvent(eventData).success(function(res) {
                                    CommonTribeService.setClanReload({
                                        shouldReload: true
                                    });
                                    $scope.close();
                                    /*$state.transitionTo('main.base.clan.main.main.events', {
                                        reload: true,
                                        inherit: false,
                                        notify: true
                                    });*/
                                });
                            }

                        };
                    },
                    windowClass: 'tribeCreate-modal-window',
                    size: 'md',
                });
            };
        }]
    };
}]).directive('commentsList', [function() {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            postid: '@',
        },
        template: '<ul class="clearfix"> <li ng-if="commentList.length !== 0 || commentList !== null" class="clearfix" ng-repeat="c in commentList | limitTo:2" class="clearfix" ng-init="cmt = c.comment;cmnt = cmt.postComment"> <div class="postCommentImg"> <img class="img-circle img-responsive" align="left" data-ng-src="{{cmt.userProfilePic}}" alt="" on-error-img-src="img/default_profile.jpg" src="img/default_profile.jpg" alt=""> </div> <div class="postCommentTextBox"> <strong>{{cmt.username}} <font>{{cmnt.comment}} <!-- @ <img class="img-circle feeling" ng-src="/img/exp_icons/{{cmnt.postCommentMood}}.png" alt=""> --></font></strong> <reply-comment cm="{{cmt}}" position="comment"></reply-comment><reply-list cmtdata="{{c}}"></reply-list> </div> <span class="spanInline" custom-popover type="comment" data="{{cmnt}}"></span> </li> <div class="noResult" ng-if="commentList.length == 0 || commentList == null">No Comments Found</div> </ul>',
        controller: function($scope, tribeService) {
            $scope.$watch(function() {
                return tribeService.getCommentRefresh();
            }, function(newValue, oldValue) {
                if (newValue) {
                    $scope.getPostCommentsList();
                }
            });
            $scope.commentList = [];
            $scope.getPostCommentsList = function() {
                tribeService.getClanPostComment($scope.postid)
                    .success(function(comments) {
                        angular.forEach(comments, function(element, index) {
                            $scope.commentList.push(element);
                        });
                    }).error(function(data, status) {});
            };

            $scope.getPostCommentsList();
        }
    };
}]).directive('replyComment', [function() {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            cm: '@',
            position: '@'
        },
        template: '<div><span ng-click="cmtLike();" ng-show="likeShw">Like</span><span ng-show="!likeShw">Unlike</span><span ng-click="replyCmt();">.Reply</span><span> . {{noOfLikes}} <i class="fa fa-thumbs-up" aria-hidden="true"></i> </span> <em>{{comment.postComment.createdAt | amDateFormat:"MMMM Do YYYY"}}</em><div class="replyDiv" ng-if="replyShw"><img class="img-responsive replyPfImg" align="left" data-ng-src="{{comment.userProfilePic}}" alt="" on-error-img-src="img/default_profile.jpg" src="img/default_profile.jpg" alt=""><input class="form-control" type="text" ng-model="replyCmtText.commentText" ng-enter="replyCommentMsg(replyCmtText);"></div></div>',
        controller: function($scope, tribeService) {
            $scope.comment = JSON.parse($scope.cm);
            $scope.replyShw = false;

            $scope.likeShw = ($scope.comment.likedStatus === true) ? false : true;
            $scope.noOfLikes = ($scope.comment.postComment.likes === null) ? 0 : $scope.comment.postComment.likes;
            var likeObj;
            if ($scope.position === "comment") {
                likeObj = {
                    "commentId": $scope.comment.postComment.commentId
                };
            } else if ($scope.position === "reply") {
                likeObj = {
                    "replyId": $scope.comment.postComment.commentId
                };
            }
            $scope.cmtLike = function() {
                tribeService.likeComment(likeObj).success(function(res) {
                    $scope.noOfLikes = parseInt($scope.noOfLikes) + 1;
                    $scope.likeShw = ($scope.comment.likedStatus === true) ? true : false;
                }).error(function(data, status) {});

            };
            $scope.replyCmt = function() {
                $scope.replyShw = true;
            };
            $scope.replyCmtText = {};
            $scope.replyCommentMsg = function(replyCmtText) {
                var replyObj = {
                    "cmtId": $scope.comment.postComment.commentId,
                    "postId": $scope.comment.postComment.postId,
                    "comment": replyCmtText.commentText,
                    "postCommentSentiment": 0,
                    "postCommentMood": "HAPPY"
                };
                tribeService.replyComment(replyObj).success(function(res) {
                    tribeService.setCommentRefresh({ 'refresh': true });
                    $scope.replyCmtText = {};
                    $scope.replyShw = false;
                }).error(function(data, status) {});
            };
        },
        link: function() {

        }
    };
}]).directive('replyList', [function() {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            cmtdata: '@',
        },
        template: '<ul class="clearfix"> <li ng-if="replyArr.length !== 0 || replyArr !== null" class="clearfix" ng-repeat="cmt in replyArr | limitTo:2" class="clearfix" ng-init="ct = cmt.postComment"> <div class="replyCommentImg"> <img class="img-circle img-responsive" align="left" data-ng-src="{{cmt.userProfilePic}}" alt="" on-error-img-src="img/default_profile.jpg" src="img/default_profile.jpg" alt=""> </div> <div class="replyCommentTextBox"> <strong>{{cmt.username}} <font>{{ct.comment}}</font></strong> <reply-comment cm="{{cmt}}" position="reply"></reply-comment> </div></li> </ul>',
        controller: function($scope, tribeService) {
            var reply = JSON.parse($scope.cmtdata);
            $scope.replyArr = reply.replies;
            // $scope.getPostCommentsList = function() {
            //     var rlistobj = { 'postId': $scope.comment.postComment.postId, 'commentId': $scope.comment.postComment.commentId };
            //     $scope.replyArr = [];
            //     tribeService.getCommentReplys(rlistobj)
            //         .success(function(cmtRplys) {
            //             $scope.replyArr = cmtRplys;
            //         }).error(function(data, status) {});
            // };

            // $scope.getPostCommentsList();
        }
    };
}]).directive('starRating', [function() {
    return {
        restrict: 'A',
        template: '<ul class="star-rating" ng-class="{readonly: readonly}">' +
            '  <li ng-repeat="star in stars" class="star" ng-class="{filled: star.filled}" ng-click="toggle($index)">' +
            '    <i class="fa fa-star rateStar"></i>' + // or &#9733
            '  </li>' +
            '</ul>',
        scope: {
            ratingValue: '=ngModel',
            max: '=?', // optional (default is 5)
            onRatingSelect: '&?',
            readonly: '=?'
        },
        link: function(scope, element, attributes) {
            if (scope.max == undefined) {
                scope.max = 5;
            }

            function updateStars() {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };
            scope.toggle = function(index) {
                if (scope.readonly == undefined || scope.readonly === false) {
                    scope.ratingValue = index + 1;
                    scope.onRatingSelect({
                        rating: index + 1
                    });
                }
            };
            scope.$watch('ratingValue', function(oldValue, newValue) {
                if (newValue) {
                    updateStars();
                }
            });
        }
    };
}]).directive('customPopover', [function() {
    'use strict';
    return {
        restrict: 'A',
        scope: {
            type: '@',
            data: '@'
        },
        template: '<div ng-click="getData(type,data)" popover-template="\'/js/app/tribe/common/templates/edit.delete.html\'" popover-toggle="isPopoverOpen" popover-placement="bottom"> <div class="popoverParent" ng-click="togglePopover();"><i class="fa fa-chevron-down" aria-hidden="true"></i> </div> </div> ',
        controller: ['$scope', '$rootScope', function($scope, $rootScope) {
            $rootScope.$on('modalOpenChk', function(event, args) {
                if ($scope.isPopoverOpen) {
                    $scope.togglePopover();
                    $scope.$digest();
                }
            });
            $scope.togglePopover = function() {
                $scope.isPopoverOpen = !$scope.isPopoverOpen;
            };
            $scope.getData = function(type, comment, post) {
                $scope.type = type;
                $scope.comment = comment;
                $scope.post = post;
            };
        }]
    };
}]).directive('editCommentPost', [function() {
    'use strict';
    return {
        restrict: 'A',
        scope: {
            type: '@',
            data: '@'
        },
        template: '<span ng-click="edit(type,data)">Edit</span>',
        controller: ['$scope', 'tribeService', '$modal', function($scope, tribeService, $modal) {
            $scope.edit = function(type, data) {
                var editData = data;
                var template;
                if (type === 'comment') {
                    template = '/js/app/tribe/common/templates/edit.comment.html';
                } else if (type === 'post') {
                    template = '/js/app/tribe/common/templates/edit.post.html';
                }
                $modal.open({
                    templateUrl: template,
                    controller: 'PostCommentEditController',
                    size: 'sm',
                    windowClass: 'tribeMessage-modal-window',
                    resolve: {
                        type: function() {
                            return type;
                        },
                        data: function() {
                            return editData;
                        }
                    }
                });
            };
        }]
    };
}]).directive('deleteCommentPost', [function() {
    'use strict';
    return {
        restrict: 'A',
        scope: {
            data: '@',
            type: '@'
        },
        template: '<span ng-click="delete(type,data)">Delete</span>',
        controller: ['$scope', 'tribeService', function($scope, tribeService) {
            $scope.delete = function(type, data) {
                var data = JSON.parse(data);
                var commentId = data.commentId;
                var postId = data.postId;
                if (type === 'comment') {
                    tribeService.deletePostComment(postId, commentId).then(function(res) {
                        if (res.data.message === 'success') {
                            tribeService.setCommentRefresh({ 'refresh': true });
                        }
                    });
                } else if (type === 'post') {
                    var postDelObj = {
                        "postName": data.postName,
                        "postId": data.postId
                    };
                    tribeService.deleteClanPost(postDelObj).then(function(res) {
                        tribeService.setCommentRefresh({ 'refresh': true });
                    });
                }
            };
        }]
    };
}]).directive('bookmarkedList', [function() {
    'use strict';
    return {
        restrict: 'E',
        scope: {
            tribeman: '@'
        },
        template: ' <div class="bookMarkList"> <i class="fa fa-star pull-right" tooltip-html-unsafe="Bookmarked Clan List" tooltip-placement="top" ng-click="openBookmark();" aria-hidden="true"></i> </div>',
        controller: ['$scope', 'tribeService', '$modal', function($scope, tribeService, $modal) {
            var tribemanData = JSON.parse($scope.tribeman);
            var bookmarksList;
            $scope.openBookmark = function(data) {
                $modal.open({
                    templateUrl: '/js/app/tribe/templates/clan/clan.bookmark.list.view.html',
                    controller: function($scope, tribeman, $modalInstance, $state) {
                        $scope.tribeman = tribeman;
                        $scope.close = function() { $modalInstance.dismiss('cancel'); };
                        $scope.getBookmarksList = function() {
                            tribeService.getBookmarkListByTribeman($scope.tribeman.tribemanId).then(function(res) {
                                $scope.bookmarkList = res.data.list;
                            }, function(error) { console.log(error); });
                        };
                        $scope.getBookmarksList();
                        $scope.goToClan = function(bookmark) {
                            $state.go('main.base.tribe.main.main.categorie.tribes.clan.post', { 'clanId': bookmark.CLAN_ID });
                            $scope.close();
                        };
                        $scope.removeClanBookmark = function(bookmark) {
                            tribeService.deleteBookmarkClan(bookmark.BOOKMARK_ID).then(function(res) {
                                $scope.getBookmarksList();
                            }, function(error) { console.log(error); });
                        };
                    },
                    size: 'md',
                    resolve: {
                        tribeman: function() {
                            return tribemanData;
                        }
                    }
                });
            };
        }]
    };
}]).directive('messagesList', [function() {
    'use strict';
    return {
        restrict: 'E',
        scope: {},
        template: '<a class="pull-right" ng-click="openMessages();">Read More</a>',
        controller: ['$scope', 'tribeService', '$modal', '$stateParams', function($scope, tribeService, $modal, $stateParams) {
            $scope.loggedInTribeman = $stateParams;
            $scope.messageList = [];
            $scope.getLoggedInTribemanDetails = function() {
                tribeService.getTribemanDetails($scope.loggedInTribeman.tribemanId).success(function(res) {
                    $scope.tribemanDetails = res;
                });
            };
            $scope.getLoggedInTribemanDetails();
            tribeService.getAllLoggedInUserMessage().then(function(res) {
                console.log(res);
                $scope.messageList = res.data;
            }, function(error) { console.log(error); });
            $scope.openMessages = function(data) {
                $modal.open({
                    templateUrl: '/js/app/tribe/templates/profile/profile.messages.list.view.html',
                    controller: function($scope, tribeman, $modalInstance, $state, msgList) {
                        $scope.tribeman = tribeman;
                        $scope.close = function() { $modalInstance.dismiss('cancel'); };
                        console.log(msgList);
                        $scope.mList = msgList;
                    },
                    size: 'md',
                    resolve: {
                        tribeman: function() {
                            return $scope.tribemanDetails;
                        },
                        msgList: function() {
                            return $scope.messageList;
                        }
                    }
                });
            };
        }]
    };
}]).directive('invitationList', [function() {
    'use strict';
    return {
        restrict: 'E',
        scope: {},
        template: '<a class="pull-right" ng-click="openInvitations();">Read More</a>',
        controller: ['$scope', 'tribeService', '$modal', '$stateParams', function($scope, tribeService, $modal, $stateParams) {
            $scope.loggedInTribeman = $stateParams;
            $scope.invitesList = [];
            $scope.getLoggedInTribemanDetails = function() {
                tribeService.getTribemanDetails($scope.loggedInTribeman.tribemanId).success(function(res) {
                    $scope.tribemanDetails = res;
                });
            };
            $scope.getLoggedInTribemanDetails();
            tribeService.getAllInvites().success(function(invites) {
                $scope.invitesList = invites;
            });
            $scope.openInvitations = function(data) {
                $modal.open({
                    templateUrl: '/js/app/tribe/templates/profile/profile.receive.invitation.list.view.html',
                    controller: function($scope, tribeman, $modalInstance, $state, invList, growl) {
                        $scope.tribeman = tribeman;
                        $scope.close = function() { $modalInstance.dismiss('cancel'); };
                        // $scope.invtList = invList;
                        tribeService.getAllInvites().success(function(invites) {
                            $scope.invtList = invites;
                        });
                        $scope.acceptClanInvt = function(invId) {
                            tribeService.acceptClanInviteRequest(invId).then(function(res) {
                                growl.success("You Successfully Accepted the clan request...!", {
                                    ttl: 5000
                                });
                                $scope.close();
                            }, function(error) { console.log(error); });
                        };
                        $scope.rejectClanInvt = function(invId) {
                            tribeService.rejectClanInviteRequest(invId).then(function(res) {
                                growl.success("You Successfully Rejected the clan request...!", {
                                    ttl: 5000
                                });
                                $scope.close();
                            }, function(error) { console.log(error); });
                        };
                    },
                    size: 'md',
                    resolve: {
                        tribeman: function() {
                            return $scope.tribemanDetails;
                        },
                        invList: function() {
                            return $scope.invitesList;
                        }
                    }
                });
            };
        }]
    };
}]);
