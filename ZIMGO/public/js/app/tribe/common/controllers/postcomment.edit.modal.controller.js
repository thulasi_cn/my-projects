var tribeModule = angular.module('tribeModule');
tribeModule.controller('PostCommentEditController', ['$scope', 'type', 'data', 'tribeService', '$modalInstance', function($scope, type, data, tribeService, $modalInstance) {
    $scope.close = function() {
        $modalInstance.dismiss('cancel');
    };
    if (type === 'comment') {
        $scope.comment = JSON.parse(data);
        $scope.commentMood = $scope.comment.postCommentMood.replace(' ', '');
        $scope.chooseCommetMood = function(mood) {
            $scope.commentMood = mood.replace(' ', '');
        };
        $scope.editPostComment = function() {
            var commentObj = {};
            commentObj.commentId = $scope.comment.commentId;
            commentObj.postId = $scope.comment.postId;
            commentObj.postCommentSentiment = $scope.comment.postCommentSentiment;
            commentObj.postCommentMood = $scope.commentMood;
            commentObj.comment = $scope.comment.comment;
            tribeService.updatePostComment(commentObj)
                .success(function(result) {
                    if (result.message === 'success') {
                        tribeService.setCommentRefresh({ 'refresh': true });
                        $scope.close();
                    }
                }).error(function(data, status) {});
        };
    } else if (type === 'post') {
        $scope.post = JSON.parse(data);
        $scope.showPostImg = true;
        $scope.changeImg = function() {
            $scope.showPostImg = false;
        };
        $scope.closeImg = function() {
            angular.element('.tribeIcons img').attr('src', '');
            angular.element('.tribeImgDiv').css('display', 'none');
        };
        $scope.updatePost = function (postData) {
            console.log(postData);
        };
    }
}]);
