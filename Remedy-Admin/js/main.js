//(function(window, document, $, undefined) {
$("#overlay").show();
var App = {
    globals: {
        selecters: {
            navigation: $('#navigation'),
            breadcrumbs: $('#breadcrumbs'),
            addBtn: $('#add'),
            addModal: $('#addModal'),
            addModalLabel: $('#addModalLabel'),
            addModalBody: $('#addModalBody'),
            addSave: $('#addSave'),
            addUpdate: $('#update'),
            deletebtn: $('.bootstrap-dialog-footer-buttons>.btn-primary'),
            logoutBtn: $('#logoutBtn')
        },
        tableInstance: null,
        activeSection: null,
        imageValues: [],
        UID: [],
        errorem: 0
    },
    init: function() {
        this.authenticate();
    },
    authenticate: function() {

        if (window.sessionStorage.getItem('credentials') !== null) {
            var sessionData = JSON.parse(window.sessionStorage.getItem('credentials'));
            var email = sessionData.email;
            var password = window.atob(sessionData.password);
            firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error, user) {
                if (error === null) {
                    console.log("User signin successfully:", user);
                } else {
                    console.log("Error signin user:", error);
                }
            }).then(function(user, error) {
                if (user) {
                    App.bindEvents();
                    App.loggedInUserData();
                    App.checkNav();
                } else if (error) {
                    console.log("Error signin user:", error);
                }
            });
        } else {
            window.location.href = "login.html";
        }
    },
    bindEvents: function() {
        // Initial Control
        this.globals.activeSection = this.globals.selecters.navigation.find('.active').data('id');
        var btnTxt = App.globals.activeSection.charAt(0).toUpperCase() + App.globals.activeSection.slice(1);
        if (App.globals.activeSection.indexOf('_') !== -1) {
            btnTxt = btnTxt.replace('_', ' ');
        }

        this.globals.selecters.addBtn.find('span').text('Add ' + btnTxt);
        this.globals.selecters.breadcrumbs.text(btnTxt);

        // Add Button Control
        this.globals.selecters.addBtn.on('click', function() {
            App.addData(App.globals.activeSection);
        });

        // Save Button Control
        this.globals.selecters.addSave.on('click', function() {
            var text = $(this).text();
            if (text === 'Save') {
                App.saveData(App.globals.activeSection);
            } else if (text === 'Update') {
                App.updateData(App.globals.activeSection);
            }
        });

        // Logout Button Control
        this.globals.selecters.logoutBtn.on('click', function() {
            $("#overlay_test").show();
            App.userLogout();
        });

        // Navigation Control
        App.globals.selecters.navigation.find('li').on('click', function(e) {
            $("#overlay_test").show();

            // Interchange Active Navigation
            App.globals.selecters.navigation.find('li').removeClass('active');
            $(this).addClass('active');

            // Store Active Tab Information
            App.globals.activeSection = App.globals.selecters.navigation.find('.active').data('id');

            // Get Information about expected class
            App.getInfo(App.globals.activeSection);

            // Update Button Texts
            var btnTxt = App.globals.activeSection.charAt(0).toUpperCase() + App.globals.activeSection.slice(1);
            if (App.globals.activeSection.indexOf('_') !== -1) {
                btnTxt = btnTxt.replace('_', ' ');
            }
            App.globals.selecters.addBtn.find('span').text('Add ' + btnTxt);
            App.globals.selecters.breadcrumbs.text(btnTxt);

            $('#dataTable').on('click', '#editbtn', function() {
                var data = [];
                var selector = $(this).parents('tr').children();
                for (var i = 0; i < selector.length; i++) {
                    console.log(selector[i].innerHTML);
                    data.push(selector[i].innerHTML);
                }
                App.updateFields(data);
                data.length = 0;
            });

        });
        $('#dataTable').on('click', '#deletebtn', function() {

            var data = [];
            var selector = $(this).parents('tr').children();
            for (var i = 0; i < selector.length; i++) {
                data.push(selector[i].innerHTML);
            }
            BootstrapDialog.confirm('Are you sure?', function(result) {
                if (result) {
                    App.deleteFieldData(data);
                    data.length = 0;
                    App.deleteData(App.globals.activeSection);
                } else {
                    console.log("not deleteing");
                }
            });
        });
    },
    checkNav: function() {
        var nav = location.hash;
        if (nav !== "") {
            // Interchange Active Navigation
            var $li = $('a[href=' + nav + ']').parent();
            App.globals.selecters.navigation.find('li').removeClass('active');
            $li.addClass('active');
            var hashNav = nav.replace('#', '');
            App.getInfo(hashNav);
        } else {
            App.getUserInfo();
        }
    },
    getInfo: function(thisData) {
        var $this = thisData;
        switch ($this) {
            case 'users':
                App.getUserInfo();
                break;
            case 'appointments':
                App.getAppointmentInfo();
                break;
            case 'insurance':
                App.getInsuranceInfo();
                break;
            case 'promocode':
                App.getPromocodeInfo();
                break;
            case 'referral':
                App.getReferralInfo();
                break;
            case 'settings':
                App.getSettingsInfo();
                break;
            case 'slots':
                App.getSlotsInfo();
                break;
            case 'users_message':
                App.getUserMessagesInfo();
                break;
            case 'zipcode':
                App.getZipCodeInfo();
                break;
            case 'staff':
                App.getStaffInfo();
                break;
        }
    },
    addData: function(thisData) {
        var $this = thisData;
        switch ($this) {
            case 'users':
                App.addUser();
                break;
            case 'appointments':
                App.addAppointment();
                break;
            case 'insurance':
                App.addInsurance();
                break;
            case 'promocode':
                App.addPromocode();
                break;
            case 'referral':
                App.addReferral();
                break;
            case 'settings':
                App.addSettings();
                break;
            case 'slots':
                App.addSlots();
                break;
            case 'users_message':
                App.addUserMessage();
                break;
            case 'zipcode':
                App.addZipCode();
                break;
            case 'staff':
                App.addStaff();
                break;
        }
        $('.modal-footer').find('.btn-primary').text('Save');
        $('#addModalLabel').text($('#addModalLabel').text().replace('Update', 'Add'));
    },
    saveData: function(thisData) {
        var $this = thisData;
        switch ($this) {
            case 'users':
                App.saveUser();
                break;
            case 'appointments':
                App.saveAppointment();
                break;
            case 'insurance':
                App.saveInsurance();
                break;
            case 'promocode':
                App.savePromocode();
                break;
            case 'referral':
                App.saveReferral();
                break;
            case 'settings':
                App.saveSettings();
                break;
            case 'slots':
                App.saveSlots();
                break;
            case 'users_message':
                App.saveUserMessage();
                break;
            case 'zipcode':
                App.saveZipCode();
                break;
            case 'staff':
                App.saveStaff();
                break;
        }
    },
    updateData: function(thisData) {
        var $this = thisData;
        switch ($this) {
            case 'appointments':
                App.updateAppointment();
                break;
            case 'insurance':
                App.updateInsurance();
                break;
            case 'promocode':
                App.updatePromocode();
                break;
            case 'referral':
                App.updateReferral();
                break;
            case 'settings':
                App.updateSettings();
                break;
            case 'slots':
                App.updateSlots();
                break;
            case 'users_message':
                App.updateUserMessage();
                break;
            case 'zipcode':
                App.updateZipCode();
                break;
            case 'staff':
                App.updateStaff();
                break;
        }
    },
    deleteData: function(thisData) {
        var $this = thisData;
        switch ($this) {
            case 'appointments':
                App.deleteAppointment();
                break;
            case 'insurance':
                App.deleteInsurance();
                break;
            case 'promocode':
                App.deletePromocode();
                break;
            case 'referral':
                App.deleteReferral();
                break;
            case 'settings':
                App.deleteSettings();
                break;
            case 'slots':
                App.deleteSlots();
                break;
            case 'users_message':
                App.deleteUserMessage();
                break;
            case 'zipcode':
                App.deleteZipCode();
                break;
            case 'staff':
                App.deleteStaff();
                break;
        }
    },
    getUserInfo: function() {
        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },
                { title: "First&nbsp;Name" },
                { title: "Last&nbsp;Name" },
                { title: "Email" },
                { title: "Phone" },
                { title: "Role" },
                { title: "Stripe&nbsp;ID" },
                { title: "Card&nbsp;Status" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }
            ];

        firebase.database().ref('users').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }

            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);

                for (; i < getID.length; i++) {
                    id = getID[i];
                    dataSet.push([
                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),
                        ((res[id].first_name !== undefined) && res[id].first_name || ""),
                        ((res[id].last_name !== undefined) && res[id].last_name || ""),
                        ((res[id].email !== undefined) && res[id].email || ""),
                        ((res[id].phone !== undefined) && res[id].phone || ""),
                        ((res[id].role !== undefined) && res[id].role || ""),
                        ((res[id].stripe_id !== undefined) && res[id].stripe_id || ""),
                        (((res[id].stripe_id !== undefined) && res[id].stripe_id || "") === '' ? "Not Saved" : "Saved"),
                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }

            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            dataSet.length = 0;
        });
    },
    addUser: function() {
        var html = '<form id="addUser" onsubmit="return false;"> <div class="form-group"> <label for="first_name" class="control-label">First Name:</label> <input type="text" class="form-control" id="first_name" name="first_name" required> </div> <div class="form-group"> <label for="last_name" class="control-label">last Name:</label> <input type="text" class="form-control" id="last_name" name="last_name"  required> </div> <div class="form-group"> <label for="email" class="control-label">Email:</label> <input type="email" class="form-control" id="email"  name="email" required> </div> <div class="form-group"> <label for="phone" class="control-label">Phone:</label> <input type="text" class="form-control" id="phone"  name="phone" maxlength="10" class="form-control inputFields" data-rule-minlength="10" data-rule-maxlength="10" data-msg-minlength="Phone numbers should be 10 digits" data-msg-maxlength="Phone numbers should be 10 digits"  required> </div><div class="form-group"> <label class="control-label" for="role">Role</label> <select id="role" name="role" class="form-control inputFields" required> <option value="" selected>Select Role</option> <option value="admin">Admin</option> <option value="user">User</option> </select> </div> <div class="form-group"> <label for="password" class="control-label">Password:</label> <input type="password" class="form-control" id="password"  name="password" required> </div> </form>';


        App.globals.selecters.addModalLabel.html('Add User');
        App.globals.selecters.addModalBody.html('').html(html);
        App.validateForms('#addUser');
        $("#phone").keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#phone-error").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
        App.globals.selecters.addModal.modal('show');
    },
    saveUser: function() {
        var form = $("#addUser");

        if (form.valid()) {
            $("#overlay_test").show();
            var userInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');
            userInformation.first_name = $('#first_name').val(),
                userInformation.last_name = $('#last_name').val(),
                userInformation.email = $('#email').val(),
                userInformation.phone = $('#phone').val(),
                userInformation.role = $('#role').val(),
                userInformation.password = $('#password').val();

            firebase.auth().createUserWithEmailAndPassword(userInformation.email, userInformation.password).catch(function(error, user) {
                if (error !== null) {
                    console.log("Error creating user:", error);
                    if (error.code == "auth/email-already-in-use") {
                        App.alertShow('danger', 'Already have an account with this email: "' + userInformation.email + '". (Email address must be unique).');
                        $btn.button('reset');
                    } else {
                        App.alertShow('danger', 'There was a problem to create your account. <br>Please try again later.');
                        $btn.button('reset');
                    }
                }
            }).then(function(snapshot) {
                firebase.database().ref('users/' + snapshot.uid).set({
                    first_name: userInformation.first_name,
                    last_name: userInformation.last_name,
                    email: userInformation.email,
                    phone: userInformation.phone,
                    role: userInformation.role,
                    createdAt: Date.now(),
                    updatedAt: Date.now()
                });
                $("#overlay_test").hide();
                App.alertShow('success', 'Account created successfully.');
                $btn.button('reset');
                setTimeout(function() {
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("AddUser form not validate yet.");
        }
    },
    getAppointmentInfo: function() {

        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },

                { title: "Patient&nbsp;Name" },
                { title: "Patient&nbsp;DOB" },
                { title: "Responsible&nbsp;Party&nbsp;Name" },
                { title: "Contact&nbsp;Phone&nbsp;Number" },
                { title: "Contact&nbsp;Email" },


                { title: "Appointment&nbsp;Type" },
                { title: "User" },

                { title: "Visit&nbsp;Time" },
                { title: "Visit&nbsp;Address" },
                /*{ title: "Visit&nbsp;Address&nbsp;2" },
                { title: "Visit&nbsp;City" },
                { title: "Visit&nbsp;State" },
                { title: "Visit&nbsp;Zip&nbsp;Code" },*/

                { title: "Patient&nbsp;Gender" },
                { title: "Patient&nbsp;Relation" },

                { title: "Bill&nbsp;Name" },
                { title: "Bill&nbsp;Address&nbsp;1" },
                { title: "Bill&nbsp;Address&nbsp;2" },
                { title: "Bill&nbsp;City" },
                { title: "Bill&nbsp;State" },
                { title: "Bill&nbsp;Zip" },
                { title: "Bill&nbsp;Promo" },

                { title: "Payment&nbsp;Date" },
                { title: "Payment&nbsp;Amount" },
                { title: "Payment&nbsp;Status" },
                { title: "Coord" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }, {
                    title: "",
                    "data": null,
                    "orderable": false,
                    "defaultContent": "<i id='editbtn' class='fa fa-pencil-square fa-2x' aria-hidden='true'></i>  <i  id='deletebtn' class='fa fa-trash-o fa-2x' aria-hidden='true'></i>"
                }
            ];

        firebase.database().ref('appointments').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }
            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);

                for (; i < getID.length; i++) {
                    id = getID[i];
                    dataSet.push([
                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),

                        ((res[id].patient_name !== undefined) && res[id].patient_name || ""),
                        ((res[id].patient_dob !== undefined) && res[id].patient_dob || ""),
                        ((res[id].responsible_party_name !== undefined) && res[id].responsible_party_name || ""),
                        ((res[id].contact_phone_number !== undefined) && res[id].contact_phone_number || ""),
                        ((res[id].contact_email !== undefined) && res[id].contact_email || ""),


                        ((res[id].appointment_type !== undefined) && res[id].appointment_type || ""),
                        ((res[id].appointment_user !== undefined) && res[id].appointment_user || ""),


                        ((res[id].visit_time !== undefined) && res[id].visit_time || ""),

                        ((res[id].visit_address_1 !== undefined) && res[id].visit_address_1 || ""),
                        /*((res[id].visit_address_2 !== undefined) && res[id].visit_address_2 || ""),
                        ((res[id].visit_city !== undefined) && res[id].visit_city || ""),
                        ((res[id].visit_state !== undefined) && res[id].visit_state || ""),
                        ((res[id].visit_zip_code !== undefined) && res[id].visit_zip_code || ""),*/

                        ((res[id].patient_gender !== undefined) && res[id].patient_gender || ""),
                        ((res[id].patient_relation !== undefined) && res[id].patient_relation || ""),

                        ((res[id].billing_name !== undefined) && res[id].billing_name || ""),

                        ((res[id].billing_address_1 !== undefined) && res[id].billing_address_1 || ""),
                        ((res[id].billing_address_2 !== undefined) && res[id].billing_address_2 || ""),
                        ((res[id].billing_city !== undefined) && res[id].billing_city || ""),
                        ((res[id].billing_state !== undefined) && res[id].billing_state || ""),
                        ((res[id].billing_zip !== undefined) && res[id].billing_zip || ""),
                        ((res[id].billing_promo !== undefined) && res[id].billing_promo || ""),
                        ((res[id].payment_date !== undefined) && (App.timeConverter(res[id].payment_date) || "")),

                        ((res[id].payment_amount !== undefined) && res[id].payment_amount || ""),

                        ((res[id].payment_status !== undefined) && res[id].payment_status || ""),

                        ((res[id].coordinates !== undefined) && res[id].coordinates || ""),


                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }
            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            dataSet.length = 0;
        });

    },
    addAppointment: function() {


        var html = '<form id="addAppointment" onsubmit="return false;"> <div class="form-group"> <label class="control-label" for="patient_name">Patient Name</label> <input id="patient_name" name="patient_name" type="text" placeholder="Patient Name" class="form-control inputFields" required> </div><div class="form-group"> <label class="control-label" for="patient_dob">Patient DOB</label> <input id="patient_dob" name="patient_dob" type="text" placeholder="Patient DOB" class="form-control inputFields" required> </div><div class="form-group"> <label class="control-label" for="responsible_party_name">Responsible Party Name</label> <input id="responsible_party_name" name="responsible_party_name" type="text" placeholder="Responsible Party Name" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="contact_phone_number">Contact Phone Number</label> <input id="contact_phone_number" name="contact_phone_number" type="number" placeholder="Contact Phone Number" class="form-control inputFields" data-rule-minlength="10" data-rule-maxlength="10" data-msg-minlength="Phone numbers should be 10 digits" data-msg-maxlength="Phone numbers should be 10 digits" required> </div><div class="form-group"> <label class="control-label" for="contact_email">Contact Email</label> <input id="contact_email" name="contact_email" type="email" placeholder="Contact Email" class="form-control inputFields" required> </div><div class="form-group"> <label class="control-label" for="appointment_type">Appointment Type</label> <select id="appointment_type" name="appointment_type" class="form-control inputFields" required> <option value="" selected>Select Visit Type</option> <option value="appointment_visit">Appointment Visit</option> <option value="video_visit">Video Visit</option> </select> </div><div class="form-group"> <label class="control-label" for="user">User</label> <input id="appointment_user" name="user" type="text" placeholder="User" class="form-control inputFields input-md" required> </div><div class="form-group"> <label class="control-label" for="visit_time">Visit Time</label> <select id="visit_time" name="visit_time" class="form-control inputFields" required> </select> </div><div class="form-group"> <label class="control-label" for="visit_address_1">Visit Address</label> <input id="visit_address_1" name="visit_address_1" type="text" placeholder="Visit Address" class="form-control inputFields" required> </div><div class="form-group"> <label class="control-label" for="patient_gender">Patient Gender</label> <select id="patient_gender" name="patient_gender" class="form-control inputFields" required> <option value="male">Male</option> <option value="female">Female</option> </select> <div class="form-group"> <label class="control-label" for="patient_relation">Patient Relation</label> <input id="patient_relation" name="patient_relation" type="text" placeholder="Patient Relation" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="billing_name">Billing Name</label> <input id="billing_name" name="billing_name" type="text" placeholder="Billing Name" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="billing_address_1">Billing Address 1</label> <input id="billing_address_1" name="billing_address_1" type="text" placeholder="Billing Address 1" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="billing_address_2">Billing Address 2</label> <input id="billing_address_2" name="billing_address_2" type="text" placeholder="Billing Address 2" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="billing_city">Billing City</label> <input id="billing_city" name="billing_city" type="text" placeholder="Billing City" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="billing_state">Billing State</label> <input id="billing_state" name="billing_state" type="text" placeholder="Billing State" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="billing_zip">Billing Zip</label> <input id="billing_zip" name="billing_zip" type="text" placeholder="Billing Zip" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="billing_promo">Billing Promo</label> <input id="billing_promo" name="billing_promo" type="text" placeholder="Billing Promo" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="payment_date">Payment Date</label> <input id="payment_date" name="payment_date" type="text" placeholder="Payment Date" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="payment_amount">Payment Amount</label> <input id="payment_amount" name="payment_amount" type="text" placeholder="Payment Amount" class="form-control inputFields"> </div><div class="form-group"> <label class="control-label" for="payment_status">Payment Status</label> <select id="payment_status" name="payment_status" class="form-control inputFields"> <option value="true">true</option> <option selected value="false">false</option> </select> <div class="form-group"> <label class="control-label" for="coordinates">Coordinates</label> <input id="coordinates" name="coordinates" type="text" placeholder="Coordinates" class="form-control inputFields"> </div></form>';

        App.globals.selecters.addModalLabel.html('Add an Appointment');
        App.globals.selecters.addModalBody.html('').html(html);
        $('#appointment_type').change(function(event) {
            var app_type = $('#appointment_type').val();
            firebase.database().ref('slots').on('value', function(slots) {
                var visitSlotsResults = [];
                for (var key in slots.val()) {
                    var slotData = slots.val()[key];
                    if (slotData.slot_status == "true" && slotData.slot_type == app_type) {
                        slotData.uid = key;
                        visitSlotsResults.push(slotData);
                    }
                }
                var todayV = new Date();
                var tdayV = todayV.getDate();
                if (todayV.getDate() < 9) {
                    tdayV = '0' + todayV.getDate();
                } else {
                    tdayV = todayV.getDate();
                }
                if ((todayV.getMonth() + 1) < 9) {
                    tmnthNum = '0' + (todayV.getMonth() + 1);
                } else {
                    tmnthNum = todayV.getMonth() + 1;
                }
                tYearV = todayV.getFullYear();
                tFormattedDateV = tYearV + '-' + tmnthNum + '-' + tdayV;

                tomorrowV = new Date(new Date().setDate(new Date().getDate() + 1));
                if (tomorrowV.getDate() < 9) {
                    tmdayV = '0' + tomorrowV.getDate();
                } else {
                    tmdayV = tomorrowV.getDate();
                }
                if ((tomorrowV.getMonth() + 1) < 9) {
                    tmrnthNum = '0' + (tomorrowV.getMonth() + 1);
                } else {
                    tmrnthNum = tomorrowV.getMonth() + 1;
                }
                tmYearV = tomorrowV.getFullYear();
                tmFormattedDateV = tmYearV + '-' + tmrnthNum + '-' + tmdayV;
                var vTime_el = [];
                for (var z = 0; z < visitSlotsResults.length; z++) {
                    var sltTim;
                    if (visitSlotsResults[z].slot_time.split('|')[1] == "today") {
                        sltTim = visitSlotsResults[z].slot_time.split('|')[0] + '|' + tFormattedDateV;
                    } else if (visitSlotsResults[z].slot_time.split('|')[1] == "tomorrow") {
                        sltTim = visitSlotsResults[z].slot_time.split('|')[0] + '|' + tmFormattedDateV;
                    }
                    vTime_el.push('<option value="', sltTim, '">', sltTim, '</option>');
                }
                var totalData = vTime_el.join('');
                $('#visit_time').html('<option value="" selected>Please choose</option>').append(totalData);
            });

        });
        App.validateForms('#addAppointment');
        App.globals.selecters.addModal.modal('show');
        $('#patient_dob').datetimepicker({
            maxDate: Date.now(),
            format: 'YYYY-MM-DD',
            widgetPositioning: {
                vertical: 'bottom',
                horizontal: 'left'
            }
        });
        $('#payment_date').datetimepicker({
            widgetPositioning: {
                vertical: 'bottom',
                horizontal: 'left'
            }
        });
    },
    saveAppointment: function() {
        var form = $("#addAppointment");
        if (form.valid()) {
            $("#overlay_test").show();
            var AppointmentInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');
            AppointmentInformation.appointment_type = $('#appointment_type').val(),
                AppointmentInformation.appointment_user = $('#appointment_user').val(),

                AppointmentInformation.contact_phone_number = $('#contact_phone_number').val(),
                AppointmentInformation.contact_email = $('#contact_email').val();
            AppointmentInformation.visit_time = $('#visit_time').val();
            AppointmentInformation.visit_address_1 = $('#visit_address_1').val() + ', ' + ($('#visit_address_2').val() != '' ? $('#visit_address_2').val() + ', ' : '') + $('#visit_city').val() + ', ' + $('#visit_state').val() + ' ' + $('#visit_zip_code').val();
            // AppointmentInformation.visit_address_2 = ;
            // AppointmentInformation.visit_city = $('#visit_city').val();
            // AppointmentInformation.visit_state = $('#visit_state').val();
            // AppointmentInformation.visit_zip_code = $('#visit_zip_code').val();
            AppointmentInformation.patient_name = $('#patient_name').val();
            AppointmentInformation.patient_dob = $('#patient_dob').val();
            AppointmentInformation.patient_gender = $('#patient_gender').val();
            AppointmentInformation.patient_relation = $('#patient_relation').val();
            AppointmentInformation.responsible_party_name = $('#responsible_party_name').val();
            AppointmentInformation.billing_name = $('#billing_name').val();
            AppointmentInformation.billing_address_1 = $('#billing_address_1').val();
            AppointmentInformation.billing_address_2 = $('#billing_address_2').val();
            AppointmentInformation.billing_city = $('#billing_city').val();
            AppointmentInformation.billing_state = $('#billing_state').val();
            AppointmentInformation.billing_zip = $('#billing_zip').val();
            AppointmentInformation.billing_promo = $('#billing_promo').val();
            AppointmentInformation.payment_date = $('#payment_date').val();
            AppointmentInformation.payment_amount = $('#payment_amount').val();
            AppointmentInformation.payment_status = $('#payment_status').val();
            AppointmentInformation.coordinates = $('#coordinates').val();
            firebase.database().ref('appointments/').push({
                appointment_type: AppointmentInformation.appointment_type,
                appointment_user: AppointmentInformation.appointment_user,
                contact_phone_number: AppointmentInformation.contact_phone_number,
                contact_email: AppointmentInformation.contact_email,
                visit_time: AppointmentInformation.visit_time,
                visit_address_1: AppointmentInformation.visit_address_1,
                /*visit_address_2: AppointmentInformation.visit_address_2,
                visit_city: AppointmentInformation.visit_city,
                visit_state: AppointmentInformation.visit_state,
                visit_zip_code: AppointmentInformation.visit_zip_code,*/
                patient_name: AppointmentInformation.patient_name,
                patient_dob: AppointmentInformation.patient_dob,
                patient_gender: AppointmentInformation.patient_gender,
                patient_relation: AppointmentInformation.patient_relation,
                responsible_party_name: AppointmentInformation.responsible_party_name,
                billing_name: AppointmentInformation.billing_name,
                billing_address_1: AppointmentInformation.billing_address_1,
                billing_address_2: AppointmentInformation.billing_address_2,
                billing_city: AppointmentInformation.billing_city,
                billing_state: AppointmentInformation.billing_state,
                billing_zip: AppointmentInformation.billing_zip,
                billing_promo: AppointmentInformation.billing_promo,
                payment_date: AppointmentInformation.payment_date,
                payment_amount: AppointmentInformation.payment_amount,
                payment_status: AppointmentInformation.payment_status,
                coordinates: AppointmentInformation.coordinates,
                createdAt: Date.now(),
                updatedAt: Date.now()

            }).then(function(snapshot) {
                $("#overlay_test").hide();
                App.alertShow('success', 'Appointment created successfully.');
                setTimeout(function() {
                    $btn.button('reset');
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("Appointment form not yet validated");
        }
    },
    updateAppointment: function() {
        var uid = App.globals.UID[0];
        var AppointmentUpdInformation = {},
            $btn = App.globals.selecters.addSave.button('loading');
        AppointmentUpdInformation.appointment_type = $('#appointment_type').val();
        AppointmentUpdInformation.appointment_user = $('#appointment_user').val();
        AppointmentUpdInformation.contact_phone_number = $('#contact_phone_number').val();
        AppointmentUpdInformation.contact_email = $('#contact_email').val();
        AppointmentUpdInformation.visit_time = $('#visit_time').val();
        AppointmentUpdInformation.visit_address_1 = $('#visit_address_1').val();
        /*AppointmentUpdInformation.visit_address_2 = $('#visit_address_2').val();
        AppointmentUpdInformation.visit_city = $('#visit_city').val();
        AppointmentUpdInformation.visit_state = $('#visit_state').val();
        AppointmentUpdInformation.visit_zip_code = $('#visit_zip_code').val();*/
        AppointmentUpdInformation.patient_name = $('#patient_name').val();
        AppointmentUpdInformation.patient_dob = $('#patient_dob').val();
        AppointmentUpdInformation.patient_gender = $('#patient_gender').val();
        AppointmentUpdInformation.patient_relation = $('#patient_relation').val();
        AppointmentUpdInformation.responsible_party_name = $('#responsible_party_name').val();
        AppointmentUpdInformation.billing_name = $('#billing_name').val();
        AppointmentUpdInformation.billing_address_1 = $('#billing_address_1').val();
        AppointmentUpdInformation.billing_address_2 = $('#billing_address_2').val();
        AppointmentUpdInformation.billing_city = $('#billing_city').val();
        AppointmentUpdInformation.billing_state = $('#billing_state').val();
        AppointmentUpdInformation.billing_zip = $('#billing_zip').val();
        AppointmentUpdInformation.billing_promo = $('#billing_promo').val();
        AppointmentUpdInformation.payment_date = $('#payment_date').val();
        AppointmentUpdInformation.payment_amount = $('#payment_amount').val();
        AppointmentUpdInformation.payment_status = $('#payment_status').val();
        AppointmentUpdInformation.coordinates = $('#coordinates').val();
        firebase.database().ref('appointments/' + uid).update({
            appointment_type: AppointmentUpdInformation.appointment_type,
            appointment_user: AppointmentUpdInformation.appointment_user,
            contact_phone_number: AppointmentUpdInformation.contact_phone_number,
            contact_email: AppointmentUpdInformation.contact_email,
            visit_time: AppointmentUpdInformation.visit_time,
            visit_address_1: AppointmentUpdInformation.visit_address_1,
            /*visit_address_2: AppointmentUpdInformation.visit_address_2,
            visit_city: AppointmentUpdInformation.visit_city,
            visit_state: AppointmentUpdInformation.visit_state,
            visit_zip_code: AppointmentUpdInformation.visit_zip_code,*/
            patient_name: AppointmentUpdInformation.patient_name,
            patient_dob: AppointmentUpdInformation.patient_dob,
            patient_gender: AppointmentUpdInformation.patient_gender,
            patient_relation: AppointmentUpdInformation.patient_relation,
            responsible_party_name: AppointmentUpdInformation.responsible_party_name,
            billing_name: AppointmentUpdInformation.billing_name,
            billing_address_1: AppointmentUpdInformation.billing_address_1,
            billing_address_2: AppointmentUpdInformation.billing_address_2,
            billing_city: AppointmentUpdInformation.billing_city,
            billing_state: AppointmentUpdInformation.billing_state,
            billing_zip: AppointmentUpdInformation.billing_zip,
            billing_promo: AppointmentUpdInformation.billing_promo,
            payment_date: AppointmentUpdInformation.payment_date,
            payment_amount: AppointmentUpdInformation.payment_amount,
            payment_status: AppointmentUpdInformation.payment_status,
            coordinates: AppointmentUpdInformation.coordinates,
            updatedAt: Date.now()

        });
        App.alertShow('success', 'Appointment Updated successfully.');
        setTimeout(function() {
            $btn.button('reset');
            App.globals.selecters.addModal.modal('hide');
        }, 1000);
    },
    deleteAppointment: function() {
        var uid = App.globals.UID[0];
        firebase.database().ref('appointments/' + uid).remove().then(function(resp) {
            App.globals.UID.length = 0;
        });
    },
    getInsuranceInfo: function() {
        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },
                { title: "User" },
                { title: "Name" },
                { title: "Images" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }, {
                    title: "",
                    "data": null,
                    "orderable": false,
                    "defaultContent": "<i id='editbtn' class='fa fa-pencil-square fa-2x' aria-hidden='true'></i>  <i  id='deletebtn' class='fa fa-trash-o fa-2x' aria-hidden='true'></i>"
                }
            ];
        firebase.database().ref('insurance').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }
            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);
                for (; i < getID.length; i++) {
                    id = getID[i];
                    var images = [];
                    if (res[id].images !== undefined) {
                        var j = 0;
                        for (; j < res[id].images.length; j++) {
                            images.push('<a href="', res[id].images[j], '" class="image-link"><img src="', res[id].images[j], '" class="incImg"></a>');
                        }
                    }
                    dataSet.push([
                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),
                        ((res[id].user !== undefined) && res[id].user || ""),
                        ((res[id].name !== undefined) && res[id].name || ""),
                        ((res[id].images !== undefined) && images.join('') || ""),
                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }
            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            $('.image-link').magnificPopup({
                type: 'image'
            });
            dataSet.length = 0;
        });
    },
    addInsurance: function() {

        var html = '<form id="addInsurance" onsubmit="return false;"> <div class="form-group"> <label class="control-label" for="user">User</label> <input id="user" name="user" type="text" placeholder="User" class="form-control inputFields input-md" required> </div> <div class="form-group"> <label class="control-label" for="name">Name</label> <input id="name" name="name" type="text" placeholder="Name" class="form-control inputFields input-md" required> </div> <div class="form-group"> <label class="control-label" for="images">Insurance Images</label> <input id="images" name="images" class="input-file inputFields" type="file" multiple required> <div id="dvPreview"></div> </div> </form>';

        App.globals.selecters.addModalLabel.html('Add Insurance');
        App.globals.selecters.addModalBody.html('').html(html);
        App.validateForms('#addInsurance');
        App.globals.selecters.addModal.modal('show');
        $('#insurance_date').datetimepicker({
            sideBySide: true,
            widgetPositioning: {
                vertical: 'bottom',
                horizontal: 'left'
            }
        });
        $("#images").change(function() {
            if (typeof(FileReader) != "undefined") {
                var dvPreview = $("#dvPreview");
                dvPreview.html("");
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png)$/;
                $($(this)[0].files).each(function() {
                    var file = $(this);
                    if (regex.test(file[0].name.toLowerCase())) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            App.globals.imageValues.push(e.target.result);
                            var img = $("<img />");
                            img.attr("style", "height:100px;width: 100px");
                            img.attr("src", e.target.result);
                            dvPreview.append(img);
                        };
                        reader.readAsDataURL(file[0]);
                    } else {
                        alert(file[0].name + " is not a valid image file.");
                        dvPreview.html("");
                        return false;
                    }
                });
            } else {
                alert("This browser does not support HTML5 FileReader.");
            }
        });
    },
    saveInsurance: function() {
        var form = $("#addInsurance");
        if (form.valid()) {
            $("#overlay_test").show();
            var InsuranceInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');
            InsuranceInformation.user = $('#user').val();
            InsuranceInformation.name = $('#name').val();
            InsuranceInformation.images = App.globals.imageValues;

            firebase.database().ref('insurance/').push({
                user: InsuranceInformation.user,
                name: InsuranceInformation.name,
                images: InsuranceInformation.images,
                createdAt: Date.now(),
                updatedAt: Date.now()

            }).then(function(snapshot) {
                $("#overlay_test").hide();
                App.globals.imageValues.length = 0;
                App.alertShow('success', 'Insurance created successfully.');
                $btn.button('reset');
                setTimeout(function() {
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("Insurance form not yet validated");
        }
    },
    updateInsurance: function() {
        var uid = App.globals.UID[0];
        var InsuranceUpdInformation = {};
        InsuranceUpdInformation.user = $('#user').val();
        InsuranceUpdInformation.name = $('#name').val();
        InsuranceUpdInformation.images = App.globals.imageValues;
        firebase.database().ref('insurance/' + uid).update({
            user: InsuranceUpdInformation.user,
            name: InsuranceUpdInformation.name,
            images: InsuranceUpdInformation.images,
            updatedAt: Date.now()
        });
        App.alertShow('success', 'Insurance Updated successfully.');
        setTimeout(function() {
            $btn.button('reset');
            App.globals.selecters.addModal.modal('hide');
        }, 1000);
    },
    deleteInsurance: function() {
        var uid = App.globals.UID[0];
        firebase.database().ref('insurance/' + uid).remove().then(function(resp) {
            App.globals.UID.length = 0;
        });
    },
    getPromocodeInfo: function() {
        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },
                { title: "Codes" },
                { title: "Expiration" },
                { title: "Amount" },
                { title: "Usage" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }, {
                    title: "",
                    "data": null,
                    "orderable": false,
                    "defaultContent": "<i id='editbtn' class='fa fa-pencil-square fa-2x' aria-hidden='true'></i>  <i  id='deletebtn' class='fa fa-trash-o fa-2x' aria-hidden='true'></i>"
                }
            ];

        firebase.database().ref('promocodes').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }

            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);

                for (; i < getID.length; i++) {
                    id = getID[i];
                    dataSet.push([
                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),
                        ((res[id].promo_codes !== undefined) && res[id].promo_codes || ""),
                        ((res[id].promo_expiration !== undefined) && res[id].promo_expiration || ""),
                        ((res[id].promo_amount !== undefined) && res[id].promo_amount || ""),
                        ((res[id].promo_usage !== undefined) && res[id].promo_usage || ""),

                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }

            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            dataSet.length = 0;
        });
    },
    addPromocode: function() {
        var html = '<form id="addPromocode" onsubmit="return false;"> <div class="form-group"> <label class="control-label" for="codes">Codes</label> <input id="promo_codes" name="codes" type="text" placeholder="Codes" class="form-control input-md inputFields" required> </div><div class="form-group"> <label class="control-label" for="expiration">Expiration</label> <input id="promo_expiration" name="expiration" type="text" placeholder="Expiration" class="form-control input-md inputFields" required> </div><div class="form-group"> <label class="control-label" for="amount">Amount</label> <input id="promo_amount" name="amount" type="number" placeholder="Amount" class="form-control input-md inputFields" required> </div><div class="form-group"> <label class="control-label" for="usage">Usage</label> <input id="promo_usage" name="usage" type="text" placeholder="Usage" class="form-control input-md inputFields" required> </div></form>';

        App.globals.selecters.addModalLabel.html('Add addPromocode');
        App.globals.selecters.addModalBody.html('').html(html);
        App.validateForms('#addPromocode');
        App.globals.selecters.addModal.modal('show');
    },
    savePromocode: function() {
        var form = $("#addPromocode");
        if (form.valid()) {
            $("#overlay_test").show();
            var PromocodeInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');
            PromocodeInformation.promo_codes = $('#promo_codes').val();
            PromocodeInformation.promo_expiration = $('#promo_expiration').val();
            PromocodeInformation.promo_amount = $('#promo_amount').val();
            PromocodeInformation.promo_usage = $('#promo_usage').val();


            firebase.database().ref('promocodes/').push({
                promo_codes: PromocodeInformation.promo_codes,
                promo_expiration: PromocodeInformation.promo_expiration,
                promo_amount: PromocodeInformation.promo_amount,
                promo_usage: PromocodeInformation.promo_usage,
                createdAt: Date.now(),
                updatedAt: Date.now()

            }).then(function(snapshot) {
                $("#overlay_test").hide();
                App.alertShow('success', 'Promocode created successfully.');
                $btn.button('reset');
                setTimeout(function() {
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("Promocode form not yet validated");
        }
    },
    updatePromocode: function() {
        var uid = App.globals.UID[0];
        var PromocodeUpdInformation = {};
        $btn = App.globals.selecters.addSave.button('loading');
        PromocodeUpdInformation.promo_codes = $('#promo_codes').val();
        PromocodeUpdInformation.promo_expiration = $('#promo_expiration').val();
        PromocodeUpdInformation.promo_amount = $('#promo_amount').val();
        PromocodeUpdInformation.promo_usage = $('#promo_usage').val();
        PromocodeUpdInformation.referral_amount = $('#referral_amount').val();
        PromocodeUpdInformation.referral_usage = $('#referral_usage').val();
        firebase.database().ref('promocodes/' + uid).update({
            promo_codes: PromocodeUpdInformation.promo_codes,
            promo_expiration: PromocodeUpdInformation.promo_expiration,
            promo_amount: PromocodeUpdInformation.promo_amount,
            promo_usage: PromocodeUpdInformation.promo_usage,
            updatedAt: Date.now()
        });
        App.alertShow('success', 'Promocode Updated successfully.');
        setTimeout(function() {
            $btn.button('reset');

            App.globals.selecters.addModal.modal('hide');
        }, 1000);
    },
    deletePromocode: function() {
        var uid = App.globals.UID[0];
        firebase.database().ref('promocodes/' + uid).remove().then(function(resp) {
            App.globals.UID.length = 0;

        });
    },
    getReferralInfo: function() {
        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },
                { title: "Codes" },
                { title: "Expiration" },
                { title: "Amount" },
                { title: "Usage" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }, {
                    title: "",
                    "data": null,
                    "orderable": false,
                    "defaultContent": "<i id='editbtn' class='fa fa-pencil-square fa-2x' aria-hidden='true'></i>  <i  id='deletebtn' class='fa fa-trash-o fa-2x' aria-hidden='true'></i>"
                }
            ];
        firebase.database().ref('referralcodes').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }
            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);
                for (; i < getID.length; i++) {
                    id = getID[i];
                    dataSet.push([
                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),
                        ((res[id].referral_codes !== undefined) && res[id].referral_codes || ""),
                        ((res[id].referral_expiration !== undefined) && res[id].referral_expiration || ""),
                        ((res[id].referral_amount !== undefined) && res[id].referral_amount || ""),
                        ((res[id].referral_usage !== undefined) && res[id].referral_usage || ""),

                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }
            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            dataSet.length = 0;
        });
    },
    addReferral: function() {

        var html = '<form id="addReferral" onsubmit="return false;"> <div class="form-group"> <label class="control-label" for="codes">Codes</label> <input id="referral_codes" name="codes" type="text" placeholder="Codes" class="form-control input-md inputFields" required> </div> <div class="form-group"> <label class="control-label" for="expiration">Expiration</label> <input id="referral_expiration" name="expiration" type="text" placeholder="Expiration" class="form-control input-md inputFields" required> </div> <div class="form-group"> <label class="control-label" for="amount">Amount</label> <input id="referral_amount" name="amount" type="text" placeholder="Amount" class="form-control input-md inputFields" required> </div> <div class="form-group"> <label class="control-label" for="usage">Usage</label> <input id="referral_usage" name="usage" type="text" placeholder="Usage" class="form-control input-md inputFields" required> </div> </form>';

        App.globals.selecters.addModalLabel.html('Add Referral');
        App.globals.selecters.addModalBody.html('').html(html);
        App.validateForms('#addReferral');
        App.globals.selecters.addModal.modal('show');
    },
    saveReferral: function() {
        var form = $("#addReferral");
        if (form.valid()) {
            $("#overlay_test").show();
            var ReferralInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');

            ReferralInformation.referral_codes = $('#referral_codes').val();
            ReferralInformation.referral_expiration = $('#referral_expiration').val();
            ReferralInformation.referral_amount = $('#referral_amount').val();
            ReferralInformation.referral_usage = $('#referral_usage').val();


            firebase.database().ref('referralcodes/').push({
                referral_codes: ReferralInformation.referral_codes,
                referral_expiration: ReferralInformation.referral_expiration,
                referral_amount: ReferralInformation.referral_amount,
                referral_usage: ReferralInformation.referral_usage,
                createdAt: Date.now(),
                updatedAt: Date.now()
            }).then(function(snapshot) {
                $("#overlay_test").hide();
                App.alertShow('success', 'Referral code created successfully.');
                $btn.button('reset');
                setTimeout(function() {
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("Referral code form not yet validated");
        };
    },
    updateReferral: function() {
        var uid = App.globals.UID[0];
        var ReferralUpdInformation = {},
            $btn = App.globals.selecters.addSave.button('loading');
        ReferralUpdInformation.referral_codes = $('#referral_codes').val();
        ReferralUpdInformation.referral_expiration = $('#referral_expiration').val();
        ReferralUpdInformation.referral_amount = $('#referral_amount').val();
        ReferralUpdInformation.referral_usage = $('#referral_usage').val();
        firebase.database().ref('referralcodes/' + uid).update({
            referral_codes: ReferralUpdInformation.referral_codes,
            referral_expiration: ReferralUpdInformation.referral_expiration,
            referral_amount: ReferralUpdInformation.referral_amount,
            referral_usage: ReferralUpdInformation.referral_usage,
            updatedAt: Date.now()
        });
        App.alertShow('success', 'Referral Updated successfully.');
        setTimeout(function() {
            $btn.button('reset');

            App.globals.selecters.addModal.modal('hide');
        }, 1000);
    },
    deleteReferral: function() {
        var uid = App.globals.UID[0];
        firebase.database().ref('referralcodes/' + uid).remove().then(function(resp) {
            App.globals.UID.length = 0;

        });
    },
    getSlotsInfo: function() {

        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },
                { title: "Slots&nbsp;Type" },
                { title: "Time" },
                { title: "Status" },
                { title: "Seat" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }, {
                    title: "",
                    "data": null,
                    "orderable": false,
                    "defaultContent": "<i id='editbtn' class='fa fa-pencil-square fa-2x' aria-hidden='true'></i>  <i  id='deletebtn' class='fa fa-trash-o fa-2x' aria-hidden='true'></i>"
                }
            ];

        firebase.database().ref('slots').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }

            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);

                for (; i < getID.length; i++) {
                    id = getID[i];
                    dataSet.push([

                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),
                        ((res[id].slot_type !== undefined) && res[id].slot_type || ""),


                        ((res[id].slot_time !== undefined) && res[id].slot_time || ""),
                        ((res[id].slot_status !== undefined) && res[id].slot_status || ""),

                        ((res[id].slot_seat !== undefined) && res[id].slot_seat || ""),

                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }
            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            dataSet.length = 0;
        });
    },
    addSlots: function() {
        var html = '<form id="addSlot" onsubmit="return false;"><div class="form-group"> <label class="control-label" for="slot_type">Slot Type</label> <select id="slot_type" name="slot_type" class="form-control inputFields" required> <option value="appointment_visit">Appointment Visit</option> <option value="video_visit">Video Visit</option> </select></div> <div class="form-group"> <label class="control-label" for="time">Time</label> <input id="slot_time" name="time" type="text" placeholder="Time" class="form-control input-md inputFields" required> </div> <div class="form-group"> <label class="control-label" for="status">Status</label> <select id="slot_status" name="status" class="form-control inputFields" required> <option value="true">true</option> <option selected value="false">false</option> </select> </div> <div class="form-group"> <label class="control-label" for="seat">Seat</label> <input id="slot_seat" name="seat" type="text" placeholder="Seat" class="form-control input-md inputFields" required> </div></form>';

        App.globals.selecters.addModalLabel.html('Add Slots');
        App.globals.selecters.addModalBody.html('').html(html);
        App.validateForms('#addSlot');
        App.globals.selecters.addModal.modal('show');
    },
    saveSlots: function() {
        var form = $("#addSlot");
        if (form.valid()) {
            $("#overlay_test").show();
            var SlotsInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');
            SlotsInformation.slot_time = $('#slot_time').val();
            SlotsInformation.slot_status = $('#slot_status').val();
            SlotsInformation.slot_seat = $('#slot_seat').val();
            SlotsInformation.slot_type = $('#slot_type').val();

            firebase.database().ref('slots/').push({
                slot_time: SlotsInformation.slot_time,
                slot_status: SlotsInformation.slot_status,
                slot_seat: SlotsInformation.slot_seat,
                slot_type: SlotsInformation.slot_type,
                createdAt: Date.now(),
                updatedAt: Date.now()
            }).then(function(snapshot) {
                $("#overlay_test").hide();
                App.alertShow('success', 'Slot created successfully.');
                $btn.button('reset');
                setTimeout(function() {
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("Slots form not yet validated");
        }
    },
    updateSlots: function() {
        var uid = App.globals.UID[0];
        var SlotsUpdInformation = {},
            $btn = App.globals.selecters.addSave.button('loading');
        SlotsUpdInformation.slot_time = $('#slot_time').val();
        SlotsUpdInformation.slot_status = $('#slot_status').val();
        SlotsUpdInformation.slot_seat = $('#slot_seat').val();
        SlotsUpdInformation.slot_type = $('#slot_type').val();


        firebase.database().ref('slots/' + uid).update({
            slot_time: SlotsUpdInformation.slot_time,
            slot_status: SlotsUpdInformation.slot_status,
            slot_seat: SlotsUpdInformation.slot_seat,
            slot_type: SlotsUpdInformation.slot_type,
            updatedAt: Date.now()
        });
        App.alertShow('success', 'Slot Updated successfully.');
        setTimeout(function() {
            $btn.button('reset');

            App.globals.selecters.addModal.modal('hide');
        }, 1000);
    },
    deleteSlots: function() {
        var uid = App.globals.UID[0];
        firebase.database().ref('slots/' + uid).remove().then(function(resp) {
            App.globals.UID.length = 0;

        });
    },
    getUserMessagesInfo: function() {
        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },
                { title: "To" },
                { title: "From" },
                { title: "Message&nbsp;Title" },
                { title: "Message&nbsp;Body" },
                { title: "Read&nbsp;Status" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }, {
                    title: "",
                    "data": null,
                    "orderable": false,
                    "defaultContent": "<i id='editbtn' class='fa fa-pencil-square fa-2x' aria-hidden='true'></i>  <i  id='deletebtn' class='fa fa-trash-o fa-2x' aria-hidden='true'></i>"
                }
            ];

        firebase.database().ref('user_message').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }

            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);

                for (; i < getID.length; i++) {
                    id = getID[i];
                    dataSet.push([
                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),
                        ((res[id].usermsessage_to !== undefined) && res[id].usermsessage_to || ""),
                        ((res[id].usermsessage_from !== undefined) && res[id].usermsessage_from || ""),
                        ((res[id].usermsessage_title !== undefined) && res[id].usermsessage_title || ""),
                        ((res[id].usermsessage_message_body !== undefined) && '<a href="javascript:void(0);" class="mesgBodyLink">View Message</a><span style="display:none;">' + res[id].usermsessage_message_body + '</span>' || ""),
                        ((res[id].usermsessage_read_status !== undefined) && res[id].usermsessage_read_status || ""),
                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }
            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            $('.mesgBodyLink').on('click', function() {
                var val = $(this).next('span').html();
                $.magnificPopup.open({
                    items: {
                        src: '<div class="white-popup"><p style="text-shadow:1px 1px 1px rgba(186,25,255,1);font-weight:normal;color:#000000;background-color:#DCFFA8;border: 1px dashed #FF3DF9;letter-spacing:0pt;word-spacing:0pt;font-size:27px;text-align:center;font-family:comic sans, comic sans ms, cursive, verdana, arial, sans-serif;line-height:2;">' + val + '</p></div>',
                        type: 'inline'
                    }
                });
            });

            dataSet.length = 0;
        });
    },
    addUserMessage: function() {
        var html = '<form id="addUserMessage" onsubmit="return false;"> <div class="form-group"> <label class="control-label" for="to">To</label> <input id="usermsessage_to" name="to" type="text" placeholder="To" class="form-control inputFields input-md" required> </div><div class="form-group"> <label class="control-label" for="from">From</label> <input id="usermsessage_from" name="from" type="text" placeholder="From" class="form-control inputFields input-md" required> </div><div class="form-group"> <label class="control-label" for="message_title">Message Title</label> <input id="usermsessage_title" name="message_title" type="text" placeholder="Message Title" class="form-control inputFields input-md" required> </div><div class="form-group"> <label class="control-label" for="message_body">Message Body</label> <textarea class="form-control inputFields" id="usermsessage_message_body" name="message_body" required></textarea> </div><div class="form-group"> <label class="control-label" for="read_status">Read Status</label> <select id="usermsessage_read_status" name="read_status" class="form-control inputFields" required> <option value="true">true</option> <option selected value="false">false</option> </select> </div></fieldset></form>';

        App.globals.selecters.addModalLabel.html('Add UserMessage');
        App.globals.selecters.addModalBody.html('').html(html);
        App.validateForms('#addUserMessage');
        App.globals.selecters.addModal.modal('show');
    },
    saveUserMessage: function() {
        var form = $("#addUserMessage");
        if (form.valid()) {
            $("#overlay_test").show();
            var UserMessageInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');
            UserMessageInformation.usermsessage_to = $('#usermsessage_to').val();
            UserMessageInformation.usermsessage_from = $('#usermsessage_from').val();
            UserMessageInformation.usermsessage_title = $('#usermsessage_title').val();
            UserMessageInformation.usermsessage_message_body = $('#usermsessage_message_body').val();
            UserMessageInformation.usermsessage_read_status = $('#usermsessage_read_status').val();

            firebase.database().ref('user_message/').push({
                usermsessage_to: UserMessageInformation.usermsessage_to,
                usermsessage_from: UserMessageInformation.usermsessage_from,
                usermsessage_title: UserMessageInformation.usermsessage_title,
                usermsessage_message_body: UserMessageInformation.usermsessage_message_body,
                usermsessage_read_status: UserMessageInformation.usermsessage_read_status,
                createdAt: Date.now(),
                updatedAt: Date.now()
            }).then(function(snapshot) {
                $("#overlay_test").hide();
                App.alertShow('success', 'UserMessage  created successfully.');
                $btn.button('reset');
                setTimeout(function() {
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("User Message form not yet validated");
        }
    },
    updateUserMessage: function() {
        var uid = App.globals.UID[0];
        var UserMessagesInformationUpdate = {};
        UserMessagesInformationUpdate.usermsessage_to = $('#usermsessage_to').val();
        UserMessagesInformationUpdate.usermsessage_from = $('#usermsessage_from').val();
        UserMessagesInformationUpdate.usermsessage_title = $('#usermsessage_title').val();
        UserMessagesInformationUpdate.usermsessage_message_body = $('#usermsessage_message_body').val();
        UserMessagesInformationUpdate.usermsessage_read_status = $('#usermsessage_read_status').val();
        firebase.database().ref('user_message/' + uid).update({
            usermsessage_to: UserMessagesInformationUpdate.usermsessage_to,
            usermsessage_from: UserMessagesInformationUpdate.usermsessage_from,
            usermsessage_title: UserMessagesInformationUpdate.usermsessage_title,
            usermsessage_message_body: UserMessagesInformationUpdate.usermsessage_message_body,
            usermsessage_read_status: UserMessagesInformationUpdate.usermsessage_read_status,
            updatedAt: Date.now()
        });
        App.alertShow('success', 'UserMessages Updated Successfully.');
        setTimeout(function() {
            $btn.button('reset');

            App.globals.selecters.addModal.modal('hide');
        }, 1000);
    },
    deleteUserMessage: function() {
        var uid = App.globals.UID[0];
        firebase.database().ref('user_message/' + uid).remove().then(function(resp) {
            App.globals.UID.length = 0;

        });
    },
    getZipCodeInfo: function() {
        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },
                { title: "Codes" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }, {
                    title: "",
                    "data": null,
                    "orderable": false,
                    "defaultContent": "<i id='editbtn' class='fa fa-pencil-square fa-2x' aria-hidden='true'></i>  <i  id='deletebtn' class='fa fa-trash-o fa-2x' aria-hidden='true'></i>"
                }
            ];

        firebase.database().ref('zip_codes').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }

            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);

                for (; i < getID.length; i++) {
                    id = getID[i];
                    dataSet.push([
                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),
                        ((res[id].zip_code !== undefined) && res[id].zip_code || ""),
                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }

            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            dataSet.length = 0;
        });
    },
    addZipCode: function() {
        var html = '<form id="addZipCode" onsubmit="return false;"> <div class="form-group"> <label class="control-label" for="code">Code</label> <input id="zip_code" name="code" type="text" placeholder="Code" class="form-control input-md inputFields" required></div> </form>';

        App.globals.selecters.addModalLabel.html('Add ZipCode');
        App.globals.selecters.addModalBody.html('').html(html);
        App.validateForms('#addZipCode');
        App.globals.selecters.addModal.modal('show');
    },
    saveZipCode: function() {
        var form = $("#addZipCode");
        if (form.valid()) {
            $("#overlay_test").show();
            var ZipCodeInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');
            ZipCodeInformation.zip_code = $('#zip_code').val();
            firebase.database().ref('zip_codes/').push({
                zip_code: ZipCodeInformation.zip_code,
                createdAt: Date.now(),
                updatedAt: Date.now()
            }).then(function(snapshot) {
                $("#overlay_test").hide();
                App.alertShow('success', 'Zip Code Created successfully.');
                $btn.button('reset');
                setTimeout(function() {
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("ZipCode form not yet validated");
        }
    },
    updateZipCode: function() {
        var uid = App.globals.UID[0];
        var ZipCodeInformationUpdate = {};
        ZipCodeInformationUpdate.zip_code = $('#zip_code').val();
        firebase.database().ref('zip_codes/' + uid).update({
            zip_code: ZipCodeInformationUpdate.zip_code,
            updatedAt: Date.now()
        });
        App.alertShow('success', 'ZipCode Updated Successfully.');
        setTimeout(function() {
            App.globals.selecters.addModal.modal('hide');
            $btn.button('reset');
        }, 1000);
    },
    deleteZipCode: function() {
        var uid = App.globals.UID[0];
        console.log(uid);
        firebase.database().ref('zip_codes/' + uid).remove().then(function(resp) {
            App.globals.UID.length = 0;

        });
    },
    getSettingsInfo: function() {
        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },
                // { title: "Option1" },
                // { title: "Option2" },
                // { title: "Option3" },
                { title: "User" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }, {
                    title: "",
                    "data": null,
                    "orderable": false,
                    "defaultContent": "<i id='editbtn' class='fa fa-pencil-square fa-2x' aria-hidden='true'></i>  <i  id='deletebtn' class='fa fa-trash-o fa-2x' aria-hidden='true'></i>"
                }
            ];

        firebase.database().ref('settings').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }

            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);

                for (; i < getID.length; i++) {
                    id = getID[i];
                    dataSet.push([
                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),
                        // ((res[id].option1 !== undefined) && res[id].option1 || ""),
                        // ((res[id].option2 !== undefined) && res[id].option2 || ""),
                        // ((res[id].option3 !== undefined) && res[id].option3 || ""),
                        ((res[id].settings_user !== undefined) && res[id].settings_user || ""),
                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }

            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            dataSet.length = 0;
        });
    },
    addSettings: function() {
        var html = '<form id="addSettings" onsubmit="return false;"><div class="form-group"> <label class="control-label" for="user">User</label> <input id="settings_user" name="user" type="text" placeholder="User" class="form-control inputFields input-md" required></div> </form>';

        App.globals.selecters.addModalLabel.html('Add Settings');
        App.globals.selecters.addModalBody.html('').html(html);
        App.validateForms('#addSettings');
        App.globals.selecters.addModal.modal('show');
    },
    saveSettings: function() {
        var form = $("#addSettings");
        if (form.valid()) {
            $("#overlay_test").show();
            var SettingsInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');
            // SettingsInformation.option1 = $('#option1').val();
            // SettingsInformation.option2 = $('#option2').val();
            // SettingsInformation.option3 = $('#option3').val();
            SettingsInformation.settings_user = $('#settings_user').val();

            firebase.database().ref('settings/').push({
                // option1: SettingsInformation.option1,
                // option2: SettingsInformation.option2,
                // option3: SettingsInformation.option3,
                settings_user: SettingsInformation.settings_user,
                createdAt: Date.now(),
                updatedAt: Date.now()
            }).then(function(snapshot) {
                $("#overlay_test").hide();
                App.alertShow('success', 'Settings created successfully.');
                $btn.button('reset');
                setTimeout(function() {
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("Settings form not yet validated");
        }
    },
    updateSettings: function() {
        var uid = App.globals.UID[0];
        var SettingsUpdInformationUpdate = {};
        $btn = App.globals.selecters.addSave.button('loading');
        // SettingsUpdInformationUpdate.option1 = $('#option1').val();
        // SettingsUpdInformationUpdate.option2 = $('#option2').val();
        // SettingsUpdInformationUpdate.option3 = $('#option3').val();
        SettingsUpdInformationUpdate.settings_user = $('#settings_user').val();

        firebase.database().ref('settings/' + uid).update({
            // option1: SettingsUpdInformationUpdate.option1,
            // option2: SettingsUpdInformationUpdate.option2,
            // option3: SettingsUpdInformationUpdate.option3,
            settings_user: SettingsUpdInformationUpdate.settings_user,
            updatedAt: Date.now()
        });
        App.alertShow('success', 'ZipCode Updated Successfully.');
        setTimeout(function() {
            App.globals.selecters.addModal.modal('hide');
            $btn.button('reset');
        }, 1000);
    },
    deleteSettings: function() {
        var uid = App.globals.UID[0];
        firebase.database().ref('settings/' + uid).remove().then(function(resp) {
            App.globals.UID.length = 0;
        });
    },
    getStaffInfo: function() {
        var dataSet = [],
            columnsSet = [
                { title: "Created&nbsp;At" },
                { title: "Staff&nbsp;Name" },
                { title: "Staff&nbsp;Phone&nbsp;Number" },
                { title: "Staff&nbsp;E-Mail" },
                { title: "Staff&nbsp;Status" },
                { title: "Updated&nbsp;At" },
                { title: "ID" }, {
                    title: "",
                    "data": null,
                    "orderable": false,
                    "defaultContent": "<i id='editbtn' class='fa fa-pencil-square fa-2x' aria-hidden='true'></i>  <i  id='deletebtn' class='fa fa-trash-o fa-2x' aria-hidden='true'></i>"
                }
            ];

        firebase.database().ref('staffs').on('value', function(e) {
            if (App.globals.tableInstance !== null) {
                App.globals.tableInstance.destroy();
                $('#dataTable').empty();
            }

            if (e.exists()) {
                var res = e.val(),
                    id, i = 0,
                    getID = Object.keys(res);

                for (; i < getID.length; i++) {
                    id = getID[i];
                    dataSet.push([
                        ((res[id].createdAt !== undefined) && (App.timeConverter(res[id].createdAt) || "")),
                        ((res[id].staff_name !== undefined) && res[id].staff_name || ""),
                        ((res[id].staff_ph_no !== undefined) && res[id].staff_ph_no || ""),
                        ((res[id].staff_email !== undefined) && res[id].staff_email || ""),
                        ((res[id].staff_status !== undefined) && res[id].staff_status || ""),
                        ((res[id].updatedAt !== undefined) && (App.timeConverter(res[id].updatedAt) || "")), id
                    ]);
                }
            }

            App.initDatatable(dataSet, columnsSet);
            $("#overlay_test").hide();
            dataSet.length = 0;
        });
    },
    addStaff: function() {
        var html = '<form id="addStaff" onsubmit="return false;">  <div class="form-group"> <label class="control-label" for="staff_name">Staff Name</label> <div> <input id="staff_name" name="staff_name" type="text" placeholder="Staff Name" class="form-control inputFields" required> </div> </div><div class="form-group"> <label class="control-label" for="staff_ph_no">Staff Phone Number</label> <div> <input id="staff_ph_no" name="staff_ph_no" type="text" placeholder="Staff Phone Number" maxlength="10" class="form-control inputFields" data-rule-minlength="10" data-rule-maxlength="10" data-msg-minlength="Phone numbers should be 10 digits" data-msg-maxlength="Phone numbers should be 10 digits" required> </div> </div> <div class="form-group"> <label class="control-label" for="staff_email">Staff E - Mail </label> <div> <input id="staff_email" name="staff_email" type="text" placeholder="Staff E-Mail" class="form-control inputFields" required> </div> </div><div class="form-group"> <label class="control-label" for="staff_status">Status</label> <select id="staff_status" name="staff_status" class="form-control inputFields" required> <option value="true">true</option> <option value="false">false</option> </select> </div> </form>';

        App.globals.selecters.addModalLabel.html('Add Staff');
        App.globals.selecters.addModalBody.html('').html(html);
        App.validateForms('#addStaff');
        App.globals.selecters.addModal.modal('show');
        $("#staff_ph_no").keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#staff_ph_no-error").html("Digits Only");
                return false;
            }
        });
        $("#staff_email").on('blur', function() {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test($("#staff_email").val())) {
                App.globals.errorem = 1;
                $("#staff_email-error").html("Please enter a valid email address");
            } else {
                App.globals.errorem = 0;
            }

        });
    },
    saveStaff: function() {
        var form = $("#addStaff");
        if (form.valid() && App.globals.errorem != 1) {
            $("#overlay_test").show();
            var StaffInformation = {},
                $btn = App.globals.selecters.addSave.button('loading');
            StaffInformation.staff_name = $('#staff_name').val();
            StaffInformation.staff_ph_no = $('#staff_ph_no').val();
            StaffInformation.staff_email = $('#staff_email').val();
            StaffInformation.staff_status = $('#staff_status').val();

            firebase.database().ref('staffs/').push({
                staff_name: StaffInformation.staff_name,
                staff_ph_no: StaffInformation.staff_ph_no,
                staff_email: StaffInformation.staff_email,
                staff_status: StaffInformation.staff_status,
                createdAt: Date.now(),
                updatedAt: Date.now()
            }).then(function(snapshot) {
                $("#overlay_test").hide();
                App.alertShow('success', 'Staff Created successfully.');
                $btn.button('reset');
                setTimeout(function() {
                    App.globals.selecters.addModal.modal('hide');
                }, 1000);
            });
        } else {
            console.log("Staff form not yet validated");
        }
    },
    updateStaff: function() {
        var uid = App.globals.UID[0];
        var StaffInformationUpdate = {};
        StaffInformationUpdate.staff_name = $('#staff_name').val();
        StaffInformationUpdate.staff_ph_no = $('#staff_ph_no').val();
        StaffInformationUpdate.staff_email = $('#staff_email').val();
        StaffInformationUpdate.staff_status = $('#staff_status').val();

        firebase.database().ref('staffs/' + uid).update({
            staff_name: StaffInformationUpdate.staff_name,
            staff_ph_no: StaffInformationUpdate.staff_ph_no,
            staff_email: StaffInformationUpdate.staff_email,
            staff_status: StaffInformationUpdate.staff_status,
            updatedAt: Date.now()
        });
        App.alertShow('success', 'Staff Updated Successfully.');
        setTimeout(function() {
            App.globals.selecters.addModal.modal('hide');
            $btn.button('reset');
        }, 1000);
    },
    deleteStaff: function() {
        var uid = App.globals.UID[0];
        firebase.database().ref('staffs/' + uid).remove().then(function(resp) {
            App.globals.UID.length = 0;

        });
    },
    timeConverter: function(timeStamp) {
        /*console.log(timeStamp);
        var a = new Date(timeStamp),
            months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            year = a.getFullYear(),
            month = months[a.getMonth()],
            date = a.getDate(),
            hour = a.getHours(),
            min = a.getMinutes(),
            sec = a.getSeconds(),
            time = month + ' ' + date + ', ' + year + ', ' + hour + ':' + min.pad(2);*/

        var covertedDate = moment(timeStamp).format();

        return covertedDate;
    },
    initDatatable: function(dataSet, columnsSet) {

        App.globals.tableInstance = $('#dataTable').DataTable({
            "data": dataSet,
            "columns": columnsSet,
            "processing": true,
            columnDefs: [{
                // type: 'uk_datetime',
                targets: 0
            }],
            "scrollX": true,
            "order": [
                [0, 'desc']
            ]
        });
    },
    alertShow: function(type, text) {
        $('.alert').remove();
        var html = '<div class="form-group"><div class="alert alert-' + type + '"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>' + type.charAt(0).toUpperCase() + type.slice(1) + '!</strong> ' + text + '</div></div>';
        $('#addModalBody').append(html);
    },
    validateForms: function(form) {

        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        var f = $(form);
        f.validate();
    },

    updateFields: function(obj) {
        App.globals.UID.length = 0;
        var data = obj,
            i = 1;
        this.addData(App.globals.activeSection);
        $("#dvPreview").html('');
        for (; i < (data.length - 2); i++) {
            if (data[i].search('href="data:image/') > 0) {
                $("#dvPreview").append(data[i]);
            } else {
                $('form').find('.inputFields').eq(i - 1).val(data[i]).change();
            }
        }
        var uid = data[data.length - 2];
        App.globals.UID.push(uid);
        $('.modal-footer').find('.btn-primary').text('Update');
        $('#addModalLabel').text($('#addModalLabel').text().replace('Add', 'Update'));
    },
    deleteFieldData: function(obj) {
        var data = obj;
        var uid = data[data.length - 2];
        App.globals.UID.push(uid);
    },
    userLogout: function() {
        firebase.auth().signOut().then(function() {
            sessionStorage.removeItem("credentials");
            sessionStorage.removeItem("logedinUser");
            $("#overlay_test").hide();
            window.location.href = 'login.html';
        }, function(error) {
            alert("Some thing went wrong");
        });
    },
    loggedInUserData: function() {
        var sessionData = JSON.parse(window.sessionStorage.getItem('logedinUser'));
        $("#loggedInUser").html(sessionData.first_name);
    }
};

App.init();

//})(window, document, jQuery);

/*
Padding any numbers with the param @size of the required number
 */
Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {
        s = "0" + s;
    }
    return s;
};
