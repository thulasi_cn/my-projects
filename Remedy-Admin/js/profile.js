//(function(window, document, $, undefined) {
$("#overlay").show();
var App = {
    globals: {
        selecters: {
            logoutBtn: $('#logoutBtn'),
            profileEdit: $('#profile_edit'),
            addModal: $('#addModal'),
            addModalLabel: $('#addModalLabel'),
            addModalBody: $('#addModalBody'),
            updateSave: $('#updateSave'),
            loderImg: $('.loaderImg'),

        },
        UID: []
    },
    init: function() {
        this.authenticate();
    },
    authenticate: function() {
        if (window.sessionStorage.getItem('credentials') !== null) {
            var sessionData = JSON.parse(window.sessionStorage.getItem('credentials'));
            var email = sessionData.email;
            var password = window.atob(sessionData.password);
            firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error, user) {
                if (error === null) {
                    console.log("User signin successfully:", user);
                } else {
                    console.log("Error signin user:", error);

                }
            }).then(function(user, error) {
                if (user) {
                    App.globals.UID.push(user.uid);
                    App.getUserInfo();
                    App.bindEvents();
                    App.loggedInUserData();
                } else if (error) {
                    console.log("Error signin user:", error);
                }
            });
        } else {
            window.location.href = "login.html";
        }
    },
    bindEvents: function() {
        // Logout Button Control
        this.globals.selecters.logoutBtn.on('click', function() {
            $("#overlay_test").show();
            App.userLogout();
        });

        this.globals.selecters.profileEdit.on('click', function() {
            App.addEditModel();
        });

        this.globals.selecters.updateSave.on('click', function() {
            $("#overlay_test").show();
            App.updateUser();
        });

    },
    getUserInfo: function() {
        var uid = App.globals.UID[0];
        firebase.database().ref('users/' + uid).on('value', function(snapshot) {
            var userDetails = snapshot.val();
            $('#profile_name').html(userDetails.first_name + " " + userDetails.last_name);
            $('#profile_show_email').html(userDetails.email);
            $('#profile_phone').html(userDetails.phone);
            $("#overlay_test").hide();
        });
    },
    addEditModel: function() {
        var html = '<form> <div class="form-group"> <label class="control-label"  for="profile_first_name">FirstName</label> <input id="profile_first_name" name="profile_first_name" type="text" placeholder="FirstName" class="form-control input-md"> </div> <div class="form-group"> <label class="control-label"  for="profile_last_name">Last Name</label> <input id="profile_last_name" name="profile_last_name" type="text" placeholder="Last Name" class="form-control input-md"> </div> <div class="form-group"> <label class="control-label"  for="profile_email">E-mail</label> <input id="profile_email" name="profile_email" type="text" placeholder="E-mail" class="form-control input-md"> </div> <div class="form-group"> <label class="control-label"  for="profile_phone_number">Phone Number</label> <input id="profile_phone_number" name="profile_phone_number" type="text" placeholder="Phone Number" class="form-control input-md"> </div> </fieldset> </form>';
        App.globals.selecters.addModalLabel.html('Add User');
        App.globals.selecters.addModalBody.html('').html(html);
        App.globals.selecters.addModal.modal('show');
        this.updateFields();

    },
    updateUser: function() {
        var uid = App.globals.UID[0];
        var user = firebase.auth().currentUser;
        user.updateEmail($('#profile_email').val()).then(function() {
            var getCredentials = JSON.parse(sessionStorage.getItem('credentials'));
            var getLogedinUser = JSON.parse(sessionStorage.getItem('logedinUser'));
            getCredentials.email = $('#profile_email').val();
            window.sessionStorage.setItem("credentials", JSON.stringify(getCredentials));
            App.authenticate();
            firebase.database().ref('users/' + uid).update({
                first_name: $('#profile_first_name').val(),
                last_name: $('#profile_last_name').val(),
                email: $('#profile_email').val(),
                phone: $('#profile_phone_number').val(),
                updatedAt: Date.now()
            });
            getLogedinUser.first_name = $('#profile_first_name').val();
            getLogedinUser.last_name = $('#profile_last_name').val();
            getLogedinUser.email = $('#profile_email').val();
            getLogedinUser.phone = $('#profile_phone_number').val();
            getLogedinUser.updatedAt = Date.now();
            window.sessionStorage.setItem("logedinUser", JSON.stringify(getLogedinUser));
            App.alertShow('success', 'User updated successfully.');
            setTimeout(function() {
                $("#overlay_test").hide();
                App.globals.selecters.addModal.modal('hide');
            }, 1000);
        }, function(error) {
            console.log(error);
        });
    },
    userLogout: function() {
        firebase.auth().signOut().then(function() {
            sessionStorage.removeItem("credentials");
            sessionStorage.removeItem("logedinUser");
            $("#overlay_test").hide();
            window.location.href = 'login.html';
        }, function(error) {
            $("#overlay_test").hide();
            alert("Some thing went wrong");
        });
    },
    updateFields: function() {
        var names = $('#profile_name').html().split(" ");
        $('#profile_first_name').val(names[0]);
        $('#profile_last_name').val(names[1]);
        $('#profile_email').val($('#profile_show_email').html());
        $('#profile_phone_number').val($('#profile_phone').html());
    },
    alertShow: function(type, text) {
        $('.alert').remove();
        var html = '<div class="form-group"><div class="alert alert-' + type + '"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>' + type.charAt(0).toUpperCase() + type.slice(1) + '!</strong> ' + text + '</div></div>';
        $('#addModalBody').append(html);
    },
    loggedInUserData: function() {
        var sessionData = JSON.parse(window.sessionStorage.getItem('logedinUser'));
        $("#loggedInUser").html(sessionData.first_name);
    }



};

App.init();

//})(window, document, jQuery);
