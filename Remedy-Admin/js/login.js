//(function(window, document, $, undefined) {
$("#overlay").show();
var App = {
    globals: {
        selecters: {
            loginBtn: $('#loginButton'),
        }
    },
    init: function() {
        $("#overlay_test").show();
        this.bindEvents();
    },
    bindEvents: function() {
        this.globals.selecters.loginBtn.on('click', function() {
            App.validateForms('#login-form');
            App.firebaseLogin();
        });
    },
    alertShow: function(type, text) {
        $('.alert').remove();
        var html = '<div class="form-group"><div class="alert alert-' + type + '"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> ' + text + '</div></div>';
        $('#addModalBody').append(html);
    },
    firebaseLogin: function() {
        var form = $("#login-form");
        if (form.valid()) {
            var email = $('#login_email').val();
            var password = $('#login_password').val();
            firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error, user) {
                if (error === null) {
                    console.log("User signin successfully:", user);
                } else {
                    console.log("Error signin user:", error);
                    if (error.code === "auth/wrong-password") {
                        App.alertShow('danger', 'Wrong Username or Password');
                    }

                }
            }).then(function(user, error) {
                if (user) {
                    firebase.database().ref('users/' + user.uid).on('value', function(snapshot) {
                        var userDetails = snapshot.val();
                        console.log(userDetails.role === undefined ? "" : userDetails.role);
                        if ((userDetails.role === undefined ? "" : userDetails.role) == 'admin') {
                            window.sessionStorage.setItem("logedinUser", JSON.stringify(userDetails));
                            window.sessionStorage.setItem("credentials", JSON.stringify({ "email": email, "password": window.btoa(password) }));
                            console.log("User signin successfully:", user);
                            window.location.href = 'index.html';
                        } else {
                            App.userLogout();
                            App.alertShow('danger', 'Wrong Username or Password.');
                        }
                    });

                } else if (error) {
                    console.log("Error signin user:", error);
                }
            });
        } else {
            console.log("form not validate yet.");
        }
    },
    validateForms: function(form) {
        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        var f = $(form);
        f.validate();
    },
    userLogout: function() {
        firebase.auth().signOut().then(function() {
            sessionStorage.removeItem("credentials");
            sessionStorage.removeItem("logedinUser");
            // window.location.href = 'login.html';
        }, function(error) {});
    }
};

App.init();

//})(window, document, jQuery);
