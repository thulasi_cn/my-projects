<?php

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    require_once('vendor/autoload.php');

    \Stripe\Stripe::setApiKey("sk_test_YqzwOrWCQP0mLwTYjwEGykPf");

    $am = $customer_id = '';

    try{

    	if (!isset($_POST['stripeToken'])){
    	    //echo json_encode(array("status" => "The Stripe Token was not generated correctly"));
    	    echo json_encode(array("status" => "Unable to process your request now. Please try later."));
    	    exit();
    	}
    	if(!isset($_POST['am'])){
    	    echo json_encode(array("status" => "Your payment has not been processed."));
    	    exit();
    	}else{
    	    $am = $_POST['am'] * 100;
    	}
    	if(!isset($_POST['receipt_email'])){
    	    echo json_encode(array("status" => "Your email was not found. Please re-enter and try again later."));
    	    exit();
    	}

        $stripeToken = $_POST['stripeToken'];

        if($_POST['create'] == 'charge'){
            \Stripe\Charge::create(array(
                "amount" => $am,
                "currency" => "usd",
                "source" => $stripeToken,
                "description" => "Charge for Remedy. " . $_POST['receipt_email'] . " only pay"
            ));
            echo json_encode(array("status" => "succeeded"));
            exit();
        }

        if($_POST['create'] == 'save'){
            $customer = \Stripe\Customer::create(array(
                "email" => $_POST['receipt_email'],
                "description" => "Remedy customer for " . $_POST['receipt_email'],
                "source" => $stripeToken
            ));

            $customer_id = $customer["id"];

            \Stripe\Charge::create(array(
                "amount" => $am,
                "currency" => "usd",
                "customer" => $customer_id,
                "description" => "Charge for Remedy. " . $_POST['receipt_email'] . " saved customer"
            ));

            echo json_encode(array("status" => "succeeded", "customer_id" => $customer_id));
            exit();
        }

        if($_POST['create'] == 'charge_save' && !empty($_POST['customer_id'])){
            \Stripe\Charge::create(array(
                "amount" => $am,
                "currency" => "usd",
                "customer" => $_POST['customer_id'],
                "description" => "Charge for Remedy. " . $_POST['receipt_email'] . " saved customer"
            ));

            echo json_encode(array("status" => "succeeded"));
            exit();
        }

        if($_POST['create'] == 'update' && !empty($_POST['customer_id'])){
            $cu = \Stripe\Customer::retrieve($_POST['customer_id']);
            $cu->description = "Remedy customer for " . $_POST['receipt_email'];
            $cu->source = $stripeToken;
            $cu->save();

            \Stripe\Charge::create(array(
                "amount" => $am,
                "currency" => "usd",
                "customer" => $_POST['customer_id'],
                "description" => "Charge for Remedy. " . $_POST['receipt_email'] . " saved customer"
            ));
        }else{
            echo json_encode(array("status" => "Your information not found. Please try again later."));
            exit();
        }

    }catch(Exception $e){
    	// The card has been declined
    	// Actual Stripe Server Message Message $e->getMessage()
        echo json_encode(array("status" => "Card declined. Please retry later."));
    }
}else{
    echo json_encode(array("status" => "Your request declined by server. Please try again later."));
    exit();
}

?>