import React, { Component } from 'react';
import { Link } from 'react-router';
import Modal from 'react-modal';
import Login from './Login';
import axios from 'axios';


const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = { modalIsOpen: false, isLoggedIn: '',loggedin_user:'' };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.logOut = this.logOut.bind(this);
        this.doLogin = this.doLogin.bind(this);
        this.doRegister = this.doRegister.bind(this);

    }
    componentDidMount() {
        if (localStorage.getItem('loggedInUser') !== null) {
            this.setState({ loggedin_user: JSON.parse(localStorage.getItem('loggedInUser')) });
            this.setState({ isLoggedIn: true });
        } else {
            this.setState({ isLoggedIn: false });
        }
    }
    openModal() {
        this.setState({ modalIsOpen: true });
    }
    closeModal() {
        this.setState({ modalIsOpen: false });
    }
    logOut() {
        var r = confirm("Are you want to logout..!");
        if (r === true) {
            this.setState({ isLoggedIn: false });
             localStorage.removeItem('loggedInUser');
        }         
    }
    doLogin(e) {
        e.preventDefault();
        var loginUserData = {};
        loginUserData.email = this.refs.lemail.value;
        loginUserData.password = this.refs.lpassword.value;
        let self = this;
        axios.post('http://noderest-thulasig7.rhcloud.com/api/user/login', loginUserData)
            .then(function(response) {
                if (response.data.status === true) {
                    self.setState({ isLoggedIn: true });
                    localStorage.setItem('loggedInUser', JSON.stringify(response.data.user));
                    self.closeModal();
                }else if(response.data.status === false){
                    alert(response.data.message);
                }
                
            })
            .catch(function(error) {
                console.log(error);
            });
      } 
      doRegister(e) {
        e.preventDefault();
        var registerData = {};
        if (this.refs.password.value === this.refs.cpassword.value) {
        registerData.first_name = this.refs.fname.value;
        registerData.last_name = this.refs.lname.value;
        registerData.email = this.refs.email.value;
        registerData.password = this.refs.password.value;
        registerData.contact_number = this.refs.mobile.value;
        let self = this;
        axios.post('http://noderest-thulasig7.rhcloud.com/api/user/register', registerData)
            .then(function(response) {
                if (response.data.message === 'User registration Success.') {
                    self.setState({ isLoggedIn: true });
                    localStorage.setItem('loggedInUser', JSON.stringify(response.data.user));
                    self.closeModal();
                }else if(response.data.message === 'User email exist,Please try with another username.'){
                    alert(response.data.message);
                }
                
            })
            .catch(function(error) {
                console.log(error);
            });
            
        }else{
            alert("Password doesn't match.");
        }
    } 
    render() {
        const isLoggedIn = this.state.isLoggedIn;
        const loggedInUserData = this.state.loggedin_user;

        return ( < nav className="navbar navbar-inverse"> < div className="container-fluid"> < div className="navbar-header"> < Link className="navbar-brand" to="/"> GTR < /Link> </div> < ul className="nav navbar-nav"> < li> < Link to="/about"> About < /Link> </li> < li> < Link to="/zones"> Zones < /Link> </li> < li> < Link to="/products"> Products < /Link> </li> < /ul> <ul className="nav navbar-nav navbar-right"> { isLoggedIn ? (<div><li className="authenticateBtn"><a>{loggedInUserData.first_name}</a></li><li onClick={this.logOut} className="authenticateBtn"><a href="#">Logout</a > < /li></div>) : ( <li onClick={this.openModal} className="authenticateBtn"><a href="#">SignIn</a > < /li> ) } </ul > < /div><Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal} style={customStyles} contentLabel="Example Modal"> <i onClick={this.closeModal} className="fa fa-times-circle fa-3 pull-right"></i > <div className="col-sm-12"> <ul className="nav nav-tabs"> <li className="active"><a href="#Login" data-toggle="tab">Login</a></li> <li><a href="#Registration" data-toggle="tab">Registration</a></li> </ul> <div className="tab-content"> <div className="tab-pane active" id="Login"> <form role="form" className="form-horizontal" onSubmit={this.doLogin}> <div className="form-group"> <label for="exampleInputPassword1" className="col-sm-2 control-label"> Email</label> <div className="col-sm-10"> <input type="email" ref="lemail" className="form-control" placeholder="Email" /> </div> </div> <div className="form-group"> <label for="exampleInputPassword1" className="col-sm-2 control-label"> Password</label> <div className="col-sm-10"> <input type="password" ref="lpassword" className="form-control" placeholder="Password" /> </div> </div> <div className="row"> <div className="col-sm-2"> </div> <div className="col-sm-10"> <button className="btn btn-primary btn-sm"> SignIn</button> </div> </div> </form> </div> <div className="tab-pane" id="Registration"> <form role="form" onSubmit={this.doRegister} className="form-horizontal"> <div className="form-group"> <label for="fname" className="col-sm-2 control-label">First Name</label> <div className="col-sm-10"> <input type="text" name="fname" ref="fname" id="fname" className="form-control" placeholder="Name" /> </div> </div> <div className="form-group"> <label for="lname" className="col-sm-2 control-label">Last Name</label> <div className="col-sm-10"> <input type="text" name="lname" ref="lname" id="lname" className="form-control" placeholder="Name" /> </div> </div> <div className="form-group"> <label for="email" className="col-sm-2 control-label"> Email</label> <div className="col-sm-10"> <input type="email" name="email" className="form-control" ref="email" id="email" placeholder="Email" /> </div> </div> <div className="form-group"> <label for="mobile" className="col-sm-2 control-label">Mobile</label> <div className="col-sm-10"> <input type="text" name="mobile" className="form-control" ref="mobile" id="mobile" placeholder="Mobile" /> </div> </div> <div className="form-group"> <label for="password" className="col-sm-2 control-label"> Password</label> <div className="col-sm-10"> <input type="password" name="password" className="form-control" ref="password" id="password" placeholder="Password" /> </div> </div> <div className="form-group"> <label for="cpassword" className="col-sm-2 control-label">Confirm Password</label> <div className="col-sm-10"> <input type="password" name="cpassword" className="form-control" ref="cpassword" id="cpassword" placeholder="Confirm Password" /> </div> </div> <div className="row"> <div className="col-sm-2"> </div> <div className="col-sm-10"> <button className="btn btn-primary btn-sm"> SignUp</button> </div> </div> </form> </div> </div> </div> < /Modal> </nav> )
    }
}
export default Navbar;
