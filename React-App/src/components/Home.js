import React, { Component } from 'react';
import Navbar from './Navbar';
import Header from './Header';
import Clients from './Clients';
import Footer from './Footer';

class Home extends Component {
    render() {
        return ( <div>< Navbar / ><Header /> <hr /> <Clients /> <Footer /></div> )
        }
}
export default Home;
