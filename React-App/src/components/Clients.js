import React, { Component } from 'react';

class Clients extends Component {
    render() {
        return (<div className="row clients"> <div className="col-sm-4"> <img className="img-circle img-responsive img-center" src="../src/asserts/img/bisleri.png" alt="" /> <h2>Bisleri</h2> <p>These marketing boxes are a great place to put some information. These can contain summaries of what the company does, promotional information, or anything else that is relevant to the company. These will usually be below-the-fold.</p> </div> <div className="col-sm-4"> <img className="img-circle img-responsive img-center" src="../src/asserts/img/aquafina.jpg" alt="" /> <h2>Aquafina</h2> <p>The images are set to be circular and responsive. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p> </div> <div className="col-sm-4"> <img className="img-circle img-responsive img-center" src="../src/asserts/img/kinley.jpg" alt="" /> <h2>Kinlay</h2> <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p> </div> </div>)
        }
}
export default Clients;