import React, { Component } from 'react';

class SingleCart extends Component {
	constructor(props) {
        super(props);
    }
    render() {
        return (<li> <div className="row"><i className="fa fa-trash pull-right" aria-hidden="true"></i> </div> <div className="row"> <div className="col-mg-12"> <div className="col-md-4"> <div className="pro_img_left"> <a><img src={this.props.cart_prd.prd_img } alt="" /></a> </div> </div> <div className="col-md-8"> <div className="pro_title_right"> <h4>{this.props.cart_prd.prd_name}</h4> <p>{this.props.cart_prd.prd_description}</p> </div> </div> </div> </div> <div className="row"> <div className="col-md-12"> <div className="row"> <div className="col-md-8"> <span>${this.props.cart_prd.prd_price} X </span> </div> <div className="col-md-4"> <select className="form-control" id="sel1"> <option>1</option> <option>2</option> <option>3</option> <option>4</option> </select> </div> </div> </div> </div> <hr /> </li>)
        }
}
export default SingleCart;

