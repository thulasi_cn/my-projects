import React, { Component } from 'react';
 import { Link } from 'react-router';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.doLogin = this.doLogin.bind(this);
    }
    doLogin(e) {
        e.preventDefault();
        var loginUserData = {};
        loginUserData.email = this.refs.lemail.value;
        loginUserData.password = this.refs.lpassword.value;
        console.log(loginUserData);
        this.setState({isLoggedIn: true});
        localStorage.setItem('loggedInUser',JSON.stringify(loginUserData));
      }  
    
    render() {
        return (<div className="col-sm-12"> <ul className="nav nav-tabs"> <li className="active"><a href="#Login" data-toggle="tab">Login</a></li> <li><a href="#Registration" data-toggle="tab">Registration</a></li> </ul> <div className="tab-content"> <div className="tab-pane active" id="Login"> <form role="form" className="form-horizontal" onSubmit={this.doLogin}> <div className="form-group"> <label for="exampleInputPassword1" className="col-sm-2 control-label"> Email</label> <div className="col-sm-10"> <input type="text" ref="lemail" className="form-control" placeholder="Email" /> </div> </div> <div className="form-group"> <label for="exampleInputPassword1" className="col-sm-2 control-label"> Password</label> <div className="col-sm-10"> <input type="password" ref="lpassword" className="form-control" placeholder="Password" /> </div> </div> <div className="row"> <div className="col-sm-2"> </div> <div className="col-sm-10"> <button type="submit" className="btn btn-primary btn-sm"> Submit</button> </div> </div> </form> </div> <div className="tab-pane" id="Registration"> <form role="form" className="form-horizontal"> <div className="form-group"> <label for="email" className="col-sm-2 control-label"> Name</label> <div className="col-sm-10"> <input type="text" className="form-control" placeholder="Name" /> </div> </div> <div className="form-group"> <label for="email" className="col-sm-2 control-label"> Email</label> <div className="col-sm-10"> <input type="email" className="form-control" id="email" placeholder="Email" /> </div> </div> <div className="form-group"> <label for="mobile" className="col-sm-2 control-label"> Mobile</label> <div className="col-sm-10"> <input type="email" className="form-control" id="mobile" placeholder="Mobile" /> </div> </div> <div className="form-group"> <label for="password" className="col-sm-2 control-label"> Password</label> <div className="col-sm-10"> <input type="password" className="form-control" id="password" placeholder="Password" /> </div> </div> <div className="form-group"> <label for="cpassword" className="col-sm-2 control-label">Confirm Password</label> <div className="col-sm-10"> <input type="password" className="form-control" id="cpassword" placeholder="Confirm Password" /> </div> </div> <div className="row"> <div className="col-sm-2"> </div> <div className="col-sm-10"> <button type="button" className="btn btn-primary btn-sm"> Save & Continue</button> </div> </div> </form> </div> </div> </div> )
    }
}
export default Login;


