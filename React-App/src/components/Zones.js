import React, { Component } from 'react';
import Navbar from './Navbar';

class Zones extends Component {
	constructor(props) {
        super(props);
        this.state = { likesCount: 0 };
        this.onLike = this.onLike.bind(this);
    }

    onLike() {
        let newLikesCount = this.state.likesCount + 1;
        this.setState({ likesCount: newLikesCount });
    }
    render() {
        return ( < div >< Navbar / > Likes: < span > { this.state.likesCount } < /span> < div > < button onClick = { this.onLike } > Like Me < /button > < /div >  < /div>)
        }
}
export default Zones;
