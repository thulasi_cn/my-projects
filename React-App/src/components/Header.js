import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (<div><header className="business-header"> <div className="container"> <div className="row"> <div className="col-lg-12"> <h1 className="tagline">GTR</h1> </div> </div> </div> </header><div className="row"> <div className="col-sm-8"> <h2>What We Do</h2> <p>Introduce the visitor to the business using clear, informative text. Use well-targeted keywords within your sentences to make sure search engines can find the business.</p> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et molestiae similique eligendi reiciendis sunt distinctio odit? Quia, neque, ipsa, adipisci quisquam ullam deserunt accusantium illo iste exercitationem nemo voluptates asperiores.</p> <p> <a className="btn btn-default btn-lg" href="#">Call to Action &raquo;</a> </p> </div><div className="col-sm-4"> <h2>Contact Us</h2> <address> <strong>Start Bootstrap</strong> 3481 Melrose Place, Beverly Hills, CA 90210 </address> <address> <span title="Phone">P:</span>(123) 456-7890  <span title="Email">E</span> <a href="mailto:#">email</a> </address> </div> </div></div>)
        }
}
export default Header;