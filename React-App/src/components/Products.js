import React, { Component } from 'react';
import Singleproduct from './Singleproduct';
import Navbar from './Navbar';
import Cartlist from './Cartlist';
import axios from 'axios';
// var Spinner = require('react-spinkit');
// < Spinner spinnerName = 'double-bounce' loaded={this.state.loaded} / >
class Products extends Component {
    constructor() {
        super();
        this.state = {
            loggedin_user: JSON.parse(localStorage.getItem('loggedInUser')),
            prodcutsList: [],
            cartList: [],
            loaded:true
        };
    }

    // checkStatus(response) {
    //     if (response.status >= 200 && response.status < 300) {
    //         return response
    //     } else {
    //         var error = new Error(response.statusText)
    //         error.response = response
    //         throw error
    //     }
    // }

    // parseJSON(response) {
    //     return response.json();
    // }
    componentDidMount() {
        // fetch('http://noderest-thulasig7.rhcloud.com/api/products').then(this.checkStatus)
        //     .then(this.parseJSON)
        //     .then(result => {
        //         this.setState({ prodcutsList: result });
        //     });
        axios.get('http://noderest-thulasig7.rhcloud.com/api/products')
            .then(res => { this.setState({ prodcutsList: res.data }); });

        let user = this.state.loggedin_user;
        axios.post('http://noderest-thulasig7.rhcloud.com/api/cart/getCartByUserId', { 'user_id': user._id }).then(res => this.setState({ cartList: res.data.data,loaded:false }));

        $('#list').click(function(event) {
            event.preventDefault();
            $('#products .item').addClass('list-group-item');
        });
        $('#grid').click(function(event) {
            event.preventDefault();
            $('#products .item').removeClass('list-group-item');
            $('#products .item').addClass('grid-group-item');
        });
    }
    render() {
            const loggedInUser = this.state.loggedin_user;
            const listProdcucts = this.state.prodcutsList.map((prd, i) => {
                        return ( < Singleproduct key = { prd._id }
                            prod = { prd }
                            user = { loggedInUser }
                            />)})
                            const cart_list = this.state.cartList;

                            return ( < div > 
                                < div className = "col-md-12" > < Navbar / > < div className = "col-md-9" > < div className = "row col-md-12" > < div className = "btn-group pull-right" > < a href = "#"
                                id = "list"
                                className = "btn btn-default btn-sm" > < span className = "glyphicon glyphicon-th-list" > < /span>List</a > < a href = "#"
                                id = "grid"
                                className = "btn btn-default btn-sm" > < span className = "glyphicon glyphicon-th" > < /span>Grid</a > < /div></div > < div id = "products"
                                className = "row list-group" > { listProdcucts } < /div> </div > < div className = "col-md-3" > < Cartlist cartList = { cart_list }
                                className = "pull-right" / > < /div></div > < /div> )
                            }
                        }
                        export default Products;
