import React, { Component } from 'react';
import axios from 'axios';

class Singleproduct extends Component {
    constructor(props) {
        super(props);
        this.state = {product:this.props.prod,loggedin_user:this.props.user};
        this.addToCart = this.addToCart.bind(this);
        this.checkLocation = this.checkLocation.bind(this);
        this.buy = this.buy.bind(this);

    }
    addToCart() {
        let currentProduct = this.state.product;
        let cartObj = {};
        cartObj.prd_id = currentProduct._id, cartObj.user_id = this.state.loggedin_user._id;
        axios.post('http://noderest-thulasig7.rhcloud.com/api/cart/addCart', cartObj)
            .then(function(response) {
                if (response.data.data.message === "Success") {
                    alert(response.data.data.message);
                }else{
                    alert(response.data.data.message);
                }
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    checkLocation(){
        let pincode = this.refs.pincode.value; 
        console.log(pincode);
    }
    buy(){
        let buyPrd = this.state; 
        console.log(buyPrd);
    }
    render() {
        return (<div className="item  col-xs-4 col-lg-4"> <div className="thumbnail clearfix"> <div className="prd-img"> <img className="group list-group-image" src={ this.props.prod.prd_img } alt="" /> </div> <div className="caption"> <h4 className="group inner list-group-item-heading"> { this.props.prod.prd_name } </h4> <p className="group inner list-group-item-text"> {this.props.prod.prd_description}</p> <div className="row"> <div className="col-xs-12 col-md-8 pin-serch "> <input type="text" className="search-query" ref="pincode" placeholder="Search Pincode" /> <i className="fa fa-search" onClick={this.checkLocation}></i></div> <div className="col-xs-12 col-md-4"> <p className="lead"> Rs { this.props.prod.prd_price } < /p> </div> </div> <div className="row"> <div className="col-xs-12 col-md-6"> <button className="btn btn-success pull-left" onClick={ this.addToCart }> Add to cart </button> </div> <div className="col-xs-12 col-md-6"> <button className="btn btn-success pull-right" onClick={ this.buy }>Buy</button> </div> </div> </div> </div> </div>)
    }
}
export default Singleproduct;
