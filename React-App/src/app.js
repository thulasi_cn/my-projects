import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, hashHistory } from 'react-router';

import Zones from './components/Zones';
/*import Navbar from './components/Navbar';
*/import Products from './components/Products';
import Home from './components/Home';
import About from './components/About';
import OrdersRiview from './components/OrdersRiview';

class App extends Component {

    render() {
        return ( < div > 
            < Router history = { hashHistory } >
            < Route path = "/" component = { Home } /> 
            < Route path ="/about" component = { About }/>
            < Route path ="/zones" component = { Zones }/>
             < Route path = "/products"  component = { Products } />
             < Route path = "/order_rivew"  component = { OrdersRiview } />
              < /Router > < /div > );


        }
    }
    ReactDOM.render( < App / > , document.getElementById('root'));
