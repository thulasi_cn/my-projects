var webpack = require('webpack');
var path = require('path');

var config = {
    entry: {
        app: './src/app.js'
    },
    output: {
        filename: './build/bundle.js',
        sourceMapFilename: './build/bundle.map'
    },
    devtool: '#source-map',
    module: {
        loaders: [{
            test: /\.jsx?/,
            exclude: /(node_modules)/,
            // include: APP_DIR,
            loader: 'babel-loader',
            query: {
                presets: ["react", "es2015"]
            }
        }]
    }
};

module.exports = config;
