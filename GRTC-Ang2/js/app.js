var app = angular.module('greateChennai', ['ui.router']);
app.config(
    ['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('main', {
                    url: '',
                    abstract: true,
                    template: '<div class="container"><div ui-view="navbar"></div><div ui-view="userUI"></div></div>'
                }).state('main.home', {
                    url: '/home',
                    views: {
                        'userUI': {
                            templateUrl: 'templates/home.html'
                        }
                    }
                }).state('main.about', {
                    url: '/about',
                    views: {
                        'navbar': {
                            templateUrl: 'templates/navBar.html'
                        },
                        'userUI': {
                            templateUrl: 'templates/about.html'
                        }
                    }
                }).state('main.services', {
                    url: '/services',
                    views: {
                        'navbar': {
                            templateUrl: 'templates/navBar.html'
                        },
                        'userUI': {
                            templateUrl: 'templates/services.html'
                        }
                    }
                }).state('main.blog', {
                    url: '/blog',
                    views: {
                        'navbar': {
                            templateUrl: 'templates/navBar.html'
                        },
                        'userUI': {
                            templateUrl: 'templates/blog.html'
                        }
                    }
                }).state('main.contact', {
                    url: '/contact',
                    views: {
                        'navbar': {
                            templateUrl: 'templates/navBar.html'
                        },
                        'userUI': {
                            templateUrl: 'templates/contact_us.html'
                        }
                    }
                });
            $urlRouterProvider.otherwise('/home');
        }
    ]);