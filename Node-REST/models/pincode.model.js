// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var pincodeSchema = new mongoose.Schema({
    pin_city: {
        type: String,
        require: true
    },
    pin_code: {
        type: String,
        require: true
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
});

// Return model
module.exports = restful.model('Pincode', pincodeSchema);
