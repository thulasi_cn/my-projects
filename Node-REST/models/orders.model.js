// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var orderSchema = new mongoose.Schema({
    product_id: {
        type: String,
        require: true
    },
    user_id: {
        type: String,
        require: true
    },
    ord_status: {
        type: String,
        require: true
    },
    ord_date: {
        type: Date,
        default: Date.now
    },
    ord_price: {
        type: String,
        require: true
    },
    ord_quantity: {
        type: String,
        require: true
    },
    ord_billing_address: {
        type: String,
        require: true
    },
    ord_shipping_address: {
        type: String,
        require: true
    },
    ord_shipping_date: {
        type: Date,
        default: Date.now
    },
    ord_payment_type: {
        type: String,
        require: true
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
});

// Return model
module.exports = restful.model('Orders', orderSchema);
