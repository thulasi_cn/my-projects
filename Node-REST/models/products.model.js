// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var productSchema = new mongoose.Schema({
    prd_name: {
        type: String,
        require: true
    },
    prd_type: {
        type: String,
        require: true
    },
    prd_description: {
        type: String,
        require: true
    },
    prd_img: {
        type: String,
        require: true
    },
    prd_status: {
        type: String,
        require: true
    },
    prd_price: {
        type: String,
        require: true
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
});

// Return model
module.exports = restful.model('Products', productSchema);
