var restful = require('node-restful');
var mongoose = restful.mongoose;


// Schema
var userSchema = new mongoose.Schema({
    first_name: {
        type: String,
        require: true
    },
    last_name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        type: String,
        require: true
    },
    contact_number: {
        type: String,
        require: true
    },
    address: {
        type: String
    },
    updated_date: {
        type: Date,
        default: Date.now
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});

// Return model
module.exports = restful.model('User', userSchema);
