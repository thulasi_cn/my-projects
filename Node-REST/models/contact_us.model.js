// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var contactSchema = new mongoose.Schema({
    cnt_name: {
        type: String,
        require: true
    },
    cnt_email: {
        type: String,
        require: true
    },
    cnt_number: {
        type: String,
        require: true
    },
    cnt_description: {
        type: String,
        require: true
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
});

// Return model
module.exports = restful.model('Contact_us', contactSchema);
