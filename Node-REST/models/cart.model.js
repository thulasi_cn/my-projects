// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;


// Schema
var cartSchema = new mongoose.Schema({
    prd_id: {
        type: String,
        require: true
    },
    prd_price: {
        type: String,
        require: true
    },
    prd_quantity: {
        type: String,
        require: true
    },
    user_id: {
        type: String,
        require: true
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
});

// Return model
module.exports = restful.model('Cart', cartSchema);
