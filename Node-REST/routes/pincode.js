// Dependencies
var express = require('express');
var router = express.Router();

// Models
var Pincode = require('../models/pincode.model');

// Routes
Pincode.methods(['get', 'put', 'post', 'delete']);
Pincode.register(router, '/pincode');

// Dont need to '/pincode'

var response = {};

router.post('/checkAvailable', function(req, res) {
    var pin_code_data = req.body;
    Pincode.findOne({ "pin_code": pin_code_data.pin_code }, function(err, pincodeData) {
        if (pincodeData) {
            response.message = 'available';
            response.status = 1;
            // response.data = pincodeData;
            res.json(response);
        } else {
            response.message = 'not available';
            response.status = 0;
            // response.data = '';
            res.json(response);
        }
    });
});

// Return router
module.exports = router;
