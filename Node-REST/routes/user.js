var express = require('express');
var userModule = express.Router();

//User Models
var User = require('../models/user.model');
// Routes
User.methods(['get','put','delete']);
User.register(userModule, '/user');

var response = {};

userModule.post('/user/register', function(req, res) {
    var regisetr_user = req.body;
    User.findOne({ "email": regisetr_user.email }, function(err, user_data) {
        // if (err) {throw err; }
        if (!user_data) {
            User.create(regisetr_user, function(err, userReg) {
                if (err) {
                    throw err;
                }
                response.message = "User registration Success.";
                response.user = userReg;
                res.json(response);
            });
        } else {
            response.message = 'User email exist,Please try with another username.';
            response.user = '';
            res.json(response);
        }
    });
});

userModule.post('/user/login', function(req, res) {
    var login_user = req.body;
    User.findOne({ "email": login_user.email }, function(err, user_data) {
        if (user_data) {
            if (login_user.password === user_data.password) {
                response.status = true;
                response.message = "User logied in Success.";
                response.user = user_data;
                res.json(response);
            } else {
                response.status = false;
                response.message = 'Password does not match,Please enter correct password';
                response.user = '';
                res.json(response);
            }
        } else {
            response.status = false;
            response.message = 'User email does not exist,Please try with registere username.';
            response.user = '';
            res.json(response);
        }
    });
});
userModule.get('/user/list', function(req, res) {
    User.find({}, function(err, users) {
        if (users) {
            response.message = "Success";
            response.user = users;
            res.json(response);
        } else {
            response.message = 'No user list found.';
            response.user = '';
            res.json(response);
        }
    });
});

module.exports = userModule;
