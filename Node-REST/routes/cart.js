var express = require('express');
var cartModule = express.Router();
var _ = require('underscore');

//Cart Models
var Cart = require('../models/cart.model');
var Products = require('../models/products.model');

// Routes
Cart.methods(['put', 'delete', 'get']);
Cart.register(cartModule, '/cart');

var response = {};

cartModule.post('/cart/addCart', function(req, res) {
    var cart_data = req.body;
    Cart.findOne({ prd_id: cart_data.prd_id, user_id: cart_data.user_id }, function(err, cart) {
        if (cart) {
            response.message = 'Already added';
            response.status = 0;
            response.data = '';
            res.json(response);
        } else {
            Cart.create(cart_data, function(err, cart_create) {
                if (err) {
                    throw err;
                }
                response.message = 'Success';
                response.status = 1;
                response.data = cart_create;
                res.json(response);
            });

        }
    });
});

cartModule.post('/cart/getCartByUserId', function(req, res) {
    var userData = req.body;
    Cart.find({ "user_id": userData.user_id }, function(err, userCartList) {
        if (userCartList) {
            var cartList = [];
            var userCart = userCartList;
            userCart.forEach(function(cart) {
                Products.find({ _id: cart.prd_id }, function(err, prd) {
                    if (prd.length !== 0) {
                        cartList.push(prd[0]);
                    }

                });

            });
            setTimeout(function() {
                response.message = 'success';
                response.status = 0;
                response.count = cartList.length;
                response.data = cartList;
                res.json(response);
            }, 500);


        } else {
            response.message = 'No result found';
            response.status = 1;
            response.data = '';
            res.json(response);
        }
    });
});

module.exports = cartModule;
