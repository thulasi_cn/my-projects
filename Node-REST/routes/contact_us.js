// Dependencies
var express = require('express');
var contactUsModule = express.Router();

// Models
var Contact = require('../models/contact_us.model');

// Routes
Contact.methods(['get', 'put', 'post', 'delete']);
Contact.register(contactUsModule, '/contact_us');

// Return router
module.exports = contactUsModule;
