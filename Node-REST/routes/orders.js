// Dependencies
var express = require('express');
var router = express.Router();

// Models
var Order = require('../models/orders.model');

// Routes
Order.methods(['get', 'put', 'post', 'delete']);
Order.register(router, '/orders');

// Return router
module.exports = router;
