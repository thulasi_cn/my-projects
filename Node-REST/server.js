// Dependencies
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var restful = require('node-restful');
var config = require('./config'); // get db config file
var _ = require('underscore');

// Express
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// serving static files
app.use('/img',express.static(__dirname + '/imgs'));

// MongoDB to database
// mongoose.connect('mongodb://localhost/rest_test');

// mongoose.Promise = global.Promise;
var connectionString = process.env.OPENSHIFT_MONGODB_DB_URL || config.database;
mongoose.connect(connectionString);

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');

    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// First Page
app.get('/', function(req, res) {
    res.send('<h1>Welocme to the Api Store</h1>');
});

// Routes
app.use('/api', require('./routes/user'));
app.use('/api', require('./routes/products'));
app.use('/api', require('./routes/orders'));
app.use('/api', require('./routes/pincode'));
app.use('/api', require('./routes/contact_us'));
app.use('/api', require('./routes/cart'));

// server ENV details
app.get('/process', function(req, res) {
    res.send(process.env);
});

// setup ports
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

// server listens in on port
app.listen(server_port, server_ip_address, function() {
    console.log("Listening on " + server_ip_address + ", server_port " + server_port);
});
